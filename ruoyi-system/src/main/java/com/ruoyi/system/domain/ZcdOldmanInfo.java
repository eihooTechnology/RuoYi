package com.ruoyi.system.domain;

import com.ruoyi.common.base.BaseEntity;

/**
 * @ClassName ZcdOldmanInfo
 * @Description TODO
 * @Author lzjian
 * @Date 2019-04-08 10:56
 **/
public class ZcdOldmanInfo extends BaseEntity {
    private static final long serialVersionUID = 1L;

    private Long id;

    private String oldmanName;// 老人姓名

    private Integer oldmanSex;// 老人性别

    private String idCard;// 老人身份证

    private String oldmanPhone;// 老人联系电话

    private String oldmanAddress;// 老人地址

    private String imgBase64;// 人脸的Base64格式图片

    private String imgOssUrl;// 老人头像的OSS的URL

    private String isFaceRegistration;// 人脸信息是否录入

    private String status;// 目前的状态（0正常 1删除 2停用 3冻结）

    @Override
    public String toString() {
        return "ZcdOldmanInfo{" +
                "id=" + id +
                ", oldmanName='" + oldmanName + '\'' +
                ", oldmanSex=" + oldmanSex +
                ", idCard='" + idCard + '\'' +
                ", oldmanPhone='" + oldmanPhone + '\'' +
                ", oldmanAddress='" + oldmanAddress + '\'' +
                ", imgBase64='" + imgBase64 + '\'' +
                ", imgOssUrl='" + imgOssUrl + '\'' +
                ", isFaceRegistration='" + isFaceRegistration + '\'' +
                ", status='" + status + '\'' +
                '}';
    }

    public String getImgOssUrl() {
        return imgOssUrl;
    }

    public void setImgOssUrl(String imgOssUrl) {
        this.imgOssUrl = imgOssUrl;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOldmanName() {
        return oldmanName;
    }

    public void setOldmanName(String oldmanName) {
        this.oldmanName = oldmanName;
    }

    public Integer getOldmanSex() {
        return oldmanSex;
    }

    public void setOldmanSex(Integer oldmanSex) {
        this.oldmanSex = oldmanSex;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getOldmanPhone() {
        return oldmanPhone;
    }

    public void setOldmanPhone(String oldmanPhone) {
        this.oldmanPhone = oldmanPhone;
    }

    public String getOldmanAddress() {
        return oldmanAddress;
    }

    public void setOldmanAddress(String oldmanAddress) {
        this.oldmanAddress = oldmanAddress;
    }

    public String getImgBase64() {
        return imgBase64;
    }

    public void setImgBase64(String imgBase64) {
        this.imgBase64 = imgBase64;
    }

    public String getIsFaceRegistration() {
        return isFaceRegistration;
    }

    public void setIsFaceRegistration(String isFaceRegistration) {
        this.isFaceRegistration = isFaceRegistration;
    }
}
