package com.ruoyi.system.domain;

import com.ruoyi.common.base.BaseEntity;

import java.math.BigDecimal;

/**
 * 食堂账单表 r_oldman_bill
 */
public class ROldmanBill extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**食堂账单id*/
    private Long id;

    /**餐厅id*/
    private Long restaurantId;

    /**老人id*/
    private Long oldManId;

    /**大荤(三选一)*/
    private Long firstFood;

    /**小荤(二选一)*/
    private Long secondFood;

    /**素菜(两份)*/
    private Long vegetables;

    /**汤(一份)*/
    private Long soup;

    /**本餐花费金额*/
    private BigDecimal foodSpend;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(Long restaurantId) {
        this.restaurantId = restaurantId;
    }

    public Long getOldManId() {
        return oldManId;
    }

    public void setOldManId(Long oldManId) {
        this.oldManId = oldManId;
    }

    public Long getFirstFood() {
        return firstFood;
    }

    public void setFirstFood(Long firstFood) {
        this.firstFood = firstFood;
    }

    public Long getSecondFood() {
        return secondFood;
    }

    public void setSecondFood(Long secondFood) {
        this.secondFood = secondFood;
    }

    public Long getVegetables() {
        return vegetables;
    }

    public void setVegetables(Long vegetables) {
        this.vegetables = vegetables;
    }

    public Long getSoup() {
        return soup;
    }

    public void setSoup(Long soup) {
        this.soup = soup;
    }

    public BigDecimal getFoodSpend() {
        return foodSpend;
    }

    public void setFoodSpend(BigDecimal foodSpend) {
        this.foodSpend = foodSpend;
    }

    @Override
    public String toString() {
        return "ROldmanBill{" +
                "id=" + id +
                ", restaurantId=" + restaurantId +
                ", oldManId=" + oldManId +
                ", firstFood=" + firstFood +
                ", secondFood=" + secondFood +
                ", vegetables=" + vegetables +
                ", soup=" + soup +
                ", foodSpend=" + foodSpend +
                '}';
    }
}
