package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 角色和社区关联 sys_role_community_copy
 *
 * @author ruoyi
 */
public class SysRoleCommunity {
    /**
     * 角色ID
     */
    private Long roleId;

    /**
     * 社区ID
     */
    private Long communityId;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("roleId", getRoleId())
                .append("communityId", getCommunityId())
                .toString();
    }
}
