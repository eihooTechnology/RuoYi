package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.base.BaseEntity;

import java.util.Date;

/**
 * @ClassName SysOrder
 * @Description 订单实体类
 * @Author JiaoSimao
 * @Date 2018/12/19 0019 10:22
 **/
public class SysOrderPreview extends BaseEntity {
    private static final long serialVersionUID = 1L;
    /**
     * 订单号
     */
    private Long id;
    /**
     * 订单组id
     */
    private Long parentId;
    /**
     * 社区编码
     */
    private Long communityId;
    /**
     * 社区名称
     */
    private String communityName;
    /**
     * 送餐地址
     */
    private String address;

    /**
     * 老人编号
     */
    private Long userCode;
    /**
     * 老人姓名
     */
    private String userName;
    /**
     * 老人电话号码
     */
    private String mobile;
    /**
     * 送餐状态
     */
    private String sendType;
    /**
     * 送餐快递员id(骑手id)
     */
    private Long sendCourierId;
    /**
     * 送餐快递员姓名(骑手姓名)
     */
    private String sendCourierName;

    /**
     * 开始配送时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startSendTime;

    /**
     * 送餐完成时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date sendFinishedTime;

    /**
     * 餐具回收状态
     */
    private String recoverType;

    /**
     * 回收员id（骑手id）
     */
    private Long recoverCourierId;

    /**
     * 回收员姓名（骑手姓名）
     */
    private String recoverCourierName;

    /**
     * 餐具回收时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date recoverTime;

    /**
     * 订单类型(0=早餐；1=午餐；2=晚餐)
     */
    private String orderType;

    /**
     * 订单状态
     */
    private Integer status;

    /**
     * 权限专用字段
     */
    private String keyIDs;

    public String getKeyIDs() {
        return keyIDs;
    }

    public void setKeyIDs(String keyIDs) {
        this.keyIDs = keyIDs;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getUserCode() {
        return userCode;
    }

    public void setUserCode(Long userCode) {
        this.userCode = userCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSendType() {
        return sendType;
    }

    public void setSendType(String sendType) {
        this.sendType = sendType;
    }

    public Long getSendCourierId() {
        return sendCourierId;
    }

    public void setSendCourierId(Long sendCourierId) {
        this.sendCourierId = sendCourierId;
    }

    public String getSendCourierName() {
        return sendCourierName;
    }

    public void setSendCourierName(String sendCourierName) {
        this.sendCourierName = sendCourierName;
    }

    public Date getStartSendTime() {
        return startSendTime;
    }

    public void setStartSendTime(Date startSendTime) {
        this.startSendTime = startSendTime;
    }

    public Date getSendFinishedTime() {
        return sendFinishedTime;
    }

    public void setSendFinishedTime(Date sendFinishedTime) {
        this.sendFinishedTime = sendFinishedTime;
    }

    public String getRecoverType() {
        return recoverType;
    }

    public void setRecoverType(String recoverType) {
        this.recoverType = recoverType;
    }

    public Long getRecoverCourierId() {
        return recoverCourierId;
    }

    public void setRecoverCourierId(Long recoverCourierId) {
        this.recoverCourierId = recoverCourierId;
    }

    public String getRecoverCourierName() {
        return recoverCourierName;
    }

    public void setRecoverCourierName(String recoverCourierName) {
        this.recoverCourierName = recoverCourierName;
    }

    public Date getRecoverTime() {
        return recoverTime;
    }

    public void setRecoverTime(Date recoverTime) {
        this.recoverTime = recoverTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    @Override
    public String toString() {
        return "SysOrder{" +
                "id=" + id +
                ", parentId=" + parentId +
                ", communityId=" + communityId +
                ", communityName='" + communityName + '\'' +
                ", address='" + address + '\'' +
                ", userCode=" + userCode +
                ", userName='" + userName + '\'' +
                ", mobile='" + mobile + '\'' +
                ", sendType='" + sendType + '\'' +
                ", sendCourierId=" + sendCourierId +
                ", sendCourierName='" + sendCourierName + '\'' +
                ", startSendTime=" + startSendTime +
                ", sendFinishedTime=" + sendFinishedTime +
                ", recoverType='" + recoverType + '\'' +
                ", recoverCourierId=" + recoverCourierId +
                ", recoverCourierName='" + recoverCourierName + '\'' +
                ", recoverTime=" + recoverTime +
                ", status=" + status +
                '}';
    }
}
