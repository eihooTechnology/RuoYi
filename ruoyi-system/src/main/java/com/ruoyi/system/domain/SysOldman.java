package com.ruoyi.system.domain;

import com.ruoyi.common.base.BaseEntity;

import java.util.Date;

/**
 * @ClassName SysOldman
 * @Description 老人管理实体类
 * @Author JiaoSimao
 * @Date 2018/12/14 0014 13:37
 **/
public class SysOldman extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**老人编号*/
    private Long userCode;

    /**老人姓名*/
    private String userName;

    /**身份证*/
    private String identityId;

    /**归属社区*/
    private String communityName;

    /**归属社区ID*/
    private Long communityId;

    /**手机号码**/

    private String mobile;

    /**状态**/
    private String status;

    /**性别*/
    private String sex;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /**市小区*/
    private String cityEstate;

    /**地址*/
    private String address;

    // 当月早餐送餐员ID
    private Long courierId;

    // 当月早餐套餐类型ID
    private Long foodPackage;

    // 当月早餐订单所选配送日期(实际吃饭天数，可修改)
    private String dateCheck;

    // 当月午餐送餐员ID
    private Long courierId2;

    // 当月午餐套餐类型ID
    private Long foodPackage2;

    // 当月午餐订单所选配送日期(实际吃饭天数，可修改)
    private String dateCheck2;

    // 当月晚餐送餐员ID
    private Long courierId3;

    // 当月晚餐套餐类型ID
    private Long foodPackage3;

    // 当月晚餐订单所选配送日期(实际吃饭天数，可修改)
    private String dateCheck3;

    // 下月早餐送餐员ID
    private Long courierIdNextMonth;

    // 下月早餐套餐类型ID
    private Long foodPackageNextMonth;

    // 下月早餐订单所选配送日期
    private String dateCheckNextMonth;

    // 下月午餐送餐员ID
    private Long courierId2NextMonth;

    // 下月午餐套餐类型ID
    private Long foodPackage2NextMonth;

    // 下月午餐订单所选配送日期
    private String dateCheck2NextMonth;

    // 下月晚餐送餐员ID
    private Long courierId3NextMonth;

    // 下月晚餐套餐类型ID
    private Long foodPackage3NextMonth;

    // 下月晚餐订单所选配送日期
    private String dateCheck3NextMonth;

    // 经度
    private String longitude;

    // 纬度
    private String latitude;

    /** 额外 **/
    private SysCourier courier;// 送餐员对象

    private SysOrderBill orderBill;//账单对象

    // 以下为自定义字段
    private String isSettle;
    private String prepaidStatus;
    private String courierName;

    private String ids;

    /**
     * 权限专用字段
     */
    private String keyIDs;

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getCourierName() {
        return courierName;
    }

    public void setCourierName(String courierName) {
        this.courierName = courierName;
    }

    public String getKeyIDs() {
        return keyIDs;
    }

    public void setKeyIDs(String keyIDs) {
        this.keyIDs = keyIDs;
    }

    public String getIsSettle() {
        return isSettle;
    }

    public void setIsSettle(String isSettle) {
        this.isSettle = isSettle;
    }

    public String getPrepaidStatus() {
        return prepaidStatus;
    }

    public void setPrepaidStatus(String prepaidStatus) {
        this.prepaidStatus = prepaidStatus;
    }

    public Long getUserCode() {
        return userCode;
    }

    public void setUserCode(Long userCode) {
        this.userCode = userCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getIdentityId() {
        return identityId;
    }

    public void setIdentityId(String identityId) {
        this.identityId = identityId;
    }

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }

    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getCourierId() {
        return courierId;
    }

    public void setCourierId(Long courierId) {
        this.courierId = courierId;
    }

    public Long getFoodPackage() {
        return foodPackage;
    }

    public void setFoodPackage(Long foodPackage) {
        this.foodPackage = foodPackage;
    }

    public String getDateCheck() {
        return dateCheck;
    }

    public void setDateCheck(String dateCheck) {
        this.dateCheck = dateCheck;
    }

    public Long getCourierId2() {
        return courierId2;
    }

    public void setCourierId2(Long courierId2) {
        this.courierId2 = courierId2;
    }

    public Long getFoodPackage2() {
        return foodPackage2;
    }

    public void setFoodPackage2(Long foodPackage2) {
        this.foodPackage2 = foodPackage2;
    }

    public String getDateCheck2() {
        return dateCheck2;
    }

    public void setDateCheck2(String dateCheck2) {
        this.dateCheck2 = dateCheck2;
    }

    public Long getCourierId3() {
        return courierId3;
    }

    public void setCourierId3(Long courierId3) {
        this.courierId3 = courierId3;
    }

    public Long getFoodPackage3() {
        return foodPackage3;
    }

    public void setFoodPackage3(Long foodPackage3) {
        this.foodPackage3 = foodPackage3;
    }

    public String getDateCheck3() {
        return dateCheck3;
    }

    public void setDateCheck3(String dateCheck3) {
        this.dateCheck3 = dateCheck3;
    }

    public Long getCourierIdNextMonth() {
        return courierIdNextMonth;
    }

    public void setCourierIdNextMonth(Long courierIdNextMonth) {
        this.courierIdNextMonth = courierIdNextMonth;
    }

    public Long getFoodPackageNextMonth() {
        return foodPackageNextMonth;
    }

    public void setFoodPackageNextMonth(Long foodPackageNextMonth) {
        this.foodPackageNextMonth = foodPackageNextMonth;
    }

    public String getDateCheckNextMonth() {
        return dateCheckNextMonth;
    }

    public void setDateCheckNextMonth(String dateCheckNextMonth) {
        this.dateCheckNextMonth = dateCheckNextMonth;
    }

    public Long getCourierId2NextMonth() {
        return courierId2NextMonth;
    }

    public void setCourierId2NextMonth(Long courierId2NextMonth) {
        this.courierId2NextMonth = courierId2NextMonth;
    }

    public Long getFoodPackage2NextMonth() {
        return foodPackage2NextMonth;
    }

    public void setFoodPackage2NextMonth(Long foodPackage2NextMonth) {
        this.foodPackage2NextMonth = foodPackage2NextMonth;
    }

    public String getDateCheck2NextMonth() {
        return dateCheck2NextMonth;
    }

    public void setDateCheck2NextMonth(String dateCheck2NextMonth) {
        this.dateCheck2NextMonth = dateCheck2NextMonth;
    }

    public Long getCourierId3NextMonth() {
        return courierId3NextMonth;
    }

    public void setCourierId3NextMonth(Long courierId3NextMonth) {
        this.courierId3NextMonth = courierId3NextMonth;
    }

    public Long getFoodPackage3NextMonth() {
        return foodPackage3NextMonth;
    }

    public void setFoodPackage3NextMonth(Long foodPackage3NextMonth) {
        this.foodPackage3NextMonth = foodPackage3NextMonth;
    }

    public String getDateCheck3NextMonth() {
        return dateCheck3NextMonth;
    }

    public void setDateCheck3NextMonth(String dateCheck3NextMonth) {
        this.dateCheck3NextMonth = dateCheck3NextMonth;
    }

    public SysCourier getCourier() {
        return courier;
    }

    public void setCourier(SysCourier courier) {
        this.courier = courier;
    }

    public SysOrderBill getOrderBill() {
        return orderBill;
    }

    public void setOrderBill(SysOrderBill orderBill) {
        this.orderBill = orderBill;
    }

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public String getCityEstate() {
        return cityEstate;
    }

    public void setCityEstate(String cityEstate) {
        this.cityEstate = cityEstate;
    }

    @Override
    public String toString() {
        return "SysOldman{" +
                "userCode=" + userCode +
                ", userName='" + userName + '\'' +
                ", identityId='" + identityId + '\'' +
                ", communityName='" + communityName + '\'' +
                ", communityId=" + communityId +
                ", mobile='" + mobile + '\'' +
                ", status='" + status + '\'' +
                ", sex='" + sex + '\'' +
                ", delFlag='" + delFlag + '\'' +
                ", cityEstate='" + cityEstate + '\'' +
                ", address='" + address + '\'' +
                ", courierId=" + courierId +
                ", foodPackage=" + foodPackage +
                ", dateCheck='" + dateCheck + '\'' +
                ", courierId2=" + courierId2 +
                ", foodPackage2=" + foodPackage2 +
                ", dateCheck2='" + dateCheck2 + '\'' +
                ", courierId3=" + courierId3 +
                ", foodPackage3=" + foodPackage3 +
                ", dateCheck3='" + dateCheck3 + '\'' +
                ", courierIdNextMonth=" + courierIdNextMonth +
                ", foodPackageNextMonth=" + foodPackageNextMonth +
                ", dateCheckNextMonth='" + dateCheckNextMonth + '\'' +
                ", courierId2NextMonth=" + courierId2NextMonth +
                ", foodPackage2NextMonth=" + foodPackage2NextMonth +
                ", dateCheck2NextMonth='" + dateCheck2NextMonth + '\'' +
                ", courierId3NextMonth=" + courierId3NextMonth +
                ", foodPackage3NextMonth=" + foodPackage3NextMonth +
                ", dateCheck3NextMonth='" + dateCheck3NextMonth + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", courier=" + courier +
                ", orderBill=" + orderBill +
                ", isSettle='" + isSettle + '\'' +
                ", prepaidStatus='" + prepaidStatus + '\'' +
                ", courierName='" + courierName + '\'' +
                ", ids='" + ids + '\'' +
                ", keyIDs='" + keyIDs + '\'' +
                '}';
    }
}
