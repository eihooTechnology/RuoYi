package com.ruoyi.system.domain;

import com.ruoyi.common.base.BaseEntity;

/**
 * @ClassName SysCourier
 * @Description TODO
 * @Author JiaoSimao
 * @Date 2018/12/18 0018 14:00
 **/
public class SysCourier extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**骑手id*/
    private Long id;

    /**骑手姓名*/
    private String courierName;

    /**手机号码*/
    private String tel;

    /**登录密码*/
    private String password;

    /** 社区ID */
    private Long communityId;

    /**社区名称*/
    private String communityName;


    /**是否注册人脸(0未注册；1已注册)*/
    private String isFaceRegistration;

    /**状态*/
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /**Base64格式的头像*/
    private String imgBase64;

    /**送餐员头像的OSS的URL*/
    private String imgOssUrl;

    /**
     * 权限专用字段
     */
    private String keyIDs;

    public String getImgBase64() {
        return imgBase64;
    }

    public void setImgBase64(String imgBase64) {
        this.imgBase64 = imgBase64;
    }

    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }

    public String getKeyIDs() {
        return keyIDs;
    }

    public void setKeyIDs(String keyIDs) {
        this.keyIDs = keyIDs;
    }

    public String getIsFaceRegistration() {
        return isFaceRegistration;
    }

    public void setIsFaceRegistration(String isFaceRegistration) {
        this.isFaceRegistration = isFaceRegistration;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCourierName() {
        return courierName;
    }

    public void setCourierName(String courierName) {
        this.courierName = courierName;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImgOssUrl() {
        return imgOssUrl;
    }

    public void setImgOssUrl(String imgOssUrl) {
        this.imgOssUrl = imgOssUrl;
    }

    @Override
    public String toString() {
        return "SysCourier{" +
                "id=" + id +
                ", courierName='" + courierName + '\'' +
                ", tel='" + tel + '\'' +
                ", password='" + password + '\'' +
                ", communityId=" + communityId +
                ", communityName='" + communityName + '\'' +
                ", isFaceRegistration='" + isFaceRegistration + '\'' +
                ", status='" + status + '\'' +
                ", delFlag='" + delFlag + '\'' +
                ", imgBase64='" + imgBase64 + '\'' +
                ", imgOssUrl='" + imgOssUrl + '\'' +
                ", keyIDs='" + keyIDs + '\'' +
                '}';
    }
}
