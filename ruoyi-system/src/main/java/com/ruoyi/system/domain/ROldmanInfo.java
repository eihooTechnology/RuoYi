package com.ruoyi.system.domain;

import com.ruoyi.common.base.BaseEntity;

import java.sql.Timestamp;

/**
 * 定餐点老人信息表 r_oldman_info
 */
public class ROldmanInfo extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**菜品id*/
    private Long id;

    /**餐厅id*/
    private Long restaurantId;

    /**老人姓名*/
    private String oldmanName;

    /**老人性别*/
    private int oldmanSex;

    /**老人年龄*/
    private Timestamp oldmanAge;

    /**身份证号码*/
    private String idCard;

    /**手机号码*/
    private String oldmanPhone;

    /**老人地址*/
    private String oldmanAddress;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(Long restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getOldmanName() {
        return oldmanName;
    }

    public void setOldmanName(String oldmanName) {
        this.oldmanName = oldmanName;
    }

    public int getOldmanSex() {
        return oldmanSex;
    }

    public void setOldmanSex(int oldmanSex) {
        this.oldmanSex = oldmanSex;
    }

    public Timestamp getOldmanAge() {
        return oldmanAge;
    }

    public void setOldmanAge(Timestamp oldmanAge) {
        this.oldmanAge = oldmanAge;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getOldmanPhone() {
        return oldmanPhone;
    }

    public void setOldmanPhone(String oldmanPhone) {
        this.oldmanPhone = oldmanPhone;
    }

    public String getOldmanAddress() {
        return oldmanAddress;
    }

    public void setOldmanAddress(String oldmanAddress) {
        this.oldmanAddress = oldmanAddress;
    }

    @Override
    public String toString() {
        return "ROldmanInfo{" +
                "id=" + id +
                ", restaurantId=" + restaurantId +
                ", oldmanName='" + oldmanName + '\'' +
                ", oldmanSex=" + oldmanSex +
                ", oldmanAge=" + oldmanAge +
                ", idCard='" + idCard + '\'' +
                ", oldmanPhone='" + oldmanPhone + '\'' +
                ", oldmanAddress='" + oldmanAddress + '\'' +
                '}';
    }
}
