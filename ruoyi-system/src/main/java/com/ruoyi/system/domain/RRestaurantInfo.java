package com.ruoyi.system.domain;

import com.ruoyi.common.base.BaseEntity;

/**
 * 餐厅信息表 r_restaurant_info
 */
public class RRestaurantInfo extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**餐厅id*/
    private Long id;

    /**餐厅名字*/
    private String restaurantName;

    /**餐厅地址*/
    private String restaurantAddress;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getRestaurantAddress() {
        return restaurantAddress;
    }

    public void setRestaurantAddress(String restaurantAddress) {
        this.restaurantAddress = restaurantAddress;
    }

    @Override
    public String toString() {
        return "RRestaurantInfo{" +
                "id=" + id +
                ", restaurantName='" + restaurantName + '\'' +
                ", restaurantAddress='" + restaurantAddress + '\'' +
                '}';
    }
}
