package com.ruoyi.system.domain;

import com.ruoyi.common.base.BaseEntity;

/**
 * @ClassName SysAppVersion
 * @Description App版本号实体类
 * @Author lzjian
 * @Date 2019/03/29 09:35
 **/
public class SysAppVersion extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * ID号
     */
    private Long id;

    /**
     * APP类型(0=送餐App,1=助餐点App)
     */
    private String appType;

    /**
     * 版本号
     */
    private Long versionCode;

    /**
     * 备注
     */
    private String remark;

    @Override
    public String toString() {
        return "SysAppVersion{" +
                "id=" + id +
                ", appType='" + appType + '\'' +
                ", versionCode=" + versionCode +
                ", remark='" + remark + '\'' +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAppType() {
        return appType;
    }

    public void setAppType(String appType) {
        this.appType = appType;
    }

    public Long getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(Long versionCode) {
        this.versionCode = versionCode;
    }

    @Override
    public String getRemark() {
        return remark;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }
}
