package com.ruoyi.system.domain;

import com.ruoyi.common.base.BaseEntity;

/**
 * @ClassName ZcdFoodPackageOldman
 * @Description TODO
 * @Author lzjian
 * @Date 2019-04-08 13:21
 **/
public class ZcdFoodPackageOldman extends BaseEntity {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Long oldmanId;// 归属的老人ID

    private String chivesFood1;// 大荤1

    private String chivesFood1Check;// 大荤1是否选中

    private String chivesFood2;// 大荤2

    private String chivesFood2Check;// 大荤2是否选中

    private String chivesFood3;// 大荤3

    private String chivesFood3Check;// 大荤3是否选中

    private String littleChivesFood1;// 小荤1

    private String littleChivesFood1Check;// 小荤1是否选中

    private String littleChivesFood2;// 小荤2

    private String littleChivesFood2Check;// 小荤2是否选中

    private String vegetarianFood1;// 素菜1

    private String vegetarianFood2;// 素菜2

    private String soupFood;// 汤

    private String foodpackageTime;// 菜单有效日期

    private String status;// 目前的状态（0正常 1删除 2停用 3冻结 4已取餐）

    @Override
    public String toString() {
        return "ZcdFoodPackageOldman{" +
                "id=" + id +
                ", oldmanId=" + oldmanId +
                ", chivesFood1='" + chivesFood1 + '\'' +
                ", chivesFood1Check='" + chivesFood1Check + '\'' +
                ", chivesFood2='" + chivesFood2 + '\'' +
                ", chivesFood2Check='" + chivesFood2Check + '\'' +
                ", chivesFood3='" + chivesFood3 + '\'' +
                ", chivesFood3Check='" + chivesFood3Check + '\'' +
                ", littleChivesFood1='" + littleChivesFood1 + '\'' +
                ", littleChivesFood1Check='" + littleChivesFood1Check + '\'' +
                ", littleChivesFood2='" + littleChivesFood2 + '\'' +
                ", littleChivesFood2Check='" + littleChivesFood2Check + '\'' +
                ", vegetarianFood1='" + vegetarianFood1 + '\'' +
                ", vegetarianFood2='" + vegetarianFood2 + '\'' +
                ", soupFood='" + soupFood + '\'' +
                ", foodpackageTime='" + foodpackageTime + '\'' +
                ", status='" + status + '\'' +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOldmanId() {
        return oldmanId;
    }

    public void setOldmanId(Long oldmanId) {
        this.oldmanId = oldmanId;
    }

    public String getChivesFood1() {
        return chivesFood1;
    }

    public void setChivesFood1(String chivesFood1) {
        this.chivesFood1 = chivesFood1;
    }

    public String getChivesFood1Check() {
        return chivesFood1Check;
    }

    public void setChivesFood1Check(String chivesFood1Check) {
        this.chivesFood1Check = chivesFood1Check;
    }

    public String getChivesFood2() {
        return chivesFood2;
    }

    public void setChivesFood2(String chivesFood2) {
        this.chivesFood2 = chivesFood2;
    }

    public String getChivesFood2Check() {
        return chivesFood2Check;
    }

    public void setChivesFood2Check(String chivesFood2Check) {
        this.chivesFood2Check = chivesFood2Check;
    }

    public String getChivesFood3() {
        return chivesFood3;
    }

    public void setChivesFood3(String chivesFood3) {
        this.chivesFood3 = chivesFood3;
    }

    public String getChivesFood3Check() {
        return chivesFood3Check;
    }

    public void setChivesFood3Check(String chivesFood3Check) {
        this.chivesFood3Check = chivesFood3Check;
    }

    public String getLittleChivesFood1() {
        return littleChivesFood1;
    }

    public void setLittleChivesFood1(String littleChivesFood1) {
        this.littleChivesFood1 = littleChivesFood1;
    }

    public String getLittleChivesFood1Check() {
        return littleChivesFood1Check;
    }

    public void setLittleChivesFood1Check(String littleChivesFood1Check) {
        this.littleChivesFood1Check = littleChivesFood1Check;
    }

    public String getLittleChivesFood2() {
        return littleChivesFood2;
    }

    public void setLittleChivesFood2(String littleChivesFood2) {
        this.littleChivesFood2 = littleChivesFood2;
    }

    public String getLittleChivesFood2Check() {
        return littleChivesFood2Check;
    }

    public void setLittleChivesFood2Check(String littleChivesFood2Check) {
        this.littleChivesFood2Check = littleChivesFood2Check;
    }

    public String getVegetarianFood1() {
        return vegetarianFood1;
    }

    public void setVegetarianFood1(String vegetarianFood1) {
        this.vegetarianFood1 = vegetarianFood1;
    }

    public String getVegetarianFood2() {
        return vegetarianFood2;
    }

    public void setVegetarianFood2(String vegetarianFood2) {
        this.vegetarianFood2 = vegetarianFood2;
    }

    public String getSoupFood() {
        return soupFood;
    }

    public void setSoupFood(String soupFood) {
        this.soupFood = soupFood;
    }

    public String getFoodpackageTime() {
        return foodpackageTime;
    }

    public void setFoodpackageTime(String foodpackageTime) {
        this.foodpackageTime = foodpackageTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
