package com.ruoyi.system.domain;

import com.ruoyi.common.base.BaseEntity;

import java.math.BigDecimal;

/**
 * 菜品实体类 r_food_info
 *
 */
public class RFoodInfo extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**菜品id*/
    private Long id;

    /**餐厅id*/
    private Long restaurantId;

    /**星期*/
    private int week;

    /**菜品类型*/
    private int foodType;

    /**菜品名字*/
    private String foodName;

    /**菜价*/
    private BigDecimal foodPrice;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(Long restaurantId) {
        this.restaurantId = restaurantId;
    }

    public int getWeek() {
        return week;
    }

    public void setWeek(int week) {
        this.week = week;
    }

    public int getFoodType() {
        return foodType;
    }

    public void setFoodType(int foodType) {
        this.foodType = foodType;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public BigDecimal getFoodPrice() {
        return foodPrice;
    }

    public void setFoodPrice(BigDecimal foodPrice) {
        this.foodPrice = foodPrice;
    }

    @Override
    public String toString() {
        return "RFoodInfo{" +
                "id=" + id +
                ", restaurantId=" + restaurantId +
                ", week=" + week +
                ", foodType=" + foodType +
                ", foodName='" + foodName + '\'' +
                ", foodPrice=" + foodPrice +
                '}';
    }
}
