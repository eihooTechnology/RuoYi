package com.ruoyi.system.domain;

/**
 * @ClassName WsCommunityEvent
 * @Description TODO
 * @Author lzjian
 * @Date 2019-03-05 09:47
 **/
public class WsCommunityEvent {

    private static final long serialVersionUID = 1L;

    private Long id;

    private int types;

    private String deviceId;

    private int deviceType;

    private String eventName;

    private String address;

    private String typeName;

    private int addTime;

    private int updateTime;

    private String level;

    private int status;

    @Override
    public String toString() {
        return "WsCommunityEvent{" +
                "id=" + id +
                ", types=" + types +
                ", deviceId='" + deviceId + '\'' +
                ", deviceType=" + deviceType +
                ", eventName='" + eventName + '\'' +
                ", address='" + address + '\'' +
                ", typeName='" + typeName + '\'' +
                ", addTime=" + addTime +
                ", updateTime=" + updateTime +
                ", level='" + level + '\'' +
                ", status=" + status +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getTypes() {
        return types;
    }

    public void setTypes(int types) {
        this.types = types;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public int getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(int deviceType) {
        this.deviceType = deviceType;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public int getAddTime() {
        return addTime;
    }

    public void setAddTime(int addTime) {
        this.addTime = addTime;
    }

    public int getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(int updateTime) {
        this.updateTime = updateTime;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
