package com.ruoyi.system.domain;

import com.ruoyi.common.base.BaseEntity;

import java.math.BigDecimal;

/**
 * @ClassName SysFoodPackage
 * @Description TODO
 * @Author JiaoSimao
 * @Date 2019/1/4 0004 16:05
 **/
public class SysFoodPackage extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**套餐id*/
    private Long id;

    /**套餐名称*/
    private String foodPackageName;

    /**套餐价格*/
    private BigDecimal money;

    /**状态*/
    private String status;

    /**套餐备注*/
    private String remark;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFoodPackageName() {
        return foodPackageName;
    }

    public void setFoodPackageName(String foodPackageName) {
        this.foodPackageName = foodPackageName;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String getRemark() {
        return remark;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "SysFoodPackage{" +
                "id=" + id +
                ", foodPackageName='" + foodPackageName + '\'' +
                ", money=" + money +
                ", status='" + status + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}
