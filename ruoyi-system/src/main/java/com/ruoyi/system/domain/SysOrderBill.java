package com.ruoyi.system.domain;

import com.ruoyi.common.base.BaseEntity;

import java.math.BigDecimal;

/**
 * @ClassName SysOrderBill
 * @Description TODO
 * @Author JiaoSimao
 * @Date 2018/12/18 0018 14:00
 **/
public class SysOrderBill extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**账单id*/
    private Long id;

    /**老人编号*/
    private Long userCode;

    /**当月早餐订单所选配送日期(预付款吃饭天数，不可改变)*/
    private String realDateCheck;

    /**当月早餐订单所选配送日期(实际吃饭天数，可修改)*/
    private String historyDateCheck;

    /**当月午餐订单所选配送日期(预付款吃饭天数，不可改变)*/
    private String realDateCheck2;

    /**当月午餐订单所选配送日期(实际吃饭天数，可修改)*/
    private String historyDateCheck2;

    /**当月晚餐订单所选配送日期(预付款吃饭天数，不可改变)*/
    private String realDateCheck3;

    /**当月晚餐订单所选配送日期(实际吃饭天数，可修改)*/
    private String historyDateCheck3;

    /**预付账单金额*/
    private BigDecimal moneyPrepaid;

    /**预付状态(0未预付；1已预付款)**/
    private String prepaidStatus;

    /**实际账单金额**/
    private BigDecimal money;

    /**实际账单是否结清(0未结账；1已结清；2退款中)**/
    private String isSettle;

    /**状态*/
    private String status;

    /**退款或补款金额*/
    private BigDecimal refundPatchMoney;

    /**早餐付款天数*/
    private int realCount;

    /**早餐实际用餐天数*/
    private int historyCount;

    /**午餐付款天数*/
    private int realCount2;

    /**午餐实际用餐天数*/
    private int historyCount2;

    /**晚餐付款天数*/
    private int realCount3;

    /**晚餐实际用餐天数*/
    private int historyCount3;

    private SysOldman oldman;

    // 以下为查询参数
    private String userName;
    private Long communityId;
    private String address;
    private String mobile;
    // 以下为自定义字段
    private String foodInfo;
    private String foodInfo2;
    private String foodInfo3;

    private String realFoodMoneyCount;// 早餐订购总金额
    private String historyFoodMoneyCount;// 早餐实际消费总金额
    private String realFoodMoneyCount2;// 午餐订购总金额
    private String historyFoodMoneyCount2;// 午餐实际消费总金额
    private String realFoodMoneyCount3;// 晚餐订购总金额
    private String historyFoodMoneyCount3;// 晚餐实际消费总金额

    /**
     * 权限专用字段
     */
    private String keyIDs;

    public String getKeyIDs() {
        return keyIDs;
    }

    public void setKeyIDs(String keyIDs) {
        this.keyIDs = keyIDs;
    }

    public BigDecimal getRefundPatchMoney() {
        return refundPatchMoney;
    }

    public void setRefundPatchMoney(BigDecimal refundPatchMoney) {
        this.refundPatchMoney = refundPatchMoney;
    }

    @Override
    public String toString() {
        return "SysOrderBill{" +
                "id=" + id +
                ", userCode=" + userCode +
                ", realDateCheck='" + realDateCheck + '\'' +
                ", historyDateCheck='" + historyDateCheck + '\'' +
                ", realDateCheck2='" + realDateCheck2 + '\'' +
                ", historyDateCheck2='" + historyDateCheck2 + '\'' +
                ", realDateCheck3='" + realDateCheck3 + '\'' +
                ", historyDateCheck3='" + historyDateCheck3 + '\'' +
                ", moneyPrepaid=" + moneyPrepaid +
                ", prepaidStatus='" + prepaidStatus + '\'' +
                ", money=" + money +
                ", isSettle='" + isSettle + '\'' +
                ", status='" + status + '\'' +
                ", realCount=" + realCount +
                ", historyCount=" + historyCount +
                ", realCount2=" + realCount2 +
                ", historyCount2=" + historyCount2 +
                ", realCount3=" + realCount3 +
                ", historyCount3=" + historyCount3 +
                ", oldman=" + oldman +
                ", userName='" + userName + '\'' +
                ", communityId=" + communityId +
                ", address='" + address + '\'' +
                ", mobile='" + mobile + '\'' +
                ", foodInfo='" + foodInfo + '\'' +
                ", foodInfo2='" + foodInfo2 + '\'' +
                ", foodInfo3='" + foodInfo3 + '\'' +
                ", realFoodMoneyCount='" + realFoodMoneyCount + '\'' +
                ", historyFoodMoneyCount='" + historyFoodMoneyCount + '\'' +
                ", realFoodMoneyCount2='" + realFoodMoneyCount2 + '\'' +
                ", historyFoodMoneyCount2='" + historyFoodMoneyCount2 + '\'' +
                ", realFoodMoneyCount3='" + realFoodMoneyCount3 + '\'' +
                ", historyFoodMoneyCount3='" + historyFoodMoneyCount3 + '\'' +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserCode() {
        return userCode;
    }

    public void setUserCode(Long userCode) {
        this.userCode = userCode;
    }

    public String getRealDateCheck() {
        return realDateCheck;
    }

    public void setRealDateCheck(String realDateCheck) {
        this.realDateCheck = realDateCheck;
    }

    public String getHistoryDateCheck() {
        return historyDateCheck;
    }

    public void setHistoryDateCheck(String historyDateCheck) {
        this.historyDateCheck = historyDateCheck;
    }

    public String getRealDateCheck2() {
        return realDateCheck2;
    }

    public void setRealDateCheck2(String realDateCheck2) {
        this.realDateCheck2 = realDateCheck2;
    }

    public String getHistoryDateCheck2() {
        return historyDateCheck2;
    }

    public void setHistoryDateCheck2(String historyDateCheck2) {
        this.historyDateCheck2 = historyDateCheck2;
    }

    public String getRealDateCheck3() {
        return realDateCheck3;
    }

    public void setRealDateCheck3(String realDateCheck3) {
        this.realDateCheck3 = realDateCheck3;
    }

    public String getHistoryDateCheck3() {
        return historyDateCheck3;
    }

    public void setHistoryDateCheck3(String historyDateCheck3) {
        this.historyDateCheck3 = historyDateCheck3;
    }

    public BigDecimal getMoneyPrepaid() {
        return moneyPrepaid;
    }

    public void setMoneyPrepaid(BigDecimal moneyPrepaid) {
        this.moneyPrepaid = moneyPrepaid;
    }

    public String getPrepaidStatus() {
        return prepaidStatus;
    }

    public void setPrepaidStatus(String prepaidStatus) {
        this.prepaidStatus = prepaidStatus;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getIsSettle() {
        return isSettle;
    }

    public void setIsSettle(String isSettle) {
        this.isSettle = isSettle;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getRealCount() {
        return realCount;
    }

    public void setRealCount(int realCount) {
        this.realCount = realCount;
    }

    public int getHistoryCount() {
        return historyCount;
    }

    public void setHistoryCount(int historyCount) {
        this.historyCount = historyCount;
    }

    public int getRealCount2() {
        return realCount2;
    }

    public void setRealCount2(int realCount2) {
        this.realCount2 = realCount2;
    }

    public int getHistoryCount2() {
        return historyCount2;
    }

    public void setHistoryCount2(int historyCount2) {
        this.historyCount2 = historyCount2;
    }

    public int getRealCount3() {
        return realCount3;
    }

    public void setRealCount3(int realCount3) {
        this.realCount3 = realCount3;
    }

    public int getHistoryCount3() {
        return historyCount3;
    }

    public void setHistoryCount3(int historyCount3) {
        this.historyCount3 = historyCount3;
    }

    public SysOldman getOldman() {
        return oldman;
    }

    public void setOldman(SysOldman oldman) {
        this.oldman = oldman;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getFoodInfo() {
        return foodInfo;
    }

    public void setFoodInfo(String foodInfo) {
        this.foodInfo = foodInfo;
    }

    public String getFoodInfo2() {
        return foodInfo2;
    }

    public void setFoodInfo2(String foodInfo2) {
        this.foodInfo2 = foodInfo2;
    }

    public String getFoodInfo3() {
        return foodInfo3;
    }

    public void setFoodInfo3(String foodInfo3) {
        this.foodInfo3 = foodInfo3;
    }

    public String getRealFoodMoneyCount() {
        return realFoodMoneyCount;
    }

    public void setRealFoodMoneyCount(String realFoodMoneyCount) {
        this.realFoodMoneyCount = realFoodMoneyCount;
    }

    public String getHistoryFoodMoneyCount() {
        return historyFoodMoneyCount;
    }

    public void setHistoryFoodMoneyCount(String historyFoodMoneyCount) {
        this.historyFoodMoneyCount = historyFoodMoneyCount;
    }

    public String getRealFoodMoneyCount2() {
        return realFoodMoneyCount2;
    }

    public void setRealFoodMoneyCount2(String realFoodMoneyCount2) {
        this.realFoodMoneyCount2 = realFoodMoneyCount2;
    }

    public String getHistoryFoodMoneyCount2() {
        return historyFoodMoneyCount2;
    }

    public void setHistoryFoodMoneyCount2(String historyFoodMoneyCount2) {
        this.historyFoodMoneyCount2 = historyFoodMoneyCount2;
    }

    public String getRealFoodMoneyCount3() {
        return realFoodMoneyCount3;
    }

    public void setRealFoodMoneyCount3(String realFoodMoneyCount3) {
        this.realFoodMoneyCount3 = realFoodMoneyCount3;
    }

    public String getHistoryFoodMoneyCount3() {
        return historyFoodMoneyCount3;
    }

    public void setHistoryFoodMoneyCount3(String historyFoodMoneyCount3) {
        this.historyFoodMoneyCount3 = historyFoodMoneyCount3;
    }
}
