package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.ZcdOldmanInfo;
import com.ruoyi.system.domain.ZcdOldmanInfo;

import java.util.List;

/**
 * @ClassName ZcdOldmanInfoMapper
 * @Description 助餐点老人  数据层
 * @Author lzjian
 * @Date 2019/04/08 11:02
 **/
public interface ZcdOldmanInfoMapper {
    
    /**
     * 分页查询老人信息
     * @param zcdOldmanInfo
     * @return
     */
    List<ZcdOldmanInfo> selectOldmanList(ZcdOldmanInfo zcdOldmanInfo);

    /**
     * 检查老人身份证号码唯一性
     * @param idCard
     * @return
     */
    ZcdOldmanInfo checkIDCardUnique(String idCard);

    /**
     * 新增老人
     * @param zcdOldmanInfo
     * @return
     */
    int saveOldman(ZcdOldmanInfo zcdOldmanInfo);

    /**
     * 通过id查询老人信息
     * @param id
     * @return
     */
    ZcdOldmanInfo selectOldmanById(Long id);

    /**
     * 根据id修改老人信息
     * @param zcdOldmanInfo
     * @return
     */
    int updateOldmanById(ZcdOldmanInfo zcdOldmanInfo);

    /**
     * 根据id删除老人
     * @param id
     * @return
     */
    int deleteOldmanById(Long id);
}
