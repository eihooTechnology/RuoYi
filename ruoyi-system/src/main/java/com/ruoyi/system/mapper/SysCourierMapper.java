package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.SysCourier;

import java.util.List;

/**
 * @ClassName SysCourierMapper
 * @Description 老人表  数据层
 * @Author JiaoSimao
 * @Date 2018/12/18 0018 14:37
 **/
public interface SysCourierMapper {
    /**
     * 分页查询骑手信息
     * @param sysCourier
     * @return
     */
    List<SysCourier> selectCourierList(SysCourier sysCourier);

    /**
     * 检查骑手手机号码唯一性
     * @param tel
     * @return
     */
    SysCourier checkPhoneUnique(String tel);

    /**
     * 新增骑手
     * @param sysCourier
     * @return
     */
    int saveCourier(SysCourier sysCourier);

    /**
     * 通过id查询骑手信息
     * @param id
     * @return
     */
    SysCourier selectCourierById(Long id);

    /**
     * 根据id修改骑手信息
     * @param sysCourier
     * @return
     */
    int updateCourier(SysCourier sysCourier);

    /**
     * 根据id删除骑手
     * @param sendCourierId
     * @return
     */
    int deleteCourierBySendCourierId(Long sendCourierId);
}
