package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.SysOrder;

import java.math.BigInteger;
import java.util.List;

/**
 * @ClassName SysOldManMapper
 * @Description 订单  数据层
 * @Author JiaoSimao
 * @Date 2018/12/14 0014 14:24
 **/
public interface SysOrderMapper {
    /**
     * 根据条件分页查询订单
     *
     * @param sysOrder 订单信息
     * @return 订单信息集合信息
     */
    List<SysOrder> selectOrderList(SysOrder sysOrder);

    /**
     * 新增订单信息
     * @param sysOrder
     * @return
     */
    int saveOrder(SysOrder sysOrder);

    /**
     * 批量新增订单信息
     * @param sysOrder
     * @return
     */
    int saveOrders(List<SysOrder> sysOrder);

    /**
     * 通过订单ID 查询订单
     * @param id
     * @return 订单对象信息
     */
    SysOrder selectOrderById(Long id);

    /**
     * 修改订单状态
     * @param sysOrder
     * @return
     */
    int updateOrder(SysOrder sysOrder);

    /**
     * 批量修改订单状态
     * @param sysOrder
     * @return
     */
    int updateOrderList(SysOrder sysOrder);

    /**
     * 刷脸修改订单状态
     * @param sysOrder
     * @return
     */
    int updateOrderByFace(SysOrder sysOrder);

    /**
     * 删除订单
     * @param id
     * @return
     */
    int deleteOrderByIds(Long[] id);

    /***********************************************************/

    /**
     * 根据条件分页查询今日订单
     *
     * @param sysOrder 订单信息
     * @return 订单信息集合信息
     */
    List<SysOrder> selectTodayOrderList(SysOrder sysOrder);



    /**
     * 通过订单ID 查询订单
     * @param id
     * @return 订单对象信息
     */
    SysOrder selectTodayOrderById(Long id);

    /**
     * 修改订单状态
     * @param sysOrder
     * @return
     */
    int updateTodayOrder(SysOrder sysOrder);

    /**
     * 删除订单
     * @param id
     * @return
     */
    int deleteTodayOrderByIds(Long[] id);

    /**
     * 删除老人前，先删除订单
     * @param userCode
     * @return
     */
    int deleteOrderByUserCode(Long userCode);

    /***
     * 根据老人编号查询老人订单
     * @param order
     * @return
     */
    List<SysOrder> courierGetOrdersListApi(SysOrder order);

    /**
     * 批量更换送餐员
     * @param order
     * @return
     */
    int updateOrderSenders(SysOrder order);
}
