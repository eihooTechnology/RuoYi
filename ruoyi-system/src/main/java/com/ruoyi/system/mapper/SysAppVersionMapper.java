package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.SysAppVersion;

/**
 * @ClassName SysAppVersionMapper
 * @Description TODO
 * @Author lzjian
 * @Date 2019/3/29 09:42
 **/
public interface SysAppVersionMapper {

    /** 获取最新的版本号 **/
    SysAppVersion getNewestAppVersion(SysAppVersion appVersion);

}
