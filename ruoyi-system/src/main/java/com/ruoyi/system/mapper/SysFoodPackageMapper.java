package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.SysCourier;
import com.ruoyi.system.domain.SysFoodPackage;

import java.util.List;

/**
 * @ClassName SysFoodPackageMapper
 * @Description TODO
 * @Author JiaoSimao
 * @Date 2019/1/4 0004 16:13
 **/
public interface SysFoodPackageMapper {
    /**分页查询套餐*/
    List<SysFoodPackage> selectFoodPackageList(SysFoodPackage sysFoodPackage);

    /**检查套餐名称是否重复*/
    SysFoodPackage checkFoodPackageNameUnique(String foodPackageName);

    /**添加套餐*/
    int saveFoodPackage(SysFoodPackage sysFoodPackage);

    /**通过id 查询套餐信息*/
    SysFoodPackage selectFoodPackageById(Long id);

    /**根据id修改套餐信息*/
    int updateFoodPackage(SysFoodPackage sysFoodPackage);

    /**根据id删除套餐*/
    int deleteFoodPackageByIds(Long[] idsArray);
}
