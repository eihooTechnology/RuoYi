package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.SysOrder;
import com.ruoyi.system.domain.SysOrderPreview;

import java.util.List;

/**
 * @ClassName SysOldManMapper
 * @Description 订单预览  数据层
 * @Author JiaoSimao
 * @Date 2018/12/14 0014 14:24
 **/
public interface SysOrderPreviewMapper {

    /**
     * 根据条件分页查询订单预览
     *
     * @param sysOrder 订单预览信息
     * @return 订单预览信息集合信息
     */
    List<SysOrder> selectOrderList(SysOrder sysOrder);

    /**
     * 根据条件分页查询订单预览2
     *
     * @param sysOrderPreview 订单预览信息
     * @return 订单预览信息集合信息
     */
    List<SysOrderPreview> selectOrderList2(SysOrderPreview sysOrderPreview);

    /**
     * 新增订单预览信息
     * @param sysOrder
     * @return
     */
    int saveOrder(SysOrder sysOrder);

    /**
     * 批量新增订单预览信息
     * @param sysOrder
     * @return
     */
    int saveOrders(List<SysOrder> sysOrder);

    /**
     * 删除所有订单预览
     * @return
     */
    int deleteAllOrder();
    
}
