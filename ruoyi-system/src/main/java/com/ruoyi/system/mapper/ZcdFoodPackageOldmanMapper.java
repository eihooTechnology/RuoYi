package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.ZcdFoodPackage;
import com.ruoyi.system.domain.ZcdFoodPackageOldman;

import java.util.List;

/**
 * @ClassName ZcdFoodPackageOldmanMapper
 * @Description 助餐点老人的菜单  数据层
 * @Author lzjian
 * @Date 2019/04/08 11:02
 **/
public interface ZcdFoodPackageOldmanMapper {
    
    /**
     * 分页查询老人的菜单信息
     * @param zcdFoodPackageOldman
     * @return
     */
    List<ZcdFoodPackageOldman> selectFoodPackageOldmanList(ZcdFoodPackageOldman zcdFoodPackageOldman);

    /**
     * 新增菜单
     * @param zcdFoodPackageOldman
     * @return
     */
    int saveFoodPackageOldman(ZcdFoodPackageOldman zcdFoodPackageOldman);

    /**
     * 批量新增菜单
     * @param zcdFoodPackageOldmanList
     * @return
     */
    int saveFoodPackageOldmanByBatch(List<ZcdFoodPackageOldman> zcdFoodPackageOldmanList);

    /**
     * 通过id查询老人的菜单信息
     * @param id
     * @return
     */
    ZcdFoodPackageOldman selectFoodPackageOldmanById(Long id);

    /**
     * 根据id修改老人的菜单信息
     * @param zcdFoodPackageOldman
     * @return
     */
    int updateFoodPackageOldmanById(ZcdFoodPackageOldman zcdFoodPackageOldman);

    /**
     * 根据id删除菜单
     * @param id
     * @return
     */
    int deleteFoodPackageOldmanById(Long id);
}
