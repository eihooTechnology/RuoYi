package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.SysRoleCommunity;

import java.util.List;

/**
 * 角色与社区关联表 数据层
 * 
 * @author ruoyi
 */
public interface SysRoleCommunityMapper
{
    /**
     * 通过角色ID删除角色和社区关联
     * 
     * @param roleId 角色ID
     * @return 结果
     */
    public int deleteRoleCommunityByRoleId(Long roleId);

    /**
     * 批量删除角色社区关联信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRoleCommunity(Long[] ids);

    /**
     * 查询社区使用数量
     * 
     * @param communityId 社区ID
     * @return 结果
     */
    public int selectCountRoleCommunityByCommunityId(Long communityId);

    /**
     * 批量新增角色社区信息
     * 
     * @param RoleCommunityList 角色社区列表
     * @return 结果
     */
    public int batchRoleCommunity(List<SysRoleCommunity> RoleCommunityList);
}
