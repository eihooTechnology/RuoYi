package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.ZcdFoodPackage;
import com.ruoyi.system.domain.ZcdFoodPackage;

import java.util.List;

/**
 * @ClassName ZcdFoodPackageMapper
 * @Description 助餐点菜单  数据层
 * @Author lzjian
 * @Date 2019/04/08 11:02
 **/
public interface ZcdFoodPackageMapper {

    /**
     * 分页查询菜单信息
     *
     * @param zcdFoodPackage
     * @return
     */
    List<ZcdFoodPackage> selectFoodPackageList(ZcdFoodPackage zcdFoodPackage);

    /**
     * 新增菜单
     *
     * @param zcdFoodPackage
     * @return
     */
    int saveFoodPackage(ZcdFoodPackage zcdFoodPackage);

    /**
     * 批量新增菜单
     *
     * @param zcdFoodPackageList
     * @return
     */
    int saveFoodPackageByBatch(List<ZcdFoodPackage> zcdFoodPackageList);

    /**
     * 通过id查询菜单信息
     *
     * @param id
     * @return
     */
    ZcdFoodPackage selectFoodPackageById(Long id);

    /**
     * 根据id修改菜单信息
     *
     * @param zcdFoodPackage
     * @return
     */
    int updateFoodPackageById(ZcdFoodPackage zcdFoodPackage);

    /**
     * 根据id删除菜单
     *
     * @param id
     * @return
     */
    int deleteFoodPackageById(Long id);
}
