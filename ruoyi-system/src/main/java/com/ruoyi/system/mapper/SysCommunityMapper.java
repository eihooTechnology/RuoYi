package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.SysCommunity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 社区管理 数据层
 *
 * @author ruoyi
 */
public interface SysCommunityMapper {
    /**
     * 查询社区人数
     *
     * @param community 社区信息
     * @return 结果
     */
    public int selectCommunityCount(SysCommunity community);

    /**
     * 查询社区是否存在用户
     *
     * @param communityId 社区ID
     * @return 结果
     */
    public int checkCommunityExistUser(Long communityId);

    /**
     * 查询社区管理数据
     *
     * @param community 社区信息
     * @return 社区信息集合
     */
    public List<SysCommunity> selectCommunityList(SysCommunity community);

    /**
     * 删除社区管理信息
     *
     * @param communityId 社区ID
     * @return 结果
     */
    public int deleteCommunityById(Long communityId);

    /**
     * 新增社区信息
     *
     * @param community 社区信息
     * @return 结果
     */
    public int insertCommunity(SysCommunity community);

    /**
     * 新增社区根节点信息
     *
     * @param community 社区信息
     * @return 结果
     */
    public int insertRootCommunityTree(SysCommunity community);

    /**
     * 修改社区信息
     *
     * @param community 社区信息
     * @return 结果
     */
    public int updateCommunity(SysCommunity community);

    /**
     * 修改子元素关系
     *
     * @param communitys 子元素
     * @return 结果
     */
    public int updateCommunityChildren(@Param("communitys") List<SysCommunity> communitys);

    /**
     * 根据社区ID查询信息
     *
     * @param communityId 社区ID
     * @return 社区信息
     */
    public SysCommunity selectCommunityById(Long communityId);

    /**
     * 校验社区名称是否唯一
     *
     * @param communityName 社区名称
     * @param parentId      父社区ID
     * @return 结果
     */
    public SysCommunity checkCommunityNameUnique(@Param("communityName") String communityName, @Param("parentId") Long parentId);

    /**
     * 根据角色ID查询社区
     *
     * @param roleId 角色ID
     * @return 社区列表
     */
    public List<String> selectRoleCommunityTree(Long roleId);
}
