package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.WsCommunityEvent;

/**
 * @ClassName WsCommunityEventMapper
 * @Description webSocket  数据层
 * @Author lzjian
 * @Date 2019-03-05 09:47
 **/
public interface WsCommunityEventMapper {

    /**
     * 新增webSocket信息
     * @param wsCommunityEvent
     * @return
     */
    int saveWsInfo(WsCommunityEvent wsCommunityEvent);

}
