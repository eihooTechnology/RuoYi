package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.SysOldman;

import java.util.List;

/**
 * @ClassName SysOldManMapper
 * @Description 老人表  用户层
 * @Author JiaoSimao
 * @Date 2018/12/14 0014 14:24
 **/
public interface SysOldManMapper {
    /**
     * 根据条件分页查询老人对象
     *
     * @param sysOldman 老人信息
     * @return 老人信息集合信息
     */
    List<SysOldman> selectOldManList(SysOldman sysOldman);

    /**
     * 新增老人时候，根据身份证查询老人唯一
     * @param identityId
     * @return 结果
     */
     int checkOldManUnique(String identityId);

    /**
     * 新增老人
     * @param sysOldman
     * @return 结果
     */
    int saveOldMan(SysOldman sysOldman);

    /**
     * 通过老人ID(老人编号) 查询老人
     * @param userCode
     * @return 老人对象信息
     */
    SysOldman selectOldManById(Long userCode);

    /**
     * 修改老人信息
     * @param sysOldman
     * @return
     */
    int updateOldMan(SysOldman sysOldman);

    /**
     * 批量修改老人信息
     * @param sysOldman
     * @return
     */
    int updateOldManList(SysOldman sysOldman);

    /**
     *  根据老人编号删除老人
     * @param userCode
     * @return
     */
    int deleteOldManByUserCode(Long userCode);

    /**
     * 清空旧的订餐日期分配信息
     */
    void clearAllOldManDateCheck();
}
