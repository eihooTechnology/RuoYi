package com.ruoyi.system.service.impl;

import com.ruoyi.common.support.Convert;
import com.ruoyi.system.domain.SysOrder;
import com.ruoyi.system.mapper.SysOrderMapper;
import com.ruoyi.system.service.ISysOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName SysOldManServiceImpl
 * @Description 订单管理业务层处理
 * @Author JiaoSimao
 * @Date 2018/12/14 0014 14:19
 **/
@Service
public class SysOrderServiceImpl implements ISysOrderService {
    @Autowired
    private SysOrderMapper orderMapper;

    /**
     * 根据条件分页查询老人对象
     *
     * @param sysOrder
     * @return
     */
    @Override
    public List<SysOrder> selectOrderList(SysOrder sysOrder) {
        return orderMapper.selectOrderList(sysOrder);
    }

    @Override
    public int saveOrder(SysOrder sysOrder) {
        int rows = orderMapper.saveOrder(sysOrder);
        return rows;
    }

    @Override
    public int saveOrders(List<SysOrder> sysOrder) {
        int rows = orderMapper.saveOrders(sysOrder);
        return rows;
    }


    /**
     * 通过订单ID 查询订单
     *
     * @param id
     * @return
     */
    @Override
    public SysOrder selectOrderById(Long id) {
        return orderMapper.selectOrderById(id);
    }

    /**
     * 修改订单状态
     *
     * @param sysOrder
     * @return
     */
    @Override
    public int updateOrder(SysOrder sysOrder) {
        return orderMapper.updateOrder(sysOrder);
    }

    @Override
    public int updateOrderList(SysOrder sysOrder) {
        return orderMapper.updateOrderList(sysOrder);
    }

    @Override
    public int updateOrderByFace(SysOrder sysOrder) {
        return orderMapper.updateOrderByFace(sysOrder);
    }

    /**
     * 删除订单
     *
     * @param ids
     * @return
     */
    @Override
    public int deleteOrderByIds(String ids) {
        Long[] id = Convert.toLongArray(ids);
        return orderMapper.deleteOrderByIds(id);
    }

    /******************************************************************/

    /**
     * 根据条件分页查询今日订单
     *
     * @param sysOrder
     * @return
     */
    @Override
    public List<SysOrder> selectTodayOrderList(SysOrder sysOrder) {
        return orderMapper.selectTodayOrderList(sysOrder);
    }


    /**
     * 通过订单ID 查询订单
     *
     * @param id
     * @return
     */
    @Override
    public SysOrder selectTodayOrderById(Long id) {
        return orderMapper.selectTodayOrderById(id);
    }

    /**
     * 修改订单状态
     *
     * @param sysOrder
     * @return
     */
    @Override
    public int updateTodayOrder(SysOrder sysOrder) {
        return orderMapper.updateTodayOrder(sysOrder);
    }

    /**
     * 删除订单
     *
     * @param ids
     * @return
     */
    @Override
    public int deleteTodayOrderByIds(String ids) {
        Long[] id = Convert.toLongArray(ids);
        return orderMapper.deleteTodayOrderByIds(id);
    }

    /**
     * 删除老人前，先删除订单
     * @param userCode
     * @return
     */
    @Override
    public int deleteOrderByUserCode(Long userCode) {
        return orderMapper.deleteOrderByUserCode(userCode);
    }

    @Override
    public List<SysOrder> courierGetOrdersListApi(SysOrder order) {
        return orderMapper.courierGetOrdersListApi(order);
    }

    @Override
    public int updateOrderSenders(SysOrder order) {
        return orderMapper.updateOrderSenders(order);
    }

}
