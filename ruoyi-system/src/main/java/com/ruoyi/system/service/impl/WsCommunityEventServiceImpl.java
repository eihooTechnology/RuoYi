package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.WsCommunityEvent;
import com.ruoyi.system.mapper.WsCommunityEventMapper;
import com.ruoyi.system.service.IWsCommunityEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @ClassName SysOldManServiceImpl
 * @Description 订单管理业务层处理
 * @Author JiaoSimao
 * @Date 2018/12/14 0014 14:19
 **/
@Service
public class WsCommunityEventServiceImpl implements IWsCommunityEventService {

    @Autowired
    private WsCommunityEventMapper wsCommunityEventMapper;

    @Override
    public int saveWsInfo(WsCommunityEvent wsCommunityEvent) {
        System.err.println("Service = " + wsCommunityEvent.toString());
        int rows = wsCommunityEventMapper.saveWsInfo(wsCommunityEvent);
        return rows;
    }

}
