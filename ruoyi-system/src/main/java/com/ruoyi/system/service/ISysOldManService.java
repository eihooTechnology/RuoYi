package com.ruoyi.system.service;

import com.ruoyi.system.domain.SysOldman;

import java.util.List;

/**
 * @ClassName ISysOldManService
 * @Description 老人管理业务层
 * @Author JiaoSimao
 * @Date 2018/12/14 0014 14:14
 **/
public interface ISysOldManService {
    /**
     * 分页查询老人信息
     * @param sysOldman
     * @return
     */
    List<SysOldman> selectOldManList(SysOldman sysOldman);

    /**
     * 检查老人身份证，判断是否有该老人信息
     * @param identityId
     * @return
     */
    String checkOldManUnique(String identityId);

    /**
     * 新增老人
     * @param sysOldman
     * @return
     */
    int saveOldMan(SysOldman sysOldman);

    /**
     * 通过老人ID(老人编号) 查询老人
     * @param userCode
     * @return
     */
    SysOldman selectOldManById(Long userCode);

    /**
     * 修改老人信息
     * @param sysOldman
     * @return
     */
    int updateOldMan(SysOldman sysOldman);

    /**
     * 批量修改老人信息
     * @param sysOldman
     * @return
     */
    int updateOldManList(SysOldman sysOldman);

    /**
     * 删除老人
     * @param userCode
     * @return
     */
    int deleteOldManByUserCode(Long userCode);

    /**
     * 清空旧的订餐日期分配信息
     */
    void clearAllOldManDateCheck();
}
