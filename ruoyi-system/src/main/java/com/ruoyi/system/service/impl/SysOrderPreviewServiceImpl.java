package com.ruoyi.system.service.impl;

import com.ruoyi.common.support.Convert;
import com.ruoyi.system.domain.SysOrder;
import com.ruoyi.system.domain.SysOrderPreview;
import com.ruoyi.system.mapper.SysOrderPreviewMapper;
import com.ruoyi.system.service.ISysOrderPreviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName SysOldManServiceImpl
 * @Description 订单管理业务层处理
 * @Author JiaoSimao
 * @Date 2018/12/14 0014 14:19
 **/
@Service
public class SysOrderPreviewServiceImpl implements ISysOrderPreviewService {
    @Autowired
    private SysOrderPreviewMapper orderPreviewMapper;

    /**
     * 根据条件分页查询老人对象
     *
     * @param sysOrder
     * @return
     */
    @Override
    public List<SysOrder> selectOrderList(SysOrder sysOrder) {
        return orderPreviewMapper.selectOrderList(sysOrder);
    }

    @Override
    public List<SysOrderPreview> selectOrderList2(SysOrderPreview sysOrderPreview) {
        return orderPreviewMapper.selectOrderList2(sysOrderPreview);
    }

    @Override
    public int saveOrder(SysOrder sysOrder) {
        int rows = orderPreviewMapper.saveOrder(sysOrder);
        return rows;
    }

    @Override
    public int saveOrders(List<SysOrder> sysOrder) {
        int rows = orderPreviewMapper.saveOrders(sysOrder);
        return rows;
    }

    /**
     * 删除订单
     *
     * @return
     */
    @Override
    public int deleteAllOrder() {
        return orderPreviewMapper.deleteAllOrder();
    }

}
