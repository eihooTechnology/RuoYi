package com.ruoyi.system.service;

import com.ruoyi.system.domain.SysOldman;
import com.ruoyi.system.domain.SysOrder;

import java.math.BigInteger;
import java.util.List;

/**
 * @ClassName ISysOldManService
 * @Description 订单管理业务层
 * @Author JiaoSimao
 * @Date 2018/12/14 0014 14:14
 **/
public interface ISysOrderService {
    /**
     * 分页查询订单信息
     *
     * @param sysOrder
     * @return
     */
    List<SysOrder> selectOrderList(SysOrder sysOrder);

    /**
     * 新增订单信息
     *
     * @param sysOrder
     * @return
     */
    int saveOrder(SysOrder sysOrder);

    /**
     * 批量新增订单信息
     *
     * @param sysOrder
     * @return
     */
    int saveOrders(List<SysOrder> sysOrder);

    /**
     * 通过订单ID 查询订单
     *
     * @param id
     * @return
     */
    SysOrder selectOrderById(Long id);

    /**
     * 修改订单状态
     *
     * @param sysOrder
     * @return
     */
    int updateOrder(SysOrder sysOrder);

    /**
     * 批量修改订单状态
     * @param sysOrder
     * @return
     */
    int updateOrderList(SysOrder sysOrder);

    /**
     * 刷脸修改订单状态
     * @param sysOrder
     * @return
     */
    int updateOrderByFace(SysOrder sysOrder);

    /**
     * 删除订单
     *
     * @param ids
     * @return
     */
    int deleteOrderByIds(String ids);

    /********************************************************************/


    /**
     * 分页查询订单信息
     *
     * @param sysOrder
     * @return
     */
    List<SysOrder> selectTodayOrderList(SysOrder sysOrder);


    /**
     * 通过订单ID 查询订单
     *
     * @param id
     * @return
     */
    SysOrder selectTodayOrderById(Long id);

    /**
     * 修改订单状态
     *
     * @param sysOrder
     * @return
     */
    int updateTodayOrder(SysOrder sysOrder);


    /**
     * 删除订单
     *
     * @param ids
     * @return
     */
    int deleteTodayOrderByIds(String ids);

    /**
     *  删除老人订单
     * @param userCode
     */
    int deleteOrderByUserCode(Long userCode);

    /***
     * 根据老人编号查询老人订单信息
     * @param order
     * @return
     */
    List<SysOrder> courierGetOrdersListApi(SysOrder order);

    /**
     * 批量更换送餐员
     * @param order
     * @return
     */
    int updateOrderSenders(SysOrder order);
}
