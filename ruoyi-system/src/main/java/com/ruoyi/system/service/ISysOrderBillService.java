package com.ruoyi.system.service;

import com.ruoyi.system.domain.SysCommunity;
import com.ruoyi.system.domain.SysOrderBill;
import com.ruoyi.system.domain.SysRole;

import java.util.List;
import java.util.Map;

/**
 * 订单账单表 服务层
 *
 * @author ruoyi
 */
public interface ISysOrderBillService {
    /**
     * 查询账单管理数据
     *
     * @param orderBill 账单信息
     * @return 账单信息集合
     */
    public List<SysOrderBill> selectOrderBillList(SysOrderBill orderBill);

    /**
     * 删除账单管理信息
     *
     * @param orderBillId 账单ID
     * @return 结果
     */
    public int deleteOrderBillById(Long orderBillId);

    /**
     * 新增账单信息
     *
     * @param orderBill 账单信息
     * @return 结果
     */
    public int insertOrderBill(SysOrderBill orderBill);

    /**
     * 新增账单信息(手动添加创建时间)
     *
     * @param orderBill 账单信息
     * @return 结果
     */
    public int insertOrderBillByCreateTime(SysOrderBill orderBill);

    /**
     * 批量新增账单信息
     * @param orderBill
     * @return
     */
    int saveOrderBills(List<SysOrderBill> orderBill);

    /**
     * 修改账单信息
     *
     * @param orderBill 账单信息
     * @return 结果
     */
    public int updateOrderBill(SysOrderBill orderBill);

    /**
     * 根据账单ID查询信息
     *
     * @param orderBillId 账单ID
     * @return 账单信息
     */
    public SysOrderBill selectOrderBillById(Long orderBillId);
}
