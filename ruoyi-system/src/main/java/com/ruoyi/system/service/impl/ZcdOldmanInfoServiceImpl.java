package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.constant.CourierConstants;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.SysCourier;
import com.ruoyi.system.domain.ZcdOldmanInfo;
import com.ruoyi.system.mapper.SysCourierMapper;
import com.ruoyi.system.mapper.ZcdOldmanInfoMapper;
import com.ruoyi.system.service.ISysCourierService;
import com.ruoyi.system.service.IZcdOldmanInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName ZcdOldmanInfoServiceImpl
 * @Description TODO
 * @Author lzjian
 * @Date 2019/04/08 11:28
 **/
@Service
public class ZcdOldmanInfoServiceImpl implements IZcdOldmanInfoService {

    @Autowired
    private ZcdOldmanInfoMapper zcdOldmanInfoMapper;

    @Override
    public List<ZcdOldmanInfo> selectOldmanList(ZcdOldmanInfo zcdOldmanInfo) {
        return zcdOldmanInfoMapper.selectOldmanList(zcdOldmanInfo);
    }

    @Override
    public ZcdOldmanInfo checkIDCardUnique(String idCard) {
        return zcdOldmanInfoMapper.checkIDCardUnique(idCard);
    }

    @Override
    public int saveOldman(ZcdOldmanInfo zcdOldmanInfo) {
        return zcdOldmanInfoMapper.saveOldman(zcdOldmanInfo);
    }

    @Override
    public ZcdOldmanInfo selectOldmanById(Long id) {
        return zcdOldmanInfoMapper.selectOldmanById(id);
    }

    @Override
    public int updateOldmanById(ZcdOldmanInfo zcdOldmanInfo) {
        return zcdOldmanInfoMapper.updateOldmanById(zcdOldmanInfo);
    }

    @Override
    public int deleteOldmanById(Long id) {
        return zcdOldmanInfoMapper.deleteOldmanById(id);
    }
}
