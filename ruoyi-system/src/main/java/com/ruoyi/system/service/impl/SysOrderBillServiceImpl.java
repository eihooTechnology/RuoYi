package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.SysCommunity;
import com.ruoyi.system.domain.SysOrderBill;
import com.ruoyi.system.domain.SysRole;
import com.ruoyi.system.mapper.SysCommunityMapper;
import com.ruoyi.system.mapper.SysOrderBillMapper;
import com.ruoyi.system.service.ISysCommunityService;
import com.ruoyi.system.service.ISysOrderBillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 社区管理 服务实现
 *
 * @author ruoyi
 */
@Service
public class SysOrderBillServiceImpl implements ISysOrderBillService {
    @Autowired
    private SysOrderBillMapper orderBillMapper;

    @Override
    @DataScope(tableAlias = "o")
    public List<SysOrderBill> selectOrderBillList(SysOrderBill orderBill) {
        return orderBillMapper.selectOrderBillList(orderBill);
    }

    @Override
    public int deleteOrderBillById(Long orderBillId) {
        return orderBillMapper.deleteOrderBillById(orderBillId);
    }

    @Override
    public int insertOrderBill(SysOrderBill orderBill) {
        return orderBillMapper.insertOrderBill(orderBill);
    }

    @Override
    public int insertOrderBillByCreateTime(SysOrderBill orderBill) {
        return orderBillMapper.insertOrderBillByCreateTime(orderBill);
    }

    @Override
    public int saveOrderBills(List<SysOrderBill> orderBill) {
        int rows = orderBillMapper.saveOrderBills(orderBill);
        return rows;
    }

    @Override
    public int updateOrderBill(SysOrderBill orderBill) {
        return orderBillMapper.updateOrderBill(orderBill);
    }

    @Override
    public SysOrderBill selectOrderBillById(Long orderBillId) {
        return orderBillMapper.selectOrderBillById(orderBillId);
    }
}
