package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.constant.OldManConstants;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.support.Convert;
import com.ruoyi.system.domain.SysOldman;
import com.ruoyi.system.mapper.SysOldManMapper;
import com.ruoyi.system.mapper.SysOrderMapper;
import com.ruoyi.system.service.ISysOldManService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName SysOldManServiceImpl
 * @Description 老人管理业务层处理
 * @Author JiaoSimao
 * @Date 2018/12/14 0014 14:19
 **/
@Service
public class SysOldManServiceImpl implements ISysOldManService {
    @Autowired
    private SysOldManMapper oldManMapper;

    @Autowired
    private SysOrderMapper orderMapper;
    /**
     * 根据条件分页查询老人对象
     * @param sysOldman
     * @return
     */
    @Override
    @DataScope(tableAlias = "o")
    public List<SysOldman> selectOldManList(SysOldman sysOldman) {
        return oldManMapper.selectOldManList(sysOldman);
    }

    /**
     * 新增老人的时候，根据身份证判断老人唯一
     * @param identityId
     * @return
     */
    @Override
    public String checkOldManUnique(String identityId) {
        int count = oldManMapper.checkOldManUnique(identityId);
        if (count > 0)
        {
            return OldManConstants.OLDMAN_IDENTITYID_NOT_UNIQUE;
        }
        return OldManConstants.OLDMAN_IDENTITYID_UNIQUE;
    }

    /**
     * 新增老人
     * @param sysOldman
     * @return
     */
    @Override
    public int saveOldMan(SysOldman sysOldman) {
        int rows = oldManMapper.saveOldMan(sysOldman);
        return rows;
    }

    /**
     * 通过老人ID(老人编号) 查询老人
     * @param userCode
     * @return
     */
    @Override
    public SysOldman selectOldManById(Long userCode) {
        return oldManMapper.selectOldManById(userCode);
    }

    /**
     * 修改老人信息
     * @param sysOldman
     * @return
     */
    @Override
    public int updateOldMan(SysOldman sysOldman) {
        return oldManMapper.updateOldMan(sysOldman);
    }

    /**
     * 批量修改老人信息
     * @param sysOldman
     * @return
     */
    @Override
    public int updateOldManList(SysOldman sysOldman) {
        return oldManMapper.updateOldManList(sysOldman);
    }

    /**
     * 删除老人
     * @param userCode
     * @return
     */
    @Override
    public int deleteOldManByUserCode(Long userCode) {
//        Long[] userCodes = Convert.toLongArray(userCode);
        return oldManMapper.deleteOldManByUserCode(userCode);

    }

    @Override
    public void clearAllOldManDateCheck() {
        oldManMapper.clearAllOldManDateCheck();
    }
}
