package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.ZcdFoodPackage;
import com.ruoyi.system.domain.ZcdOldmanInfo;
import com.ruoyi.system.mapper.ZcdFoodPackageMapper;
import com.ruoyi.system.mapper.ZcdOldmanInfoMapper;
import com.ruoyi.system.service.IZcdFoodPackageService;
import com.ruoyi.system.service.IZcdOldmanInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName ZcdOldmanInfoServiceImpl
 * @Description TODO
 * @Author lzjian
 * @Date 2019/04/08 11:28
 **/
@Service
public class ZcdFoodPackageServiceImpl implements IZcdFoodPackageService {

    @Autowired
    private ZcdFoodPackageMapper zcdFoodPackageMapper;

    @Override
    public List<ZcdFoodPackage> selectFoodPackageList(ZcdFoodPackage zcdFoodPackage) {
        return zcdFoodPackageMapper.selectFoodPackageList(zcdFoodPackage);
    }

    @Override
    public int saveFoodPackage(ZcdFoodPackage zcdFoodPackage) {
        return zcdFoodPackageMapper.saveFoodPackage(zcdFoodPackage);
    }

    @Override
    public int saveFoodPackageByBatch(List<ZcdFoodPackage> zcdFoodPackageList) {
        return zcdFoodPackageMapper.saveFoodPackageByBatch(zcdFoodPackageList);
    }

    @Override
    public ZcdFoodPackage selectFoodPackageById(Long id) {
        return zcdFoodPackageMapper.selectFoodPackageById(id);
    }

    @Override
    public int updateFoodPackageById(ZcdFoodPackage zcdFoodPackage) {
        return zcdFoodPackageMapper.updateFoodPackageById(zcdFoodPackage);
    }

    @Override
    public int deleteFoodPackageById(Long id) {
        return zcdFoodPackageMapper.deleteFoodPackageById(id);
    }

}
