package com.ruoyi.system.service;

import com.ruoyi.system.domain.SysCommunity;
import com.ruoyi.system.domain.SysRole;

import java.util.List;
import java.util.Map;

/**
 * 社区管理 服务层
 *
 * @author ruoyi
 */
public interface ISysCommunityService {
    /**
     * 查询社区管理数据
     *
     * @param community 社区信息
     * @return 社区信息集合
     */
    public List<SysCommunity> selectCommunityList(SysCommunity community);

    /**
     * 查询社区管理树
     *
     * @param community 社区信息
     * @return 所有社区信息
     */
    public List<Map<String, Object>> selectCommunityTree(SysCommunity community);

    /**
     * 根据角色ID查询菜单
     *
     * @param role 角色对象
     * @return 菜单列表
     */
    public List<Map<String, Object>> roleCommunityTreeData(SysRole role);

    /**
     * 查询社区人数
     *
     * @param parentId 父社区ID
     * @return 结果
     */
    public int selectCommunityCount(Long parentId);

    /**
     * 查询社区是否存在用户
     *
     * @param communityId 社区ID
     * @return 结果 true 存在 false 不存在
     */
    public boolean checkCommunityExistUser(Long communityId);

    /**
     * 删除社区管理信息
     *
     * @param communityId 社区ID
     * @return 结果
     */
    public int deleteCommunityById(Long communityId);

    /**
     * 新增保存社区信息
     *
     * @param community 社区信息
     * @return 结果
     */
    public int insertCommunity(SysCommunity community);

    /**
     * 新增保存社区根节点信息
     *
     * @param community 社区信息
     * @return 结果
     */
    public int insertRootCommunityTree(SysCommunity community);

    /**
     * 修改保存社区信息
     *
     * @param community 社区信息
     * @return 结果
     */
    public int updateCommunity(SysCommunity community);

    /**
     * 根据社区ID查询信息
     *
     * @param communityId 社区ID
     * @return 社区信息
     */
    public SysCommunity selectCommunityById(Long communityId);

    /**
     * 校验社区名称是否唯一
     *
     * @param community 社区信息
     * @return 结果
     */
    public String checkCommunityNameUnique(SysCommunity community);
}
