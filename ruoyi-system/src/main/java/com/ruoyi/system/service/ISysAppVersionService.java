package com.ruoyi.system.service;

import com.ruoyi.system.domain.SysAppVersion;
import com.ruoyi.system.domain.SysFoodPackage;

import java.util.List;

/**
 * @ClassName ISysAppVersionService
 * @Description TODO
 * @Author lzjian
 * @Date 2019/3/29 09:45
 **/
public interface ISysAppVersionService {

    /** 获取最新的版本号 **/
    SysAppVersion getNewestAppVersion(SysAppVersion appVersion);

}
