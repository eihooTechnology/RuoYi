package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.constant.CourierConstants;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.support.Convert;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.SysCourier;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.mapper.SysCourierMapper;
import com.ruoyi.system.service.ISysCourierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName SysCourierServiceImpl
 * @Description TODO
 * @Author JiaoSimao
 * @Date 2018/12/18 0018 14:34
 **/
@Service
public class SysCourierServiceImpl implements ISysCourierService {
    @Autowired
    private SysCourierMapper courierMapper;

    /**
     * 分页查询骑手信息
     * @param sysCourier
     * @return
     */
    @Override
    @DataScope(tableAlias = "o")
    public List<SysCourier> selectCourierList(SysCourier sysCourier) {
        return courierMapper.selectCourierList(sysCourier);
    }

    /**
     * 检验手机号码是否唯一
     * @param sysCourier
     * @return
     */
    @Override
    public String checkPhoneUnique(SysCourier sysCourier) {
        String tel = StringUtils.isNull(sysCourier.getTel()) ? "-1" : sysCourier.getTel();
        SysCourier info = courierMapper.checkPhoneUnique(sysCourier.getTel());
        if (StringUtils.isNotNull(info) && info.getTel() != tel)
        {
            return CourierConstants.USER_PHONE_NOT_UNIQUE;
        }
        return CourierConstants.USER_PHONE_UNIQUE;
    }

    /**
     * 添加骑手
     * @param sysCourier
     * @return
     */
    @Override
    public int saveCourier(SysCourier sysCourier) {
        int rows = courierMapper.saveCourier(sysCourier);
        return rows;
    }

    /**
     * 通过id 查询骑手信息
     * @param id
     * @return
     */
    @Override
    public SysCourier selectCourierById(Long id) {
        return courierMapper.selectCourierById(id);
    }

    /**
     * 根据id修改骑手信息
     * @param sysCourier
     * @return
     */
    @Override
    public int updateCourier(SysCourier sysCourier) {
        return courierMapper.updateCourier(sysCourier);
    }

    /**
     * 根据id删除骑手信息
     * @param ids
     * @return
     */
    @Override
    public int deleteCourierBySendCourierId(Long sendCourierId) {
//        Long[] idsArray = Convert.toLongArray(ids);
        return courierMapper.deleteCourierBySendCourierId(sendCourierId);
    }
}
