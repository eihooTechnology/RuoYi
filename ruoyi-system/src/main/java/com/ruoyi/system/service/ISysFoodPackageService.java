package com.ruoyi.system.service;

import com.ruoyi.system.domain.SysCourier;
import com.ruoyi.system.domain.SysFoodPackage;

import java.util.List;

/**
 * @ClassName ISysFoodPackageService
 * @Description TODO
 * @Author JiaoSimao
 * @Date 2019/1/4 0004 16:00
 **/
public interface ISysFoodPackageService {
    /**分页查询套餐*/
    List<SysFoodPackage> selectFoodPackageList(SysFoodPackage sysFoodPackage);

    /**
     * 检查套餐名是否一致
     * @param sysFoodPackage
     * @return
     */
    String checkFoodPackageNameUnique(SysFoodPackage sysFoodPackage);

    /**
     * 填写套餐后 新增套餐
     * @param sysFoodPackage
     * @return
     */
    int saveFoodPackage(SysFoodPackage sysFoodPackage);

    /**
     * 根据id查询套餐信息
     * @param id
     * @return
     */
    SysFoodPackage selectFoodPackageById(Long id);

    /**
     * 根据id修改套餐信息
     * @param sysFoodPackage
     * @return
     */
    int updateFoodPackage(SysFoodPackage sysFoodPackage);

    /**
     * 根据id删除套餐
     * @param ids
     * @return
     */
    int deleteFoodPackageByIds(String ids);
}
