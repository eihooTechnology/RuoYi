package com.ruoyi.system.service;

import com.ruoyi.system.domain.WsCommunityEvent;

/**
 * @ClassName IWsCommunityEventService
 * @Description webSocket业务层
 * @Author lzjian
 * @Date 2019-03-05 09:47
 **/
public interface IWsCommunityEventService {

    /**
     * 新增webSocket信息
     * @param wsCommunityEvent
     * @return
     */
    int saveWsInfo(WsCommunityEvent wsCommunityEvent);

}
