package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.constant.CourierConstants;
import com.ruoyi.common.constant.FoodPackageConstants;
import com.ruoyi.common.support.Convert;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.SysCourier;
import com.ruoyi.system.domain.SysFoodPackage;
import com.ruoyi.system.mapper.SysFoodPackageMapper;
import com.ruoyi.system.service.ISysFoodPackageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName ISysFoodPackageServiceImpl
 * @Description 套餐管理业务实现层
 * @Author JiaoSimao
 * @Date 2019/1/4 0004 16:00
 **/
@Service
public class ISysFoodPackageServiceImpl implements ISysFoodPackageService {
    @Autowired
    private SysFoodPackageMapper foodPackageMapper;


    /**
     * 分页查询套餐
     * @param sysFoodPackage
     * @return
     */
    @Override
    @DataScope(tableAlias = "o")
    public List<SysFoodPackage> selectFoodPackageList(SysFoodPackage sysFoodPackage) {
        return foodPackageMapper.selectFoodPackageList(sysFoodPackage);
    }

    /**
     * 检查套餐名称是否重复
     * @param sysFoodPackage
     * @return
     */
    @Override
    public String checkFoodPackageNameUnique(SysFoodPackage sysFoodPackage) {
        String foodPackageName = StringUtils.isNull(sysFoodPackage.getFoodPackageName()) ? "-1" : sysFoodPackage.getFoodPackageName();
        SysFoodPackage info = foodPackageMapper.checkFoodPackageNameUnique(sysFoodPackage.getFoodPackageName());
        if (StringUtils.isNotNull(info) && info.getFoodPackageName() != foodPackageName)
        {
            return FoodPackageConstants.Food_PACKAGE_NAME_NOT_UNIQUE;
        }
        return FoodPackageConstants.Food_PACKAGE_NAME_UNIQUE;
    }

    /**
     * 添加套餐
     * @param sysFoodPackage
     * @return
     */
    @Override
    public int saveFoodPackage(SysFoodPackage sysFoodPackage) {
        int rows = foodPackageMapper.saveFoodPackage(sysFoodPackage);
        return rows;
    }

    /**
     * 通过id 查询套餐信息
     * @param id
     * @return
     */
    @Override
    public SysFoodPackage selectFoodPackageById(Long id) {
        return foodPackageMapper.selectFoodPackageById(id);
    }

    /**
     * 根据id修改套餐信息
     * @param sysFoodPackage
     * @return
     */
    @Override
    public int updateFoodPackage(SysFoodPackage sysFoodPackage) {
        return foodPackageMapper.updateFoodPackage(sysFoodPackage);
    }

    /**
     * 根据id删除套餐
     * @param ids
     * @return
     */
    @Override
    public int deleteFoodPackageByIds(String ids) {
        Long[] idsArray = Convert.toLongArray(ids);
        return foodPackageMapper.deleteFoodPackageByIds(idsArray);
    }
}
