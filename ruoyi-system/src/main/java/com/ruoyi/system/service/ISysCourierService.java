package com.ruoyi.system.service;

import com.ruoyi.system.domain.SysCourier;

import java.util.List;

/**
 * @ClassName ISysCourierService
 * @Description 骑手管理业务层
 * @Author JiaoSimao
 * @Date 2018/12/18 0018 14:29
 **/
public interface ISysCourierService {
    /**
     * 分页查询骑手信息
     * @param sysCourier
     * @return 骑手信息
     */
    List<SysCourier> selectCourierList(SysCourier sysCourier);

    /**
     * 检验手机号码是否唯一
     * @param sysCourier
     * @return 结果
     */
    String checkPhoneUnique(SysCourier sysCourier);

    /**
     * 添加骑手
     * @param sysCourier
     * @return 结果
     */
    int saveCourier(SysCourier sysCourier);

    /**
     * 根据id 查询骑手信息
     * @param id
     * @return
     */
    SysCourier selectCourierById(Long id);

    /**
     * 根据id修改老人信息
     * @param sysCourier
     * @return
     */
    int updateCourier(SysCourier sysCourier);

    /**
     * 根据id 删除骑手
     * @param ids
     * @return
     */
    int deleteCourierBySendCourierId(Long sendCourierId);
}
