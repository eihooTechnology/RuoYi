package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.constant.FoodPackageConstants;
import com.ruoyi.common.support.Convert;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.SysAppVersion;
import com.ruoyi.system.domain.SysFoodPackage;
import com.ruoyi.system.mapper.SysAppVersionMapper;
import com.ruoyi.system.mapper.SysFoodPackageMapper;
import com.ruoyi.system.service.ISysAppVersionService;
import com.ruoyi.system.service.ISysFoodPackageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName SysAppVersionServiceImpl
 * @Description App版本号业务实现层
 * @Author lzjian
 * @Date 2019/3/29 09:46
 **/
@Service
public class SysAppVersionServiceImpl implements ISysAppVersionService {

    @Autowired
    private SysAppVersionMapper appVersionMapper;

    @Override
    public SysAppVersion getNewestAppVersion(SysAppVersion appVersion) {
        return appVersionMapper.getNewestAppVersion(appVersion);
    }

}
