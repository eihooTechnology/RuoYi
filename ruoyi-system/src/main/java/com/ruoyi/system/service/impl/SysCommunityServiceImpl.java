package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.SysCommunity;
import com.ruoyi.system.domain.SysRole;
import com.ruoyi.system.mapper.SysCommunityMapper;
import com.ruoyi.system.service.ISysCommunityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 社区管理 服务实现
 * 
 * @author ruoyi
 */
@Service
public class SysCommunityServiceImpl implements ISysCommunityService
{
    @Autowired
    private SysCommunityMapper communityMapper;

    /**
     * 查询社区管理数据
     * 
     * @param community 社区信息
     * @return 社区信息集合
     */
    @Override
    @DataScope(tableAlias = "d")
    public List<SysCommunity> selectCommunityList(SysCommunity community)
    {
        return communityMapper.selectCommunityList(community);
    }

    /**
     * 查询社区管理树
     * 
     * @param community 社区信息
     * @return 所有社区信息
     */
    @Override
    @DataScope(tableAlias = "d")
    public List<Map<String, Object>> selectCommunityTree(SysCommunity community)
    {
        List<Map<String, Object>> trees = new ArrayList<Map<String, Object>>();
        List<SysCommunity> communityList = communityMapper.selectCommunityList(community);
        trees = getTrees(communityList, false, null);
        return trees;
    }

    /**
     * 根据角色ID查询社区（数据权限）
     *
     * @param role 角色对象
     * @return 社区列表（数据权限）
     */
    @Override
    public List<Map<String, Object>> roleCommunityTreeData(SysRole role)
    {
        Long roleId = role.getRoleId();
        List<Map<String, Object>> trees = new ArrayList<Map<String, Object>>();
        List<SysCommunity> communityList = selectCommunityList(new SysCommunity());
        if (StringUtils.isNotNull(roleId))
        {
            List<String> rolecommunityList = communityMapper.selectRoleCommunityTree(roleId);
            trees = getTrees(communityList, true, rolecommunityList);
        }
        else
        {
            trees = getTrees(communityList, false, null);
        }
        return trees;
    }

    /**
     * 对象转社区树
     *
     * @param communityList 社区列表
     * @param isCheck 是否需要选中
     * @param roleCommunityList 角色已存在菜单列表
     * @return
     */
    public List<Map<String, Object>> getTrees(List<SysCommunity> communityList, boolean isCheck, List<String> roleCommunityList)
    {

        List<Map<String, Object>> trees = new ArrayList<Map<String, Object>>();
        for (SysCommunity community : communityList)
        {
            if (UserConstants.COMMUNITY_NORMAL.equals(community.getStatus()))
            {
                Map<String, Object> communityMap = new HashMap<String, Object>();
                communityMap.put("id", community.getCommunityId());
                communityMap.put("pId", community.getParentId());
                communityMap.put("name", community.getCommunityName());
                communityMap.put("title", community.getCommunityName());
                if (isCheck)
                {
                    communityMap.put("checked", roleCommunityList.contains(community.getCommunityId() + community.getCommunityName()));
                }
                else
                {
                    communityMap.put("checked", false);
                }
                trees.add(communityMap);
            }
        }
        return trees;
    }

    /**
     * 查询社区人数
     * 
     * @param parentId 社区ID
     * @return 结果
     */
    @Override
    public int selectCommunityCount(Long parentId)
    {
        SysCommunity community = new SysCommunity();
        community.setParentId(parentId);
        return communityMapper.selectCommunityCount(community);
    }

    /**
     * 查询社区是否存在用户
     * 
     * @param communityId 社区ID
     * @return 结果 true 存在 false 不存在
     */
    @Override
    public boolean checkCommunityExistUser(Long communityId)
    {
        int result = communityMapper.checkCommunityExistUser(communityId);
        return result > 0 ? true : false;
    }

    /**
     * 删除社区管理信息
     * 
     * @param communityId 社区ID
     * @return 结果
     */
    @Override
    public int deleteCommunityById(Long communityId)
    {
        return communityMapper.deleteCommunityById(communityId);
    }

    /**
     * 新增保存社区信息
     * 
     * @param community 社区信息
     * @return 结果
     */
    @Override
    public int insertCommunity(SysCommunity community)
    {
        SysCommunity info = communityMapper.selectCommunityById(community.getParentId());
        community.setAncestors(info.getAncestors() + "," + community.getParentId());
        return communityMapper.insertCommunity(community);
    }

    @Override
    public int insertRootCommunityTree(SysCommunity community) {
        SysCommunity info = communityMapper.selectCommunityById(community.getParentId());
        community.setAncestors("0");
        return communityMapper.insertRootCommunityTree(community);
    }

    /**
     * 修改保存社区信息
     * 
     * @param community 社区信息
     * @return 结果
     */
    @Override
    public int updateCommunity(SysCommunity community)
    {
        SysCommunity info = communityMapper.selectCommunityById(community.getParentId());
        if (StringUtils.isNotNull(info))
        {
            String ancestors = info.getAncestors() + "," + community.getParentId();
            community.setAncestors(ancestors);
            updateCommunityChildren(community.getCommunityId(), ancestors);
        }
        return communityMapper.updateCommunity(community);
    }

    /**
     * 修改子元素关系
     * 
     * @param communityId 社区ID
     * @param ancestors 元素列表
     */
    public void updateCommunityChildren(Long communityId, String ancestors)
    {
        SysCommunity community = new SysCommunity();
        community.setParentId(communityId);
        List<SysCommunity> childrens = communityMapper.selectCommunityList(community);
        for (SysCommunity children : childrens)
        {
            children.setAncestors(ancestors + "," + community.getParentId());
        }
        if (childrens.size() > 0)
        {
            communityMapper.updateCommunityChildren(childrens);
        }
    }

    /**
     * 根据社区ID查询信息
     * 
     * @param communityId 社区ID
     * @return 社区信息
     */
    @Override
    public SysCommunity selectCommunityById(Long communityId)
    {
        return communityMapper.selectCommunityById(communityId);
    }

    /**
     * 校验社区名称是否唯一
     * 
     * @param community 社区信息
     * @return 结果
     */
    @Override
    public String checkCommunityNameUnique(SysCommunity community)
    {
        Long communityId = StringUtils.isNull(community.getCommunityId()) ? -1L : community.getCommunityId();
        SysCommunity info = communityMapper.checkCommunityNameUnique(community.getCommunityName(), community.getParentId());
        if (StringUtils.isNotNull(info) && info.getCommunityId().longValue() != communityId.longValue())
        {
            return UserConstants.COMMUNITY_NAME_NOT_UNIQUE;
        }
        return UserConstants.COMMUNITY_NAME_UNIQUE;
    }
}
