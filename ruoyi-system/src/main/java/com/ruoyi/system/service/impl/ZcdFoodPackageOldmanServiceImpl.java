package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.ZcdFoodPackage;
import com.ruoyi.system.domain.ZcdFoodPackageOldman;
import com.ruoyi.system.mapper.ZcdFoodPackageMapper;
import com.ruoyi.system.mapper.ZcdFoodPackageOldmanMapper;
import com.ruoyi.system.service.IZcdFoodPackageOldmanService;
import com.ruoyi.system.service.IZcdFoodPackageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName ZcdOldmanInfoServiceImpl
 * @Description TODO
 * @Author lzjian
 * @Date 2019/04/08 11:28
 **/
@Service
public class ZcdFoodPackageOldmanServiceImpl implements IZcdFoodPackageOldmanService {

    @Autowired
    private ZcdFoodPackageOldmanMapper zcdFoodPackageOldmanMapper;

    @Override
    public List<ZcdFoodPackageOldman> selectFoodPackageOldmanList(ZcdFoodPackageOldman zcdFoodPackageOldman) {
        return zcdFoodPackageOldmanMapper.selectFoodPackageOldmanList(zcdFoodPackageOldman);
    }

    @Override
    public int saveFoodPackageOldman(ZcdFoodPackageOldman zcdFoodPackageOldman) {
        return zcdFoodPackageOldmanMapper.saveFoodPackageOldman(zcdFoodPackageOldman);
    }

    @Override
    public int saveFoodPackageOldmanByBatch(List<ZcdFoodPackageOldman> zcdFoodPackageOldmanList) {
        return zcdFoodPackageOldmanMapper.saveFoodPackageOldmanByBatch(zcdFoodPackageOldmanList);
    }

    @Override
    public ZcdFoodPackageOldman selectFoodPackageOldmanById(Long id) {
        return zcdFoodPackageOldmanMapper.selectFoodPackageOldmanById(id);
    }

    @Override
    public int updateFoodPackageOldmanById(ZcdFoodPackageOldman zcdFoodPackageOldman) {
        return zcdFoodPackageOldmanMapper.updateFoodPackageOldmanById(zcdFoodPackageOldman);
    }

    @Override
    public int deleteFoodPackageOldmanById(Long id) {
        return zcdFoodPackageOldmanMapper.deleteFoodPackageOldmanById(id);
    }

}
