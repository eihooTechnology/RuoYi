package com.ruoyi.common.constant;

/**
 * @ClassName FoodPackageConstants
 * @Description TODO
 * @Author JiaoSimao
 * @Date 2019/1/4 0004 16:46
 **/
public class FoodPackageConstants {
    /** 套餐名称是否唯一的返回结果 */
    public final static String Food_PACKAGE_NAME_UNIQUE = "0";
    public final static String Food_PACKAGE_NAME_NOT_UNIQUE = "1";
}
