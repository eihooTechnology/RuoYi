package com.ruoyi.common.constant;

/**
 * @ClassName CourierConstants
 * @Description TODO
 * @Author JiaoSimao
 * @Date 2018/12/18 0018 15:57
 **/
public class CourierConstants {
    /** 手机号码是否唯一的返回结果 */
    public final static String USER_PHONE_UNIQUE = "0";
    public final static String USER_PHONE_NOT_UNIQUE = "1";
}
