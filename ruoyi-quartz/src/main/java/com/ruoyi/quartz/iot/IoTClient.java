package com.ruoyi.quartz.iot;

import com.alibaba.fastjson.JSONObject;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.quartz.util.AliyunIoTSignUtil;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.poi.ss.formula.functions.T;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName IoTClient
 * @Description TODO
 * @Author lzjian
 * @Date 2019/2/14 0014 16:31
 **/
public class IoTClient {

    public static String productKey = "a1Zi7VWCj6q";
    public static String deviceName = "SleepMonitoring";
    public static String deviceSecret = "D3v39Tai9QXiIycNHkPsjOFEl8SqVn6U";
    public static String regionId = "cn-shanghai";

    //物模型-属性上报topic
    private static String pubTopic = "/sys/" + productKey + "/" + deviceName + "/thing/event/property/post";
    //高级版 物模型-属性上报payload
    private static final String payloadJson =
            "{" +
                    "    \"id\": %s," +
                    "    \"params\": {" +
                    "        \"respiratoryRate\": %s," +
                    "        \"heartRate\": %s," +
                    "        \"turnOverRate\": %s," +
                    "        \"bodyMotionRate\": %s," +
                    "        \"deviceID\": %s," +
                    "        \"address\": %s," +
                    "        \"inBed\": %s," +
                    "        \"offBed\": %s," +
                    "        \"signalIntensity\": %s," +
                    "        \"longitude\": %s," +
                    "        \"latitude\": %s" +
//                    "        \"leaveTheBedRate\": %s" +
                    "    }," +
                    "    \"method\": \"thing.event.property.post\"" +
                    "}";

    private static MqttClient mqttClient;
    private static Random random = new Random();
    private static DecimalFormat dfDecimal = new DecimalFormat("0.#");
    private static DecimalFormat dfInteger = new DecimalFormat("0");

    public static void startService() {

        try {
            initAliyunIoTClient();
            ScheduledExecutorService scheduledThreadPool = new ScheduledThreadPoolExecutor(1, new ThreadFactoryBuilder().setNameFormat("thread-runner-%d").build());
            scheduledThreadPool.scheduleAtFixedRate(() -> postDeviceProperties(), 10, 10, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void initAliyunIoTClient() {

        try {
            String clientId = "java" + System.currentTimeMillis();

            Map<String, String> params = new HashMap<>(16);
            params.put("productKey", productKey);
            params.put("deviceName", deviceName);
            params.put("clientId", clientId);
            String timestamp = String.valueOf(System.currentTimeMillis());
            params.put("timestamp", timestamp);

            // cn-shanghai
            String targetServer = "tcp://" + productKey + ".iot-as-mqtt." + regionId + ".aliyuncs.com:1883";

            String mqttclientId = clientId + "|securemode=3,signmethod=hmacsha1,timestamp=" + timestamp + "|";
            String mqttUsername = deviceName + "&" + productKey;
            String mqttPassword = AliyunIoTSignUtil.sign(params, deviceSecret, "hmacsha1");

            connectMqtt(targetServer, mqttclientId, mqttUsername, mqttPassword);

        } catch (Exception e) {
            System.out.println("initAliyunIoTClient error " + e.getMessage());
        }
    }

    public static void connectMqtt(String url, String clientId, String mqttUsername, String mqttPassword) throws Exception {

        MemoryPersistence persistence = new MemoryPersistence();
        mqttClient = new MqttClient(url, clientId, persistence);
        MqttConnectOptions connOpts = new MqttConnectOptions();
        // MQTT 3.1.1
        connOpts.setMqttVersion(4);
        connOpts.setAutomaticReconnect(false);
        connOpts.setCleanSession(true);

        connOpts.setUserName(mqttUsername);
        connOpts.setPassword(mqttPassword.toCharArray());
        connOpts.setKeepAliveInterval(60);

        mqttClient.connect(connOpts);

    }

    public static void postDeviceProperties() {

        try {
            /**1.登录认证*/
            /**登录认证请求地址*/
            String loginPath = "http://101.132.191.208:8080/ECSServer/userws/userAuthenticate.json";
            /**创建一个提交数据的容器*/
            List<BasicNameValuePair> loginParames = new ArrayList<>();
            loginParames.add(new BasicNameValuePair("token", "35321128"));
            loginParames.add(new BasicNameValuePair("userName", "e41bd85e86d96f88"));
            loginParames.add(new BasicNameValuePair("encryptedName", "BjODqmK41cvBJVj1FgBKUg=="));
            loginParames.add(new BasicNameValuePair("encryptedPwd", "J3uXwZDT3FmbWFht5UvnkQ=="));
            loginParames.add(new BasicNameValuePair("userType", "eihoo"));
            /**发送请求*/
            HttpResponse loginResponse = sendPost(null, loginPath, loginParames);
            /**获取响应实体*/
            HttpEntity httpEntity = loginResponse.getEntity();
            /**处理响应实体数据*/
            HttpResponse equipDataresponse = null;
            HttpResponse carePeopleResponse = null;
            String sessionId = null;
            if (httpEntity != null) {
                JSONObject loginCertificate = readContent(httpEntity.getContent());
                sessionId = loginCertificate.get("sessionId").toString();
                // System.err.println("sessionId = " + sessionId);// 用户sessionId

                /**2.获取设备最新当前状态的数据*/
                /**获取设备最新状态数据请求地址*/
                String getEquipDataPath = "http://101.132.191.208:8080/ECSServer/devicews/getLatestSmbPerMinuteRecord.json";
                //创建一个提交数据的容器
                List<BasicNameValuePair> equipDataParames = new ArrayList<>();

                equipDataParames.add(new BasicNameValuePair("sessionId", sessionId));
                equipDataParames.add(new BasicNameValuePair("devId", "A11E0036"));
                equipDataresponse = sendPost(sessionId, getEquipDataPath, equipDataParames);


            }
            /**3.获取所有关心人的列表*/
            /**获取所有关心人的列表请求地址*/
            String carePeoplePath = "http://101.132.191.208:8080/ECSServer/userws/getConcernPerson.json";
            /**创建一个提交数据的容器*/
            List<BasicNameValuePair> carePeopleParames = new ArrayList<>();
            carePeopleParames.add(new BasicNameValuePair("sessionId", sessionId));
            carePeopleResponse = sendPost(null, carePeoplePath, carePeopleParames);

            /**获取设备当前最新的状态，并处理数据*/
            HttpEntity equipDataresponseEntity = equipDataresponse.getEntity();
            HttpEntity carePeopleResponseEntity = carePeopleResponse.getEntity();
            String address = null;
            if (equipDataresponseEntity != null) {
                if (carePeopleResponseEntity != null) {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(carePeopleResponseEntity.getContent(), "UTF-8"), 8 * 1024);
                    String line = null;
                    StringBuilder entityStringBuilder = new StringBuilder();
                    while ((line = bufferedReader.readLine()) != null) {
                        entityStringBuilder.append(line);
                    }
                    String s = entityStringBuilder.toString();
                    JSONObject jsonObject = JSONObject.parseObject(s);
                    Object retValue = jsonObject.get("retValue");
                    List t = (List) retValue;
                    for (Object o : t) {
                        JSONObject jsonObject1 = JSONObject.parseObject(o.toString());
                        Object cid = jsonObject1.get("cid");
                        if ("201902201324180000010493377402".equals(cid)) {
                            address = String.valueOf(jsonObject1.get("address"));
                        }
                    }

                }
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(equipDataresponseEntity.getContent(), "UTF-8"), 8 * 1024);
                String line = null;
                StringBuilder entityStringBuilder = new StringBuilder();
                while ((line = bufferedReader.readLine()) != null) {
                    entityStringBuilder.append(line);
                }
                String s = entityStringBuilder.toString();
                JSONObject jsonObject = JSONObject.parseObject(s);
                /**上报数据*/
                String heartRateValue = "\"" + jsonObject.get("heartRateCount") + " 次/分钟\"";// 心率
                String respiratoryRateValue = "\"" + jsonObject.get("breathRateCount") + " 次/分钟\"";// 呼吸率
                String turnOverRateValue = "\"" + jsonObject.get("turnOverCount") + " 次/天\"";// 翻身次数
                String bodyMotionRateValue = "\"" + jsonObject.get("bodyMoveCount") + " 次/天\"";// 体动次数

                String deviceValue = String.valueOf(jsonObject.get("devId"));// 设备类型
                String addressValue = address;// 安装地址
                String inBed = "\"" + jsonObject.get("bodyMoveCount") + " (true=在床)\"";// 是否在床
                String offBed = "\"" + jsonObject.get("bodyMoveCount") + " (true=离床)\"";// 是否离床
                String signalIntensity = String.valueOf(jsonObject.get("bodyMoveCount"));// 信号强度
                String longitude = "\"" + "121.9095557014" + "\"";//经度
                String latitude = "\"" + "30.8842672633" + "\"";//纬度

//                String heartRateValue = "\"" + dfInteger.format(50 + random.nextFloat() * 30) + " 次/分钟\"";// 心率
//                String respiratoryRateValue = "\"" + dfInteger.format(10 + random.nextFloat() * 10) + " 次/分钟\"";// 呼吸率
//                String turnOverRateValue = "\"" + dfInteger.format(20 + random.nextFloat() * 20) + " 次/天\"";// 翻身次数
//                String bodyMotionRateValue = "\"" + dfInteger.format(200 + random.nextFloat() * 30) + " 次/天\"";// 体动次数
//                String leaveTheBedRateValue = "\"" + dfInteger.format(random.nextFloat() * 10) + " 次/天\"";// 离床次数

                String payload = String.format(payloadJson, System.currentTimeMillis(), respiratoryRateValue, heartRateValue, turnOverRateValue, bodyMotionRateValue, deviceValue, addressValue, inBed, offBed, signalIntensity, longitude, latitude);

                // System.out.println("post :" + payload);

                MqttMessage message = new MqttMessage(payload.getBytes("utf-8"));
                message.setQos(1);

                mqttClient.publish(pubTopic, message);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 发送post请求
     */
    public static HttpResponse sendPost(String sessionId, String path, List<BasicNameValuePair> parames) throws IOException {
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(path);
        //封装容器到请求参数中
        HttpEntity entity = new UrlEncodedFormEntity(parames);
        //设置请求参数到post请求中
        httpPost.setEntity(entity);
        //执行post请求
        BufferedReader bufferedReader;
        HttpResponse response = client.execute(httpPost);
        return response;
    }

    /**
     * 读取请求获取响应，获取数据
     */
    public static JSONObject readContent(InputStream content) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(content, "UTF-8"), 8 * 1024);
        String line = null;
        StringBuilder entityStringBuilder = new StringBuilder();
        while ((line = bufferedReader.readLine()) != null) {
            entityStringBuilder.append(line);
        }
        String s = entityStringBuilder.toString();
        JSONObject jsonObject = JSONObject.parseObject(JSONObject.parseObject(s).get("retValue").toString());
        return jsonObject;
    }
}
