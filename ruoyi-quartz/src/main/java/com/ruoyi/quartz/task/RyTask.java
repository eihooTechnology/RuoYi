package com.ruoyi.quartz.task;

import com.ruoyi.quartz.iot.IoTClient;
import com.ruoyi.quartz.util.Encryption;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 定时任务调度测试
 *
 * @author ruoyi
 */
@Component("ryTask")
public class RyTask {

    public void ryParams(String params) {
        System.out.println("执行有参方法：" + params);
    }

//    public void ryNoParams() {
//        System.out.println("执行无参方法");
//    }

    /**
     * 生成每日送餐订单的定时任务
     * 每天凌晨两点执行
     *
     * @throws Exception
     */
    public void addTodayOrders() throws Exception {
        System.err.println("执行定时任务【生成每日送餐订单】");

        String path = "http://127.0.0.1:8081/system/task/addTodayOrdersApi";

        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(path);
        //创建一个提交数据的容器
        List<BasicNameValuePair> parames = new ArrayList<>();

        Date date = new Date();
        String nowDate = String.valueOf(date.getTime());
//        System.err.println("nowDate=" + nowDate);
        String md5 = Encryption.encrypt(nowDate);
//        Thread.sleep(10000);
        parames.add(new BasicNameValuePair("timeStamp", nowDate));
        parames.add(new BasicNameValuePair("MD5", md5));

        //封装容器到请求参数中
        HttpEntity entity = new UrlEncodedFormEntity(parames);
        //设置请求参数到post请求中
        httpPost.setEntity(entity);

        //执行post请求
        HttpResponse response = client.execute(httpPost);

        System.err.println("status=" + response.getStatusLine().getStatusCode());
    }

    /**
     * 生成今日待回收订单的定时任务
     * 每天凌晨三点执行
     *
     * @throws Exception
     */
    public void updateRecoverOrders() throws Exception {
        System.err.println("执行定时任务【今日待回收订单】");

        String path = "http://127.0.0.1:8081/system/task/updateRecoverOrdersApi";

        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(path);
        //创建一个提交数据的容器
        List<BasicNameValuePair> parames = new ArrayList<>();

        Date date = new Date();
        String nowDate = String.valueOf(date.getTime());
//        System.err.println("nowDate=" + nowDate);
        String md5 = Encryption.encrypt(nowDate);
//        Thread.sleep(10000);
        parames.add(new BasicNameValuePair("timeStamp", nowDate));
        parames.add(new BasicNameValuePair("MD5", md5));

        //封装容器到请求参数中
        HttpEntity entity = new UrlEncodedFormEntity(parames);
        //设置请求参数到post请求中
        httpPost.setEntity(entity);

        //执行post请求
        HttpResponse response = client.execute(httpPost);

        System.err.println("status=" + response.getStatusLine().getStatusCode());
    }

    public void updateOrderAndOldManInfo() throws Exception {
        System.out.println("执行定时任务【每月底晚上清空老人表中当月的旧送餐日期】");

        String path = "http://127.0.0.1:8081/system/oldMan/updateOrderAndOldManInfoApi";

        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(path);
        //创建一个提交数据的容器
        List<BasicNameValuePair> parames = new ArrayList<>();

        Date date = new Date();
        String nowDate = String.valueOf(date.getTime());
//        System.err.println("nowDate=" + nowDate);
        String md5 = Encryption.encrypt(nowDate);
//        Thread.sleep(10000);
        parames.add(new BasicNameValuePair("timeStamp", nowDate));
        parames.add(new BasicNameValuePair("MD5", md5));

        //封装容器到请求参数中
        HttpEntity entity = new UrlEncodedFormEntity(parames);
        //设置请求参数到post请求中
        httpPost.setEntity(entity);

        //执行post请求
        HttpResponse response = client.execute(httpPost);

        System.err.println("status=" + response.getStatusLine().getStatusCode());
    }

    public void useOldAddTodayOrders() throws Exception {
        System.err.println("定时任务");
        // interfaceUtil("http://localhost:8080/a/sendfood/orderDistribution/saveTodayInterface", "");
        String path = "http://47.100.232.241:8980/a/sendfood/orderDistribution/saveTodayInterface";
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(path);
        //创建一个提交数据的容器
        List<BasicNameValuePair> parames = new ArrayList<>();

        Date date = new Date();
        String nowDate = String.valueOf(date.getTime());
//        System.err.println("nowDate=" + nowDate);
        String md5 = Encryption.encrypt(nowDate);

        parames.add(new BasicNameValuePair("timeStamp", nowDate));
        parames.add(new BasicNameValuePair("MD5", md5));

        //封装容器到请求参数中
        HttpEntity entity = new UrlEncodedFormEntity(parames);
        //设置请求参数到post请求中
        httpPost.setEntity(entity);

        //执行post请求
        HttpResponse response = client.execute(httpPost);

        System.err.println("status=" + response.getStatusLine().getStatusCode());
    }

    public void generateTorromowOrdersPreview() throws Exception {
        System.err.println("定时任务");
        // interfaceUtil("http://localhost:8080/a/sendfood/orderDistribution/saveTodayInterface", "");
        String path = "http://127.0.0.1:8081/system/task/generateTorromowOrdersPreviewApi";
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(path);
        //创建一个提交数据的容器
        List<BasicNameValuePair> parames = new ArrayList<>();

        Date date = new Date();
        String nowDate = String.valueOf(date.getTime());
//        System.err.println("nowDate=" + nowDate);
        String md5 = Encryption.encrypt(nowDate);

        parames.add(new BasicNameValuePair("timeStamp", nowDate));
        parames.add(new BasicNameValuePair("MD5", md5));

        //封装容器到请求参数中
        HttpEntity entity = new UrlEncodedFormEntity(parames);
        //设置请求参数到post请求中
        httpPost.setEntity(entity);

        //执行post请求
        HttpResponse response = client.execute(httpPost);

        System.err.println("status=" + response.getStatusLine().getStatusCode());
    }

}
