package com.ruoyi.quartz.util;

import com.mysql.jdbc.Statement;
import com.ruoyi.system.domain.WsCommunityEvent;

import java.sql.*;

/**
 * @ClassName DBHelper
 * @Description TODO
 * @Author lzjian
 * @Date 2019-03-05 11:35
 **/
public class DBHelper {

    // 获取连接
    private static Connection getConn(String ipAdd, String dbName, String uname, String pwd) {
        String driver = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://" + ipAdd + ":3306/" + dbName + "?characterEncoding=utf8&useSSL=false";
        String username = uname;
        String password = pwd;
        Connection conn = null;
        try {
            Class.forName(driver); // classLoader,加载对应驱动
            conn = (Connection) DriverManager.getConnection(url, username, password);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    // 增
    public static long DBinsert(WsCommunityEvent ws, String ipAdd, String dbName, String uname, String pwd) {
        PreparedStatement pstmt = null;
        Connection conn = getConn(ipAdd, dbName, uname, pwd);
        long i = 0;
        String sql = "insert into t_community_event(id,types,device_id,device_type,event_name,address,type_name,add_time,update_time,level,status)values(" + ws.getId() + "," + ws.getTypes() + ",'" + ws.getDeviceId() + "'," + ws.getDeviceType() + ",'" + ws.getEventName() + "','" + ws.getAddress() + "','" + ws.getTypeName() + "'," + ws.getAddTime() + "," + ws.getUpdateTime() + ",'" + ws.getLevel() + "'," + ws.getStatus() + ");";
        try {
            pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pstmt.executeUpdate();
            ResultSet generatedKeys = pstmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                i = generatedKeys.getLong(1);
            }
            // System.err.println("id = " + i);
            pstmt.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return i;
    }

    //查询已制作种类标签的数量
//    public static int DBselectType(int typeID,String ipAdd) {
//        int i = 0;
//        Connection conn = getConn(ipAdd);
//        String sql = "SELECT COUNT(*) AS rowCount FROM tbl_rfid_info WHERE RFID_TYPE = "+typeID+" and RFID_EPC is not null;";
//        PreparedStatement pstmt;
//        try {
//            pstmt = (PreparedStatement) conn.prepareStatement(sql);
//            ResultSet rs = pstmt.executeQuery();
//            rs.next();
//            i = rs.getInt("rowCount");
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return i;
//    }

    // 查id
//    public static int DBselectid(String TID,String ipAdd) {
//        int i = 0;
//        Connection conn = getConn(ipAdd);
//        String sql = "SELECT * FROM tbl_rfid_info WHERE RFID_ID = '"+TID+"'";
//        PreparedStatement pstmt;
//        try {
//            pstmt = (PreparedStatement) conn.prepareStatement(sql);
//            ResultSet rs = pstmt.executeQuery();
//            while (rs.next()) {
//                i = rs.getInt("ID");
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return i;
//    }

    // 改
//    public static int DBupdate(TBLRFIDINFO tblinfo,String ipAdd) {
//        int i = 0;
//        Connection conn = getConn(ipAdd);
//        String sql = "UPDATE tbl_rfid_info SET RFID_EPC = '" + tblinfo.getRFID_EPC() + "' WHERE RFID_ID = '"
//                + tblinfo.getRFID_ID() + "'";
//        PreparedStatement pstmt;
//        try {
//            pstmt = (PreparedStatement) conn.prepareStatement(sql);
//            i = pstmt.executeUpdate();
//            // System.out.println("resutl: " + i);
//            pstmt.close();
//            conn.close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return i;
//    }

    //查询后两百条数据
//    public static List<TBLRFIDINFO> DBselectDesc(int typeID,String ipAdd) {
//        List<TBLRFIDINFO> rfidlist = new ArrayList<>();
//        TBLRFIDINFO rfid = null;
//        Connection conn = getConn(ipAdd);
//        String sql = "SELECT * FROM tbl_rfid_info WHERE RFID_TYPE = "+typeID+" and RFID_EPC is not null ORDER BY id DESC LIMIT 25 ";
//        PreparedStatement pstmt;
//        try {
//            pstmt = (PreparedStatement) conn.prepareStatement(sql);
//            ResultSet rs = pstmt.executeQuery();
//            while (rs.next()) {
//                rfid = new TBLRFIDINFO();
//                rfid.setID(rs.getInt("ID"));
//                rfid.setRFID_ID(rs.getString("RFID_ID"));
//                rfid.setRFID_EPC(rs.getString("RFID_EPC"));
//                rfidlist.add(rfid);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return rfidlist;
//    }

}
