package com.ruoyi.quartz.util;

import com.alibaba.fastjson.JSON;
import com.ruoyi.system.domain.WsCommunityEvent;
import com.ruoyi.system.service.IWsCommunityEventService;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.Properties;

/**
 * @ClassName SimpleKafkaConsumer
 * @Description TODO
 * @Author lzjian
 * @Date 2019-03-04 14:33
 **/

public class SimpleKafkaConsumer {

    @Autowired
    private static IWsCommunityEventService wsCommunityEventService;

    private static KafkaConsumer<String, String> consumer;
    private final static String TOPIC = "pt_aihuwang";
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public SimpleKafkaConsumer() {
        Properties props = new Properties();
        props.put("bootstrap.servers", "kafka.opg-iot.com:9092");
        //每个消费者分配独立的组号
        props.put("group.id", "consumer_aihuwang");
        //如果value合法，则自动提交偏移量
        props.put("enable.auto.commit", "true");
        //设置多久一次更新被消费消息的偏移量
        props.put("auto.commit.interval.ms", "1000");
        //设置会话响应的时间，超过这个时间kafka可以选择放弃消费或者消费下一条消息
        props.put("session.timeout.ms", "30000");
        //自动重置offset
        props.put("auto.offset.reset", "earliest");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        consumer = new KafkaConsumer<String, String>(props);
    }

    public void consume() {
        consumer.subscribe(Arrays.asList(TOPIC));
        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(100);
            for (ConsumerRecord<String, String> record : records) {
                System.err.printf("offset = %d, key = %s, value = %s", record.offset(), record.key(), record.value());
                System.out.println();
                WsCommunityEvent ws = new WsCommunityEvent();
                try {
                    // 将获取到的JSON转换为map
                    Map postMap = JSON.parseObject(record.value(), Map.class);
                    String dateJson = postMap.get("data") == null ? null : postMap.get("data").toString();

                    Map jsonMap = JSON.parseObject(dateJson, Map.class);
                    String id = jsonMap.get("recordId") == null ? null : jsonMap.get("recordId").toString();
                    String types = jsonMap.get("priority") == null ? null : jsonMap.get("priority").toString();
                    String deviceId = jsonMap.get("deviceId") == null ? null : jsonMap.get("deviceId").toString();
                    String deviceType = jsonMap.get("deviceType") == null ? null : jsonMap.get("deviceType").toString();
                    String eventName = jsonMap.get("eventName") == null ? null : jsonMap.get("eventName").toString();
                    String address = jsonMap.get("address") == null ? null : jsonMap.get("address").toString();
                    String typeName = jsonMap.get("typeName") == null ? null : jsonMap.get("typeName").toString();
                    // 将日期转换为Unix时间戳
                    Date date = null;
                    date = sdf.parse(jsonMap.get("beginTime") == null ? null : jsonMap.get("beginTime").toString());
                    String timeAdd = String.valueOf(date.getTime()).substring(0,10);
                    System.out.println("timeAdd = " + timeAdd);
                    long uTime = 0;
                    // 将日期转换为Unix时间戳
                    Date date2 = null;
                    date2 = sdf.parse(jsonMap.get("updateTime") == null ? null : jsonMap.get("updateTime").toString());
                    String timeUpdate = String.valueOf(date2.getTime()).substring(0,10);
                    System.out.println("timeUpdate = " + timeUpdate);
                    String level = jsonMap.get("priorityName") == null ? null : jsonMap.get("priorityName").toString();
                    String status = jsonMap.get("status") == null ? null : jsonMap.get("status").toString();

                    ws.setId(Long.valueOf(id));
                    ws.setTypes(Integer.valueOf(types));
                    ws.setDeviceId(deviceId);
                    ws.setDeviceType(Integer.valueOf(deviceType));
                    ws.setEventName(eventName);
                    ws.setAddress(address);
                    ws.setTypeName(typeName);
                    ws.setAddTime(Integer.valueOf(timeAdd.substring(0,10)));
                    ws.setUpdateTime(Integer.valueOf(timeUpdate.substring(0,10)));
                    ws.setLevel(level);
                    ws.setStatus(Integer.valueOf(status));
                    System.err.println("【Kafka插入数据库】");
//                    System.err.println("WsCommunityEvent = " + ws.toString());
//                    wsCommunityEventService.saveWsInfo(ws);
                    DBHelper.DBinsert(ws,"47.101.165.88","service","ehuser","z001w6xx");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

//    public static void main(String[] args) {
//        new SimpleKafkaConsumer().consume();
//    }

}
