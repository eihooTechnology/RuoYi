package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.base.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.framework.web.base.BaseController;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.quartz.util.Encryption;
import com.ruoyi.system.domain.SysCourier;
import com.ruoyi.system.domain.SysFoodPackage;
import com.ruoyi.system.service.ISysFoodPackageService;
import com.ruoyi.system.service.ISysPostService;
import com.ruoyi.system.service.ISysRoleService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName SysFoodPackageController
 * @Description 套餐管理调用层
 * @Author JiaoSimao
 * @Date 2019/1/4 0004 15:56
 **/
@Controller
@RequestMapping("/system/foodPackage")
public class SysFoodPackageController extends BaseController {
    private String prefix = "system/foodPackage";

    @Autowired
    private ISysFoodPackageService foodPackageService;

    @Autowired
    private ISysRoleService roleService;

    @Autowired
    private ISysPostService postService;

    /**
     * 点击套餐管理，跳转到套餐管理页面
     * @return
     */
    @RequiresPermissions("system:foodPackage:view")
    @GetMapping()
    public String foodPackage() {
        return prefix + "/foodPackage";
    }

    /**
     * 分页查询套餐信息
     * @param sysFoodPackage
     * @return
     */
    @RequiresPermissions("system:foodPackage:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysFoodPackage sysFoodPackage) {
        startPage();
        if (StringUtils.isBlank(sysFoodPackage.getStatus())) {
            sysFoodPackage.setStatus("0");
        }
        List<SysFoodPackage> list = foodPackageService.selectFoodPackageList(sysFoodPackage);
        return getDataTable(list);
    }

    /**
     * 点击新增跳转至填写套餐信息
     */
    @GetMapping("/add")
    public String add(ModelMap mmap) {
        mmap.put("roles", roleService.selectRoleAll());
        mmap.put("posts", postService.selectPostAll());
        return prefix + "/add";
    }


    /**
     * 校验套餐名是否重复
     * @param sysFoodPackage
     * @return
     */
    @PostMapping("/checkFoodPackageNameUnique")
    @ResponseBody
    public String checkFoodPackageNameUnique(SysFoodPackage sysFoodPackage) {
        return foodPackageService.checkFoodPackageNameUnique(sysFoodPackage);
    }

    /**
     * 填写套餐信息后，新增套餐
     * @param sysFoodPackage
     * @return
     */
    @RequiresPermissions("system:foodPackage:add")
    @Log(title = "套餐管理", businessType = BusinessType.INSERT)
    @PostMapping("/addFoodPackage")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public AjaxResult addSave(SysFoodPackage sysFoodPackage) {
        sysFoodPackage.setCreateBy(ShiroUtils.getLoginName());
        return toAjax(foodPackageService.saveFoodPackage(sysFoodPackage));
    }


    /**
     * 修改套餐 跳转修改页面
     * @param id
     * @param mmap
     * @return
     */
    @GetMapping("/edit/{id}")
    public String  edit(@PathVariable("id") Long id, ModelMap mmap) {
        System.out.println("id = " + id);
        mmap.put("foodPackage",foodPackageService.selectFoodPackageById(id));
        return prefix + "/edit";
    }

    /**
     * 修改套餐信息
     * @param sysFoodPackage
     * @return
     */
    @RequiresPermissions("system:foodPackage:edit")
    @Log(title = "套餐管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public AjaxResult editSave(SysFoodPackage sysFoodPackage) {
        sysFoodPackage.setUpdateBy(ShiroUtils.getLoginName());
        return toAjax(foodPackageService.updateFoodPackage(sysFoodPackage));
    }

    /**
     * 删除套餐
     * @param ids
     * @return
     */
    @RequiresPermissions("system:foodPackage:remove")
    @Log(title = "套餐管理", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        System.out.println("ids = " + ids);
        try {
            return toAjax(foodPackageService.deleteFoodPackageByIds(ids));
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }
}
