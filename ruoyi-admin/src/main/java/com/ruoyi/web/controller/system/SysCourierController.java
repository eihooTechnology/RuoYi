package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.base.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.Md5Utils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.framework.web.base.BaseController;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.quartz.util.Encryption;
import com.ruoyi.system.domain.SysCommunity;
import com.ruoyi.system.domain.SysCourier;
import com.ruoyi.system.domain.SysOldman;
import com.ruoyi.system.service.ISysCommunityService;
import com.ruoyi.system.service.ISysCourierService;
import com.ruoyi.system.service.ISysPostService;
import com.ruoyi.system.service.ISysRoleService;
import com.ruoyi.web.controller.tool.CommunityGetKeyIDs;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName SysCourierController
 * @Description 骑手管理调用层
 * @Author JiaoSimao
 * @Date 2018/12/18 0018 12:38
 **/
@Controller
@RequestMapping("/system/courier")
public class SysCourierController extends BaseController {
    private String prefix = "system/courier";

    @Autowired
    private ISysCourierService courierService;

    @Autowired
    private ISysRoleService roleService;

    @Autowired
    private ISysPostService postService;

    @Autowired
    private ISysCommunityService communityService;
    /**
     * 点击骑手管理，跳转到骑手管理页面
     * @return
     */
    @RequiresPermissions("system:courier:view")
    @GetMapping()
    public String user() {
        return prefix + "/courier";
    }

    /**
     * 分页查询骑手信息
     * @param sysCourier
     * @return
     */
    @RequiresPermissions("system:courier:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysCourier sysCourier) {
        //数据查看范围权限
        SysCommunity s = new SysCommunity();
        s.setStatus("0");
        List<SysCommunity> communityList = communityService.selectCommunityList(s);
        CommunityGetKeyIDs getKeyIDs = new CommunityGetKeyIDs();
        sysCourier.setKeyIDs(getKeyIDs.communityGetKeyIDs(communityList));
        startPage();
        if (StringUtils.isBlank(sysCourier.getStatus())) {
            sysCourier.setStatus("0");
        }
        List<SysCourier> list = courierService.selectCourierList(sysCourier);
        return getDataTable(list);
    }


    /**
     * 点击新增跳转至填写骑手信息
     */
    @GetMapping("/add")
    public String add(ModelMap mmap) {
        mmap.put("roles", roleService.selectRoleAll());
        mmap.put("posts", postService.selectPostAll());
        return prefix + "/add";
    }


    /**
     * 校验骑手是否重复
     * @param sysCourier
     * @return
     */
    @PostMapping("/checkPhoneUnique")
    @ResponseBody
    public String checkPhoneUnique(SysCourier sysCourier) {
        return courierService.checkPhoneUnique(sysCourier);
    }

    /**
     * 填写骑手信息后，新增骑手
     * @param sysCourier
     * @return
     */
    @RequiresPermissions("system:oldMan:add")
    @Log(title = "老人管理", businessType = BusinessType.INSERT)
    @PostMapping("/addCourier")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public AjaxResult addSave(SysCourier sysCourier) {
        sysCourier.setCreateBy(ShiroUtils.getLoginName());
        sysCourier.setPassword(Encryption.encrypt(sysCourier.getPassword()));
        return toAjax(courierService.saveCourier(sysCourier));
    }


    /**
     * 修改骑手 跳转修改页面
     * @param id
     * @param mmap
     * @return
     */
    @GetMapping("/edit/{id}")
    public String  edit(@PathVariable("id") Long id, ModelMap mmap) {
        System.out.println("id = " + id);
        mmap.put("courier",courierService.selectCourierById(id));
        return prefix + "/edit";
    }

    /**
     * 修改骑手信息
     * @param sysCourier
     * @return
     */
    @RequiresPermissions("system:courier:edit")
    @Log(title = "骑手管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public AjaxResult editSave(SysCourier sysCourier) {
        sysCourier.setUpdateBy(ShiroUtils.getLoginName());
        sysCourier.setPassword(Encryption.encrypt(sysCourier.getPassword()));
        return toAjax(courierService.updateCourier(sysCourier));
    }

    /**
     * 删除骑手
     * @param sendCourierId
     * @return
     */
    @RequiresPermissions("system:courier:remove")
    @Log(title = "骑手管理", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public String remove(Long sendCourierId) {
        System.err.println("sendCourierId = " + sendCourierId);
        int i = courierService.deleteCourierBySendCourierId(sendCourierId);
        if(i > 0){
            return "删除该骑手成功！";
        }else{
            return "系统错误，删除该骑手失败！";
        }
    }
}
