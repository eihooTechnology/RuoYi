package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.base.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.framework.web.base.BaseController;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.system.domain.SysCommunity;
import com.ruoyi.system.domain.SysCourier;
import com.ruoyi.system.domain.SysOldman;
import com.ruoyi.system.domain.SysOrder;
import com.ruoyi.system.service.*;
import com.ruoyi.web.controller.tool.CommunityGetKeyIDs;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 所有订单管理调用层
 *
 * @author jiaosimao
 */
@Controller
@RequestMapping("/system/allOrder")
public class SysOrderController extends BaseController {
    private String prefix = "system/allOrder";

    @Autowired
    private ISysOrderService orderService;

    @Autowired
    private ISysOrderPreviewService orderPreviewService;

    @Autowired
    private ISysCourierService courierService;

    @Autowired
    private ISysRoleService roleService;

    @Autowired
    private ISysPostService postService;

    @Autowired
    private ISysCommunityService communityService;

    /**
     * 点击所有订单管理页面
     * @return
     */
    @RequiresPermissions("system:allOrder:view")
    @GetMapping()
    public String order() {
        return prefix + "/allOrder";
    }

    /**
     * 分页查询所有订单列表
     * @param sysOrder
     * @return
     */
    @RequiresPermissions("system:allOrder:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysOrder sysOrder) {

        // 数据查看范围权限(要放在分页startPage();前面，否则只能查询出10个)
        SysCommunity s = new SysCommunity();
        s.setStatus("0");
        List<SysCommunity> communityList = communityService.selectCommunityList(s);
        CommunityGetKeyIDs getKeyIDs = new CommunityGetKeyIDs();
        sysOrder.setKeyIDs(getKeyIDs.communityGetKeyIDs(communityList));

        startPage();
        if (sysOrder.getStatus() == null) {
            sysOrder.setStatus(0);
        }
        List<SysOrder> list = orderService.selectOrderList(sysOrder);
        return getDataTable(list);
    }


    /**
     * 修改订单 弹出修改订单页面
     * @param id
     * @param mmap
     * @return
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        SysCourier courier = new SysCourier();
        mmap.put("courierList", courierService.selectCourierList(courier));
        mmap.put("todayOrder",orderService.selectOrderById(id));
        return prefix + "/todayEdit";
    }

    /**
     * 修改订单状态
     * @param sysOrder
     * @return
     */
    @RequiresPermissions("system:allOrder:edit")
    @Log(title = "所有订单管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public AjaxResult editSave(SysOrder sysOrder) {
        sysOrder.setUpdateBy(ShiroUtils.getLoginName());
        return toAjax(orderService.updateOrder(sysOrder));
    }

    /**
     * 删除订单
     * @param ids
     * @return
     */
    @RequiresPermissions("system:allOrder:remove")
    @Log(title = "订单管理", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        System.out.println("ids = " + ids);
        try {
            return toAjax(orderService.deleteOrderByIds(ids));
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }

    /*****************************************************************************/

    /**
     * 点击今日订单管理页面
     * @return
     */
    @RequiresPermissions("system:todayOrder:view")
    @GetMapping("/today")
    public String todayOrder() {
        return prefix + "/todayOrder";
    }


    /**
     * 分页查询今日订单列表
     * @param sysOrder
     * @return
     */
    @RequiresPermissions("system:todayOrder:list")
    @PostMapping("/today/list")
    @ResponseBody
    public TableDataInfo todayList(SysOrder sysOrder) {

        // 数据查看范围权限(要放在分页startPage();前面，否则只能查询出10个)
        SysCommunity s = new SysCommunity();
        s.setStatus("0");
        List<SysCommunity> communityList = communityService.selectCommunityList(s);
        CommunityGetKeyIDs getKeyIDs = new CommunityGetKeyIDs();
        sysOrder.setKeyIDs(getKeyIDs.communityGetKeyIDs(communityList));

        startPage();
        if (sysOrder.getStatus() == null) {
            sysOrder.setStatus(0);
        }
        Map<String, Object> params = new HashMap<>();
        params.put("beginTime",getToday()+" 00:00:00");
        params.put("endTime",getToday()+" 23:59:59");
        sysOrder.setParams(params);
        List<SysOrder> list = orderService.selectTodayOrderList(sysOrder);
        return getDataTable(list);
    }


    /**
     * 修改今日订单 弹出修改订单页面
     * @param id
     * @param mmap
     * @return
     */
    @GetMapping("/today/edit/{id}")
    public String todayEdit(@PathVariable("id") Long id, ModelMap mmap) {
        SysCourier courier = new SysCourier();
        mmap.put("courierList", courierService.selectCourierList(courier));
        mmap.put("todayOrder",orderService.selectTodayOrderById(id));
        return prefix + "/todayEdit";
    }

    /**
     * 修改今日订单状态
     * @param sysOrder
     * @return
     */
    @RequiresPermissions("system:todayOrder:edit")
    @Log(title = "今日订单管理", businessType = BusinessType.UPDATE)
    @PostMapping("/today/edit")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public AjaxResult todayEditSave(SysOrder sysOrder) {
        sysOrder.setUpdateBy(ShiroUtils.getLoginName());
        return toAjax(orderService.updateTodayOrder(sysOrder));
    }

    /**
     * 删除今日订单
     * @param ids
     * @return
     */
    @RequiresPermissions("system:todayOrder:remove")
    @Log(title = "订单管理", businessType = BusinessType.DELETE)
    @PostMapping("/today/remove")
    @ResponseBody
    public AjaxResult todayRemove(String ids) {
        System.out.println("ids = " + ids);
        try {
            return toAjax(orderService.deleteTodayOrderByIds(ids));
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }

    /***************************************************************************************/

    /**
     * 点击明日订单预览页面
     * @return
     */
    @RequiresPermissions("system:tomorrowOrder:viewTomorrow")
    @GetMapping("/tomorrow")
    public String operTomorrowlog() {
        return prefix + "/TomorrowOrders";
    }

    /**
     * 分页查询所有订单列表
     * @param sysOrder
     * @return
     */
    @RequiresPermissions("system:tomorrowOrder:list")
    @PostMapping("/tomorrow/list")
    @ResponseBody
    public TableDataInfo tomorrowList(SysOrder sysOrder) {
        // 数据查看范围权限(要放在分页startPage();前面，否则只能查询出10个)
        SysCommunity s = new SysCommunity();
        s.setStatus("0");
        List<SysCommunity> communityList = communityService.selectCommunityList(s);
        CommunityGetKeyIDs getKeyIDs = new CommunityGetKeyIDs();
        sysOrder.setKeyIDs(getKeyIDs.communityGetKeyIDs(communityList));

        startPage();
        if (sysOrder.getStatus() == null) {
            sysOrder.setStatus(0);
        }
        List<SysOrder> list = orderPreviewService.selectOrderList(sysOrder);
        return getDataTable(list);
    }

    /******************************************************************************************/

    /**
     * 删除老人之前，检查是否存在当日订单
     * @param userCode
     * @return
     */
    @RequiresPermissions("system:allOrder:removeCheck")
    @Log(title = "订单管理", businessType = BusinessType.DELETE)
    @PostMapping("/removeCheck")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public String removeCheck(Long userCode) {
        SysOrder sysOrder = new SysOrder();
        Map<String, Object> params = new HashMap<>();
        params.put("beginTime",getToday()+" 00:00:00");
        params.put("endTime",getToday()+" 23:59:59");
        sysOrder.setParams(params);
        sysOrder.setUserCode(userCode);
        List<SysOrder> sysOrders = orderService.selectTodayOrderList(sysOrder);
        if(sysOrders.size() > 0){
            return "当前老人有今日订单，确定要删除订单吗？";
        }else{
            return "当前老人无今日订单，确定要删除老人吗？";
        }
    }

    /**
     * 删除老人之前，先删除当日订单
     * @param userCode
     * @return
     */
    @RequiresPermissions("system:allOrder:removeTodayOrder")
    @Log(title = "订单管理", businessType = BusinessType.DELETE)
    @PostMapping("/removeTodayOrder")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public String removeTodayOrder(Long userCode) {
        int i = orderService.deleteOrderByUserCode(userCode);
        if(i > 0){
            return "确定删除老人吗？";
        }else{
            return "系统错误，删除老人订单失败！";
        }
    }

    /******************************************************************************************/

    /**
     * 删除骑手之前，检查是否存在当日订单
     * @param sendCourierId
     * @return
     */
    @RequiresPermissions("system:courier:removeCheck")
    @Log(title = "订单管理", businessType = BusinessType.DELETE)
    @PostMapping("/removeCourierCheck")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public List<SysOrder> removeCourierCheck(Long sendCourierId) {
        SysOrder sysOrder = new SysOrder();
        Map<String, Object> params = new HashMap<>();
        params.put("beginTime",getToday()+" 00:00:00");
        params.put("endTime",getToday()+" 23:59:59");
        sysOrder.setParams(params);
        sysOrder.setSendCourierId(sendCourierId);
        List<SysOrder> sysOrders = orderService.selectTodayOrderList(sysOrder);
        if(sysOrders.size() > 0){
            return sysOrders;
//            return "当前骑手有今日订单，请将订单重新分配!";
        }else{
            return sysOrders;
//            return "当前骑手无今日订单，确定要删除骑手吗?";
        }
    }

    public String getToday() {
        Date date = new Date();//取时间
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(calendar.DATE, 0);
        date = calendar.getTime(); //这个时间就是日期往后推一天的结果
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(date);
        return dateString;
    }


}