package com.ruoyi.web.controller.tool;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.web.entity.huaweiface.*;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @ClassName FaceTool
 * @Description TODO
 * @Author lzjian
 * @Date 2019-04-22 14:11
 **/
public class FaceTool {

    /************************************************/
    private static String ak_id = "Z7B6ODTF2WBW6UNJ3PJO"; // 用户ak
    private static String ak_secret = "kkSqdiEOmhHOScC1uwMbzyfgxKDahXUqhonwu9BL"; // 用户ak_secret
    /************************************************/
    public static String rider_debug = "rider_debug";// 骑手测试人脸库
    // public static String rider_release = "rider_release";// 骑手正式人脸库
    public static String elderly_debug = "elderly_debug";// 老人测试人脸库
    // public static String elderly_release = "elderly_release";// 老人正式人脸库
    /************************************************/
    private static String errorKey413 = "413 Request Entity Too Large";// 图像过大错误(Body不能大于2M)

    public static void main(String[] args) {
        // 获取华为Token
        String token = getHuaWeiToken();
        if (token != null) {// 当获取Token成功时进入

            // 获取所有人脸库列表
            List<FaceSetsInfoBean> faceSetsInfoBeanList = findFaceSets(token, "");
            if (faceSetsInfoBeanList != null) {// 当人脸库对象不为空时进入
                for (FaceSetsInfoBean b : faceSetsInfoBeanList) {// 遍历所有人脸库列表
                    System.err.println("当前人脸库【" + b.getFace_set_name() + "】的当前人脸数量有" + b.getFace_number() + "个 \t 人脸库ID为:" + b.getFace_set_id());
                }
            }

            // 使用imgBase64进行人脸库搜索
            String jsonStr = JSON.toJSONString(new FaceSearchSendBean());
            // FacesBean facesBean = findFaceByBase64AndCheck(token, rider_debug, jsonStr);
            // System.err.println("当前人脸对比成功度为：" + facesBean.getSimilarity() + "\t 人脸ID为：" + facesBean.getFace_id() + "\t 人脸自定义ID为：" + facesBean.getExternal_image_id());

            // 使用imgBase64新增人脸至指定人脸库
            // AddFaceSuccessBean addSuccessBean = addFaceByBase64(token, rider_debug, jsonStr);
            // System.err.println(addSuccessBean);

            // 删除指定的人脸库中人脸的信息
            // DeleteFacesSuccessBean deleteSuccessBean = deleteFaceinfo(token, rider_debug, "666");
            // System.err.println(deleteSuccessBean);

            // 批量删除指定的人脸库中人脸的信息
            // DeleteFacesSuccessBean deleteByBatchSuccessBean = deleteFaceinfoByBatch(token, elderly_debug);
            // System.err.println(deleteByBatchSuccessBean);
        }
    }

    /**
     * 获取华为Token
     *
     * @return
     */
    public static String getHuaWeiToken() {
        // 发送POST请求
        String url = "https://iam.cn-north-1.myhuaweicloud.com/v3/auth/tokens";
        String body = JSON.toJSONString(new HuaWeiTokenReq());
        String token;
        try {
            token = sendPost(url, body);
            return token;
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            return null;
        }
    }

    /**
     * 使用图片的BASE64编码新增人脸至指定的人脸库中
     *
     * @param token
     * @param faceSetName
     * @param imgBase64
     * @return
     */
    public static AddFaceSuccessBean addFaceByBase64(String token, String faceSetName, String imgBase64) {

        String url = "https://face.cn-north-1.myhuaweicloud.com/v1/b4a88b0c68e04273a66c5dfab10edf67/face-sets/" + faceSetName + "/faces";

        try {
            System.out.println(url);

            URL realUrl = new URL(url);
            // 将url 以 open方法返回的urlConnection  连接强转为HttpURLConnection连接  (标识一个url所引用的远程对象连接)
            // 此时cnnection只是为一个连接对象,待连接中
            HttpURLConnection connection = (HttpURLConnection) realUrl.openConnection();
            // 设置连接输出流为true,默认false (post 请求是以流的方式隐式的传递参数)
            connection.setDoOutput(true);
            // 设置连接输入流为true
            connection.setDoInput(true);
            // 设置请求方式为post
            connection.setRequestMethod("POST");
            // post请求缓存设为false
            connection.setUseCaches(false);
            // 设置该HttpURLConnection实例是否自动执行重定向
            connection.setInstanceFollowRedirects(true);
            // 设置请求头里面的各个属性 (以下为设置内容的类型,设置为经过urlEncoded编码过的from参数)
            // application/x-javascript text/xml->xml数据 application/x-javascript->json对象 application/x-www-form-urlencoded->表单数据
            // ;charset=utf-8 必须要，不然妙兜那边会出现乱码【★★★★★】
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("X-Auth-Token", token);
            // 建立连接 (请求未开始,直到connection.getInputStream()方法调用时才发起,以上各个参数设置需在此方法之前进行)
            connection.connect();
            // 创建输入输出流,用于往连接里面输出携带的参数,(输出内容为?后面的内容)
            DataOutputStream dataout = new DataOutputStream(connection.getOutputStream());
            // System.out.println("传递参数：" + imgBase64);
            // 将参数输出到连接
            dataout.writeBytes(imgBase64);// 此处传递Body参数
            // 输出完成后刷新并关闭流
            dataout.flush();
            dataout.close(); // 重要且易忽略步骤 (关闭流,切记!)
            //System.out.println(connection.getResponseCode());
            // 连接发起请求,处理服务器响应  (从连接获取到输入流并包装为bufferedReader)
            BufferedReader bf = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
            String line;
            StringBuilder sb = new StringBuilder(); // 用来存储响应数据

            // 循环读取流,若不到结尾处
            while ((line = bf.readLine()) != null) {
                //sb.append(bf.readLine());
                sb.append(line).append(System.getProperty("line.separator"));
            }
            bf.close();    // 重要且易忽略步骤 (关闭流,切记!)
            connection.disconnect(); // 销毁连接
            // System.err.println(sb.toString());

            AddFaceSuccessBean successBean = JSON.parseObject(sb.toString(), AddFaceSuccessBean.class);

            return successBean;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * 删除指定的人脸库中人脸的信息
     *
     * @param token
     * @param faceSetName
     * @param imageID
     * @return
     */
    public static DeleteFacesSuccessBean deleteFaceinfo(String token, String faceSetName, String imageID) {

        String url = "https://face.cn-north-1.myhuaweicloud.com/v1/b4a88b0c68e04273a66c5dfab10edf67/face-sets/" + faceSetName + "/faces?external_image_id=" + imageID;

        try {
            System.out.println(url);

            URL realUrl = new URL(url);
            // 将url 以 open方法返回的urlConnection  连接强转为HttpURLConnection连接  (标识一个url所引用的远程对象连接)
            // 此时cnnection只是为一个连接对象,待连接中
            HttpURLConnection connection = (HttpURLConnection) realUrl.openConnection();
            // 设置连接输出流为true,默认false (post 请求是以流的方式隐式的传递参数)
            connection.setDoOutput(true);
            // 设置连接输入流为true
            connection.setDoInput(true);
            // 设置请求方式为post
            connection.setRequestMethod("DELETE");
            // post请求缓存设为false
            connection.setUseCaches(false);
            // 设置该HttpURLConnection实例是否自动执行重定向
            connection.setInstanceFollowRedirects(true);
            // 设置请求头里面的各个属性 (以下为设置内容的类型,设置为经过urlEncoded编码过的from参数)
            // application/x-javascript text/xml->xml数据 application/x-javascript->json对象 application/x-www-form-urlencoded->表单数据
            // ;charset=utf-8 必须要，不然妙兜那边会出现乱码【★★★★★】
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("X-Auth-Token", token);
            // 建立连接 (请求未开始,直到connection.getInputStream()方法调用时才发起,以上各个参数设置需在此方法之前进行)
            connection.connect();

            System.out.println(connection.getResponseCode());

            if (connection.getResponseCode() != 400) {
                // 连接发起请求,处理服务器响应  (从连接获取到输入流并包装为bufferedReader)
                BufferedReader bf = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
                String line;
                StringBuilder sb = new StringBuilder(); // 用来存储响应数据

                // 循环读取流,若不到结尾处
                while ((line = bf.readLine()) != null) {
                    //sb.append(bf.readLine());
                    sb.append(line).append(System.getProperty("line.separator"));
                }
                bf.close();    // 重要且易忽略步骤 (关闭流,切记!)
                connection.disconnect(); // 销毁连接
                System.err.println(sb.toString());

                DeleteFacesSuccessBean successBean = JSON.parseObject(sb.toString(), DeleteFacesSuccessBean.class);

                return successBean;
            } else {
                DeleteFacesSuccessBean successBean = new DeleteFacesSuccessBean();
                successBean.setFace_number(66666);
                return successBean;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * 批量删除指定的人脸库中人脸的信息
     *
     * @param token
     * @param faceSetName
     * @return
     */
    public static DeleteFacesSuccessBean deleteFaceinfoByBatch(String token, String faceSetName) {

        String url = "https://face.cn-north-1.myhuaweicloud.com/v1/b4a88b0c68e04273a66c5dfab10edf67/face-sets/" + faceSetName + "/faces/batch";

        try {
            System.out.println(url);

            URL realUrl = new URL(url);
            // 将url 以 open方法返回的urlConnection  连接强转为HttpURLConnection连接  (标识一个url所引用的远程对象连接)
            // 此时cnnection只是为一个连接对象,待连接中
            HttpURLConnection connection = (HttpURLConnection) realUrl.openConnection();
            // 设置连接输出流为true,默认false (post 请求是以流的方式隐式的传递参数)
            connection.setDoOutput(true);
            // 设置连接输入流为true
            connection.setDoInput(true);
            // 设置请求方式为post
            connection.setRequestMethod("DELETE");
            // post请求缓存设为false
            connection.setUseCaches(false);
            // 设置该HttpURLConnection实例是否自动执行重定向
            connection.setInstanceFollowRedirects(true);
            // 设置请求头里面的各个属性 (以下为设置内容的类型,设置为经过urlEncoded编码过的from参数)
            // application/x-javascript text/xml->xml数据 application/x-javascript->json对象 application/x-www-form-urlencoded->表单数据
            // ;charset=utf-8 必须要，不然妙兜那边会出现乱码【★★★★★】
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("X-Auth-Token", token);
            // 建立连接 (请求未开始,直到connection.getInputStream()方法调用时才发起,以上各个参数设置需在此方法之前进行)
            connection.connect();
            // 创建输入输出流,用于往连接里面输出携带的参数,(输出内容为?后面的内容)
            DataOutputStream dataout = new DataOutputStream(connection.getOutputStream());
            // System.out.println("传递参数：" + imgBase64);
            // 将参数输出到连接
            String jsonStr = "{\n" +
                    "    \"filter\": \"!external_image_id:0\"\n" +
                    "}";
            dataout.writeBytes(jsonStr);// 此处传递Body参数
            // 输出完成后刷新并关闭流
            dataout.flush();
            dataout.close(); // 重要且易忽略步骤 (关闭流,切记!)
            // System.out.println(connection.getResponseCode());
            // 连接发起请求,处理服务器响应  (从连接获取到输入流并包装为bufferedReader)
            BufferedReader bf = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
            String line;
            StringBuilder sb = new StringBuilder(); // 用来存储响应数据

            // 循环读取流,若不到结尾处
            while ((line = bf.readLine()) != null) {
                //sb.append(bf.readLine());
                sb.append(line).append(System.getProperty("line.separator"));
            }
            bf.close();    // 重要且易忽略步骤 (关闭流,切记!)
            connection.disconnect(); // 销毁连接
            // System.err.println(sb.toString());

            DeleteFacesSuccessBean successBean = JSON.parseObject(sb.toString(), DeleteFacesSuccessBean.class);

            return successBean;
        } catch (Exception e) {
            e.printStackTrace();
            DeleteFacesSuccessBean successBean = new DeleteFacesSuccessBean();
            successBean.setFace_number(0);
            successBean.setFace_set_id(null);
            successBean.setFace_set_name("faceSetName");
            return successBean;
        }

    }

    /**
     * 使用图片的BASE64编码查询人脸并对比
     *
     * @param token
     * @param faceSetName
     * @param imgBase64
     * @return
     */
    public static FacesBean findFaceByBase64AndCheck(String token, String faceSetName, String imgBase64) {

        String url = "https://face.cn-north-1.myhuaweicloud.com/v1/b4a88b0c68e04273a66c5dfab10edf67/face-sets/" + faceSetName + "/search";

        try {
            System.out.println(url);

            URL realUrl = new URL(url);
            // 将url 以 open方法返回的urlConnection  连接强转为HttpURLConnection连接  (标识一个url所引用的远程对象连接)
            // 此时cnnection只是为一个连接对象,待连接中
            HttpURLConnection connection = (HttpURLConnection) realUrl.openConnection();
            // 设置连接输出流为true,默认false (post 请求是以流的方式隐式的传递参数)
            connection.setDoOutput(true);
            // 设置连接输入流为true
            connection.setDoInput(true);
            // 设置请求方式为post
            connection.setRequestMethod("POST");
            // post请求缓存设为false
            connection.setUseCaches(false);
            // 设置该HttpURLConnection实例是否自动执行重定向
            connection.setInstanceFollowRedirects(true);
            // 设置请求头里面的各个属性 (以下为设置内容的类型,设置为经过urlEncoded编码过的from参数)
            // application/x-javascript text/xml->xml数据 application/x-javascript->json对象 application/x-www-form-urlencoded->表单数据
            // ;charset=utf-8 必须要，不然妙兜那边会出现乱码【★★★★★】
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("X-Auth-Token", token);
            // 建立连接 (请求未开始,直到connection.getInputStream()方法调用时才发起,以上各个参数设置需在此方法之前进行)
            connection.connect();
            // 创建输入输出流,用于往连接里面输出携带的参数,(输出内容为?后面的内容)
            DataOutputStream dataout = new DataOutputStream(connection.getOutputStream());
            // System.out.println("传递参数：" + imgBase64);
            // 将参数输出到连接
            dataout.writeBytes(imgBase64);// 此处传递Body参数
            // 输出完成后刷新并关闭流
            dataout.flush();
            dataout.close(); // 重要且易忽略步骤 (关闭流,切记!)
            //System.out.println(connection.getResponseCode());
            // 连接发起请求,处理服务器响应  (从连接获取到输入流并包装为bufferedReader)
            BufferedReader bf = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
            String line;
            StringBuilder sb = new StringBuilder(); // 用来存储响应数据

            // 循环读取流,若不到结尾处
            while ((line = bf.readLine()) != null) {
                //sb.append(bf.readLine());
                sb.append(line).append(System.getProperty("line.separator"));
            }
            bf.close();    // 重要且易忽略步骤 (关闭流,切记!)
            connection.disconnect(); // 销毁连接
            // System.err.println(sb.toString());

            GetFacesBean getFacesBean = JSON.parseObject(sb.toString(), GetFacesBean.class);

            if (getFacesBean.getFaces().size() > 1) {
                System.err.println("查询到相似的人脸有" + getFacesBean.getFaces().size() + "个");
                return getFacesBean.getFaces().get(0);
            } else {

                if (getFacesBean.getFaces().size() > 0) {
                    return getFacesBean.getFaces().get(0);
                } else {
                    return null;
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * 查询人脸库(若无faceSetName则查询所有)
     *
     * @param token
     * @param faceSetName
     * @return
     */
    public static List<FaceSetsInfoBean> findFaceSets(String token, String faceSetName) {

        String url = "https://face.cn-north-1.myhuaweicloud.com/v1/b4a88b0c68e04273a66c5dfab10edf67/face-sets";

        if (StringUtils.isNotBlank(faceSetName)) {
            url += "/" + faceSetName;
        }

        try {
            System.out.println(url);

            URL realUrl = new URL(url);    //把字符串转换为URL请求地址
            HttpURLConnection connection = (HttpURLConnection) realUrl.openConnection();// 打开连接
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("X-Auth-Token", token);
            connection.connect();// 连接会话
            // 获取输入流
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
            String line;
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {// 循环读取流
                sb.append(line);
            }

            // 将获取到的JSON转换为map
            Map jsonMap = JSON.parseObject(sb.toString(), Map.class);
            String face_set_info = jsonMap.get("face_set_info") == null ? null : jsonMap.get("face_set_info").toString();
            String face_sets_info = jsonMap.get("face_sets_info") == null ? null : jsonMap.get("face_sets_info").toString();

            List<FaceSetsInfoBean> faceSetsInfoBeanList = new ArrayList<>();

            if (StringUtils.isNotBlank(faceSetName)) {// 如果指定查询人脸库则进入

                FaceSetsInfoBean bean = JSON.parseObject(face_set_info, FaceSetsInfoBean.class);
                faceSetsInfoBeanList.add(bean);

                br.close();// 关闭流
                connection.disconnect();// 断开连接
                // System.err.println(sb.toString());
                return faceSetsInfoBeanList;

            } else {

                faceSetsInfoBeanList = JSONArray.parseArray(face_sets_info, FaceSetsInfoBean.class);

                br.close();// 关闭流
                connection.disconnect();// 断开连接
                // System.err.println(sb.toString());
                return faceSetsInfoBeanList;

            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    /********************************************************************************************/

    /**
     * 向指定 URL 发送POST方法的请求
     *
     * @param url   发送请求的 URL
     * @param param 请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return 所代表远程资源的响应结果
     */
    public static String sendPost(String url, String param) {
        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            // 发送请求参数
            out.print(param);
            // flush输出流的缓冲
            out.flush();

            // 定义BufferedReader输入流来读取URL的响应
//            in = new BufferedReader(
//                    new InputStreamReader(conn.getInputStream()));
//            String line;
//            while ((line = in.readLine()) != null) {
//                result += line;
//            }
//            System.err.println("result:" + result);

            // 获取到Token
            result = conn.getHeaderField("X-Subject-Token");
            // System.err.println(result);
            return result;
        } catch (Exception e) {
            System.out.println("发送 POST 请求出现异常！" + e);
            e.printStackTrace();
            return null;
        }
        //使用finally块来关闭输出流、输入流
        finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
                return null;
            }
        }

    }

}
