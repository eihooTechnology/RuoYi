package com.ruoyi.web.controller.system;

import java.text.SimpleDateFormat;
import java.util.*;

import com.ruoyi.system.domain.SysCommunity;
import com.ruoyi.system.domain.SysOrder;
import com.ruoyi.system.service.ISysCommunityService;
import com.ruoyi.system.service.ISysOrderService;
import com.ruoyi.web.controller.tool.CommunityGetKeyIDs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import com.ruoyi.common.config.Global;
import com.ruoyi.system.domain.SysMenu;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.ISysMenuService;
import com.ruoyi.framework.web.base.BaseController;

/**
 * 首页 业务处理
 *
 * @author ruoyi
 */
@Controller
public class SysIndexController extends BaseController {
    @Autowired
    private ISysMenuService menuService;

    @Autowired
    private ISysOrderService orderService;

    @Autowired
    private ISysCommunityService communityService;

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    // 系统首页
    @GetMapping("/index")
    public String index(ModelMap mmap) {
        // 取身份信息
        SysUser user = getSysUser();
        // 根据用户id取出菜单
        List<SysMenu> menus = menuService.selectMenusByUser(user);
        mmap.put("menus", menus);
        mmap.put("user", user);
        mmap.put("copyrightYear", Global.getCopyrightYear());
        return "index";
    }

    // 系统介绍
    @GetMapping("/system/main")
    public String main(ModelMap mmap) {
        // 数据查看范围权限(要放在分页startPage();前面，否则只能查询出10个)
        SysCommunity s = new SysCommunity();
        s.setStatus("0");
        List<SysCommunity> communityList = communityService.selectCommunityList(s);
        CommunityGetKeyIDs getKeyIDs = new CommunityGetKeyIDs();

        /***************************** 开始计算用餐人数和对比昨日人数 *****************************/
        Map<String, String> resultMap = new HashMap<>();
        List<SysOrder> todayOrderList = new ArrayList<>();
        List<SysOrder> yesterdayOrderList = new ArrayList<>();
        int todayCount1 = 0;// 今日早餐用餐人数
        int yesterdayCount1 = 0;// 昨日早餐用餐人数
        // int difference1 = 0;// 今日早餐对比昨日早餐的差值
        int todayCount2 = 0;// 今日午餐用餐人数
        int yesterdayCount2 = 0;// 昨日午餐用餐人数
        // int difference2 = 0;// 今日午餐对比昨日午餐的差值
        int todayCount3 = 0;// 今日晚餐用餐人数
        int yesterdayCount3 = 0;// 昨日晚餐用餐人数
        // int difference3 = 0;// 今日晚餐对比昨日晚餐的差值

        Map<String, Object> params = new HashMap<>();
        params.put("beginTime", getToday() + " 00:00:00");
        params.put("endTime", getToday() + " 23:59:59");
        SysOrder order = new SysOrder();
        order.setKeyIDs(getKeyIDs.communityGetKeyIDs(communityList));
        order.setStatus(0);
        order.setParams(params);
        todayOrderList = orderService.selectTodayOrderList(order);
        order.setParams(null);
        params.put("beginTime", getYesterday() + " 00:00:00");
        params.put("endTime", getYesterday() + " 23:59:59");
        order.setParams(params);
        yesterdayOrderList = orderService.selectTodayOrderList(order);

        // 统计今日的早中晚餐用餐人数
        for (SysOrder o : todayOrderList) {
            switch (o.getOrderType()) {
                case "0":
                    todayCount1++;
                    break;
                case "1":
                    todayCount2++;
                    break;
                case "2":
                    todayCount3++;
                    break;
            }
        }

        // 统计昨日的早中晚餐用餐人数
        for (SysOrder o : yesterdayOrderList) {
            switch (o.getOrderType()) {
                case "0":
                    yesterdayCount1++;
                    break;
                case "1":
                    yesterdayCount2++;
                    break;
                case "2":
                    yesterdayCount3++;
                    break;
            }
        }

        resultMap.put("todayCount1", String.valueOf(todayCount1));// 今日早餐用餐人数
        resultMap.put("todayCount2", String.valueOf(todayCount2));// 今日午餐用餐人数
        resultMap.put("todayCount3", String.valueOf(todayCount3));// 今日晚餐用餐人数

//        resultMap.put("yesterdayCount1", String.valueOf(yesterdayCount1));
//        resultMap.put("yesterdayCount2", String.valueOf(yesterdayCount2));
//        resultMap.put("yesterdayCount3", String.valueOf(yesterdayCount3));

        // 早餐对比差值
        if ((todayCount1 - yesterdayCount1) > 0) {
            resultMap.put("difference1", "增加了" + String.valueOf(todayCount1 - yesterdayCount1) + "份");
        } else if ((todayCount1 - yesterdayCount1) < 0) {
            resultMap.put("difference1", "减少了" + String.valueOf(todayCount1 - yesterdayCount1) + "份");
        } else {
            resultMap.put("difference1", "不变");
        }

        // 午餐对比差值
        if ((todayCount2 - yesterdayCount2) > 0) {
            resultMap.put("difference2", "增加了" + String.valueOf(todayCount2 - yesterdayCount2) + "份");
        } else if ((todayCount2 - yesterdayCount2) < 0) {
            resultMap.put("difference2", "减少了" + String.valueOf(todayCount2 - yesterdayCount2) + "份");
        } else {
            resultMap.put("difference2", "不变");
        }

        // 晚餐对比差值
        if ((todayCount3 - yesterdayCount3) > 0) {
            resultMap.put("difference3", "增加了" + String.valueOf(todayCount3 - yesterdayCount3) + "份");
        } else if ((todayCount3 - yesterdayCount3) < 0) {
            resultMap.put("difference3", "减少了" + String.valueOf(todayCount3 - yesterdayCount3) + "份");
        } else {
            resultMap.put("difference3", "不变");
        }
        /***************************** 结束计算用餐人数和对比昨日人数 *****************************/

        /********************** 开始计算本月平均值 **********************/
        Map<String, Object> params2 = new HashMap<>();
        params2.put("beginTime", getYear() + "-" + getMonth() + "-01" + " 00:00:00");// 月头
        params2.put("endTime", getYear() + "-" + getMonth() + "-" + getDay() + " 23:59:59");// 月尾
        SysOrder orderAvg = new SysOrder();
        orderAvg.setKeyIDs(getKeyIDs.communityGetKeyIDs(communityList));
        orderAvg.setStatus(0);
        orderAvg.setParams(params2);
        List<SysOrder> orderThisMonthList = orderService.selectOrderList(orderAvg);

        // 根据订单日期以及订单类型进行去重且累加
        Map<String, SysOrder> avgMap = new HashMap<>();
        for (int i = 0; i < orderThisMonthList.size(); i++) {
            SysOrder bean = orderThisMonthList.get(i);
            String key = sdf.format(bean.getCreateTime()).toString().substring(0, 10) + "*KEY*" + bean.getOrderType();
            SysOrder bean2 = avgMap.get(key);
            //如果有存过
            if (bean2 != null) {
                //TODO 计算数量
                bean2.setAvgCount(bean2.getAvgCount() + 1);
                avgMap.put(key, bean2);
            } else {
                bean.setAvgCount(1);
                avgMap.put(key, bean);
            }
        }
        List<SysOrder> orderAvgList = new ArrayList<>(avgMap.values());// 经过去重且累加的结果列表

        double orderAvg1 = 0;// 早餐的月平均值
        double orderAvg2 = 0;// 午餐的月平均值
        double orderAvg3 = 0;// 晚餐的月平均值

        int[] avg1 = {};
        int[] avg2 = {};
        int[] avg3 = {};
        for (int i = 0; i < orderAvgList.size(); i++) {
            SysOrder bean = orderAvgList.get(i);
            String orderType = bean.getOrderType();
            if (orderType.equals("0")) {
                avg1 = Arrays.copyOf(avg1, avg1.length + 1);
                avg1[avg1.length - 1] = bean.getAvgCount();
            }
            if (orderType.equals("1")) {
                avg2 = Arrays.copyOf(avg2, avg2.length + 1);
                avg2[avg2.length - 1] = bean.getAvgCount();
            }
            if (orderType.equals("2")) {
                avg3 = Arrays.copyOf(avg3, avg3.length + 1);
                avg3[avg3.length - 1] = bean.getAvgCount();
            }

            // 进行计算早中晚餐的平均值(只计算含有早中晚餐或任意一项的天数，并不是按整月实际天数计算)
            if ((i + 1) >= orderAvgList.size()) {
                orderAvg1 = average(avg1);
                orderAvg2 = average(avg2);
                orderAvg3 = average(avg3);
            }
        }
        /********************** 结束计算本月平均值 **********************/

        resultMap.put("orderAvg1", String.format("%.2f", orderAvg1));
        resultMap.put("orderAvg2", String.format("%.2f", orderAvg2));
        resultMap.put("orderAvg3", String.format("%.2f", orderAvg3));

        mmap.put("resultMap", resultMap);
        mmap.put("version", Global.getVersion());
        return "main2";
    }

    /*****************************************************************************************************************/

    /**
     * 获取当前年份
     *
     * @param
     * @return
     */
    public int getYear() {
        Calendar c = Calendar.getInstance(Locale.CHINA);//
        return c.get(Calendar.YEAR); // 获取当前年份
    }

    /**
     * 获取当前月份
     *
     * @param
     * @return
     */
    public int getMonth() {
        Calendar c = Calendar.getInstance(Locale.CHINA);//
        return c.get(Calendar.MONTH) + 1; //
    }

    /**
     * 获取当前月份的天数
     *
     * @param
     * @return
     */
    public int getDay() {
        Calendar a = Calendar.getInstance();
        a.set(Calendar.DATE, 1);
        a.roll(Calendar.DATE, -1);
        return a.get(Calendar.DATE);
    }

    /**
     * 返回今天
     *
     * @return
     */
    public String getToday() {
        Date date = new Date();//取时间
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(calendar.DATE, 0);
        date = calendar.getTime(); //这个时间就是日期往后推一天的结果
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(date);
        return dateString;
    }

    /**
     * 返回昨天
     *
     * @return
     */
    public String getYesterday() {
        Date date = new Date();//取时间
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(calendar.DATE, -1);
        date = calendar.getTime(); //这个时间就是日期往前推一天的结果
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(date);
        return dateString;
    }

    /**
     * 求int的平均值
     *
     * @param array
     * @return
     */
    public static double average(int[] array) {
        double temp = 0;
        double sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum = sum + array[i];
        }
        temp = sum / array.length;
        return temp;
    }

}
