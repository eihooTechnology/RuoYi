package com.ruoyi.web.controller.tool;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.ListObjectsRequest;
import com.aliyun.oss.model.OSSObjectSummary;
import com.aliyun.oss.model.ObjectListing;
import sun.misc.BASE64Decoder;

import java.io.*;
import java.util.List;

/**
 * @ClassName OssTool
 * @Description TODO
 * @Author lzjian
 * @Date 2019-04-22 19:05
 **/
public class OssTool {

    /************************************************/
    public static String rider_debug = "rider-debug";// 骑手测试人脸库
    // public static String rider_release = "rider-release";// 骑手正式人脸库
    public static String elderly_debug = "elderly-debug";// 老人测试人脸库
    // public static String elderly_release = "elderly-release";// 老人正式人脸库
    /************************************************/
    public static String riderImgPath = "/usr/local/ry-rider-img/";
    public static String elderlyImgPath = "/usr/local/ry-elderly-img/";
    /************************************************/
    public static String riderOssUrlDebug = "https://rider-debug.oss-cn-hangzhou.aliyuncs.com/";
    public static String elderlyOssUrlDebug = "https://elderly-debug.oss-cn-hangzhou.aliyuncs.com/";
    public static String riderOssUrlRelease = "https://rider-release.oss-cn-hangzhou.aliyuncs.com/";
    public static String elderlyOssUrlRelease = "https://elderly-release.oss-cn-hangzhou.aliyuncs.com/";

    public static void main(String[] args) {

        // generateImage(imgBase64, riderImgPath, "888");

        // uploadOssFile(rider_debug, "888", riderImgPath);

        // checkFile(elderly_debug, "10");

        // deleteFile(elderly_debug, "10");

        // showFilesList(rider_debug);

        showFilesList(elderly_debug);

    }

    /**
     * OSS简单上传文件
     *
     * @param bucketName
     * @param fileName
     * @param imgPath
     */
    public static Boolean uploadOssFile(String bucketName, String fileName, String imgPath) {

        try {
            // Endpoint以杭州为例，其它Region请按实际情况填写。
            String endpoint = "http://oss-cn-hangzhou.aliyuncs.com";
            // 云账号AccessKey有所有API访问权限，建议遵循阿里云安全最佳实践，创建并使用RAM子账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建。
            String accessKeyId = "LTAI6XTCYwvnXOdr";
            String accessKeySecret = "zVfcI02o4hMp5vTwEcZU6go6j4o7d0";

            // 创建OSSClient实例。
            OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);

            // 上传文件。<yourLocalFile>由本地文件路径加文件名包括后缀组成，例如/users/local/myfile.txt。
            ossClient.putObject(bucketName, fileName, new File(imgPath + fileName + ".jpg"));

            // 关闭OSSClient。
            ossClient.shutdown();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    /**
     * 判断OSS服务器中的指定文件是否存在
     *
     * @param bucketName
     * @param fileName
     * @return
     */
    public static Boolean checkFile(String bucketName, String fileName) {
        // Endpoint以杭州为例，其它Region请按实际情况填写。
        String endpoint = "http://oss-cn-hangzhou.aliyuncs.com";
        // 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建RAM账号。
        String accessKeyId = "LTAI6XTCYwvnXOdr";
        String accessKeySecret = "zVfcI02o4hMp5vTwEcZU6go6j4o7d0";

        // 创建OSSClient实例。
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);

        // 判断文件是否存在。True=存在；False=不存在
        boolean found = ossClient.doesObjectExist(bucketName, fileName);
        System.err.println(found);

        // 关闭OSSClient。
        ossClient.shutdown();

        return found;
    }

    /**
     * 删除单个文件
     *
     * @param bucketName
     * @param fileName
     */
    public static void deleteFile(String bucketName, String fileName) {
        try {
            // Endpoint以杭州为例，其它Region请按实际情况填写。
            String endpoint = "http://oss-cn-hangzhou.aliyuncs.com";
            // 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建RAM账号。
            String accessKeyId = "LTAI6XTCYwvnXOdr";
            String accessKeySecret = "zVfcI02o4hMp5vTwEcZU6go6j4o7d0";

            // 创建OSSClient实例。
            OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);

            // 删除文件。
            ossClient.deleteObject(bucketName, fileName);

            // 关闭OSSClient。
            ossClient.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 查询列举指定个数的文件
     *
     * @param bucketName
     */
    public static void showFilesList(String bucketName) {
        // Endpoint以杭州为例，其它Region请按实际情况填写。
        String endpoint = "http://oss-cn-hangzhou.aliyuncs.com";
        // 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建RAM账号。
        String accessKeyId = "LTAI6XTCYwvnXOdr";
        String accessKeySecret = "zVfcI02o4hMp5vTwEcZU6go6j4o7d0";

        // 创建OSSClient实例
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);

        // 设置最大个数。
        final int maxKeys = 1000;
        // 列举文件。
        ObjectListing objectListing = ossClient.listObjects(new ListObjectsRequest(bucketName).withMaxKeys(maxKeys));
        List<OSSObjectSummary> sums = objectListing.getObjectSummaries();
        for (OSSObjectSummary s : sums) {
            System.out.println("\t" + s.getKey());
        }

        // 关闭OSSClient。
        ossClient.shutdown();
    }

    /**************************************************************************************************/


    /**
     * Base64字符串转化成图片文件
     *
     * @param imgBase64
     * @param imgPath
     * @param fileName
     * @return
     */
    public static boolean generateImage(String imgBase64, String imgPath, String fileName) {   //对字节数组字符串进行Base64解码并生成图片
        if (imgBase64 == null) //图像数据为空
            return false;
        BASE64Decoder decoder = new BASE64Decoder();
        try {
            //Base64解码
            byte[] b = decoder.decodeBuffer(imgBase64);
            for (int i = 0; i < b.length; ++i) {
                if (b[i] < 0) {//调整异常数据
                    b[i] += 256;
                }
            }
            //生成jpeg图片
            String imgFilePath = imgPath + fileName + ".jpg";//新生成的图片
            OutputStream out = new FileOutputStream(imgFilePath);
            out.write(b);
            out.flush();
            out.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
