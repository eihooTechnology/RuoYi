package com.ruoyi.web.controller.tool;

/**
 * @ClassName ResultDTO (数据传输对象)
 * @Description TODO
 * @Author lzjian
 * @Date 2019/1/2 0002 15:50
 **/
public class ResultDTO<T> {
    /*传输状态：0:成功；1:失败*/
    private int code = 1;

    /*传输信息*/
    private String msg = "处理失败!请联系管理员";

    /*数据总数*/
    private long count;

    /*传输数据*/
    private T data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ResultDTO{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", count=" + count +
                ", data=" + data +
                '}';
    }
}
