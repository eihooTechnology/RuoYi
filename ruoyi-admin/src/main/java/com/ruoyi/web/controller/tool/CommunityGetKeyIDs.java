package com.ruoyi.web.controller.tool;

import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.SysCommunity;
import com.ruoyi.system.service.ISysCommunityService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @ClassName CommunityTest
 * @Description TODO
 * @Author lzjian
 * @Date 2019/1/16 0016 14:38
 **/
public class CommunityGetKeyIDs {

    /**
     * 获取当前登录用户绑定的社区ID下的所有子节点(字符串)
     *
     * @param communityList
     * @return
     */
    public String communityGetKeyIDs(List<SysCommunity> communityList) {
        if (ShiroUtils.getSysUser().getCommunityId() != null) {
            Long keyID = Long.valueOf(ShiroUtils.getSysUser().getCommunityId());// 关键ID(当前登录用户绑定的社区ID)

            SysCommunity s = new SysCommunity();
            s.setStatus("0");

            StringBuffer sb = new StringBuffer("");// 属于当前关键ID的社区ID(子级)
            String[] splitStr = null;
            for (SysCommunity c : communityList) {// 将属于当前关键ID的社区ID(子级)全部加入sb中
                splitStr = null;
                splitStr = c.getAncestors().split(",");
                for (String str : splitStr) {
                    if (str.equals(String.valueOf(keyID))) {
                        sb.append(c.getCommunityId() + ",");
                    }
                }
            }

            String keyIDs = "";
            if (StringUtils.isNotBlank(sb.toString())) {
                keyIDs = keyID + "," + sb.toString();
                keyIDs = keyIDs.substring(0, keyIDs.lastIndexOf(","));
            }
            // System.err.println("keyIDs=" + keyIDs);

            return keyIDs;
        } else {
            return "";
        }
    }

//    public void communityGetKeyIDs() {
//        Long keyID = Long.valueOf(ShiroUtils.getSysUser().getCommunityId());// 关键ID(当前登录用户绑定的社区ID)
//
//        SysCommunity s = new SysCommunity();
//        s.setStatus("0");
//        List<SysCommunity> communityList = communityService.selectCommunityList(s);// 获取到所有的社区节点信息(所有根节点与子节点)
//
//        StringBuffer sb = new StringBuffer("");// 属于当前关键ID的社区ID(子级)
//        String[] splitStr = null;
//        for (SysCommunity c : communityList) {// 将属于当前关键ID的社区ID(子级)全部加入sb中
//            splitStr = null;
//            splitStr = c.getAncestors().split(",");
//            for (String str : splitStr) {
//                if (str.equals(String.valueOf(keyID))) {
//                    sb.append(c.getCommunityId() + ",");
//                }
//            }
//        }
//
//        String keyIDs = "";
//        if (StringUtils.isNotBlank(sb.toString())) {
//            keyIDs = sb.toString().substring(0, sb.toString().lastIndexOf(","));
//        }
//        System.err.println("keyIDs=" + keyIDs);
//
//        s.setKeyIDs(keyIDs);
//        List<SysCommunity> resultList = communityService.selectCommunityList(s);
//
//        for (SysCommunity c : resultList) {
//            System.err.println(c);
//        }
//    }

}
