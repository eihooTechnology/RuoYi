package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.base.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.framework.web.base.BaseController;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.quartz.util.Encryption;
import com.ruoyi.system.domain.SysCommunity;
import com.ruoyi.system.domain.SysOldman;
import com.ruoyi.system.domain.SysOrderBill;
import com.ruoyi.system.service.*;
import com.ruoyi.web.controller.tool.CommunityGetKeyIDs;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 定餐点老人管理调用层
 *
 * @author ruoyi
 */
@Controller
@RequestMapping("/system/restaurantOldMan")
public class SysRestaurantOldManController extends BaseController {
    private String prefix = "system/restaurantOldMan";

    @Autowired
    private ISysOldManService oldManService;

    @Autowired
    private ISysRoleService roleService;

    @Autowired
    private ISysPostService postService;

    @Autowired
    private ISysOrderBillService orderBillService;

    @Autowired
    private ISysCommunityService communityService;

    private SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");

    /**
     * 点击定餐点老人管理页面
     *
     * @return
     */
    @RequiresPermissions("system:restaurantOldMan:view")
    @GetMapping()
    public String user() {
        return prefix + "/oldMan";
    }

    /**
     * 分页查询定餐点老人列表
     *
     * @param sysOldman
     * @return
     */
    @RequiresPermissions("system:oldMan:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysOldman sysOldman) {
        // 数据查看范围权限(要放在分页startPage();前面，否则只能查询出10个)
        SysCommunity s = new SysCommunity();
        s.setStatus("0");
        List<SysCommunity> communityList = communityService.selectCommunityList(s);
        CommunityGetKeyIDs getKeyIDs = new CommunityGetKeyIDs();
        sysOldman.setKeyIDs(getKeyIDs.communityGetKeyIDs(communityList));

        startPage();
        if (StringUtils.isBlank(sysOldman.getStatus())) {
            sysOldman.setStatus("0");
        }
        List<SysOldman> list = oldManService.selectOldManList(sysOldman);
        return getDataTable(list);
    }

    /**
     * 点击新增跳转至填写定餐点老人信息
     */
    @GetMapping("/add")
    public String add(ModelMap mmap) {
        mmap.put("roles", roleService.selectRoleAll());
        mmap.put("posts", postService.selectPostAll());
        return prefix + "/add";
    }

    /**
     * 校验定餐点老人是否重复
     *
     * @param sysOldman
     * @return
     */
    @PostMapping("/checkOldManUnique")
    @ResponseBody
    public String checkOldManUnique(SysOldman sysOldman) {
        return oldManService.checkOldManUnique(sysOldman.getIdentityId());
    }

    /**
     * 填写定餐点老人信息后，新增定餐点老人
     *
     * @param sysOldman
     * @return
     */
    @RequiresPermissions("system:oldMan:add")
    @Log(title = "定餐点老人管理", businessType = BusinessType.INSERT)
    @PostMapping("/saveOldMan")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public AjaxResult addSave(SysOldman sysOldman) {
        sysOldman.setCreateBy(ShiroUtils.getLoginName());
        return toAjax(oldManService.saveOldMan(sysOldman));
    }


    /**
     * 修改用户 跳转修改页面
     *
     * @param userCode
     * @param mmap
     * @return
     */
    @GetMapping("/edit/{userCode}")
    public String edit(@PathVariable("userCode") Long userCode, ModelMap mmap) {
        System.out.println("userCode = " + userCode);
        mmap.put("oldMan", oldManService.selectOldManById(userCode));
//        mmap.put("roles", roleService.selectRolesByUserId(userCode));
//        mmap.put("posts", postService.selectPostsByUserId(userCode));
        return prefix + "/edit";
    }

    /**
     * 修改定餐点老人信息
     *
     * @param sysOldman
     * @return
     */
    @RequiresPermissions("system:oldMan:edit")
    @Log(title = "定餐点老人管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public AjaxResult editSave(SysOldman sysOldman) throws Exception {

        Long userCode = sysOldman.getUserCode();

        // 此处进行老人修改代码

        return toAjax(oldManService.updateOldMan(sysOldman));
    }

    /**
     * 删除定餐点老人
     *
     * @param userCode
     * @return
     */
    @RequiresPermissions("system:oldMan:remove")
    @Log(title = "定餐点老人管理", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public String remove(Long userCode) {
        int i = oldManService.deleteOldManByUserCode(userCode);
        if (i > 0) {
            return "删除该定餐点老人以及定餐点老人订单成功！";
        } else {
            return "系统错误，删除该定餐点老人以及定餐点老人订单失败！";
        }
    }

    /*****************************************************************************************************************/

    /**
     * 获取当前年份
     *
     * @param
     * @return
     */
    public int getYear() {
        Calendar c = Calendar.getInstance(Locale.CHINA);//
        return c.get(Calendar.YEAR); // 获取当前年份
    }

    /**
     * 获取当前月份
     *
     * @param
     * @return
     */
    public int getMonth() {
        Calendar c = Calendar.getInstance(Locale.CHINA);//
        return c.get(Calendar.MONTH) + 1; //
    }

    /**
     * 获取当前月份的天数
     *
     * @param
     * @return
     */
    public int getDay() {
        Calendar a = Calendar.getInstance();
        a.set(Calendar.DATE, 1);
        a.roll(Calendar.DATE, -1);
        return a.get(Calendar.DATE);
    }

    /**
     * 描述:获取下一个月的第一天.
     *
     * @return
     */
    public String getPerFirstDayOfMonth() {
        SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        return dft.format(calendar.getTime());
    }

    /**
     * 描述:获取下个月的最后一天.
     *
     * @return
     */
    public String getLastMaxMonthDate() {
        SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return dft.format(calendar.getTime());
    }

}