package com.ruoyi.web.controller.system;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.web.base.BaseController;
import com.ruoyi.quartz.util.Encryption;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.service.*;
import com.ruoyi.web.controller.tool.*;
import com.ruoyi.web.entity.PadCourierBean;
import com.ruoyi.web.entity.SysOrderExport;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ImageBanner;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.font.ImageGraphicAttribute;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 老人管理调用层
 *
 * @author ruoyi
 */
@Controller
@RequestMapping("/system/api")
public class SysApiController extends BaseController {
    private String prefix = "system/api";

    @Autowired
    private ISysOldManService oldManService;

    @Autowired
    private ISysRoleService roleService;

    @Autowired
    private ISysPostService postService;

    @Autowired
    private ISysOrderBillService orderBillService;

    @Autowired
    private ISysCourierService courierService;

    @Autowired
    private ISysOrderService orderService;

    @Autowired
    private ISysOrderPreviewService orderPreviewService;

    @Autowired
    private ISysAppVersionService appVersionService;

    @Autowired
    private IZcdOldmanInfoService zcdOldmanInfoService;

    @Autowired
    private IZcdFoodPackageService zcdFoodPackageService;

    @Autowired
    private IZcdFoodPackageOldmanService zcdFoodPackageOldmanService;

    @Autowired
    private ISysCommunityService communityService;

    private SimpleDateFormat sdfMDY = new SimpleDateFormat("MM/dd/yyyy");
    private SimpleDateFormat sdfYMD = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * 验证送餐员登录(接口)【账号密码登录】
     *
     * @return
     */
    @Log(title = "验证送餐员登录Api", businessType = BusinessType.INSERT)
    @PostMapping("/courierLoginApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO courierLoginApi(HttpServletRequest request, String dataJson) throws Exception {
        System.err.println("验证送餐员登录Api【账号密码登录】，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        /**********************接口请求合法性验证********************/
//        Date date = new Date();
//        Long nowTime = date.getTime();
        String timeStamp = request.getHeader("timeStamp");
        String md5 = request.getHeader("MD5");
        /**********************接口请求合法性验证********************/
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(md5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(86400000);// 超时时间为1天
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过30秒
                if (Encryption.encrypt(timeStamp).equals(md5)) {
                    /**********开始执行逻辑**********/
                    System.err.println("dataJson=" + dataJson);
                    // 将获取到的JSON转换为map
                    Map jsonMap = JSON.parseObject(dataJson, Map.class);
                    String tel = jsonMap.get("tel") == null ? null : jsonMap.get("tel").toString();
                    String password = jsonMap.get("password") == null ? null : jsonMap.get("password").toString();

                    List<SysCourier> courierList = new ArrayList<>();
                    SysCourier courier = new SysCourier();
                    courier.setTel(tel);
                    courierList = courierService.selectCourierList(courier);

                    //就不给你们写注释
                    //这代码写得这么费劲
                    //所以你们读着也得费劲
                    if (courierList.size() > 0) {
                        courier = courierList.get(0);
                        if (courier.getPassword().equals(Encryption.encrypt(password))) {
                            Map<String, String> resultMap = new HashMap<>();
                            resultMap.put("id", String.valueOf(courier.getId()));
                            resultMap.put("courierName", courier.getCourierName());
                            resultMap.put("isFaceRegistration", courier.getIsFaceRegistration());

                            result.setCode(0);// 0=成功
                            result.setMsg("登录成功!");
                            result.setData(resultMap);
                            result.setCount(0);
                            return result;
                        } else {
                            System.out.println("密码错误");
                            result.setCode(1);// 1=失败
                            result.setMsg("密码错误");
                            return result;
                        }
                    } else {
                        System.out.println("无相关用户信息，请检查");
                        result.setCode(1);// 1=失败
                        result.setMsg("无相关用户信息，请检查");
                        return result;
                    }
                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过一天时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }

    /**
     * 获取送餐员送餐列表
     *
     * @return
     */
    @Log(title = "送餐员订单Api", businessType = BusinessType.INSERT)
    @PostMapping("/courierOrderListApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO selectCourierOrderListApi(HttpServletRequest request, String dataJson) throws Exception {
        System.err.println("验证送餐员登录Api【账号密码登录】，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        /**********************接口请求合法性验证********************/
        String timeStamp = request.getHeader("timeStamp");
        String md5 = request.getHeader("MD5");
        /**********************接口请求合法性验证********************/
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(md5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(86400000);// 超时时间为1天
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过30秒
                if (Encryption.encrypt(timeStamp).equals(md5)) {
                    /**********开始执行逻辑**********/
                    System.err.println("dataJson=" + dataJson);
                    // 将获取到的JSON转换为map
                    Map jsonMap = JSON.parseObject(dataJson, Map.class);
                    Long id = jsonMap.get("id") == null ? null : Long.valueOf(jsonMap.get("id").toString());

                    List<SysCourier> courierList = new ArrayList<>();
                    SysCourier courier = new SysCourier();
                    courier.setId(id);
                    courierList = courierService.selectCourierList(courier);

                    //就不给你们写注释
                    //这代码写得这么费劲
                    //所以你们读着也得费劲
                    if (courierList.size() > 0) {
                        courier = courierList.get(0);

                        List<SysOrder> orderList = new ArrayList<>();
                        SysOrder order = new SysOrder();
                        order.setStatus(0);
                        order.setSendCourierId(courier.getId());
                        Map<String, Object> params = new HashMap<>();
                        params.put("beginTime", getToday() + " 00:00:00");
                        params.put("endTime", getToday() + " 23:59:59");
                        order.setParams(params);
                        orderList = orderService.selectTodayOrderList(order);

                        Map<String, String> resultMap = new HashMap<>();

                        SysOrder order2 = new SysOrder();
                        Map<String, Object> params2 = new HashMap<>();
                        params2.put("beginTime", getYesterday() + " 00:00:00");
                        params2.put("endTime", getYesterday() + " 23:59:59");
                        order2.setStatus(0);
                        order2.setRecoverCourierId(Long.valueOf(id));
                        order2.setParams(params2);
                        order2.setRecoverType("0");
                        List<SysOrder> recoverOrderList = new ArrayList<>();
                        recoverOrderList = orderService.selectTodayOrderList(order2);// 得到昨天所有的待回收订单

                        resultMap = getMealCountAndList(resultMap, orderList, recoverOrderList, courier);

                        resultMap.put("timeStamp", timeStamp);
                        resultMap.put("MD5", md5);

                        result.setCode(0);// 0=成功
                        result.setMsg("获取了相关订单统计信息");
                        result.setData(resultMap);
                        result.setCount(orderList.size());
                        return result;
                    } else {
                        System.out.println("无相关用户信息，请检查");
                        result.setCode(1);// 1=失败
                        result.setMsg("无相关用户信息，请检查");
                        return result;
                    }
                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过一天时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }

    /**
     * 验证送餐员登录(接口)【刷脸登录】
     *
     * @return
     */
    @Log(title = "验证送餐员登录Api", businessType = BusinessType.INSERT)
    @PostMapping("/courierLoginByFaceApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO courierLoginByFaceApi(HttpServletRequest request, String dataJson) throws Exception {
        System.err.println("验证送餐员登录Api【刷脸登录】，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        /**********************接口请求合法性验证********************/
//        Date date = new Date();
//        Long nowTime = date.getTime();
        String timeStamp = request.getHeader("timeStamp");
        String md5 = request.getHeader("MD5");
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(md5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(86400000);// 超时时间为1天
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过30秒
                if (Encryption.encrypt(timeStamp).equals(md5)) {
                    /**********开始执行逻辑**********/
                    System.err.println("dataJson=" + dataJson);
                    // 将获取到的JSON转换为map
                    Map jsonMap = JSON.parseObject(dataJson, Map.class);
                    String id = jsonMap.get("id") == null ? null : jsonMap.get("id").toString();

                    List<SysCourier> courierList = new ArrayList<>();
                    SysCourier courier = new SysCourier();
                    courier.setId(Long.valueOf(id));
                    courierList = courierService.selectCourierList(courier);

                    //就不给你们写注释
                    //这代码写得这么费劲
                    //所以你们读着也得费劲
                    if (courierList.size() > 0) {
                        courier = courierList.get(0);
                        List<SysOrder> orderList = new ArrayList<>();
                        SysOrder order = new SysOrder();
                        order.setStatus(0);
                        order.setSendCourierId(courier.getId());
                        Map<String, Object> params = new HashMap<>();
                        params.put("beginTime", getToday() + " 00:00:00");
                        params.put("endTime", getToday() + " 23:59:59");
                        order.setParams(params);
                        orderList = orderService.selectTodayOrderList(order);

                        Map<String, String> resultMap = new HashMap<>();

                        SysOrder order2 = new SysOrder();
                        Map<String, Object> params2 = new HashMap<>();
                        params2.put("beginTime", getYesterday() + " 00:00:00");
                        params2.put("endTime", getYesterday() + " 23:59:59");
                        order2.setStatus(0);
                        order2.setRecoverCourierId(Long.valueOf(id));
                        order2.setParams(params2);
                        order2.setRecoverType("0");
                        List<SysOrder> recoverOrderList = new ArrayList<>();
                        recoverOrderList = orderService.selectTodayOrderList(order2);// 得到昨天所有的待回收订单

                        resultMap = getMealCountAndList(resultMap, orderList, recoverOrderList, courier);

                        // 生成验证MD5和timeStamp,有效期1天
                        String md5Str = Encryption.encrypt(String.valueOf(nowTime));
                        resultMap.put("timeStamp", String.valueOf(nowTime));
                        resultMap.put("MD5", md5Str);

                        result.setCode(0);// 0=成功
                        result.setMsg("登录成功!并获取了相关订单统计信息");
                        result.setData(resultMap);
                        result.setCount(orderList.size());
                        return result;
                    } else {
                        System.out.println("无相关用户信息，请检查");
                        result.setCode(1);// 1=失败
                        result.setMsg("无相关用户信息，请检查");
                        return result;
                    }
                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过一天时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }

    /**
     * 送餐员改变人脸注册状态(接口)
     *
     * @return
     */
    @Log(title = "送餐员改变人脸注册状态Api", businessType = BusinessType.INSERT)
    @PostMapping("/courierFaceRegistrationStatusApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO courierFaceRegistrationStatusApi(HttpServletRequest request, String dataJson) throws Exception {
        System.err.println("送餐员改变人脸注册状态Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();

        String timeStamp = request.getHeader("timeStamp");
        String md5 = request.getHeader("MD5");

        /**********************接口请求合法性验证********************/
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(md5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(86400000);// 超时时间为1天
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过30秒
                if (Encryption.encrypt(timeStamp).equals(md5)) {
                    /**********开始执行逻辑**********/

                    System.err.println("dataJson=" + dataJson);
                    // 将获取到的JSON转换为map
                    Map jsonMap = JSON.parseObject(dataJson, Map.class);
                    String id = jsonMap.get("id") == null ? null : jsonMap.get("id").toString();
                    String imgBase64 = jsonMap.get("imgBase64") == null ? null : jsonMap.get("imgBase64").toString();

                    SysCourier courier = new SysCourier();
                    courier.setId(Long.valueOf(id));
                    courier.setIsFaceRegistration("1");
                    courier.setImgBase64(imgBase64);
                    int count = courierService.updateCourier(courier);

                    result.setCode(0);// 0=成功
                    result.setMsg("改变人脸注册状态成功!");
                    result.setData(count);
                    result.setCount(count);
                    return result;

                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过一天时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }

    /**
     * 送餐员刷脸签到领餐(接口)
     *
     * @return
     */
    @Log(title = "送餐员刷脸签到领餐Api", businessType = BusinessType.INSERT)
    @PostMapping("/courierVerificationApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO courierVerificationApi(HttpServletRequest request, String dataJson) throws Exception {
        System.err.println("送餐员刷脸签到领餐Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        String timeStamp = request.getHeader("timeStamp");
        String md5 = request.getHeader("MD5");
        /**********************接口请求合法性验证********************/
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(md5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(86400000);// 超时时间为1天
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过30秒
                if (Encryption.encrypt(timeStamp).equals(md5)) {
                    /**********开始执行逻辑**********/

                    System.err.println("dataJson=" + dataJson);
                    // 将获取到的JSON转换为map
                    Map jsonMap = JSON.parseObject(dataJson, Map.class);
                    String id = jsonMap.get("id") == null ? null : jsonMap.get("id").toString();
                    String orderType = jsonMap.get("orderType") == null ? null : jsonMap.get("orderType").toString();
//                    String courierName = jsonMap.get("courierName") == null ? null : jsonMap.get("courierName").toString();

                    SysOrder order = new SysOrder();
                    order.setSendCourierId(Long.valueOf(id));
                    order.setOrderType(orderType);
//                    order.setUpdateBy(courierName);

                    int count = orderService.updateOrderByFace(order);

                    result.setCode(0);// 0=成功
                    result.setMsg("刷脸签到成功!");
                    result.setData(count);
                    result.setCount(count);
                    return result;

                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过一天时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }

    /**
     * 送餐员获取送餐信息(接口)
     *
     * @return
     */
    @Log(title = "送餐员获取送餐信息Api", businessType = BusinessType.INSERT)
    @PostMapping("/courierGetOrdersApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO courierGetOrdersApi(HttpServletRequest request, String dataJson) throws Exception {
        System.err.println("送餐员获取送餐信息Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        String timeStamp = request.getHeader("timeStamp");
        String md5 = request.getHeader("MD5");
        /**********************接口请求合法性验证********************/
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(md5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(86400000);// 超时时间为1天
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过30秒
                if (Encryption.encrypt(timeStamp).equals(md5)) {
                    /**********开始执行逻辑**********/

                    System.err.println("dataJson=" + dataJson);
                    // 将获取到的JSON转换为map
                    Map jsonMap = JSON.parseObject(dataJson, Map.class);
                    String id = jsonMap.get("id") == null ? null : jsonMap.get("id").toString();
                    String orderType = jsonMap.get("orderType") == null ? null : jsonMap.get("orderType").toString();
                    String sendType = jsonMap.get("sendType") == null ? null : jsonMap.get("sendType").toString();
                    String recoverType = jsonMap.get("recoverType") == null ? null : jsonMap.get("recoverType").toString();

                    List<SysOrder> orderList = new ArrayList<>();
                    SysOrder order = new SysOrder();

                    if (recoverType == null) {
                        order.setStatus(0);
                        order.setSendCourierId(Long.valueOf(id));
                        Map<String, Object> params = new HashMap<>();
                        params.put("beginTime", getToday() + " 00:00:00");
                        params.put("endTime", getToday() + " 23:59:59");
                        order.setParams(params);

                        order.setSendType(sendType);
                        order.setOrderType(orderType);
                        // order.setRecoverType(recoverType);

                        orderList = orderService.selectTodayOrderList(order);// 得到今天的所有订单

                        orderList.sort(Comparator.reverseOrder());// 使列表按ID从大到小排序

                        result.setCode(0);// 0=成功
                        result.setMsg("获取订单信息成功!");
                        result.setData(orderList);
                        result.setCount(orderList.size());
                        return result;
                    } else {
                        SysOrder order2 = new SysOrder();
                        Map<String, Object> params2 = new HashMap<>();
                        params2.put("beginTime", getYesterday() + " 00:00:00");
                        params2.put("endTime", getYesterday() + " 23:59:59");
                        order2.setStatus(0);
                        order2.setRecoverCourierId(Long.valueOf(id));
                        order2.setParams(params2);
                        order2.setRecoverType("0");
                        order2.setOrderType(orderType);
                        List<SysOrder> recoverOrderList = new ArrayList<>();
                        recoverOrderList = orderService.selectTodayOrderList(order2);// 得到昨天所有的待回收订单

                        recoverOrderList.sort(Comparator.reverseOrder());// 使列表按ID从大到小排序

                        result.setCode(0);// 0=成功
                        result.setMsg("获取订单信息成功!");
                        result.setData(recoverOrderList);
                        result.setCount(recoverOrderList.size());
                        return result;
                    }


                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过一天时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }

    /**
     * 根据老人编号以及餐点信息，获取该老人下的送餐信息(接口)
     *
     * @return
     */
    @Log(title = "送餐员获取送餐信息Api", businessType = BusinessType.INSERT)
    @PostMapping("/courierGetOrdersListApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO courierGetOrdersListApi(HttpServletRequest request, String dataJson) throws Exception {
        System.err.println("送餐员获取老人送餐信息列表Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        String timeStamp = request.getHeader("timeStamp");
        String md5 = request.getHeader("MD5");
        /**********************接口请求合法性验证********************/
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(md5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(86400000);// 超时时间为1天
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过30秒
                if (Encryption.encrypt(timeStamp).equals(md5)) {
                    /**********开始执行逻辑**********/

                    System.err.println("dataJson=" + dataJson);
                    // 将获取到的JSON转换为map
                    Map jsonMap = JSON.parseObject(dataJson, Map.class);
                    Long userCode = jsonMap.get("userCode") == null ? null : Long.valueOf(jsonMap.get("userCode").toString());
                    String orderType = jsonMap.get("orderType") == null ? null : jsonMap.get("orderType").toString();
                    String sendType = jsonMap.get("sendType") == null ? null : jsonMap.get("sendType").toString();
                    String recoverType = jsonMap.get("recoverType") == null ? null : jsonMap.get("recoverType").toString();

                    if (recoverType.equals("-1")) {
                        List<SysOrder> orderList = new ArrayList<>();
                        SysOrder order = new SysOrder();
                        order.setUserCode(userCode);
                        order.setOrderType(orderType);
                        order.setSendType(sendType);
                        order.setStatus(0);
                        // order.setRecoverType(recoverType);
                        Map<String, Object> params = new HashMap<>();
                        params.put("beginTime", getToday() + " 00:00:00");
                        params.put("endTime", getToday() + " 23:59:59");
                        order.setParams(params);

                        orderList = orderService.selectTodayOrderList(order);// 得到今天的所有订单
                        orderList.sort(Comparator.reverseOrder());// 使列表按ID从大到小排序

                        result.setCode(0);// 0=成功
                        result.setMsg("获取订单信息成功!");
                        result.setData(orderList);
                        result.setCount(orderList.size());
                        return result;
                    }

                    SysOrder order2 = new SysOrder();
                    Map<String, Object> params2 = new HashMap<>();
                    params2.put("beginTime", getYesterday() + " 00:00:00");
                    params2.put("endTime", getYesterday() + " 23:59:59");
                    order2.setStatus(0);
                    order2.setUserCode(Long.valueOf(userCode));
                    order2.setParams(params2);
                    order2.setRecoverType("0");
                    order2.setOrderType(orderType);
                    List<SysOrder> recoverOrderList = new ArrayList<>();
                    recoverOrderList = orderService.selectTodayOrderList(order2);// 得到昨天所有的待回收订单

                    recoverOrderList.sort(Comparator.reverseOrder());// 使列表按ID从大到小排序

                    result.setCode(0);// 0=成功
                    result.setMsg("获取订单信息成功!");
                    result.setData(recoverOrderList);
                    result.setCount(recoverOrderList.size());
                    return result;

                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过一天时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }

    /**
     * 送餐员确认餐点送达(接口)
     *
     * @return
     */
    @Log(title = "送餐员确认餐点送达Api", businessType = BusinessType.INSERT)
    @PostMapping("/courierConfirmApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO courierConfirmApi(HttpServletRequest request, String dataJson) throws Exception {
        System.err.println("送餐员确认餐点送达Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        String timeStamp = request.getHeader("timeStamp");
        String md5 = request.getHeader("MD5");
        /**********************接口请求合法性验证********************/
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(md5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(86400000);// 超时时间为1天
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过30秒
                if (Encryption.encrypt(timeStamp).equals(md5)) {
                    /**********开始执行逻辑**********/

                    System.err.println("dataJson=" + dataJson);
                    // 将获取到的JSON转换为map
                    Map jsonMap = JSON.parseObject(dataJson, Map.class);
                    String id = jsonMap.get("id") == null ? null : jsonMap.get("id").toString();
                    String courierName = jsonMap.get("courierName") == null ? null : jsonMap.get("courierName").toString();
                    String orderId = jsonMap.get("orderId") == null ? null : jsonMap.get("orderId").toString();
                    String lng = jsonMap.get("longitude") == null ? null : jsonMap.get("longitude").toString();
                    String lat = jsonMap.get("latitude") == null ? null : jsonMap.get("latitude").toString();

                    SysOrder order = new SysOrder();
                    order.setId(Long.valueOf(orderId));
                    order.setSendType("2");// 送餐完成
                    order.setSendFinishedTime(sdf.parse(getNowTime()));
//                    order.setRecoverType("1");// 已回收
                    order.setRecoverTime(sdf.parse(getNowTime()));
                    order.setUpdateBy(courierName);

                    order.setLng(Double.valueOf(lng));
                    order.setLat(Double.valueOf(lat));

                    int flag = orderService.updateOrder(order);

                    if (flag > 0) {
                        result.setCode(0);// 0=成功
                        result.setMsg("关闭订单成功!");
                        result.setData(flag);
                        result.setCount(flag);
                        return result;
                    } else {
                        result.setCode(1);// 1=失败
                        result.setMsg("关闭订单失败");
                        result.setData(null);
                        result.setCount(0);
                        return result;
                    }

                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过一天时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }

    /**
     * 送餐员确认回收(接口)
     *
     * @return
     */
    @Log(title = "送餐员确认已回收Api", businessType = BusinessType.INSERT)
    @PostMapping("/courierRecoverApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO courierRecoverApi(HttpServletRequest request, String dataJson) throws Exception {
        System.err.println("送餐员确认回收Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        String timeStamp = request.getHeader("timeStamp");
        String md5 = request.getHeader("MD5");
        /**********************接口请求合法性验证********************/
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(md5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(86400000);// 超时时间为1天
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过30秒
                if (Encryption.encrypt(timeStamp).equals(md5)) {
                    /**********开始执行逻辑**********/

                    System.err.println("dataJson=" + dataJson);
                    // 将获取到的JSON转换为map
                    Map jsonMap = JSON.parseObject(dataJson, Map.class);
                    String id = jsonMap.get("id") == null ? null : jsonMap.get("id").toString();
                    String courierName = jsonMap.get("courierName") == null ? null : jsonMap.get("courierName").toString();
                    String orderId = jsonMap.get("orderId") == null ? null : jsonMap.get("orderId").toString();

                    SysOrder order = new SysOrder();
                    order.setId(Long.valueOf(orderId));
//                    order.setSendType("4");// 已回收
                    order.setSendFinishedTime(sdf.parse(getNowTime()));
                    order.setRecoverType("1");// 已回收
                    order.setRecoverTime(sdf.parse(getNowTime()));
                    order.setUpdateBy(courierName);
                    int flag = orderService.updateOrder(order);

                    if (flag > 0) {
                        result.setCode(0);// 0=成功
                        result.setMsg("回收成功!");
                        result.setData(flag);
                        result.setCount(flag);
                        return result;
                    } else {
                        result.setCode(1);// 1=失败
                        result.setMsg("回收失败");
                        result.setData(null);
                        result.setCount(0);
                        return result;
                    }

                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过一天时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }

    /**
     * 检测App最新版本号Api(接口)
     *
     * @return
     */
    @Log(title = "检测App最新版本号Api", businessType = BusinessType.INSERT)
    @PostMapping("/getAppVersion")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO getAppVersion(HttpServletRequest request, String dataJson) throws Exception {
        System.err.println("检测App最新版本号Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        String timeStamp = request.getHeader("timeStamp");
        String md5 = request.getHeader("MD5");
        /**********************接口请求合法性验证********************/
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(md5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(86400000);// 超时时间为1天
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过10秒
                if (Encryption.encrypt(timeStamp).equals(md5)) {
                    /**********开始执行逻辑**********/

                    System.err.println("dataJson=" + dataJson);

                    String type = "";

                    if (dataJson != null) {
                        try {
                            // 将获取到的JSON转换为map
                            Map jsonMap = JSON.parseObject(dataJson, Map.class);
                            type = jsonMap.get("type") == null ? null : jsonMap.get("type").toString();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    SysAppVersion appVersion = null;

                    if (type.equals("1")) {// type=1则进入助餐点App版本号查询
                        SysAppVersion bean = new SysAppVersion();
                        bean.setAppType("1");
                        appVersion = appVersionService.getNewestAppVersion(bean);
                    } else {// 否则进入送餐App版本号查询
                        SysAppVersion bean = new SysAppVersion();
                        bean.setAppType("0");
                        appVersion = appVersionService.getNewestAppVersion(bean);
                    }

                    System.err.println("appVersion = " + appVersion.toString());

                    if (appVersion != null) {
                        result.setCode(0);// 0=成功
                        result.setMsg("最新App版本号获取成功!");
                        result.setData(appVersion);
                        result.setCount(appVersion.toString().length());
                        return result;
                    } else {
                        result.setCode(1);// 0=成功
                        result.setMsg("未能成功获取最新App版本号!");
                        result.setData(null);
                        result.setCount(0);
                        return result;
                    }

                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过10秒时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }

    /**
     * 送餐App更新下载Api(接口)
     *
     * @return
     */
    @Log(title = "送餐App更新下载Api", businessType = BusinessType.INSERT)
    @GetMapping("/getApkPackage")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO getApkPackage(HttpServletResponse response, String dataJson) throws Exception {
        System.err.println("送餐App更新下载Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();

        /**********开始执行逻辑**********/

        // 下载本地文件
        String fileName = "asc.apk".toString(); // 文件的默认保存名
        // 读到流中
        InputStream inStream = new FileInputStream("/usr/local/apkpackage/" + fileName);// 文件的存放路径
        // 设置输出的格式
        response.reset();
        response.setContentType("bin");
        File file = new File("/usr/local/apkpackage/" + fileName);
        response.setHeader("Content-Length", "" + file.length());// 显示下载文件大小
        response.addHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
        // 循环取出流中的数据
        byte[] b = new byte[100];
        int len;
        try {
            while ((len = inStream.read(b)) > 0)
                response.getOutputStream().write(b, 0, len);
            inStream.close();
            response.getOutputStream().close();
        } catch (IOException e) {
            inStream.close();
            response.getOutputStream().close();
            e.printStackTrace();
            result.setCode(1);// 1=失败
            result.setMsg("最新App安装包获取失败!");
            result.setData(null);
            result.setCount(0);
            return result;
        } finally {
            try {
                inStream.close();
                response.getOutputStream().close();
            } catch (Exception e) {
                e.printStackTrace();
                result.setCode(1);// 1=失败
                result.setMsg("最新App安装包获取失败!");
                result.setData(null);
                result.setCount(0);
                return result;
            }
        }

        result.setCode(0);// 0=成功
        result.setMsg("最新App安装包获取成功!");
        result.setData(null);
        result.setCount(0);
        return result;

        /**********执行逻辑结束**********/

    }

    /**
     * 助餐点App更新下载Api(接口)
     *
     * @return
     */
    @Log(title = "助餐点App更新下载Api", businessType = BusinessType.INSERT)
    @GetMapping("/getZcdApkPackage")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO getZcdApkPackage(HttpServletResponse response, String dataJson) throws Exception {
        System.err.println("助餐点App更新下载Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();

        /**********开始执行逻辑**********/

        // 下载本地文件
        String fileName = "zcd.apk".toString(); // 文件的默认保存名
        // 读到流中
        InputStream inStream = new FileInputStream("/usr/local/apkpackage/" + fileName);// 文件的存放路径
        // 设置输出的格式
        response.reset();
        response.setContentType("bin");
        File file = new File("/usr/local/apkpackage/" + fileName);
        response.setHeader("Content-Length", "" + file.length());// 显示下载文件大小
        response.addHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
        // 循环取出流中的数据
        byte[] b = new byte[100];
        int len;
        try {
            while ((len = inStream.read(b)) > 0)
                response.getOutputStream().write(b, 0, len);
            inStream.close();
            response.getOutputStream().close();
        } catch (IOException e) {
            inStream.close();
            response.getOutputStream().close();
            e.printStackTrace();
            result.setCode(1);// 1=失败
            result.setMsg("最新App安装包获取失败!");
            result.setData(null);
            result.setCount(0);
            return result;
        } finally {
            try {
                inStream.close();
                response.getOutputStream().close();
            } catch (Exception e) {
                e.printStackTrace();
                result.setCode(1);// 1=失败
                result.setMsg("最新App安装包获取失败!");
                result.setData(null);
                result.setCount(0);
                return result;
            }
        }

        result.setCode(0);// 0=成功
        result.setMsg("最新App安装包获取成功!");
        result.setData(null);
        result.setCount(0);
        return result;

        /**********执行逻辑结束**********/

    }

    /******************************************** 助餐点Pad相关Api接口 ********************************************/

    /**
     * 助餐点老人注册(接口)
     *
     * @return
     */
    @Log(title = "助餐点老人注册Api", businessType = BusinessType.INSERT)
    @PostMapping("/zcdOldmanRegisterApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO zcdOldmanRegisterApi(HttpServletRequest request, String dataJson) throws Exception {
        System.err.println("助餐点老人注册Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        /**********************接口请求合法性验证********************/
//        Date date = new Date();
//        Long nowTime = date.getTime();
        String timeStamp = request.getHeader("timeStamp");
        String md5 = request.getHeader("MD5");
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(md5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(86400000);// 超时时间为1天
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过30秒
                if (Encryption.encrypt(timeStamp).equals(md5)) {
                    /**********开始执行逻辑**********/
                    System.err.println("dataJson=" + dataJson);
                    // 将获取到的JSON转换为map
                    Map jsonMap = JSON.parseObject(dataJson, Map.class);
                    String oldmanName = jsonMap.get("oldmanName") == null ? null : jsonMap.get("oldmanName").toString();
                    String idCard = jsonMap.get("idCard") == null ? null : jsonMap.get("idCard").toString();
                    String oldmanPhone = jsonMap.get("oldmanPhone") == null ? null : jsonMap.get("oldmanPhone").toString();
                    String oldmanAddress = jsonMap.get("oldmanAddress") == null ? null : jsonMap.get("oldmanAddress").toString();
                    // String imgBase64 = jsonMap.get("imgBase64") == null ? null : jsonMap.get("imgBase64").toString();

                    boolean registerFlag = false;

                    ZcdOldmanInfo oldmanRegisterFlag = zcdOldmanInfoService.checkIDCardUnique(idCard);

                    if (oldmanRegisterFlag != null) {
                        registerFlag = true;
                    }

                    if (!registerFlag) {// 当前身份证未注册过
                        ZcdOldmanInfo oldmanInfo = new ZcdOldmanInfo();
                        oldmanInfo.setOldmanName(oldmanName);
                        oldmanInfo.setIdCard(idCard);
                        oldmanInfo.setOldmanPhone(oldmanPhone);
                        oldmanInfo.setOldmanAddress(oldmanAddress);
                        // oldmanInfo.setImgBase64(imgBase64);
                        oldmanInfo.setStatus("0");// 老人状态为未删除

                        int success = zcdOldmanInfoService.saveOldman(oldmanInfo);

                        if (success > 0) {
                            result.setCode(0);// 0=成功
                            result.setMsg("新增助餐点老人成功!");
                            result.setData(oldmanInfo);
                            result.setCount(1);
                            return result;
                        } else {
                            System.out.println("新增助餐点老人失败");
                            result.setCode(1);// 1=失败
                            result.setMsg("新增助餐点老人失败");
                            return result;
                        }
                    } else {
                        System.out.println("当前助餐点老人身份证已经注册过了");
                        result.setCode(1);// 1=失败
                        result.setMsg("当前助餐点老人身份证已经注册过了");
                        return result;
                    }

                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过一天时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }

    /**
     * 助餐点老人改变人脸录入状态(接口)
     *
     * @return
     */
//    @Log(title = "助餐点老人改变人脸录入状态Api", businessType = BusinessType.INSERT)
//    @PostMapping("/zcdOldmanFaceRegistrationStatusApi")
//    @Transactional(rollbackFor = Exception.class)
//    @ResponseBody
//    public ResultDTO zcdOldmanFaceRegistrationStatusApi(HttpServletRequest request, String dataJson) throws Exception {
//        System.err.println("助餐点老人改变人脸录入状态Api，执行时间：" + df.format(new Date()));
//        ResultDTO result = new ResultDTO();
//
//        String timeStamp = request.getHeader("timeStamp");
//        String md5 = request.getHeader("MD5");
//
//        /**********************接口请求合法性验证********************/
//        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(md5)) {// 当全部参数不为空时才允许进入
//            Date date = new Date();
//            Long nowTime = date.getTime();
//            Long apiTime = Long.valueOf(timeStamp);
//            Long val = nowTime - apiTime;
//            Long timeEnd = new Long(86400000);// 超时时间为1天
//            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");
//
//            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过30秒
//                if (Encryption.encrypt(timeStamp).equals(md5)) {
//                    /**********开始执行逻辑**********/
//
//                    System.err.println("dataJson=" + dataJson);
//                    // 将获取到的JSON转换为map
//                    Map jsonMap = JSON.parseObject(dataJson, Map.class);
//                    String id = jsonMap.get("id") == null ? null : jsonMap.get("id").toString();
//                    String imgBase64 = jsonMap.get("imgBase64") == null ? null : jsonMap.get("imgBase64").toString();
//
//                    ZcdOldmanInfo oldmanInfo = new ZcdOldmanInfo();
//                    oldmanInfo.setId(Long.valueOf(id));
//                    oldmanInfo.setIsFaceRegistration("1");
//                    oldmanInfo.setImgBase64(imgBase64);
//
//                    try {
//                        zcdOldmanInfoService.updateOldmanById(oldmanInfo);
//                        result.setCode(0);// 0=成功
//                        result.setMsg("助餐点老人改变人脸录入状态成功!");
//                        result.setData(1);
//                        result.setCount(1);
//                        return result;
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                        result.setCode(1);
//                        result.setMsg("助餐点老人改变人脸录入状态失败");
//                        return result;
//                    }
//
//                    /**********执行逻辑结束**********/
//                } else {
//                    System.err.println("非法请求，已过滤【加密错误】");
//                    result.setCode(1);// 1=失败
//                    result.setMsg("非法请求，已过滤【加密错误】");
//                    return result;
//                }
//            } else {// 距离请求参数生成时间超过一天时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
//                System.err.println("非法请求，已过滤【超时错误】");
//                result.setCode(1);// 1=失败
//                result.setMsg("非法请求，已过滤【超时错误】");
//                return result;
//            }
//        } else {
//            System.err.println("非法请求，已过滤【参数错误】");
//            result.setCode(1);// 1=失败
//            result.setMsg("非法请求，已过滤【参数错误】");
//            return result;
//        }
//
//    }

    /**
     * 助餐点老人修改信息(接口)
     *
     * @return
     */
    @Log(title = "助餐点老人修改信息Api", businessType = BusinessType.INSERT)
    @PostMapping("/zcdOldmanUpdateInfoApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO zcdOldmanUpdateInfoApi(HttpServletRequest request, String dataJson) throws Exception {
        System.err.println("助餐点老人修改信息Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();

        String timeStamp = request.getHeader("timeStamp");
        String md5 = request.getHeader("MD5");

        /**********************接口请求合法性验证********************/
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(md5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(86400000);// 超时时间为1天
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过30秒
                if (Encryption.encrypt(timeStamp).equals(md5)) {
                    /**********开始执行逻辑**********/

                    System.err.println("dataJson=" + dataJson);
                    // 将获取到的JSON转换为map
                    Map jsonMap = JSON.parseObject(dataJson, Map.class);
                    String id = jsonMap.get("id") == null ? null : jsonMap.get("id").toString();
                    String oldmanName = jsonMap.get("oldmanName") == null ? null : jsonMap.get("oldmanName").toString();
                    String idCard = jsonMap.get("idCard") == null ? null : jsonMap.get("idCard").toString();
                    String oldmanPhone = jsonMap.get("oldmanPhone") == null ? null : jsonMap.get("oldmanPhone").toString();
                    String oldmanAddress = jsonMap.get("oldmanAddress") == null ? null : jsonMap.get("oldmanAddress").toString();

                    ZcdOldmanInfo oldmanInfo = new ZcdOldmanInfo();
                    oldmanInfo.setId(Long.valueOf(id));
                    oldmanInfo.setOldmanName(oldmanName);
                    oldmanInfo.setIdCard(idCard);
                    oldmanInfo.setOldmanPhone(oldmanPhone);
                    oldmanInfo.setOldmanAddress(oldmanAddress);

                    try {
                        zcdOldmanInfoService.updateOldmanById(oldmanInfo);
                        result.setCode(0);// 0=成功
                        result.setMsg("助餐点老人修改信息成功!");
                        result.setData(1);
                        result.setCount(1);
                        return result;
                    } catch (Exception e) {
                        e.printStackTrace();
                        result.setCode(1);
                        result.setMsg("助餐点老人修改信息失败");
                        return result;
                    }

                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过一天时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }

    /**
     * 获取助餐点老人列表(接口)
     *
     * @return
     */
    @Log(title = "获取助餐点老人列表Api", businessType = BusinessType.INSERT)
    @PostMapping("/getZcdOldmanInfoListApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO getZcdOldmanInfoListApi(HttpServletRequest request, String dataJson) throws Exception {
        System.err.println("获取助餐点老人列表Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        /**********************接口请求合法性验证********************/
//        Date date = new Date();
//        Long nowTime = date.getTime();
        String timeStamp = request.getHeader("timeStamp");
        String md5 = request.getHeader("MD5");
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(md5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(86400000);// 超时时间为1天
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过30秒
                if (Encryption.encrypt(timeStamp).equals(md5)) {
                    /**********开始执行逻辑**********/
                    System.err.println("dataJson=" + dataJson);
                    // 将获取到的JSON转换为map
                    Map jsonMap = JSON.parseObject(dataJson, Map.class);
                    String id = jsonMap.get("id") == null ? null : jsonMap.get("id").toString();
                    String oldmanName = jsonMap.get("oldmanName") == null ? null : jsonMap.get("oldmanName").toString();
                    // String idCard = jsonMap.get("idCard") == null ? null : jsonMap.get("idCard").toString();
                    // String oldmanPhone = jsonMap.get("oldmanPhone") == null ? null : jsonMap.get("oldmanPhone").toString();
                    // String oldmanAddress = jsonMap.get("oldmanAddress") == null ? null : jsonMap.get("oldmanAddress").toString();

                    ZcdOldmanInfo oldmanInfo = new ZcdOldmanInfo();
                    if (id != null) {
                        oldmanInfo.setId(Long.valueOf(id));
                    }

                    if (oldmanName != null) {
                        oldmanInfo.setOldmanName(oldmanName);
                    }

//                    if (idCard != null) {
//                        oldmanInfo.setIdCard(idCard);
//                    }
//
//                    if (oldmanPhone != null) {
//                        oldmanInfo.setOldmanPhone(oldmanPhone);
//                    }
//
//                    if (oldmanAddress != null) {
//                        oldmanInfo.setOldmanAddress(oldmanAddress);
//                    }

                    oldmanInfo.setStatus("0");// 老人状态为未删除

                    List<ZcdOldmanInfo> zcdOldmanInfoList = zcdOldmanInfoService.selectOldmanList(oldmanInfo);

                    result.setCode(0);// 0=成功
                    result.setMsg("获取助餐点老人列表成功!");
                    result.setData(zcdOldmanInfoList);
                    result.setCount(zcdOldmanInfoList.size());
                    return result;

                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过一天时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }

    /**
     * 助餐点菜单获取(接口)
     *
     * @return
     */
    @Log(title = "助餐点菜单获取Api", businessType = BusinessType.INSERT)
    @PostMapping("/getZcdFoodPackageApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO getZcdFoodPackageApi(HttpServletRequest request, String dataJson) throws Exception {
        System.err.println("助餐点菜单获取Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        /**********************接口请求合法性验证********************/
//        Date date = new Date();
//        Long nowTime = date.getTime();
        String timeStamp = request.getHeader("timeStamp");
        String md5 = request.getHeader("MD5");
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(md5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(86400000);// 超时时间为1天
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过30秒
                if (Encryption.encrypt(timeStamp).equals(md5)) {
                    /**********开始执行逻辑**********/
                    System.err.println("dataJson=" + dataJson);
                    // 将获取到的JSON转换为map
                    Map jsonMap = JSON.parseObject(dataJson, Map.class);
                    String startTime = jsonMap.get("startTime") == null ? null : jsonMap.get("startTime").toString();
                    String endTime = jsonMap.get("endTime") == null ? null : jsonMap.get("endTime").toString();

                    // String timeStr = getTimeInterval(new Date());
                    // String startTime = timeStr.substring(0, timeStr.indexOf(",")) + " 00:00:00";
                    // String endTime = timeStr.substring(timeStr.indexOf(",") + 1, timeStr.length()) + " 23:59:59";

                    ZcdFoodPackage foodPackage = new ZcdFoodPackage();

                    if (startTime != null && endTime != null) {
                        String startTime2 = startTime + " 00:00:00";
                        String endTime2 = endTime + " 23:59:59";
                        System.err.println("startTime = " + startTime2);
                        System.err.println("endTime = " + endTime2);
                        Map<String, Object> params = new HashMap<>();
                        params.put("beginTime", startTime);
                        params.put("endTime", endTime);
                        foodPackage.setParams(params);
                    }

                    List<ZcdFoodPackage> foodPackageList = zcdFoodPackageService.selectFoodPackageList(foodPackage);

                    if (startTime != null && endTime != null) {
                        foodPackageList.sort(Comparator.reverseOrder());// 使列表按ID从大到小排序
                    }

                    result.setCode(0);// 0=成功
                    result.setMsg("助餐点本星期的菜单查询获取成功!");
                    result.setData(foodPackageList);
                    result.setCount(foodPackageList.size());
                    return result;

                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过一天时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }

    /**
     * 助餐点菜单新增(接口)
     *
     * @return
     */
    @Log(title = "助餐点菜单新增Api", businessType = BusinessType.INSERT)
    @PostMapping("/addZcdFoodPackageApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO addZcdFoodPackageApi(HttpServletRequest request, String dataJson) throws Exception {
        System.err.println("助餐点菜单新增Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        /**********************接口请求合法性验证********************/
//        Date date = new Date();
//        Long nowTime = date.getTime();
        String timeStamp = request.getHeader("timeStamp");
        String md5 = request.getHeader("MD5");
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(md5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(86400000);// 超时时间为1天
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过30秒
                if (Encryption.encrypt(timeStamp).equals(md5)) {
                    /**********开始执行逻辑**********/
                    System.err.println("dataJson=" + dataJson);
                    // 将获取到的JSON转换为map
                    Map jsonMap = JSON.parseObject(dataJson, Map.class);
                    String foodPackageStr = jsonMap.get("foodPackageList") == null ? null : jsonMap.get("foodPackageList").toString();
                    String startTime = jsonMap.get("startTime") == null ? null : jsonMap.get("startTime").toString();
                    String endTime = jsonMap.get("endTime") == null ? null : jsonMap.get("endTime").toString();

                    startTime = startTime + " 00:00:00";
                    endTime = endTime + " 23:59:59";

                    System.err.println("startTime = " + startTime);
                    System.err.println("endTime = " + endTime);

                    Map<String, Object> params = new HashMap<>();
                    params.put("beginTime", startTime);
                    params.put("endTime", endTime);

                    ZcdFoodPackage zcdFoodPackage = new ZcdFoodPackage();
                    zcdFoodPackage.setParams(params);

                    List<ZcdFoodPackage> flag = zcdFoodPackageService.selectFoodPackageList(zcdFoodPackage);

                    /**
                     * 判断是否存在已经点过的菜单
                     */
                    if (flag.size() > 0) {// 如果存在已经点过的菜单
                        System.out.println("助餐点菜单新增失败,本星期已经存在菜单了");
                        result.setCode(1);// 1=失败
                        result.setMsg("助餐点菜单新增失败,本星期已经存在菜单了");
                        return result;
                    } else {// 如果不存在已经点过的菜单

                        List<ZcdFoodPackage> foodPackageList = JSONObject.parseArray(foodPackageStr, ZcdFoodPackage.class);

                        try {
                            zcdFoodPackageService.saveFoodPackageByBatch(foodPackageList);
                            result.setCode(0);// 0=成功
                            result.setMsg("助餐点菜单新增成功!");
                            result.setData(foodPackageList.size());
                            result.setCount(foodPackageList.size());
                            return result;
                        } catch (Exception e) {
                            e.printStackTrace();
                            System.out.println("助餐点菜单新增失败：" + e);
                            result.setCode(1);// 1=失败
                            result.setMsg("助餐点菜单新增失败：" + e);
                            return result;
                        }
                    }

                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过一天时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }

    /**
     * 助餐点老人的点菜单获取(接口)
     *
     * @return
     */
    @Log(title = "助餐点老人的点菜单获取Api", businessType = BusinessType.INSERT)
    @PostMapping("/getZcdFoodPackageOldmanApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO getZcdFoodPackageOldmanApi(HttpServletRequest request, String dataJson) throws Exception {
        System.err.println("助餐点老人的点菜单获取Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        /**********************接口请求合法性验证********************/
//        Date date = new Date();
//        Long nowTime = date.getTime();
        String timeStamp = request.getHeader("timeStamp");
        String md5 = request.getHeader("MD5");
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(md5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(86400000);// 超时时间为1天
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过30秒
                if (Encryption.encrypt(timeStamp).equals(md5)) {
                    /**********开始执行逻辑**********/
                    System.err.println("dataJson=" + dataJson);
                    Map jsonMap = JSON.parseObject(dataJson, Map.class);
                    String id = jsonMap.get("id") == null ? null : jsonMap.get("id").toString();

                    String timeStr = getTimeInterval(new Date());
                    String startTime = timeStr.substring(0, timeStr.indexOf(",")) + " 00:00:00";
                    String endTime = timeStr.substring(timeStr.indexOf(",") + 1, timeStr.length()) + " 23:59:59";

                    System.err.println("startTime = " + startTime);
                    System.err.println("endTime = " + endTime);

                    ZcdFoodPackageOldman foodPackageOldman = new ZcdFoodPackageOldman();
                    Map<String, Object> params = new HashMap<>();
                    params.put("beginTime", null);
                    params.put("endTime", null);
                    foodPackageOldman.setParams(params);
                    foodPackageOldman.setOldmanId(Long.valueOf(id));

                    List<ZcdFoodPackageOldman> foodPackageOldmanList = zcdFoodPackageOldmanService.selectFoodPackageOldmanList(foodPackageOldman);

                    result.setCode(0);// 0=成功
                    result.setMsg("助餐点本星期老人的点菜单获取成功!");
                    result.setData(foodPackageOldmanList);
                    result.setCount(foodPackageOldmanList.size());
                    return result;

                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过一天时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }

    /**
     * 助餐点老人当天菜单获取(接口)
     *
     * @return
     */
    @Log(title = "助餐点老人当天菜单获取Api", businessType = BusinessType.INSERT)
    @PostMapping("/getTodayZcdFoodPackageOldmanApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO getTodayZcdFoodPackageOldmanApi(HttpServletRequest request, String dataJson) throws Exception {
        System.err.println("助餐点老人当天菜单获取Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        /**********************接口请求合法性验证********************/
//        Date date = new Date();
//        Long nowTime = date.getTime();
        String timeStamp = request.getHeader("timeStamp");
        String md5 = request.getHeader("MD5");
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(md5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(86400000);// 超时时间为1天
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过30秒
                if (Encryption.encrypt(timeStamp).equals(md5)) {
                    /**********开始执行逻辑**********/
                    System.err.println("dataJson=" + dataJson);
                    Map jsonMap = JSON.parseObject(dataJson, Map.class);
                    String id = jsonMap.get("id") == null ? null : jsonMap.get("id").toString();

                    String startTime = getToday() + " 00:00:00";
                    String endTime = getToday() + " 23:59:59";

                    System.err.println("startTime = " + startTime);
                    System.err.println("endTime = " + endTime);

                    ZcdFoodPackageOldman foodPackageOldman = new ZcdFoodPackageOldman();
                    Map<String, Object> params = new HashMap<>();
                    params.put("beginTime", startTime);
                    params.put("endTime", endTime);
                    foodPackageOldman.setParams(params);
                    foodPackageOldman.setOldmanId(Long.valueOf(id));
                    foodPackageOldman.setStatus("0");// 只获取未取过餐的菜单

                    List<ZcdFoodPackageOldman> foodPackageOldmanList = zcdFoodPackageOldmanService.selectFoodPackageOldmanList(foodPackageOldman);

                    result.setCode(0);// 0=成功
                    result.setMsg("助餐点老人当天菜单获取成功!");
                    result.setData(foodPackageOldmanList);
                    result.setCount(foodPackageOldmanList.size());
                    return result;

                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过一天时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }

    /**
     * 助餐点老人的点菜单新增(接口)
     *
     * @return
     */
    @Log(title = "助餐点老人的点菜单新增Api", businessType = BusinessType.INSERT)
    @PostMapping("/addZcdFoodPackageOldmanApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO addZcdFoodPackageOldmanApi(HttpServletRequest request, String dataJson) throws Exception {
        System.err.println("助餐点老人的点菜单新增Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        /**********************接口请求合法性验证********************/
//        Date date = new Date();
//        Long nowTime = date.getTime();
        String timeStamp = request.getHeader("timeStamp");
        String md5 = request.getHeader("MD5");
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(md5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(86400000);// 超时时间为1天
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过30秒
                if (Encryption.encrypt(timeStamp).equals(md5)) {
                    /**********开始执行逻辑**********/
                    System.err.println("dataJson=" + dataJson);
                    // 将获取到的JSON转换为map
                    Map jsonMap = JSON.parseObject(dataJson, Map.class);
                    String foodPackageOldmanStr = jsonMap.get("foodPackageOldmanList") == null ? null : jsonMap.get("foodPackageOldmanList").toString();
                    String startTime = jsonMap.get("startTime") == null ? null : jsonMap.get("startTime").toString();
                    String endTime = jsonMap.get("endTime") == null ? null : jsonMap.get("endTime").toString();
                    String id = jsonMap.get("id") == null ? null : jsonMap.get("id").toString();

                    startTime = startTime + " 00:00:00";
                    endTime = endTime + " 23:59:59";

                    System.err.println("startTime = " + startTime);
                    System.err.println("endTime = " + endTime);

                    Map<String, Object> params = new HashMap<>();
                    params.put("beginTime", startTime);
                    params.put("endTime", endTime);

                    ZcdFoodPackageOldman zcdFoodPackageOldman = new ZcdFoodPackageOldman();
                    zcdFoodPackageOldman.setOldmanId(Long.valueOf(id));
                    zcdFoodPackageOldman.setParams(params);

                    List<ZcdFoodPackageOldman> flag = zcdFoodPackageOldmanService.selectFoodPackageOldmanList(zcdFoodPackageOldman);

                    /**
                     * 判断是否存在已经点过的菜单
                     */
                    if (flag.size() > 0) {// 如果存在已经点过的菜单
                        System.out.println("助餐点老人的点菜单新增失败,已经存在点过的菜单了");
                        result.setCode(1);// 1=失败
                        result.setMsg("助餐点老人的点菜单新增失败,已经存在点过的菜单了");
                        return result;
                    } else {// 如果不存在已经点过的菜单

                        List<ZcdFoodPackageOldman> foodPackageOldmanList = JSONObject.parseArray(foodPackageOldmanStr, ZcdFoodPackageOldman.class);

                        try {
                            zcdFoodPackageOldmanService.saveFoodPackageOldmanByBatch(foodPackageOldmanList);
                            result.setCode(0);// 0=成功
                            result.setMsg("助餐点老人的点菜单新增成功!");
                            result.setData(foodPackageOldmanList.size());
                            result.setCount(foodPackageOldmanList.size());
                            return result;
                        } catch (Exception e) {
                            e.printStackTrace();
                            System.out.println("助餐点老人的点菜单新增失败：" + e);
                            result.setCode(1);// 1=失败
                            result.setMsg("助餐点老人的点菜单新增失败：" + e);
                            return result;
                        }

                    }

                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过一天时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }

    /**
     * 助餐点修改老人点菜单状态(接口)
     *
     * @return
     */
    @Log(title = "助餐点修改老人点菜单状态Api", businessType = BusinessType.INSERT)
    @PostMapping("/updateZcdFoodPackageOldmanStatusApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO updateZcdFoodPackageOldmanStatusApi(HttpServletRequest request, String dataJson) throws Exception {
        System.err.println("助餐点修改老人点菜单状态Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        /**********************接口请求合法性验证********************/
//        Date date = new Date();
//        Long nowTime = date.getTime();
        String timeStamp = request.getHeader("timeStamp");
        String md5 = request.getHeader("MD5");
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(md5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(86400000);// 超时时间为1天
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过30秒
                if (Encryption.encrypt(timeStamp).equals(md5)) {
                    /**********开始执行逻辑**********/
                    System.err.println("dataJson=" + dataJson);
                    Map jsonMap = JSON.parseObject(dataJson, Map.class);
                    String id = jsonMap.get("id") == null ? null : jsonMap.get("id").toString();
                    String status = jsonMap.get("status") == null ? null : jsonMap.get("status").toString();

                    ZcdFoodPackageOldman foodPackageOldman = new ZcdFoodPackageOldman();
                    foodPackageOldman.setId(Long.valueOf(id));
                    foodPackageOldman.setStatus(status);

                    try {
                        zcdFoodPackageOldmanService.updateFoodPackageOldmanById(foodPackageOldman);
                        result.setCode(0);// 0=成功
                        result.setMsg("助餐点修改老人点菜单状态成功!");
                        result.setData(1);
                        result.setCount(1);
                        return result;
                    } catch (Exception e) {
                        e.printStackTrace();
                        result.setCode(1);
                        result.setMsg("助餐点修改老人点菜单状态失败");
                        result.setData(0);
                        result.setCount(0);
                        return result;
                    }

                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过一天时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }

    /******************************************** Pad控制端临时重新分配送餐员接口 ********************************************/

    /**
     * Pad控制端登录(接口)
     *
     * @return
     */
    @Log(title = "Pad控制端登录Api", businessType = BusinessType.INSERT)
    @PostMapping("/padLoginApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO padLoginApi(HttpServletRequest request, String dataJson) throws Exception {
        System.err.println("Pad控制端登录Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        String timeStamp = request.getHeader("timeStamp");
        String md5 = request.getHeader("MD5");
        /**********************接口请求合法性验证********************/
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(md5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(86400000);// 超时时间为1天
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过30秒
                if (Encryption.encrypt(timeStamp).equals(md5)) {
                    /**********开始执行逻辑**********/

                    System.err.println("dataJson=" + dataJson);
                    // 将获取到的JSON转换为map
                    Map jsonMap = JSON.parseObject(dataJson, Map.class);
                    String username = jsonMap.get("username") == null ? null : jsonMap.get("username").toString();
                    String password = jsonMap.get("password") == null ? null : jsonMap.get("password").toString();

                    UsernamePasswordToken token = new UsernamePasswordToken(username, password, false);
                    Subject subject = SecurityUtils.getSubject();
                    try {
                        subject.login(token);
                        result.setCode(0);// 0=成功
                        result.setMsg("用户登录成功!");
                        result.setData(success());
                        result.setCount(1);
                        return result;
                    } catch (AuthenticationException e) {
                        result.setCode(1);
                        result.setMsg("用户或密码错误");
                        return result;
                    }

                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过一天时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }

    /**
     * Pad控制端获取当日所有未完成的订单(接口)
     *
     * @return
     */
    @Log(title = "Pad控制端获取当日所有未完成的订单Api", businessType = BusinessType.INSERT)
    @PostMapping("/padGetOrdersApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO padGetOrdersApi(HttpServletRequest request, String dataJson) throws Exception {
        System.err.println("Pad控制端获取当日所有未完成的订单Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        String timeStamp = request.getHeader("timeStamp");
        String md5 = request.getHeader("MD5");
        /**********************接口请求合法性验证********************/
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(md5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(86400000);// 超时时间为1天
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过30秒
                if (Encryption.encrypt(timeStamp).equals(md5)) {
                    /**********开始执行逻辑**********/

                    System.err.println("dataJson=" + dataJson);

                    List<SysOrder> orderList = new ArrayList<>();
                    SysOrder order = new SysOrder();
                    order.setStatus(0);
                    order.setSendType("0");// 0待取餐，1待送餐，2送餐完成
                    Map<String, Object> params = new HashMap<>();
                    params.put("beginTime", getToday() + " 00:00:00");
                    params.put("endTime", getToday() + " 23:59:59");
                    order.setParams(params);

                    // order.setSendType(sendType);
                    // order.setOrderType(orderType);
                    // order.setRecoverType(recoverType);

                    orderList = orderService.selectTodayOrderList(order);// 得到今天的所有订单

                    // orderList.sort(Comparator.reverseOrder());// 使列表按ID从大到小排序

                    List<PadCourierBean> padCourierBeanList = new ArrayList<>();
                    List<SysOrder> so = null;
                    PadCourierBean pb = null;

                    // 将今天的所有订单根据转化为PadCourierBean列表
                    for (SysOrder o : orderList) {
                        pb = new PadCourierBean();
                        pb.setId(o.getSendCourierId());
                        pb.setCourierName(o.getSendCourierName());
                        pb.setTel(o.getTel());

                        so = new ArrayList<>();
                        so.add(o);
                        pb.setOrderList(so);
                        padCourierBeanList.add(pb);
                    }

                    /** 订单根据骑手ID去重且累加 **/
                    Map<Long, PadCourierBean> resultMap = new HashMap<>();
                    for (int i = 0; i < padCourierBeanList.size(); i++) {
                        PadCourierBean bean = padCourierBeanList.get(i);
                        long key = bean.getId();
                        PadCourierBean bean2 = resultMap.get(key);
                        //如果有存过
                        if (bean2 != null) {
                            //TODO 计算数量
                            List<SysOrder> oList = bean2.getOrderList();
                            oList.add(bean.getOrderList().get(0));
                            bean2.setOrderList(null);
                            bean2.setOrderList(oList);
                            resultMap.put(key, bean2);
                        } else {
                            resultMap.put(key, bean);
                        }
                    }
                    List<PadCourierBean> beanList = new ArrayList<>(resultMap.values());

                    result.setCode(0);// 0=成功
                    result.setMsg("获取订单信息成功!");
                    result.setData(beanList);
                    result.setCount(beanList.size());
                    return result;

                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过一天时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }

    /**
     * Pad控制端获取所有送餐员列表(接口)
     *
     * @return
     */
    @Log(title = "Pad控制端获取所有送餐员列表Api", businessType = BusinessType.INSERT)
    @PostMapping("/padGetCouriersApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO padGetCouriersApi(HttpServletRequest request, String dataJson) throws Exception {
        System.err.println("Pad控制端获取所有送餐员列表Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        String timeStamp = request.getHeader("timeStamp");
        String md5 = request.getHeader("MD5");
        /**********************接口请求合法性验证********************/
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(md5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(86400000);// 超时时间为1天
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过30秒
                if (Encryption.encrypt(timeStamp).equals(md5)) {
                    /**********开始执行逻辑**********/

                    System.err.println("dataJson=" + dataJson);

                    List<SysCourier> courierList = new ArrayList<>();
                    SysCourier courier = new SysCourier();
                    courier.setStatus("0");

                    courierList = courierService.selectCourierList(courier);

                    for (SysCourier c : courierList) {
                        c.setImgBase64(null);
                    }

                    result.setCode(0);// 0=成功
                    result.setMsg("Pad控制端获取所有送餐员列表成功!");
                    result.setData(courierList);
                    result.setCount(courierList.size());
                    return result;

                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过一天时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }

    /**
     * Pad控制端批量重新分配送餐员(接口)
     *
     * @return
     */
    @Log(title = "Pad控制端批量重新分配送餐员Api", businessType = BusinessType.INSERT)
    @PostMapping("/padUpdateOrderSendersApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO padUpdateOrderSendersApi(HttpServletRequest request, String dataJson) throws Exception {
        System.err.println("Pad控制端批量重新分配送餐员Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        String timeStamp = request.getHeader("timeStamp");
        String md5 = request.getHeader("MD5");
        /**********************接口请求合法性验证********************/
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(md5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(86400000);// 超时时间为1天
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过30秒
                if (Encryption.encrypt(timeStamp).equals(md5)) {
                    /**********开始执行逻辑**********/

                    System.err.println("dataJson=" + dataJson);
                    // 将获取到的JSON转换为map
                    Map jsonMap = JSON.parseObject(dataJson, Map.class);
                    String sendCourierId = jsonMap.get("sendCourierId") == null ? null : jsonMap.get("sendCourierId").toString();
                    String sendCourierName = jsonMap.get("sendCourierName") == null ? null : jsonMap.get("sendCourierName").toString();
                    String ids = jsonMap.get("ids") == null ? null : jsonMap.get("ids").toString();

                    SysOrder order = new SysOrder();
                    order.setSendCourierId(Long.valueOf(sendCourierId));
                    order.setSendCourierName(sendCourierName);
                    order.setRecoverCourierId(Long.valueOf(sendCourierId));
                    order.setRecoverCourierName(sendCourierName);
                    order.setIds(ids);

                    int success = orderService.updateOrderSenders(order);

                    if (success > 0) {
                        result.setCode(0);// 0=成功
                        result.setMsg("Pad控制端批量重新分配送餐员成功!");
                        result.setData(success);
                        result.setCount(success);
                        return result;
                    } else {
                        result.setCode(1);
                        result.setMsg("Pad控制端批量重新分配送餐员失败");
                        return result;
                    }

                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过一天时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }

    /*************************************************************************************/
    /*************************************************************************************/
    /*************************************************************************************/
    /*************************************************************************************/
    /*************************************************************************************/
    /*************************************************************************************/
    /*************************************************************************************/
    /*************************************************************************************/
    /*************************************************************************************/
    /************************************** 额外接口 **************************************/

    /**
     * PHP加密Eihoo账号密码Api(接口)
     *
     * @return
     */
    @Log(title = "测试Api", businessType = BusinessType.INSERT)
    @PostMapping("/encryptEihooApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO encryptEihooApi(String uname, String password) throws Exception {
        System.err.println("PHP加密Eihoo账号密码Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        /**********开始执行逻辑**********/
        Map<String, String> resultMap = new HashMap<>();
        if (StringUtils.isNotBlank(uname) && StringUtils.isNotBlank(password)) {
            byte[] realkeyUsername = UserEncrypt.encryptHashCode((uname + password).hashCode());
            byte[] realkeyPassword = UserEncrypt.encryptHashCode((password + uname).hashCode());
            try {
                String userName = UserEncrypt.md5_16bytes(uname);
                String encryptedUsername = UserEncrypt.aesEncrypt(uname, realkeyUsername);
                String encryptedPassword = UserEncrypt.aesEncrypt(password, realkeyPassword);
                System.out.println(userName + " : " + encryptedUsername + " : " + encryptedPassword);
                resultMap.put("encryptedUsernameFor16", userName);
                resultMap.put("encryptedUsername", encryptedUsername);
                resultMap.put("encryptedPassword", encryptedPassword);
            } catch (Exception e) {
                e.printStackTrace();
            }

            result.setCode(0);// 0=成功
            result.setMsg("调用成功!");
            result.setData(resultMap);
            result.setCount(resultMap.size());
            return result;
        } else {
            result.setCode(1);// 1=失败
            result.setMsg("执行失败，参数为空");
            return result;
        }

        /**********执行逻辑结束**********/
    }

    /**
     * 导出送餐订单Excel统计表格(接口)
     *
     * @return
     */
    @RequestMapping(value = "/exportExcel", method = RequestMethod.GET)
    public ResultDTO exportExcel(HttpServletRequest request, HttpServletResponse res, String startTime, String endTime, String type) throws Exception {
        System.err.println("导出送餐订单Excel统计表格Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();

        /**********开始执行逻辑**********/

        if (StringUtils.isNotBlank(startTime) && StringUtils.isNotBlank(endTime) || type.equals("1")) {
            startTime = startTime + " 00:00:00";
            endTime = endTime + " 23:59:59";

            System.err.println("startTime = " + startTime);
            System.err.println("endTime = " + endTime);

            Map<String, Object> params = new HashMap<>();
            params.put("beginTime", startTime);
            params.put("endTime", endTime);

            List<SysOrder> orderList = new ArrayList<>();

            if (type.equals("0")) {
                // 数据查看范围权限(要放在分页startPage();前面，否则只能查询出10个)
                SysOrder orderParam = new SysOrder();
                orderParam.setParams(params);
                orderParam.setStatus(0);

                SysCommunity s = new SysCommunity();
                s.setStatus("0");
                List<SysCommunity> communityList = communityService.selectCommunityList(s);
                CommunityGetKeyIDs getKeyIDs = new CommunityGetKeyIDs();
                orderParam.setKeyIDs(getKeyIDs.communityGetKeyIDs(communityList));

                orderList = orderService.selectOrderList(orderParam);
            } else if (type.equals("1")) {
                SysOrderPreview orderPreview = new SysOrderPreview();
                // 数据查看范围权限(要放在分页startPage();前面，否则只能查询出10个)
                SysCommunity s = new SysCommunity();
                s.setStatus("0");
                List<SysCommunity> communityList = communityService.selectCommunityList(s);
                CommunityGetKeyIDs getKeyIDs = new CommunityGetKeyIDs();
                orderPreview.setKeyIDs(getKeyIDs.communityGetKeyIDs(communityList));
                orderPreview.setStatus(0);

                List<SysOrderPreview> list = orderPreviewService.selectOrderList2(orderPreview);
                SysOrder order = null;

                for (SysOrderPreview o : list) {
                    order = new SysOrder();
                    order.setUserCode(o.getUserCode());
                    order.setUserName(o.getUserName());
                    order.setMobile(o.getMobile());
                    order.setCommunityName(o.getCommunityName());
                    order.setAddress(o.getAddress());
                    orderList.add(order);
                }

            }

            try {
                String fileName = "";
                // 生成所选中的详情生成其Excel到本地文件夹
                if (orderList.size() > 0) {
                    ExcelExport e = new ExcelExport();
                    List<SysOrderExport> eList = e.toGetSendOrderList(orderList, type);
                    eList.sort(Comparator.reverseOrder());// 使列表按ID从大到小排序
                    fileName = ExcelExport.toSendOrderExcelExport(eList, startTime, endTime, type);
                    // 将上面创建的Excel作为下载文件给用户
                    res.setHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes(), "iso-8859-1"));
                    res.setContentType("application/octet-stream;charset=UTF-8");
                    byte[] buff = new byte[1024];
                    BufferedInputStream bis = null;
                    OutputStream os = null;
                    try {
                        os = res.getOutputStream();
                        bis = new BufferedInputStream(new FileInputStream(new File("/usr/local/excelExport/" + fileName)));
                        int i = bis.read(buff);
                        while (i != -1) {
                            os.write(buff, 0, buff.length);
                            os.flush();
                            i = bis.read(buff);
                        }
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } finally {
                        if (bis != null) {
                            try {
                                bis.close();
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }
                        }
                    }

                    result.setCode(0);// 0=成功
                    result.setMsg("导出送餐订单Excel统计表格成功!");
                    result.setData(1);
                    result.setCount(1);
                    return result;

                } else {

                    result.setCode(1);
                    result.setMsg("导出送餐订单Excel统计表格失败(所选择条件内未查询到任何信息)");
                    return result;

                }

            } catch (Exception e) {
                e.printStackTrace();
                result.setCode(1);
                result.setMsg("导出送餐订单Excel统计表格失败(接口异常)");
                return result;
            } finally {
                System.err.println("==========================调用导出的Excel下载接口结束============================");
            }
        } else {
            result.setCode(1);
            result.setMsg("导出送餐订单Excel统计表格失败(缺少时间范围参数)");
            return result;
        }

        /**********执行逻辑结束**********/

    }

    /************************************* 测试接口模板 **************************************/

    /**
     * 测试Api(接口)
     *
     * @return
     */
    @Log(title = "测试Api", businessType = BusinessType.INSERT)
    @PostMapping("/testApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO testApi(String timeStamp, String MD5, String dataJson) throws Exception {
        System.err.println("验证送餐员登录Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        /**********************接口请求合法性验证********************/
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(MD5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(10000);// 超时时间为10s
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过10秒
                if (Encryption.encrypt(timeStamp).equals(MD5)) {
                    /**********开始执行逻辑**********/

                    System.err.println("dataJson=" + dataJson);
                    // 将获取到的JSON转换为map
                    Map jsonMap = JSON.parseObject(dataJson, Map.class);
                    String keyWord = jsonMap.get("keyWord") == null ? null : jsonMap.get("keyWord").toString();
                    System.err.println("keyWord=" + keyWord);

                    result.setCode(0);// 0=成功
                    result.setMsg("调用成功!");
                    result.setData(JSON.toJSONString("调用成功"));
                    result.setCount(JSON.toJSONString("调用成功").length());
                    return result;
                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过10秒时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }

    /**
     * 获取timeStamp和MD5(接口)
     *
     * @return
     */
    @Log(title = "获取timeStamp和MD5", businessType = BusinessType.INSERT)
    @PostMapping("/getTokenApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO getTokenApi() {
        System.err.println("获取timeStamp和MD5，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        Date date = new Date();
        Long nowTime = date.getTime();
        String md5Str = Encryption.encrypt(String.valueOf(nowTime));
        Map<String, String> resultMap = new HashMap<>();
        resultMap.put("timeStamp", String.valueOf(nowTime));
        resultMap.put("MD5", md5Str);
        result.setCode(0);// 0=成功
        result.setMsg("调用成功!");
        result.setData(resultMap);
        result.setCount(resultMap.size());
        return result;
    }

    /****************************** 公共方法 ******************************/

    public Map getMealCountAndList(Map resultMap, List<SysOrder> orderList, List<SysOrder> recoverOrderList, SysCourier courier) {
        int meal = 0; //早餐数量
        int waitingForTakeMealCount = 0;// 早餐待取餐
        int waitingForSendMealCount = 0;// 早餐待送餐
        int deliveryFinishedCount = 0;// 早餐已送达
        int revocerCount = 0; // 早餐待回收

        int meal2 = 0; //午餐数量
        int waitingForTakeMealCount2 = 0;// 午餐待取餐
        int waitingForSendMealCount2 = 0;// 午餐待送餐
        int deliveryFinishedCount2 = 0;// 午餐已送达
        int revocerCount2 = 0; // 午餐待回收

        int meal3 = 0; //晚餐数量
        int waitingForTakeMealCount3 = 0;// 晚餐待取餐
        int waitingForSendMealCount3 = 0;// 晚餐待送餐
        int deliveryFinishedCount3 = 0;// 晚餐已送达
        int revocerCount3 = 0; // 晚餐待回收

        for (SysOrder s : recoverOrderList) {
            if (s.getRecoverType().equals("0") && s.getOrderType().equals("0")) {
                revocerCount++;
            }
            if (s.getRecoverType().equals("0") && s.getOrderType().equals("1")) {
                revocerCount2++;
            }
            if (s.getRecoverType().equals("0") && s.getOrderType().equals("2")) {
                revocerCount3++;
            }
        }

        Boolean flag1 = false;
        Boolean flag2 = false;
        Boolean flag3 = false;

        for (SysOrder s : orderList) {
            if (s.getOrderType().equals("0") && s.getSendType().equals("1")) {
                meal++;
            }
            if (s.getSendType().equals("0") && s.getOrderType().equals("0")) {
                waitingForTakeMealCount++;
            }
            if (s.getSendType().equals("1") && s.getOrderType().equals("0")) {
                waitingForSendMealCount++;
            }
            if (s.getSendType().equals("2") && s.getOrderType().equals("0")) {
                deliveryFinishedCount++;
            }

            if (s.getOrderType().equals("1") && s.getSendType().equals("1")) {
                meal2++;
            }
            if (s.getSendType().equals("0") && s.getOrderType().equals("1")) {
                waitingForTakeMealCount2++;
            }
            if (s.getSendType().equals("1") && s.getOrderType().equals("1")) {
                waitingForSendMealCount2++;
            }
            if (s.getSendType().equals("2") && s.getOrderType().equals("1")) {
                deliveryFinishedCount2++;
            }

            if (s.getOrderType().equals("3") && s.getSendType().equals("1")) {
                meal3++;
            }
            if (s.getSendType().equals("0") && s.getOrderType().equals("2")) {
                waitingForTakeMealCount3++;
            }
            if (s.getSendType().equals("1") && s.getOrderType().equals("2")) {
                waitingForSendMealCount3++;
            }
            if (s.getSendType().equals("2") && s.getOrderType().equals("2")) {
                deliveryFinishedCount3++;
            }

            /******************************************/
            if (s.getOrderType().equals("0")) {
                if (s.getSendType().equals("0")) {
                    flag1 = true;
                }
            }
            if (s.getOrderType().equals("1")) {
                if (s.getSendType().equals("0")) {
                    flag2 = true;
                }
            }
            if (s.getOrderType().equals("2")) {
                if (s.getSendType().equals("0")) {
                    flag3 = true;
                }
            }

        }

        resultMap.put("meal", String.valueOf(meal));// 早餐数量
        resultMap.put("waitingForTakeMeal", String.valueOf(waitingForTakeMealCount));// 早餐待取餐数量
        resultMap.put("waitingForSendMeal", String.valueOf(waitingForSendMealCount));// 早餐待送餐数量
        resultMap.put("deliveryFinished", String.valueOf(deliveryFinishedCount));// 早餐已完成送餐数量
        resultMap.put("recover", String.valueOf(revocerCount));// 早餐待回收数量

        resultMap.put("meal2", String.valueOf(meal2));// 午餐数量
        resultMap.put("waitingForTakeMeal2", String.valueOf(waitingForTakeMealCount2));// 午餐待取餐数量
        resultMap.put("waitingForSendMeal2", String.valueOf(waitingForSendMealCount2));// 午餐待送餐数量
        resultMap.put("deliveryFinished2", String.valueOf(deliveryFinishedCount2));// 午餐已完成送餐数量
        resultMap.put("recover2", String.valueOf(revocerCount2));// 午餐待回收数量

        resultMap.put("meal3", String.valueOf(meal3));// 晚餐数量
        resultMap.put("waitingForTakeMeal3", String.valueOf(waitingForTakeMealCount3));// 晚餐待取餐数量
        resultMap.put("waitingForSendMeal3", String.valueOf(waitingForSendMealCount3));// 晚餐待送餐数量
        resultMap.put("deliveryFinished3", String.valueOf(deliveryFinishedCount3));// 晚餐已完成送餐数量
        resultMap.put("recover3", String.valueOf(revocerCount3));// 晚餐待回收数量

        resultMap.put("id", String.valueOf(courier.getId()));
        resultMap.put("courierName", courier.getCourierName());
        resultMap.put("isFaceRegistration", courier.getIsFaceRegistration());

        resultMap.put("flag1", flag1);// 早餐是否需要签到,true = 需要签到
        resultMap.put("flag2", flag2);// 午餐是否需要签到,true = 需要签到
        resultMap.put("flag3", flag3);// 晚餐是否需要签到,true = 需要签到

        return resultMap;
    }

    /******************************************************** 【工具方法】 ********************************************************/

    /**
     * 获取当前年份
     *
     * @param
     * @return
     */
    public int getYear() {
        Calendar c = Calendar.getInstance(Locale.CHINA);//
        return c.get(Calendar.YEAR); // 获取当前年份
    }

    /**
     * 获取当前月份
     *
     * @param
     * @return
     */
    public int getMonth() {
        Calendar c = Calendar.getInstance(Locale.CHINA);//
        return c.get(Calendar.MONTH) + 1; //
    }

    /**
     * 获取当前月份的天数
     *
     * @param
     * @return
     */
    public int getDay() {
        Calendar a = Calendar.getInstance();
        a.set(Calendar.DATE, 1);
        a.roll(Calendar.DATE, -1);
        return a.get(Calendar.DATE);
    }

    /**
     * 返回今天
     *
     * @return
     */
    public String getToday() {
        Date date = new Date();//取时间
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(calendar.DATE, 0);
        date = calendar.getTime(); //这个时间就是日期往后推一天的结果
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(date);
        return dateString;
    }

    /**
     * 返回昨天
     *
     * @return
     */
    public String getYesterday() {
        Date date = new Date();//取时间
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(calendar.DATE, -1);
        date = calendar.getTime(); //这个时间就是日期往后推一天的结果
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(date);
        return dateString;
    }

    /**
     * 返回当前时间
     *
     * @return
     */
    public String getNowTime() {
        Date date = new Date();//取时间
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(calendar.DATE, 0);
        date = calendar.getTime(); //这个时间就是日期往后推一天的结果
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(date);
        return dateString;
    }

    /**
     * 获取当前日期获得所在周的日期区间（周一和周日日期）
     *
     * @param date
     * @return
     */
    public String getTimeInterval(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        // 判断要计算的日期是否是周日，如果是则减一天计算周六的，否则会出问题，计算到下一周去了
        int dayWeek = cal.get(Calendar.DAY_OF_WEEK);// 获得当前日期是一个星期的第几天
        if (1 == dayWeek) {
            cal.add(Calendar.DAY_OF_MONTH, -1);
        }
        // System.out.println("要计算日期为:" + sdf.format(cal.getTime())); // 输出要计算日期
        // 设置一个星期的第一天，按中国的习惯一个星期的第一天是星期一
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        // 获得当前日期是一个星期的第几天
        int day = cal.get(Calendar.DAY_OF_WEEK);
        // 根据日历的规则，给当前日期减去星期几与一个星期第一天的差值
        cal.add(Calendar.DATE, cal.getFirstDayOfWeek() - day);
        String imptimeBegin = sdf2.format(cal.getTime());
        // System.out.println("所在周星期一的日期：" + imptimeBegin);
        cal.add(Calendar.DATE, 6);
        String imptimeEnd = sdf2.format(cal.getTime());
        // System.out.println("所在周星期日的日期：" + imptimeEnd);
        return imptimeBegin + "," + imptimeEnd;
    }

}