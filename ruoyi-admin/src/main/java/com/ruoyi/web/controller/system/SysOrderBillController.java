package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.base.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.framework.web.base.BaseController;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.quartz.util.Encryption;
import com.ruoyi.system.domain.SysCommunity;
import com.ruoyi.system.domain.SysFoodPackage;
import com.ruoyi.system.domain.SysOldman;
import com.ruoyi.system.domain.SysOrderBill;
import com.ruoyi.system.service.*;
import com.ruoyi.web.controller.tool.CommunityGetKeyIDs;
import com.sun.jna.platform.win32.OaIdl;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 老人管理调用层
 *
 * @author ruoyi
 */
@Controller
@RequestMapping("/system/bill")
public class SysOrderBillController extends BaseController {
    private String prefix = "system/bill";

    @Autowired
    private ISysOldManService oldManService;

    @Autowired
    private ISysRoleService roleService;

    @Autowired
    private ISysPostService postService;

    @Autowired
    private ISysOrderBillService orderBillService;

    @Autowired
    private ISysFoodPackageService foodPackageService;

    @Autowired
    private ISysCommunityService communityService;

    private SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");

    /**
     * 点击本月账单页面
     *
     * @return
     */
    @RequiresPermissions("system:bill:view")
    @GetMapping()
    public String user() {
        return prefix + "/thisMonthBill";
    }

    /**
     * 分页查询当月账单列表
     *
     * @param sysOldman
     * @return
     */
    @RequiresPermissions("system:bill:list")
    @PostMapping("/thisMonthList")
    @ResponseBody
    public TableDataInfo thisMonthList(SysOldman sysOldman) {

        // 数据查看范围权限(要放在分页startPage();前面，否则只能查询出10个)
        SysCommunity s = new SysCommunity();
        s.setStatus("0");
        List<SysCommunity> communityList = communityService.selectCommunityList(s);
        CommunityGetKeyIDs getKeyIDs = new CommunityGetKeyIDs();

        startPage();
        SysOrderBill orderBill = new SysOrderBill();
        if (StringUtils.isBlank(sysOldman.getStatus())) {
            sysOldman.setStatus("0");
            orderBill.setStatus("0");
        }

        Map<String, Object> params = new HashMap<>();
        params.put("beginTime", getYear() + "-" + getMonth() + "-01" + " 00:00:00");// 月头
        params.put("endTime", getYear() + "-" + getMonth() + "-" + getDay() + " 23:59:59");// 月尾
        orderBill.setParams(params);

        orderBill.setMobile(sysOldman.getMobile());
        orderBill.setUserName(sysOldman.getUserName());
        orderBill.setAddress(sysOldman.getAddress());
        orderBill.setCommunityId(sysOldman.getCommunityId());

        orderBill.setPrepaidStatus(sysOldman.getPrepaidStatus());
        orderBill.setIsSettle(sysOldman.getIsSettle());

        orderBill.setKeyIDs(getKeyIDs.communityGetKeyIDs(communityList));

        List<SysOrderBill> list = orderBillService.selectOrderBillList(orderBill);
        return getDataTable(statistics(list));
    }

    /******************************************************************************************************************/

    /**
     * 点击下月预付账单页面
     *
     * @return
     */
    @RequiresPermissions("system:bill:view")
    @GetMapping("/nextBill")
    public String nextBill() {
        return prefix + "/nextMonthBill";
    }

    /**
     * 分页查询下月预付账单列表
     *
     * @param sysOldman
     * @return
     */
    @RequiresPermissions("system:bill:next")
    @PostMapping("/nextMonthList")
    @ResponseBody
    public TableDataInfo nextMonthList(SysOldman sysOldman) {
        // 数据查看范围权限(要放在分页startPage();前面，否则只能查询出10个)
        SysCommunity s = new SysCommunity();
        s.setStatus("0");
        List<SysCommunity> communityList = communityService.selectCommunityList(s);
        CommunityGetKeyIDs getKeyIDs = new CommunityGetKeyIDs();

        startPage();
        SysOrderBill orderBill = new SysOrderBill();
        if (StringUtils.isBlank(sysOldman.getStatus())) {
            sysOldman.setStatus("0");
            orderBill.setStatus("0");
        }

        Map<String, Object> params = new HashMap<>();
        params.put("beginTime", getPerFirstDayOfMonth() + " 00:00:00");// 下月头
        params.put("endTime", getLastMaxMonthDate() + " 23:59:59");// 下月尾
        orderBill.setParams(params);

        orderBill.setMobile(sysOldman.getMobile());
        orderBill.setUserName(sysOldman.getUserName());
        orderBill.setAddress(sysOldman.getAddress());
        orderBill.setCommunityId(sysOldman.getCommunityId());

        orderBill.setPrepaidStatus(sysOldman.getPrepaidStatus());
        orderBill.setIsSettle(sysOldman.getIsSettle());

        orderBill.setKeyIDs(getKeyIDs.communityGetKeyIDs(communityList));

        List<SysOrderBill> list = orderBillService.selectOrderBillList(orderBill);
        return getDataTable(statistics(list));
    }

    /******************************************************************************************************************/

    /**
     * 点击所有账单页面
     *
     * @return
     */
    @RequiresPermissions("system:bill:view")
    @GetMapping("/allBill")
    public String all() {
        return prefix + "/allBill";
    }

    /**
     * 分页查询所有账单列表
     *
     * @param sysOldman
     * @return
     */
    @RequiresPermissions("system:bill:list")
    @PostMapping("/allBillList")
    @ResponseBody
    public TableDataInfo allList(SysOldman sysOldman) {
        // 数据查看范围权限(要放在分页startPage();前面，否则只能查询出10个)
        SysCommunity s = new SysCommunity();
        s.setStatus("0");
        List<SysCommunity> communityList = communityService.selectCommunityList(s);
        CommunityGetKeyIDs getKeyIDs = new CommunityGetKeyIDs();

        startPage();
        SysOrderBill orderBill = new SysOrderBill();
        if (StringUtils.isBlank(sysOldman.getStatus())) {
            sysOldman.setStatus("0");
            orderBill.setStatus("0");
        }

        orderBill.setParams(sysOldman.getParams());

        orderBill.setMobile(sysOldman.getMobile());
        orderBill.setUserName(sysOldman.getUserName());
        orderBill.setAddress(sysOldman.getAddress());
        orderBill.setCommunityId(sysOldman.getCommunityId());

        orderBill.setPrepaidStatus(sysOldman.getPrepaidStatus());
        orderBill.setIsSettle(sysOldman.getIsSettle());

        orderBill.setKeyIDs(getKeyIDs.communityGetKeyIDs(communityList));

        List<SysOrderBill> list = orderBillService.selectOrderBillList(orderBill);
        return getDataTable(statistics(list));
    }

    /******************************************************************************************************************/

    /**
     * 点击待退款账单页面
     *
     * @return
     */
    @RequiresPermissions("system:bill:waitingForRefund")
    @GetMapping("/waitingForRefund")
    public String waitingForRefund() {
        return prefix + "/waitingForRefundBill";
    }

    /**
     * 分页查询所有待退款账单列表
     *
     * @param sysOldman
     * @return
     */
    @RequiresPermissions("system:bill:waitingForRefund")
    @PostMapping("/waitingForRefundBillList")
    @ResponseBody
    public TableDataInfo waitingForRefundList(SysOldman sysOldman) {
        // 数据查看范围权限(要放在分页startPage();前面，否则只能查询出10个)
        SysCommunity s = new SysCommunity();
        s.setStatus("0");
        List<SysCommunity> communityList = communityService.selectCommunityList(s);
        CommunityGetKeyIDs getKeyIDs = new CommunityGetKeyIDs();

        startPage();
        SysOrderBill orderBill = new SysOrderBill();
        if (StringUtils.isBlank(sysOldman.getStatus())) {
            sysOldman.setStatus("0");
            orderBill.setStatus("0");
        }

        orderBill.setIsSettle("2");// 待退款状态

        orderBill.setParams(sysOldman.getParams());

        orderBill.setMobile(sysOldman.getMobile());
        orderBill.setUserName(sysOldman.getUserName());
        orderBill.setAddress(sysOldman.getAddress());
        orderBill.setCommunityId(sysOldman.getCommunityId());

        orderBill.setKeyIDs(getKeyIDs.communityGetKeyIDs(communityList));

        List<SysOrderBill> list = orderBillService.selectOrderBillList(orderBill);
        return getDataTable(statistics(list));
    }

    /******************************************************************************************************************/

    /**
     * 修改账单信息 跳转修改页面
     *
     * @param idStr
     * @param mmap
     * @return
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String idStr, ModelMap mmap) {
        String idStrDecode = URLDecoder.decode(idStr);
        String key = idStrDecode.substring(idStrDecode.indexOf("KKKEEEYYY") + 9, idStrDecode.length());
        String id = idStrDecode.substring(0, idStrDecode.indexOf("KKKEEEYYY"));

        String foodInfo = "未选择";
        String foodInfo2 = "未选择";
        String foodInfo3 = "未选择";
        String realFoodInfo = "未选择套餐，无法计算";
        String historyFoodInfo = "未选择套餐，无法计算";
        String realFoodInfo2 = "未选择套餐，无法计算";
        String historyFoodInfo2 = "未选择套餐，无法计算";
        String realFoodInfo3 = "未选择套餐，无法计算";
        String historyFoodInfo3 = "未选择套餐，无法计算";
        BigDecimal realMoney = BigDecimal.valueOf(0);
        BigDecimal historyMoney = BigDecimal.valueOf(0);

        SysOrderBill orderBill = new SysOrderBill();
        orderBill = orderBillService.selectOrderBillById(Long.valueOf(id));

        if (key.equals("3")) {//如果为下月预付账单
            SysFoodPackage foodPackage = new SysFoodPackage();
            if (orderBill.getOldman().getFoodPackageNextMonth() != 0) {// 早餐
                foodPackage = foodPackageService.selectFoodPackageById(orderBill.getOldman().getFoodPackageNextMonth());
                foodInfo = foodPackage.getFoodPackageName() + "：单份 " + foodPackage.getMoney() + " 元" + "； 备注：" + foodPackage.getRemark() + "； ";
                // 计算实际消费金额
                String[] splitStr;
                if (StringUtils.isNotBlank(orderBill.getRealDateCheck())) {
                    splitStr = orderBill.getRealDateCheck().split(",");
                    realFoodInfo = "预付：" + splitStr.length + " * " + foodPackage.getMoney() + " = " + (foodPackage.getMoney().multiply(BigDecimal.valueOf(splitStr.length))) + " 元";
                    realMoney = realMoney.add(foodPackage.getMoney().multiply(BigDecimal.valueOf(splitStr.length)));
                }
                // 计算预付金额
                if (StringUtils.isNotBlank(orderBill.getHistoryDateCheck())) {
                    splitStr = orderBill.getHistoryDateCheck().split(",");
                    historyFoodInfo = "实际消费：" + splitStr.length + " * " + foodPackage.getMoney() + " = " + (foodPackage.getMoney().multiply(BigDecimal.valueOf(splitStr.length))) + " 元";
                    historyMoney = historyMoney.add(foodPackage.getMoney().multiply(BigDecimal.valueOf(splitStr.length)));
                }
            }

            foodPackage = null;
            if (orderBill.getOldman().getFoodPackage2NextMonth() != 0) {// 午餐
                foodPackage = foodPackageService.selectFoodPackageById(orderBill.getOldman().getFoodPackage2NextMonth());
                foodInfo2 = foodPackage.getFoodPackageName() + "：单份 " + foodPackage.getMoney() + " 元" + "； 备注：" + foodPackage.getRemark() + "； ";
                // 计算实际消费金额
                String[] splitStr;
                if (StringUtils.isNotBlank(orderBill.getRealDateCheck2())) {
                    splitStr = orderBill.getRealDateCheck2().split(",");
                    realFoodInfo2 = "预付：" + splitStr.length + " * " + foodPackage.getMoney() + " = " + (foodPackage.getMoney().multiply(BigDecimal.valueOf(splitStr.length))) + " 元";
                    realMoney = realMoney.add(foodPackage.getMoney().multiply(BigDecimal.valueOf(splitStr.length)));
                }
                // 计算预付金额
                if (StringUtils.isNotBlank(orderBill.getHistoryDateCheck2())) {
                    splitStr = orderBill.getHistoryDateCheck2().split(",");
                    historyFoodInfo2 = "实际消费：" + splitStr.length + " * " + foodPackage.getMoney() + " = " + (foodPackage.getMoney().multiply(BigDecimal.valueOf(splitStr.length))) + " 元";
                    historyMoney = historyMoney.add(foodPackage.getMoney().multiply(BigDecimal.valueOf(splitStr.length)));
                }
            }

            foodPackage = null;
            if (orderBill.getOldman().getFoodPackage3NextMonth() != 0) {// 晚餐
                foodPackage = foodPackageService.selectFoodPackageById(orderBill.getOldman().getFoodPackage3NextMonth());
                foodInfo3 = foodPackage.getFoodPackageName() + "：单份 " + foodPackage.getMoney() + " 元" + "； 备注：" + foodPackage.getRemark() + "； ";
                // 计算实际消费金额
                String[] splitStr;
                if (StringUtils.isNotBlank(orderBill.getRealDateCheck3())) {
                    splitStr = orderBill.getRealDateCheck3().split(",");
                    realFoodInfo3 = "预付：" + splitStr.length + " * " + foodPackage.getMoney() + " = " + (foodPackage.getMoney().multiply(BigDecimal.valueOf(splitStr.length))) + " 元";
                    realMoney = realMoney.add(foodPackage.getMoney().multiply(BigDecimal.valueOf(splitStr.length)));
                }
                // 计算预付金额
                if (StringUtils.isNotBlank(orderBill.getHistoryDateCheck3())) {
                    splitStr = orderBill.getHistoryDateCheck3().split(",");
                    historyFoodInfo3 = "实际消费：" + splitStr.length + " * " + foodPackage.getMoney() + " = " + (foodPackage.getMoney().multiply(BigDecimal.valueOf(splitStr.length))) + " 元";
                    historyMoney = historyMoney.add(foodPackage.getMoney().multiply(BigDecimal.valueOf(splitStr.length)));
                }
            }
        } else { // 其他正常账单
            SysFoodPackage foodPackage = new SysFoodPackage();
            if (orderBill.getOldman().getFoodPackage() != 0) {// 早餐
                foodPackage = foodPackageService.selectFoodPackageById(orderBill.getOldman().getFoodPackage());
                foodInfo = foodPackage.getFoodPackageName() + "：单份 " + foodPackage.getMoney() + " 元" + "； 备注：" + foodPackage.getRemark() + "； ";
                // 计算实际消费金额
                String[] splitStr;
                if (StringUtils.isNotBlank(orderBill.getRealDateCheck())) {
                    splitStr = orderBill.getRealDateCheck().split(",");
                    realFoodInfo = "预付：" + splitStr.length + " * " + foodPackage.getMoney() + " = " + (foodPackage.getMoney().multiply(BigDecimal.valueOf(splitStr.length))) + " 元";
                    realMoney = realMoney.add(foodPackage.getMoney().multiply(BigDecimal.valueOf(splitStr.length)));
                }
                // 计算预付金额
                if (StringUtils.isNotBlank(orderBill.getHistoryDateCheck())) {
                    splitStr = orderBill.getHistoryDateCheck().split(",");
                    historyFoodInfo = "实际消费：" + splitStr.length + " * " + foodPackage.getMoney() + " = " + (foodPackage.getMoney().multiply(BigDecimal.valueOf(splitStr.length))) + " 元";
                    historyMoney = historyMoney.add(foodPackage.getMoney().multiply(BigDecimal.valueOf(splitStr.length)));
                }
            }

            foodPackage = null;
            if (orderBill.getOldman().getFoodPackage2() != 0) {// 午餐
                foodPackage = foodPackageService.selectFoodPackageById(orderBill.getOldman().getFoodPackage2());
                foodInfo2 = foodPackage.getFoodPackageName() + "：单份 " + foodPackage.getMoney() + " 元" + "； 备注：" + foodPackage.getRemark() + "； ";
                // 计算实际消费金额
                String[] splitStr;
                if (StringUtils.isNotBlank(orderBill.getRealDateCheck2())) {
                    splitStr = orderBill.getRealDateCheck2().split(",");
                    realFoodInfo2 = "预付：" + splitStr.length + " * " + foodPackage.getMoney() + " = " + (foodPackage.getMoney().multiply(BigDecimal.valueOf(splitStr.length))) + " 元";
                    realMoney = realMoney.add(foodPackage.getMoney().multiply(BigDecimal.valueOf(splitStr.length)));
                }
                // 计算预付金额
                if (StringUtils.isNotBlank(orderBill.getHistoryDateCheck2())) {
                    splitStr = orderBill.getHistoryDateCheck2().split(",");
                    historyFoodInfo2 = "实际消费：" + splitStr.length + " * " + foodPackage.getMoney() + " = " + (foodPackage.getMoney().multiply(BigDecimal.valueOf(splitStr.length))) + " 元";
                    historyMoney = historyMoney.add(foodPackage.getMoney().multiply(BigDecimal.valueOf(splitStr.length)));
                }
            }

            foodPackage = null;
            if (orderBill.getOldman().getFoodPackage3() != 0) {// 晚餐
                foodPackage = foodPackageService.selectFoodPackageById(orderBill.getOldman().getFoodPackage3());
                foodInfo3 = foodPackage.getFoodPackageName() + "：单份 " + foodPackage.getMoney() + " 元" + "； 备注：" + foodPackage.getRemark() + "； ";
                // 计算实际消费金额
                String[] splitStr;
                if (StringUtils.isNotBlank(orderBill.getRealDateCheck3())) {
                    splitStr = orderBill.getRealDateCheck3().split(",");
                    realFoodInfo3 = "预付：" + splitStr.length + " * " + foodPackage.getMoney() + " = " + (foodPackage.getMoney().multiply(BigDecimal.valueOf(splitStr.length))) + " 元";
                    realMoney = realMoney.add(foodPackage.getMoney().multiply(BigDecimal.valueOf(splitStr.length)));
                }
                // 计算预付金额
                if (StringUtils.isNotBlank(orderBill.getHistoryDateCheck3())) {
                    splitStr = orderBill.getHistoryDateCheck3().split(",");
                    historyFoodInfo3 = "实际消费：" + splitStr.length + " * " + foodPackage.getMoney() + " = " + (foodPackage.getMoney().multiply(BigDecimal.valueOf(splitStr.length))) + " 元";
                    historyMoney = historyMoney.add(foodPackage.getMoney().multiply(BigDecimal.valueOf(splitStr.length)));
                }
            }
        }

        // 赋予套餐信息展示内容
        orderBill.setFoodInfo(foodInfo);
        orderBill.setFoodInfo2(foodInfo2);
        orderBill.setFoodInfo3(foodInfo3);

        // 赋予预付套餐金额统计信息
        orderBill.setRealFoodMoneyCount(realFoodInfo);
        orderBill.setRealFoodMoneyCount2(realFoodInfo2);
        orderBill.setRealFoodMoneyCount3(realFoodInfo3);

        // 赋予实际消费套餐金额统计信息
        orderBill.setHistoryFoodMoneyCount(historyFoodInfo);
        orderBill.setHistoryFoodMoneyCount2(historyFoodInfo2);
        orderBill.setHistoryFoodMoneyCount3(historyFoodInfo3);

        // 赋予实际消费、预付的总金额
        orderBill.setMoneyPrepaid(realMoney);// 预付总款
        orderBill.setMoney(historyMoney);// 实际消费总款

        if (key.equals("0")) {// 正常修改页面
            mmap.put("bill", orderBill);
//        mmap.put("roles", roleService.selectRolesByUserId(userCode));
//        mmap.put("posts", postService.selectPostsByUserId(userCode));
            return prefix + "/billUpdate";
        } else if (key.equals("1")) {// 待退款修改页面
            mmap.put("bill", orderBill);
            return prefix + "/billUpdateForRefund";
        } else if (key.equals("2")) {// 本月预付款页面
            mmap.put("bill", orderBill);
            return prefix + "/prepayments";
        } else if (key.equals("3")) {// 下月预付款页面
            mmap.put("bill", orderBill);
            return prefix + "/prepaymentsNextMonth";
        } else {
            return "error/500";
        }

    }

    /**
     * 修改账单信息
     *
     * @param orderBill
     * @return
     */
    @RequiresPermissions("system:bill:edit")
    @Log(title = "修改账单", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public AjaxResult editSave(SysOrderBill orderBill) {
        SysOldman oldman = new SysOldman();
        oldman.setUserCode(orderBill.getUserCode());
        oldman.setDateCheck(orderBill.getHistoryDateCheck());
        oldman.setDateCheck2(orderBill.getHistoryDateCheck2());
        oldman.setDateCheck3(orderBill.getHistoryDateCheck3());
        oldManService.updateOldMan(oldman);
        orderBill.setUpdateBy(ShiroUtils.getLoginName());
        return toAjax(orderBillService.updateOrderBill(orderBill));

    }

    /**
     * 删除账单信息
     *
     * @param id
     * @return
     */
    @RequiresPermissions("system:bill:remove")
    @Log(title = "删除账单", businessType = BusinessType.DELETE)
    @PostMapping("/remove/{id}")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") Long id) {
        try {
            return toAjax(orderBillService.deleteOrderBillById(id));
        } catch (Exception e) {
            return error(e.getMessage());
        }
    }

    /*****************************************************************************************************************/

    /**
     * 获取当前年份
     *
     * @param
     * @return
     */
    public int getYear() {
        Calendar c = Calendar.getInstance(Locale.CHINA);//
        return c.get(Calendar.YEAR); // 获取当前年份
    }

    /**
     * 获取当前月份
     *
     * @param
     * @return
     */
    public int getMonth() {
        Calendar c = Calendar.getInstance(Locale.CHINA);//
        return c.get(Calendar.MONTH) + 1; //
    }

    /**
     * 获取当前月份的天数
     *
     * @param
     * @return
     */
    public int getDay() {
        Calendar a = Calendar.getInstance();
        a.set(Calendar.DATE, 1);
        a.roll(Calendar.DATE, -1);
        return a.get(Calendar.DATE);
    }

    /**
     * 描述:获取下一个月的第一天.
     *
     * @return
     */
    public String getPerFirstDayOfMonth() {
        SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        return dft.format(calendar.getTime());
    }

    /**
     * 描述:获取下个月的最后一天.
     *
     * @return
     */
    public String getLastMaxMonthDate() {
        SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return dft.format(calendar.getTime());
    }

    /**
     * 统计付款天数(首页显示)
     *
     * @param list
     * @return
     */
    public List<SysOrderBill> statistics(List<SysOrderBill> list) {
        for (SysOrderBill b : list) {
            String[] splitStr;
            if (StringUtils.isNotBlank(b.getRealDateCheck())) {
                splitStr = null;
                splitStr = b.getRealDateCheck().split(",");
                b.setRealCount(splitStr.length);
            } else {
                b.setRealCount(0);
            }
            if (StringUtils.isNotBlank(b.getRealDateCheck2())) {
                splitStr = null;
                splitStr = b.getRealDateCheck2().split(",");
                b.setRealCount2(splitStr.length);
            } else {
                b.setRealCount2(0);
            }
            if (StringUtils.isNotBlank(b.getRealDateCheck3())) {
                splitStr = null;
                splitStr = b.getRealDateCheck3().split(",");
                b.setRealCount3(splitStr.length);
            } else {
                b.setRealCount3(0);
            }

            if (StringUtils.isNotBlank(b.getHistoryDateCheck())) {
                splitStr = null;
                splitStr = b.getHistoryDateCheck().split(",");
                b.setHistoryCount(splitStr.length);
            } else {
                b.setHistoryCount(0);
            }
            if (StringUtils.isNotBlank(b.getHistoryDateCheck2())) {
                splitStr = null;
                splitStr = b.getHistoryDateCheck2().split(",");
                b.setHistoryCount2(splitStr.length);
            } else {
                b.setHistoryCount2(0);
            }
            if (StringUtils.isNotBlank(b.getHistoryDateCheck3())) {
                splitStr = null;
                splitStr = b.getHistoryDateCheck3().split(",");
                b.setHistoryCount3(splitStr.length);
            } else {
                b.setHistoryCount3(0);
            }

        }
        return list;
    }

}