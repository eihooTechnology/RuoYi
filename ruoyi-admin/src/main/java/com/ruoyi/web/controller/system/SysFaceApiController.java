package com.ruoyi.web.controller.system;

import com.alibaba.fastjson.JSON;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.web.base.BaseController;
import com.ruoyi.quartz.util.Encryption;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.service.*;
import com.ruoyi.web.controller.tool.FaceTool;
import com.ruoyi.web.controller.tool.OssTool;
import com.ruoyi.web.controller.tool.ResultDTO;
import com.ruoyi.web.entity.huaweiface.AddFaceSuccessBean;
import com.ruoyi.web.entity.huaweiface.DeleteFacesSuccessBean;
import com.ruoyi.web.entity.huaweiface.FaceSearchSendBean;
import com.ruoyi.web.entity.huaweiface.FacesBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 老人管理调用层
 *
 * @author ruoyi
 */
@Controller
@RequestMapping("/system/faceApi")
public class SysFaceApiController extends BaseController {
    private String prefix = "system/faceApi";

    @Autowired
    private ISysOldManService oldManService;

    @Autowired
    private ISysRoleService roleService;

    @Autowired
    private ISysPostService postService;

    @Autowired
    private ISysOrderBillService orderBillService;

    @Autowired
    private ISysCourierService courierService;

    @Autowired
    private ISysOrderService orderService;

    @Autowired
    private ISysOrderPreviewService orderPreviewService;

    @Autowired
    private ISysAppVersionService appVersionService;

    @Autowired
    private IZcdOldmanInfoService zcdOldmanInfoService;

    @Autowired
    private IZcdFoodPackageService zcdFoodPackageService;

    @Autowired
    private IZcdFoodPackageOldmanService zcdFoodPackageOldmanService;

    private SimpleDateFormat sdfMDY = new SimpleDateFormat("MM/dd/yyyy");
    private SimpleDateFormat sdfYMD = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");

    /******************************************** 助餐点Pad相关Api接口 ********************************************/

    /**
     * 注册助餐点老人人脸(接口)
     *
     * @return
     */
    @Log(title = "注册助餐点老人人脸Api", businessType = BusinessType.INSERT)
    @PostMapping("/registerZcdOldmanFaceApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO registerZcdOldmanFaceApi(HttpServletRequest request, String dataJson) throws Exception {
        System.err.println("注册助餐点老人人脸Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        /**********************接口请求合法性验证********************/
//        Date date = new Date();
//        Long nowTime = date.getTime();
        String timeStamp = request.getHeader("timeStamp");
        String md5 = request.getHeader("MD5");
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(md5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(86400000);// 超时时间为1天
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过30秒
                if (Encryption.encrypt(timeStamp).equals(md5)) {
                    /**********开始执行逻辑**********/
                    // System.err.println("dataJson=" + dataJson);
                    // 将获取到的JSON转换为map
                    Map jsonMap = JSON.parseObject(dataJson, Map.class);
                    String external_image_id = jsonMap.get("external_image_id") == null ? null : jsonMap.get("external_image_id").toString();
                    String image_base64 = jsonMap.get("image_base64") == null ? null : jsonMap.get("image_base64").toString();

                    String faceSetName = "elderly_release";// 指定的人脸库
                    String bucketName = "elderly-release";// 指定的OSS目录
                    String imgPath = OssTool.elderlyImgPath;// 指定的图片保存的本地路径
                    String imgUrl = OssTool.elderlyOssUrlRelease;// 指定的图片保存的OSS路径
                    String token = FaceTool.getHuaWeiToken();

                    boolean registerFlag = false;
                    FaceSearchSendBean faceSearchSendBean = new FaceSearchSendBean();
                    faceSearchSendBean.setExternal_image_id(external_image_id);
                    faceSearchSendBean.setImage_base64(image_base64);
                    String jsonStr = JSON.toJSONString(faceSearchSendBean);
                    FacesBean facesBean = FaceTool.findFaceByBase64AndCheck(token, faceSetName, jsonStr);

                    if (facesBean != null) {

                        if (facesBean.getSimilarity() >= 0.8) {
                            registerFlag = true;
                        } else {
                            registerFlag = false;
                        }

                    }

                    if (!registerFlag) {// 当前身份证未注册过

                        AddFaceSuccessBean addSuccessBean = FaceTool.addFaceByBase64(token, faceSetName, jsonStr);
                        // System.err.println(addSuccessBean);

                        ZcdOldmanInfo oldman = new ZcdOldmanInfo();
                        oldman.setId(Long.valueOf(external_image_id));
                        oldman.setImgBase64(image_base64);
                        oldman.setIsFaceRegistration("1");

                        /** 将Base64转为图片文件并上传至OSS服务器 **/
                        Boolean base64ToImgFlag = OssTool.generateImage(image_base64, imgPath, external_image_id);
                        if (base64ToImgFlag) {
                            System.err.println("Base64转换为图片成功!");

                            /** 判断同名文件是否已经存在 **/
                            Boolean fileExtFlag = OssTool.checkFile(bucketName, external_image_id);

                            if (!fileExtFlag) {// 如果没有同名文件，则直接上传

                                Boolean uploadOssFlag = OssTool.uploadOssFile(bucketName, external_image_id, OssTool.elderlyImgPath);
                                if (uploadOssFlag) {
                                    System.err.println("图片成功上传OSS服务器!");
                                    /** 将上传OSS的图片的URL保存至当前老人信息中 **/
                                    oldman.setImgOssUrl(imgUrl + external_image_id);
                                } else {
                                    System.err.println("图片上传OSS服务器失败");
                                    /** 若上传OSS失败，则删除之前添加至华为人脸库的人脸信息 **/
                                    DeleteFacesSuccessBean deleteSuccessBean = FaceTool.deleteFaceinfo(token, faceSetName, external_image_id);
                                    if (deleteSuccessBean != null) {
                                        System.err.println("删除华为人脸库『" + faceSetName + "』中的 " + external_image_id + " 号人脸信息成功!");
                                    } else {
                                        System.err.println("删除华为人脸库『" + faceSetName + "』中的 " + external_image_id + " 号人脸信息失败");
                                    }

                                    System.out.println("注册助餐点老人人脸失败(上传OSS失败)");
                                    result.setCode(1);// 1=失败
                                    result.setMsg("注册助餐点老人人脸失败(上传OSS失败)");
                                    return result;
                                }

                            } else {// 如果存在同名文件，则删除同名文件后再上传

                                /** 先删除 **/
                                OssTool.deleteFile(faceSetName, external_image_id);
                                System.err.println("已删除同名文件");
                                /** 再上传 **/
                                Boolean uploadOssFlag = OssTool.uploadOssFile(bucketName, external_image_id, OssTool.elderlyImgPath);
                                if (uploadOssFlag) {
                                    System.err.println("图片成功上传OSS服务器!");
                                    /** 将上传OSS的图片的URL保存至当前老人信息中 **/
                                    oldman.setImgOssUrl(imgUrl + external_image_id);
                                } else {
                                    System.err.println("图片上传OSS服务器失败");
                                    /** 若上传OSS失败，则删除之前添加至华为人脸库的人脸信息 **/
                                    DeleteFacesSuccessBean deleteSuccessBean = FaceTool.deleteFaceinfo(token, faceSetName, external_image_id);
                                    if (deleteSuccessBean != null) {
                                        System.err.println("删除华为人脸库『" + faceSetName + "』中的 " + external_image_id + " 号人脸信息成功!");
                                    } else {
                                        System.err.println("删除华为人脸库『" + faceSetName + "』中的 " + external_image_id + " 号人脸信息失败");
                                    }

                                    System.out.println("注册助餐点老人人脸失败(上传OSS失败)");
                                    result.setCode(1);// 1=失败
                                    result.setMsg("注册助餐点老人人脸失败(上传OSS失败)");
                                    return result;
                                }

                            }

                        } else {
                            System.err.println("Base64未能转换为图片");
                            /** 若生成图片失败，则删除之前添加至华为人脸库的人脸信息 **/
                            DeleteFacesSuccessBean deleteSuccessBean = FaceTool.deleteFaceinfo(token, faceSetName, external_image_id);
                            if (deleteSuccessBean != null) {
                                System.err.println("删除华为人脸库『" + faceSetName + "』中的 " + external_image_id + " 号人脸信息成功!");
                            } else {
                                System.err.println("删除华为人脸库『" + faceSetName + "』中的 " + external_image_id + " 号人脸信息失败");
                            }

                            System.out.println("注册助餐点老人人脸失败(本地图片生成失败)");
                            result.setCode(1);// 1=失败
                            result.setMsg("注册助餐点老人人脸失败(本地图片生成失败)");
                            return result;
                        }

                        int success = zcdOldmanInfoService.updateOldmanById(oldman);

                        if (addSuccessBean != null && success > 0) {
                            result.setCode(0);// 0=成功
                            result.setMsg("注册助餐点老人人脸成功!");
                            result.setData(addSuccessBean);
                            result.setCount(1);
                            return result;
                        } else {
                            System.out.println("注册助餐点老人人脸失败");
                            result.setCode(1);// 1=失败
                            result.setMsg("注册助餐点老人人脸失败");
                            return result;
                        }
                    } else {
                        System.out.println("当前助餐点老人的人脸信息已经注册过了，注册的ID为：" + facesBean.getExternal_image_id());
                        result.setCode(1);// 1=失败
                        result.setMsg("当前助餐点老人的人脸信息已经注册过了，注册的ID为：" + facesBean.getExternal_image_id());
                        return result;
                    }

                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过一天时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }

    /**
     * 删除助餐点老人人脸(接口)
     *
     * @return
     */
    @Log(title = "删除助餐点老人人脸Api", businessType = BusinessType.INSERT)
    @PostMapping("/deleteZcdOldmanFaceApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO deleteZcdOldmanFaceApi(HttpServletRequest request, String dataJson) throws Exception {
        System.err.println("删除助餐点老人人脸Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        /**********************接口请求合法性验证********************/
//        Date date = new Date();
//        Long nowTime = date.getTime();
        String timeStamp = request.getHeader("timeStamp");
        String md5 = request.getHeader("MD5");
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(md5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(86400000);// 超时时间为1天
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过30秒
                if (Encryption.encrypt(timeStamp).equals(md5)) {
                    /**********开始执行逻辑**********/
                    // System.err.println("dataJson=" + dataJson);
                    // 将获取到的JSON转换为map
                    Map jsonMap = JSON.parseObject(dataJson, Map.class);
                    String imageID = jsonMap.get("imageID") == null ? null : jsonMap.get("imageID").toString();

                    String faceSetName = "elderly_release";// 指定的人脸库
                    String bucketName = "elderly-release";// 指定的OSS目录
                    String token = FaceTool.getHuaWeiToken();

                    DeleteFacesSuccessBean deleteSuccessBean = FaceTool.deleteFaceinfo(token, faceSetName, imageID);
                    // System.err.println(deleteSuccessBean);
                    ZcdOldmanInfo oldman = new ZcdOldmanInfo();
                    oldman.setId(Long.valueOf(imageID));
                    oldman.setImgBase64(null);
                    oldman.setImgOssUrl(null);
                    oldman.setIsFaceRegistration("0");
                    int success = zcdOldmanInfoService.updateOldmanById(oldman);

                    if (deleteSuccessBean != null && success > 0) {

                        OssTool.deleteFile(bucketName, imageID);
                        System.err.println("已删除相关的OSS图片文件");

                        result.setCode(0);// 0=成功
                        result.setMsg("删除助餐点老人人脸成功!");
                        result.setData(deleteSuccessBean);
                        result.setCount(1);
                        return result;
                    } else {
                        System.out.println("删除助餐点老人人脸失败");
                        result.setCode(1);// 1=失败
                        result.setMsg("删除助餐点老人人脸失败");
                        return result;
                    }

                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过一天时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }

    /**
     * 对比助餐点老人人脸(接口)
     *
     * @return
     */
    @Log(title = "对比助餐点老人人脸Api", businessType = BusinessType.INSERT)
    @PostMapping("/findZcdOldmanFaceApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO findZcdOldmanFaceApi(HttpServletRequest request, String dataJson) throws Exception {
        System.err.println("对比助餐点老人人脸Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        /**********************接口请求合法性验证********************/
//        Date date = new Date();
//        Long nowTime = date.getTime();
        String timeStamp = request.getHeader("timeStamp");
        String md5 = request.getHeader("MD5");
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(md5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(86400000);// 超时时间为1天
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过30秒
                if (Encryption.encrypt(timeStamp).equals(md5)) {
                    /**********开始执行逻辑**********/
                    // System.err.println("dataJson=" + dataJson);
                    // 将获取到的JSON转换为map
                    Map jsonMap = JSON.parseObject(dataJson, Map.class);
                    String image_base64 = jsonMap.get("image_base64") == null ? null : jsonMap.get("image_base64").toString();

                    String faceSetName = "elderly_release";// 指定的人脸库
                    String token = FaceTool.getHuaWeiToken();

                    FaceSearchSendBean faceSearchSendBean = new FaceSearchSendBean();
                    faceSearchSendBean.setImage_base64(image_base64);
                    String jsonStr = JSON.toJSONString(faceSearchSendBean);
                    FacesBean facesBean = FaceTool.findFaceByBase64AndCheck(token, faceSetName, jsonStr);
                    // System.err.println("当前人脸对比成功度为：" + facesBean.getSimilarity() + "\t 人脸ID为：" + facesBean.getFace_id() + "\t 人脸自定义ID为：" + facesBean.getExternal_image_id());

                    if (facesBean != null) {
                        if (facesBean.getSimilarity() >= 0.8) {
                            result.setCode(0);// 0=成功
                            result.setMsg("对比助餐点老人人脸成功!");
                            result.setData(facesBean);
                            result.setCount(1);
                            return result;
                        } else {
                            System.out.println("对比助餐点老人人脸失败(相似度过低)");
                            result.setCode(1);// 1=失败
                            result.setMsg("对比助餐点老人人脸失败(相似度过低)");
                            return result;
                        }
                    } else {
                        System.out.println("对比助餐点老人人脸失败(不存在当前人脸信息)");
                        result.setCode(2);// 2=自定义数值
                        result.setMsg("对比助餐点老人人脸失败(不存在当前人脸信息)");
                        return result;
                    }

                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过一天时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }

    /******************************************* 送餐员面部识别相关接口 *****************************************/

    /**
     * 对比送餐员人脸(接口)
     *
     * @return
     */
    @Log(title = "对比送餐员人脸Api", businessType = BusinessType.INSERT)
    @PostMapping("/findCourierFaceApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO findCourierFaceApi(HttpServletRequest request, String dataJson) throws Exception {
        System.err.println("对比送餐员人脸Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        /**********************接口请求合法性验证********************/
//        Date date = new Date();
//        Long nowTime = date.getTime();
        String timeStamp = request.getHeader("timeStamp");
        String md5 = request.getHeader("MD5");
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(md5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(86400000);// 超时时间为1天
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过30秒
                if (Encryption.encrypt(timeStamp).equals(md5)) {
                    /**********开始执行逻辑**********/
                    // System.err.println("dataJson=" + dataJson);
                    // 将获取到的JSON转换为map
                    Map jsonMap = JSON.parseObject(dataJson, Map.class);
                    String image_base64 = jsonMap.get("image_base64") == null ? null : jsonMap.get("image_base64").toString();

                    String faceSetName = "rider_release";// 指定的人脸库
                    String token = FaceTool.getHuaWeiToken();

                    FaceSearchSendBean faceSearchSendBean = new FaceSearchSendBean();
                    faceSearchSendBean.setImage_base64(image_base64);
                    String jsonStr = JSON.toJSONString(faceSearchSendBean);
                    FacesBean facesBean = FaceTool.findFaceByBase64AndCheck(token, faceSetName, jsonStr);
                    // System.err.println("当前人脸对比成功度为：" + facesBean.getSimilarity() + "\t 人脸ID为：" + facesBean.getFace_id() + "\t 人脸自定义ID为：" + facesBean.getExternal_image_id());

                    if (facesBean != null) {
                        if (facesBean.getSimilarity() >= 0.8) {
                            result.setCode(0);// 0=成功
                            result.setMsg("对比送餐员人脸成功!");
                            result.setData(facesBean);
                            result.setCount(1);
                            return result;
                        } else {
                            System.out.println("对比送餐员人脸失败(相似度过低)");
                            result.setCode(1);// 1=失败
                            result.setMsg("对比送餐员人脸失败(相似度过低)");
                            return result;
                        }
                    } else {
                        System.out.println("对比送餐员人脸失败(不存在当前人脸信息)");
                        result.setCode(2);// 2=自定义数值
                        result.setMsg("对比送餐员人脸失败(不存在当前人脸信息)");
                        return result;
                    }

                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过一天时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }
    
    /*************************************************************************************/
    /*************************************************************************************/
    /*************************************************************************************/
    /*************************************************************************************/
    /*************************************************************************************/
    /*************************************************************************************/
    /*************************************************************************************/
    /*************************************************************************************/
    /************************************** 额外接口 **************************************/

    /************************************* 测试接口模板 **************************************/

    /**
     * 测试Api(接口)
     *
     * @return
     */
    @Log(title = "测试Api", businessType = BusinessType.INSERT)
    @PostMapping("/testApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public ResultDTO testApi(String timeStamp, String MD5, String dataJson) throws Exception {
        System.err.println("验证助餐点老人登录Api，执行时间：" + df.format(new Date()));
        ResultDTO result = new ResultDTO();
        /**********************接口请求合法性验证********************/
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(MD5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(10000);// 超时时间为10s
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过10秒
                if (Encryption.encrypt(timeStamp).equals(MD5)) {
                    /**********开始执行逻辑**********/

                    System.err.println("dataJson=" + dataJson);
                    // 将获取到的JSON转换为map
                    Map jsonMap = JSON.parseObject(dataJson, Map.class);
                    String keyWord = jsonMap.get("keyWord") == null ? null : jsonMap.get("keyWord").toString();
                    System.err.println("keyWord=" + keyWord);

                    result.setCode(0);// 0=成功
                    result.setMsg("调用成功!");
                    result.setData(JSON.toJSONString("调用成功"));
                    result.setCount(JSON.toJSONString("调用成功").length());
                    return result;
                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    result.setCode(1);// 1=失败
                    result.setMsg("非法请求，已过滤【加密错误】");
                    return result;
                }
            } else {// 距离请求参数生成时间超过10秒时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                result.setCode(1);// 1=失败
                result.setMsg("非法请求，已过滤【超时错误】");
                return result;
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            result.setCode(1);// 1=失败
            result.setMsg("非法请求，已过滤【参数错误】");
            return result;
        }

    }

    /******************************************************** 【工具方法】 ********************************************************/

    /**
     * 获取当前年份
     *
     * @param
     * @return
     */
    public int getYear() {
        Calendar c = Calendar.getInstance(Locale.CHINA);//
        return c.get(Calendar.YEAR); // 获取当前年份
    }

    /**
     * 获取当前月份
     *
     * @param
     * @return
     */
    public int getMonth() {
        Calendar c = Calendar.getInstance(Locale.CHINA);//
        return c.get(Calendar.MONTH) + 1; //
    }

    /**
     * 获取当前月份的天数
     *
     * @param
     * @return
     */
    public int getDay() {
        Calendar a = Calendar.getInstance();
        a.set(Calendar.DATE, 1);
        a.roll(Calendar.DATE, -1);
        return a.get(Calendar.DATE);
    }

    /**
     * 返回今天
     *
     * @return
     */
    public String getToday() {
        Date date = new Date();//取时间
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(calendar.DATE, 0);
        date = calendar.getTime(); //这个时间就是日期往后推一天的结果
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(date);
        return dateString;
    }

    /**
     * 返回昨天
     *
     * @return
     */
    public String getYesterday() {
        Date date = new Date();//取时间
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(calendar.DATE, -1);
        date = calendar.getTime(); //这个时间就是日期往后推一天的结果
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(date);
        return dateString;
    }

    /**
     * 返回当前时间
     *
     * @return
     */
    public String getNowTime() {
        Date date = new Date();//取时间
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(calendar.DATE, 0);
        date = calendar.getTime(); //这个时间就是日期往后推一天的结果
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(date);
        return dateString;
    }

    /**
     * 获取当前日期获得所在周的日期区间（周一和周日日期）
     *
     * @param date
     * @return
     */
    public String getTimeInterval(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        // 判断要计算的日期是否是周日，如果是则减一天计算周六的，否则会出问题，计算到下一周去了
        int dayWeek = cal.get(Calendar.DAY_OF_WEEK);// 获得当前日期是一个星期的第几天
        if (1 == dayWeek) {
            cal.add(Calendar.DAY_OF_MONTH, -1);
        }
        // System.out.println("要计算日期为:" + sdf.format(cal.getTime())); // 输出要计算日期
        // 设置一个星期的第一天，按中国的习惯一个星期的第一天是星期一
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        // 获得当前日期是一个星期的第几天
        int day = cal.get(Calendar.DAY_OF_WEEK);
        // 根据日历的规则，给当前日期减去星期几与一个星期第一天的差值
        cal.add(Calendar.DATE, cal.getFirstDayOfWeek() - day);
        String imptimeBegin = sdf2.format(cal.getTime());
        // System.out.println("所在周星期一的日期：" + imptimeBegin);
        cal.add(Calendar.DATE, 6);
        String imptimeEnd = sdf2.format(cal.getTime());
        // System.out.println("所在周星期日的日期：" + imptimeEnd);
        return imptimeBegin + "," + imptimeEnd;
    }

}