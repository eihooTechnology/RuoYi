package com.ruoyi.web.controller.tool;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.SysCourier;
import com.ruoyi.system.domain.SysOldman;
import com.ruoyi.system.domain.SysOrder;
import com.ruoyi.system.service.ISysCourierService;
import com.ruoyi.system.service.ISysOldManService;
import com.ruoyi.system.service.ISysOrderService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @ClassName OrderGenerateHelper
 * @Description TODO
 * @Author lzjian
 * @Date 2019/1/8 0008 10:19
 **/
public class OrderGenerateHelper {

    private SimpleDateFormat sdfMDY = new SimpleDateFormat("MM/dd/yyyy");
    private SimpleDateFormat sdfYMD = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");

    /**
     * 根据参数生成相应类型的所有今日订单
     *
     * @param todayOrderList
     * @param oldmanList
     * @param courierList
     * @param orderType
     * @return
     * @throws Exception
     */
    public OrderResultDTO generateTodayOrders(List<SysOrder> todayOrderList, List<SysOldman> oldmanList, List<SysCourier> courierList, String orderType) throws Exception {
        OrderResultDTO resultDTO = new OrderResultDTO();
        int SendOrdersCount = 0;
        int repeatOrdersCount = 0;
        /******************************************/
        List<SysOldman> unRepeatOldmanList = new ArrayList<>();// 经过去重后的老人List
        Map<Long, String> courierMap = new HashMap<>();// 骑手Map
        for (SysCourier c : courierList) {
            courierMap.put(c.getId(), c.getCourierName());
        }

        // 进行老人的去重(把今日已经生成过早餐订单的老人去除)
        boolean presence;
        for (SysOldman oldman : oldmanList) {
            presence = false;
            for (SysOrder order : todayOrderList) {
                if (order.getUserCode().longValue() == oldman.getUserCode().longValue()) {
                    presence = true;
                }
            }
            if (presence) {
                repeatOrdersCount++;
                System.err.println("当前老人今日已经存在订单：" + oldman.getUserName());
            } else {
                unRepeatOldmanList.add(oldman);
            }
        }

        if (orderType.equals("0")) {// 生成早餐
            List<SysOrder> orderList = new ArrayList<>();
            if (unRepeatOldmanList.size() > 0) {
                for (SysOldman oldman : unRepeatOldmanList) {
                    if (StringUtils.isNotBlank(oldman.getDateCheck()) && oldman.getCourierId() != null && oldman.getCourierId() != 0 && StringUtils.isNotBlank(oldman.getAddress())) {// 判断老人是否设置了早餐送餐日期、骑手、送餐地址

                        String today = getToday();
                        Date date = sdfYMD.parse(today);

                        /******** 此处比对订单分配里面的早餐送餐日期，若含有今天的早餐日期，则生成早餐送餐信息 *******/
                        boolean flag = false;
                        flag = isFlag1(oldman, today, flag);

                        if (flag) {// 含有今天日期并且有骑手、送餐地址信息的老人
                            System.err.println("今日需要早餐送餐的老人：" + oldman.getUserName());

                            SysOrder order = new SysOrder();
                            order.setAddress(oldman.getAddress());
                            // torders.setParentId(orderConfig.getGroupId());
                            String time = today + " 06:00:00";
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            order.setStartSendTime(sdf.parse(time));// 设置开始早餐送餐时间
                            order.setSendType("0");//待取餐
                            order.setSendCourierId(oldman.getCourierId());
                            order.setSendCourierName(courierMap.get(oldman.getCourierId()));
                            order.setRecoverType("-1");
                            order.setRecoverCourierId(oldman.getCourierId());
                            order.setRecoverCourierName(courierMap.get(oldman.getCourierId()));
                            // order.setCreateTime(date);
                            order.setOrderType("0");// 早餐类型
                            order.setUserCode(oldman.getUserCode());
                            order.setUserName(oldman.getUserName());
                            order.setMobile(oldman.getMobile());
                            order.setCommunityId(oldman.getCommunityId());
                            order.setCommunityName(oldman.getCommunityName());
                            orderList.add(order);
                        }
                    } else {// 没有设置早餐用餐日期、骑手、送餐地址信息的老人的老人
                        // System.err.println("当前老人没设置早餐送餐日期：" + o.getUserName());
                    }
                }

                /******************************************/
                //                if (orderList.size() > 0) {
//                    SendOrdersCount = orderService.saveOrders(orderList);
//                    return "已成功自动生成 " + SendOrdersCount + " 份今日早餐订单，已去除了重复的 " + repeatOrdersCount + " 份早餐订单；";
//                } else {
//                    return "【早餐】：未能生成早餐订单，因为没有足够的满足早餐订单生成条件的老人；";
//                }
                resultDTO.setOrderList(orderList);
                resultDTO.setRepeatOrdersCount(repeatOrdersCount);
                return resultDTO;
            } else {
                //                if (oldmanList.size() > 0) {
//                    return "未能生成早餐订单，因为去除了重复的 " + repeatOrdersCount + " 份早餐订单；";
//                } else {
//                    return "【早餐】：未能成功获取老人的相关信息，请联系管理员；";
//                }
                resultDTO.setOrderList(orderList);
                resultDTO.setRepeatOrdersCount(repeatOrdersCount);
                return resultDTO;
            }
        } else if (orderType.equals("1")) {// 生成午餐
            List<SysOrder> orderList = new ArrayList<>();
            if (unRepeatOldmanList.size() > 0) {
                for (SysOldman oldman : unRepeatOldmanList) {
                    if (StringUtils.isNotBlank(oldman.getDateCheck2()) && oldman.getCourierId2() != null && oldman.getCourierId2() != 0 && StringUtils.isNotBlank(oldman.getAddress())) {// 判断老人是否设置了午餐送餐日期、骑手、送餐地址

                        String today = getToday();
                        Date date = sdfYMD.parse(today);

                        /******** 此处比对订单分配里面的午餐送餐日期，若含有今天的午餐日期，则生成午餐送餐信息 *******/
                        boolean flag = false;
                        flag = isFlag2(oldman, today, flag);

                        if (flag) {// 含有今天日期并且有骑手、送餐地址信息的老人
                            System.err.println("今日需要午餐送餐的老人：" + oldman.getUserName());

                            SysOrder order = new SysOrder();
                            order.setAddress(oldman.getAddress());
                            // torders.setParentId(orderConfig.getGroupId());
                            String time = today + " 09:50:00";
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            order.setStartSendTime(sdf.parse(time));// 设置开始午餐送餐时间
                            order.setSendType("0");//待取餐
                            order.setSendCourierId(oldman.getCourierId2());
                            order.setSendCourierName(courierMap.get(oldman.getCourierId2()));
                            order.setRecoverType("-1");
                            order.setRecoverCourierId(oldman.getCourierId2());
                            order.setRecoverCourierName(courierMap.get(oldman.getCourierId2()));
                            // order.setCreateTime(date);
                            order.setOrderType(orderType);// 午餐类型
                            order.setUserCode(oldman.getUserCode());
                            order.setUserName(oldman.getUserName());
                            order.setMobile(oldman.getMobile());
                            order.setCommunityId(oldman.getCommunityId());
                            order.setCommunityName(oldman.getCommunityName());
                            orderList.add(order);
                        }
                    } else {// 没有设置午餐用餐日期、骑手、送餐地址信息的老人的老人
                        // System.err.println("当前老人没设置午餐送餐日期：" + o.getUserName());
                    }
                }

                /******************************************/
//                if (orderList.size() > 0) {
//                    SendOrdersCount = orderService.saveOrders(orderList);
//                    return "已成功自动生成 " + SendOrdersCount + " 份今日午餐订单，已去除了重复的 " + repeatOrdersCount + " 份午餐订单；";
//                } else {
//                    return "【午餐】：未能生成午餐订单，因为没有足够的满足午餐订单生成条件的老人；";
//                }

                resultDTO.setOrderList(orderList);
                resultDTO.setRepeatOrdersCount(repeatOrdersCount);
                return resultDTO;
            } else {
//                if (oldmanList.size() > 0) {
//                    return "未能生成午餐订单，因为去除了重复的 " + repeatOrdersCount + " 份午餐订单；";
//                } else {
//                    return "【午餐】：未能成功获取老人的相关信息，请联系管理员；";
//                }

                resultDTO.setOrderList(orderList);
                resultDTO.setRepeatOrdersCount(repeatOrdersCount);
                return resultDTO;
            }
        } else if (orderType.equals("2")) {// 生成晚餐
            List<SysOrder> orderList = new ArrayList<>();
            if (unRepeatOldmanList.size() > 0) {
                for (SysOldman oldman : unRepeatOldmanList) {
                    if (StringUtils.isNotBlank(oldman.getDateCheck3()) && oldman.getCourierId3() != null && oldman.getCourierId3() != 0 && StringUtils.isNotBlank(oldman.getAddress())) {// 判断老人是否设置了晚餐送餐日期、骑手、送餐地址

                        String today = getToday();
                        Date date = sdfYMD.parse(today);

                        /******** 此处比对订单分配里面的晚餐送餐日期，若含有今天的晚餐日期，则生成晚餐送餐信息 *******/
                        boolean flag = false;
                        flag = isFlag3(oldman, today, flag);

                        if (flag) {// 含有今天日期并且有骑手、送餐地址信息的老人
                            System.err.println("今日需要晚餐送餐的老人：" + oldman.getUserName());

                            SysOrder order = new SysOrder();
                            order.setAddress(oldman.getAddress());
                            // torders.setParentId(orderConfig.getGroupId());
                            String time = today + " 15:00:00";
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            order.setStartSendTime(sdf.parse(time));// 设置开始晚餐送餐时间
                            order.setSendType("0");//待取餐
                            order.setSendCourierId(oldman.getCourierId3());
                            order.setSendCourierName(courierMap.get(oldman.getCourierId3()));
                            order.setRecoverType("-1");
                            order.setRecoverCourierId(oldman.getCourierId3());
                            order.setRecoverCourierName(courierMap.get(oldman.getCourierId3()));
                            // order.setCreateTime(date);
                            order.setOrderType(orderType);// 晚餐类型
                            order.setUserCode(oldman.getUserCode());
                            order.setUserName(oldman.getUserName());
                            order.setMobile(oldman.getMobile());
                            order.setCommunityId(oldman.getCommunityId());
                            order.setCommunityName(oldman.getCommunityName());
                            orderList.add(order);
                        }
                    } else {// 没有设置晚餐用餐日期、骑手、送餐地址信息的老人的老人
                        // System.err.println("当前老人没设置晚餐送餐日期：" + o.getUserName());
                    }
                }

                /******************************************/
//                if (orderList.size() > 0) {
//                    SendOrdersCount = orderService.saveOrders(orderList);
//                    return "已成功自动生成 " + SendOrdersCount + " 份今日晚餐订单，已去除了重复的 " + repeatOrdersCount + " 份晚餐订单；";
//                } else {
//                    return "【晚餐】：未能生成晚餐订单，因为没有足够的满足晚餐订单生成条件的老人；";
//                }

                resultDTO.setOrderList(orderList);
                resultDTO.setRepeatOrdersCount(repeatOrdersCount);
                return resultDTO;
            } else {
//                if (oldmanList.size() > 0) {
//                    return "未能生成晚餐订单，因为去除了重复的 " + repeatOrdersCount + " 份晚餐订单；";
//                } else {
//                    return "【晚餐】：未能成功获取老人的相关信息，请联系管理员；";
//                }

                resultDTO.setOrderList(orderList);
                resultDTO.setRepeatOrdersCount(repeatOrdersCount);
                return resultDTO;
            }
        } else {// 未识别生成餐点类型
            resultDTO.setOrderList(null);
            resultDTO.setRepeatOrdersCount(repeatOrdersCount);
            return resultDTO;
        }
    }

    /**
     * 根据参数生成相应类型的所有明日订单
     *
     * @param todayOrderList
     * @param oldmanList
     * @param courierList
     * @param orderType
     * @return
     * @throws Exception
     */
    public OrderResultDTO generateTorromowOrders(List<SysOrder> todayOrderList, List<SysOldman> oldmanList, List<SysCourier> courierList, String orderType) throws Exception {
        OrderResultDTO resultDTO = new OrderResultDTO();
        int SendOrdersCount = 0;
        int repeatOrdersCount = 0;
        /******************************************/
        List<SysOldman> unRepeatOldmanList = new ArrayList<>();// 经过去重后的老人List
        Map<Long, String> courierMap = new HashMap<>();// 骑手Map
        for (SysCourier c : courierList) {
            courierMap.put(c.getId(), c.getCourierName());
        }

        // 进行老人的去重(把今日已经生成过订单的老人去除)
        boolean presence;
        for (SysOldman oldman : oldmanList) {
            presence = false;
            for (SysOrder order : todayOrderList) {
                if (order.getUserCode().longValue() == oldman.getUserCode().longValue()) {
                    presence = true;
                }
            }
            if (presence) {
                repeatOrdersCount++;
                System.err.println("当前老人今日已经存在订单：" + oldman.getUserName());
            } else {
                unRepeatOldmanList.add(oldman);
            }
        }

        if (orderType.equals("0")) {// 生成早餐
            List<SysOrder> orderList = new ArrayList<>();
            if (unRepeatOldmanList.size() > 0) {
                for (SysOldman oldman : unRepeatOldmanList) {
                    if (StringUtils.isNotBlank(oldman.getDateCheck()) && oldman.getCourierId() != null && oldman.getCourierId() != 0 && StringUtils.isNotBlank(oldman.getAddress())) {// 判断老人是否设置了送餐日期、骑手、送餐地址

                        String today = getTomorrow();
                        Date date2 = sdfYMD.parse(today);

                        /******** 此处比对订单分配里面的送餐日期，若含有今天的日期，则生成送餐信息 *******/
                        boolean flag = false;
                        flag = isFlag1(oldman, today, flag);

                        if (flag) {// 含有今天日期并且有骑手、送餐地址信息的老人
                            System.err.println("今日需要送餐的老人：" + oldman.getUserName());

                            SysOrder order = new SysOrder();
                            order.setAddress(oldman.getAddress());
                            // torders.setParentId(orderConfig.getGroupId());
                            String time = today + " 08:00:00";
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            order.setStartSendTime(sdf.parse(time));// 设置开始送餐时间
                            order.setSendType("0");//待取餐
                            order.setSendCourierId(oldman.getCourierId());
                            order.setSendCourierName(courierMap.get(oldman.getCourierId()));
                            order.setRecoverType("-1");
                            order.setRecoverCourierId(oldman.getCourierId());
                            order.setRecoverCourierName(courierMap.get(oldman.getCourierId()));
                            // order.setCreateTime(date);
                            order.setOrderType(orderType);// 早餐类型
                            order.setUserCode(oldman.getUserCode());
                            order.setUserName(oldman.getUserName());
                            order.setMobile(oldman.getMobile());
                            order.setCommunityId(oldman.getCommunityId());
                            order.setCommunityName(oldman.getCommunityName());
                            orderList.add(order);
                        }
                    } else {// 没有设置用餐日期、骑手、送餐地址信息的老人的老人
                        // System.err.println("当前老人没设置送餐日期：" + o.getUserName());
                    }
                }

                /******************************************/
                resultDTO.setOrderList(orderList);
                resultDTO.setRepeatOrdersCount(repeatOrdersCount);
                return resultDTO;

            } else {
                resultDTO.setOrderList(orderList);
                resultDTO.setRepeatOrdersCount(repeatOrdersCount);
                return resultDTO;
            }
        } else if (orderType.equals("1")) {// 生成午餐
            List<SysOrder> orderList = new ArrayList<>();
            if (unRepeatOldmanList.size() > 0) {
                for (SysOldman oldman : unRepeatOldmanList) {
                    if (StringUtils.isNotBlank(oldman.getDateCheck2()) && oldman.getCourierId2() != null && oldman.getCourierId2() != 0 && StringUtils.isNotBlank(oldman.getAddress())) {// 判断老人是否设置了送餐日期、骑手、送餐地址

                        String today = getTomorrow();
                        Date date2 = sdfYMD.parse(today);

                        /******** 此处比对订单分配里面的送餐日期，若含有今天的日期，则生成送餐信息 *******/
                        boolean flag = false;
                        flag = isFlag2(oldman, today, flag);

                        if (flag) {// 含有今天日期并且有骑手、送餐地址信息的老人
                            System.err.println("今日需要送餐的老人：" + oldman.getUserName());

                            SysOrder order = new SysOrder();
                            order.setAddress(oldman.getAddress());
                            // torders.setParentId(orderConfig.getGroupId());
                            String time = today + " 08:00:00";
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            order.setStartSendTime(sdf.parse(time));// 设置开始送餐时间
                            order.setSendType("0");//待取餐
                            order.setSendCourierId(oldman.getCourierId2());
                            order.setSendCourierName(courierMap.get(oldman.getCourierId2()));
                            order.setRecoverType("-1");
                            order.setRecoverCourierId(oldman.getCourierId2());
                            order.setRecoverCourierName(courierMap.get(oldman.getCourierId2()));
                            // order.setCreateTime(date);
                            order.setOrderType(orderType);// 午餐类型
                            order.setUserCode(oldman.getUserCode());
                            order.setUserName(oldman.getUserName());
                            order.setMobile(oldman.getMobile());
                            order.setCommunityId(oldman.getCommunityId());
                            order.setCommunityName(oldman.getCommunityName());
                            orderList.add(order);
                        }
                    } else {// 没有设置用餐日期、骑手、送餐地址信息的老人的老人
                        // System.err.println("当前老人没设置送餐日期：" + o.getUserName());
                    }
                }

                /******************************************/
                resultDTO.setOrderList(orderList);
                resultDTO.setRepeatOrdersCount(repeatOrdersCount);
                return resultDTO;

            } else {
                resultDTO.setOrderList(orderList);
                resultDTO.setRepeatOrdersCount(repeatOrdersCount);
                return resultDTO;
            }
        } else if (orderType.equals("2")) {// 生成晚餐
            List<SysOrder> orderList = new ArrayList<>();
            if (unRepeatOldmanList.size() > 0) {
                for (SysOldman oldman : unRepeatOldmanList) {
                    if (StringUtils.isNotBlank(oldman.getDateCheck3()) && oldman.getCourierId3() != null && oldman.getCourierId3() != 0 && StringUtils.isNotBlank(oldman.getAddress())) {// 判断老人是否设置了送餐日期、骑手、送餐地址

                        String today = getTomorrow();
                        Date date2 = sdfYMD.parse(today);

                        /******** 此处比对订单分配里面的送餐日期，若含有今天的日期，则生成送餐信息 *******/
                        boolean flag = false;
                        flag = isFlag3(oldman, today, flag);

                        if (flag) {// 含有今天日期并且有骑手、送餐地址信息的老人
                            System.err.println("今日需要送餐的老人：" + oldman.getUserName());

                            SysOrder order = new SysOrder();
                            order.setAddress(oldman.getAddress());
                            // torders.setParentId(orderConfig.getGroupId());
                            String time = today + " 08:00:00";
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            order.setStartSendTime(sdf.parse(time));// 设置开始送餐时间
                            order.setSendType("0");//待取餐
                            order.setSendCourierId(oldman.getCourierId3());
                            order.setSendCourierName(courierMap.get(oldman.getCourierId3()));
                            order.setRecoverType("-1");
                            order.setRecoverCourierId(oldman.getCourierId3());
                            order.setRecoverCourierName(courierMap.get(oldman.getCourierId3()));
                            // order.setCreateTime(date);
                            order.setOrderType(orderType);// 晚餐类型
                            order.setUserCode(oldman.getUserCode());
                            order.setUserName(oldman.getUserName());
                            order.setMobile(oldman.getMobile());
                            order.setCommunityId(oldman.getCommunityId());
                            order.setCommunityName(oldman.getCommunityName());
                            orderList.add(order);
                        }
                    } else {// 没有设置用餐日期、骑手、送餐地址信息的老人的老人
                        // System.err.println("当前老人没设置送餐日期：" + o.getUserName());
                    }
                }

                /******************************************/
                resultDTO.setOrderList(orderList);
                resultDTO.setRepeatOrdersCount(repeatOrdersCount);
                return resultDTO;

            } else {
                resultDTO.setOrderList(orderList);
                resultDTO.setRepeatOrdersCount(repeatOrdersCount);
                return resultDTO;
            }
        } else {// 未识别生成餐点类型
            resultDTO.setOrderList(null);
            resultDTO.setRepeatOrdersCount(repeatOrdersCount);
            return resultDTO;
        }
    }

/******************************************************************************************************************/

    /**
     * 获取当前年份
     *
     * @param
     * @return
     */
    public int getYear() {
        Calendar c = Calendar.getInstance(Locale.CHINA);//
        return c.get(Calendar.YEAR); // 获取当前年份
    }

    /**
     * 获取当前月份
     *
     * @param
     * @return
     */
    public int getMonth() {
        Calendar c = Calendar.getInstance(Locale.CHINA);//
        return c.get(Calendar.MONTH) + 1; //
    }

    /**
     * 获取当前月份的天数
     *
     * @param
     * @return
     */
    public int getDay() {
        Calendar a = Calendar.getInstance();
        a.set(Calendar.DATE, 1);
        a.roll(Calendar.DATE, -1);
        return a.get(Calendar.DATE);
    }

    public int getDaysOfMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    /**
     * 获取当前日期所对应的天数
     *
     * @param
     * @return
     */
    public static int getDayOfMonth() {
        Calendar c = Calendar.getInstance(Locale.CHINA);//
        return c.get(Calendar.DAY_OF_MONTH); //
    }

    /**
     * 返回明天
     *
     * @return
     */
    public String getTomorrow() {
        Date date = new Date();//取时间
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(calendar.DATE, 1);//把日期往后增加一天.整数往后推,负数往前移动
        date = calendar.getTime(); //这个时间就是日期往后推一天的结果
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(date);
        return dateString;
    }

    /**
     * 返回今天
     *
     * @return
     */
    public String getToday() {
        Date date = new Date();//取时间
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(calendar.DATE, 0);
        date = calendar.getTime(); //这个时间就是日期往后推一天的结果
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(date);
        return dateString;
    }

    /**
     * 判断是否存在日期范围(早餐)
     *
     * @param o
     * @param date
     * @param flag
     * @return
     * @throws ParseException
     */
    private boolean isFlag1(SysOldman o, String date, boolean flag) throws ParseException {
        if (StringUtils.isNotBlank(o.getDateCheck())) {
            String[] dateSplit = o.getDateCheck().split(",");
            for (String s : dateSplit) {
                s.trim();
//                        String bool = sdfYMD.format(sdfMDY.parse(s));
                if (sdfYMD.format(sdfMDY.parse(s)).equals(date)) {
                    flag = true;
                }
            }
        }
        return flag;
    }

    /**
     * 判断是否存在日期范围(午餐)
     *
     * @param o
     * @param date
     * @param flag
     * @return
     * @throws ParseException
     */
    private boolean isFlag2(SysOldman o, String date, boolean flag) throws ParseException {
        if (StringUtils.isNotBlank(o.getDateCheck2())) {
            String[] dateSplit = o.getDateCheck2().split(",");
            for (String s : dateSplit) {
                s.trim();
//                        String bool = sdfYMD.format(sdfMDY.parse(s));
                if (sdfYMD.format(sdfMDY.parse(s)).equals(date)) {
                    flag = true;
                }
            }
        }
        return flag;
    }

    /**
     * 判断是否存在日期范围(晚餐)
     *
     * @param o
     * @param date
     * @param flag
     * @return
     * @throws ParseException
     */
    private boolean isFlag3(SysOldman o, String date, boolean flag) throws ParseException {
        if (StringUtils.isNotBlank(o.getDateCheck3())) {
            String[] dateSplit = o.getDateCheck3().split(",");
            for (String s : dateSplit) {
                s.trim();
//                        String bool = sdfYMD.format(sdfMDY.parse(s));
                if (sdfYMD.format(sdfMDY.parse(s)).equals(date)) {
                    flag = true;
                }
            }
        }
        return flag;
    }

    /**
     * 生成每日订单的公共方法
     *
     * @param sendOrdersCount
     * @param oldmanList
     * @return
     * @throws ParseException
     */
    public int getSendOrdersCount(int sendOrdersCount, List<SysOldman> oldmanList) throws ParseException {

        String dc = "";

        for (SysOldman o : oldmanList) {
            dc = o.getDateCheck() + o.getDateCheck2() + o.getDateCheck3();
            if (StringUtils.isNotBlank(dc) && o.getCourierId() != null && o.getCourierId() != 0 && StringUtils.isNotBlank(o.getAddress())) {// 判断老人是否设置了送餐日期、骑手、送餐地址

                String today = getToday();
                Date date = sdfYMD.parse(today);

                /******** 此处比对订单分配里面的送餐日期，若含有今天的日期，则生成送餐信息 *******/
                boolean flag1 = false;
                flag1 = isFlag1(o, today, flag1);
                if (flag1) {// 含有今天早餐日期并且有骑手、送餐地址信息的老人
                    System.err.println("今日需要送早餐的老人：" + o.getUserName());
                    sendOrdersCount++;
                }

                boolean flag2 = false;
                flag2 = isFlag2(o, today, flag2);
                if (flag2) {// 含有今天午餐日期并且有骑手、送餐地址信息的老人
                    System.err.println("今日需要送午餐的老人：" + o.getUserName());
                    sendOrdersCount++;
                }

                boolean flag3 = false;
                flag3 = isFlag3(o, today, flag3);
                if (flag3) {// 含有今天晚餐日期并且有骑手、送餐地址信息的老人
                    System.err.println("今日需要送晚餐的老人：" + o.getUserName());
                    sendOrdersCount++;
                }

            } else {// 没有设置用餐日期、骑手、送餐地址信息的老人的老人
                // System.err.println("当前老人没设置送餐日期：" + o.getUserName());
            }
        }
        return sendOrdersCount;
    }
}
