package com.ruoyi.web.controller.system;

import ch.qos.logback.core.net.SyslogOutputStream;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.base.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ExcelUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.framework.web.base.BaseController;
import com.ruoyi.framework.web.domain.server.Sys;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.quartz.util.Encryption;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.service.*;
import com.ruoyi.web.controller.tool.CommunityGetKeyIDs;
import com.ruoyi.web.controller.tool.OrderGenerateHelper;
import com.ruoyi.web.controller.tool.OrderResultDTO;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 送餐分配信息操作处理
 *
 * @author ruoyi
 */
@Controller
@RequestMapping("/system/sendFood")
public class SysSendFoodController extends BaseController {
    private String prefix = "system/sendFood";

    @Autowired
    private ISysOldManService oldManService;

    @Autowired
    private ISysCourierService courierService;

    @Autowired
    private ISysOrderService orderService;

    @Autowired
    private ISysOrderBillService orderBillService;

    @Autowired
    private ISysFoodPackageService foodPackageService;

    @Autowired
    private ISysCommunityService communityService;

    private SimpleDateFormat sdfMDY = new SimpleDateFormat("MM/dd/yyyy");
    private SimpleDateFormat sdfYMD = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");

    @RequiresPermissions("system:sendFood:view")
    @GetMapping()
    public String operlog() {
        return prefix + "/sendFood";
    }

    @RequiresPermissions("system:sendFood:view")
    @GetMapping("/2")
    public String operlog2() {
        return prefix + "/sendFood2";
    }

    @RequiresPermissions("system:sendFood:view")
    @GetMapping("/3")
    public String operlog3() {
        return prefix + "/sendFood3";
    }

    @RequiresPermissions("system:sendFood:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysOldman oldman) {

        // 数据查看范围权限(要放在分页startPage();前面，否则只能查询出10个)
        SysCommunity s = new SysCommunity();
        s.setStatus("0");
        List<SysCommunity> communityList = communityService.selectCommunityList(s);
        CommunityGetKeyIDs getKeyIDs = new CommunityGetKeyIDs();
        oldman.setKeyIDs(getKeyIDs.communityGetKeyIDs(communityList));

        startPage();
        if (StringUtils.isBlank(oldman.getStatus())) {
            oldman.setStatus("0");
        }
        List<SysOldman> list = oldManService.selectOldManList(oldman);
        return getDataTable(list);
    }

    /**
     * 生成今日送餐订单(手动)
     *
     * @return
     */
    @RequiresPermissions("system:sendFood:addTodayOrders")
    @Log(title = "生成今日送餐订单", businessType = BusinessType.INSERT)
    @PostMapping("/addTodayOrders")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public String addTodayOrders() throws Exception {
        System.err.println("进入生成今日送餐订单");

        List<SysOldman> oldmanList = new ArrayList<>();// 接收到的所有老人List
        SysOldman oStatus = new SysOldman();
        oStatus.setStatus("0");
        oldmanList = oldManService.selectOldManList(oStatus);// 获取所有状态正常的老人信息
        List<SysCourier> courierList = new ArrayList<>();// 骑手List
        SysCourier cStatus = new SysCourier();
        cStatus.setStatus("0");
        courierList = courierService.selectCourierList(cStatus);// 获取所有状态正常的骑手信息

        SysOrder orderParam = new SysOrder();
        orderParam.setStatus(0);
        Map<String, Object> params = new HashMap<>();
        params.put("beginTime", getToday() + " 00:00:00");
        params.put("endTime", getToday() + " 23:59:59");
        orderParam.setParams(params);

        List<SysOrder> todayOrderList1 = new ArrayList<>();
        orderParam.setOrderType("0");// 餐点类型
        todayOrderList1 = orderService.selectTodayOrderList(orderParam);// 获取今天的所有早餐订单

        List<SysOrder> todayOrderList2 = new ArrayList<>();
        orderParam.setOrderType("1");// 餐点类型
        todayOrderList2 = orderService.selectTodayOrderList(orderParam);// 获取今天的所有午餐订单

        List<SysOrder> todayOrderList3 = new ArrayList<>();
        orderParam.setOrderType("2");// 餐点类型
        todayOrderList3 = orderService.selectTodayOrderList(orderParam);// 获取今天的所有晚餐订单


        OrderGenerateHelper helper = new OrderGenerateHelper();
        StringBuffer result = new StringBuffer();

        OrderResultDTO resultDTO = new OrderResultDTO();

        List<SysOrder> breakfastList = new ArrayList<>();// 早餐集合
        List<SysOrder> lunchList = new ArrayList<>();// 午餐集合
        List<SysOrder> dinnerList = new ArrayList<>();// 晚餐集合

        // 生成早餐
        resultDTO = helper.generateTodayOrders(todayOrderList1, oldmanList, courierList, "0");
        breakfastList = resultDTO.getOrderList();
        if (breakfastList.size() > 0) {
            int SendOrdersCount = orderService.saveOrders(breakfastList);
            result.append("【早餐】：已成功自动生成 " + SendOrdersCount + " 份今日早餐订单，已去除了重复的 " + resultDTO.getRepeatOrdersCount() + " 份早餐订单；");
        } else {
            result.append("【早餐】：未能生成早餐订单，因为没有足够的满足早餐订单生成条件的老人；");
        }

        // 生成午餐
        resultDTO = null;
        resultDTO = helper.generateTodayOrders(todayOrderList2, oldmanList, courierList, "1");
        lunchList = resultDTO.getOrderList();
        if (lunchList.size() > 0) {
            int SendOrdersCount = orderService.saveOrders(lunchList);
            result.append("【午餐】：已成功自动生成 " + SendOrdersCount + " 份今日午餐订单，已去除了重复的 " + resultDTO.getRepeatOrdersCount() + " 份午餐订单；");
        } else {
            result.append("【午餐】：未能生成午餐订单，因为没有足够的满足午餐订单生成条件的老人；");
        }

        // 生成晚餐
        resultDTO = null;
        resultDTO = helper.generateTodayOrders(todayOrderList3, oldmanList, courierList, "2");
        dinnerList = resultDTO.getOrderList();
        if (dinnerList.size() > 0) {
            int SendOrdersCount = orderService.saveOrders(dinnerList);
            result.append("【晚餐】：已成功自动生成 " + SendOrdersCount + " 份今日晚餐订单，已去除了重复的 " + resultDTO.getRepeatOrdersCount() + " 份晚餐订单；");
        } else {
            result.append("【晚餐】：未能生成晚餐订单，因为没有足够的满足晚餐订单生成条件的老人；");
        }

        return result.toString();
    }

    /**
     * 生成今日送餐订单(手动，单独老人)
     *
     * @return
     */
    @RequiresPermissions("system:sendFood:addTodayOrderByOldManId")
    @Log(title = "生成今日送餐订单", businessType = BusinessType.INSERT)
    @PostMapping("/addTodayOrderByOldManId")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public String addTodayOrderByOldManId(long userCode) throws Exception {
        System.err.println("进入生成今日送餐订单【单独老人】");
        /******************************************/
        List<SysOldman> oldmanList = new ArrayList<>();// 接收到的所有老人List
        List<SysCourier> courierList = new ArrayList<>();// 骑手List
        SysOldman oStatus = new SysOldman();
        oStatus.setUserCode(userCode);
        oStatus.setStatus("0");
        oldmanList = oldManService.selectOldManList(oStatus);// 获取所有状态正常的老人信息
        SysCourier cStatus = new SysCourier();
        cStatus.setStatus("0");
        courierList = courierService.selectCourierList(cStatus);// 获取所有状态正常的骑手信息

        SysOrder orderParam = new SysOrder();
        orderParam.setStatus(0);
        Map<String, Object> params = new HashMap<>();
        params.put("beginTime", getToday() + " 00:00:00");
        params.put("endTime", getToday() + " 23:59:59");
        orderParam.setParams(params);

        List<SysOrder> todayOrderList1 = new ArrayList<>();
        orderParam.setOrderType("0");// 餐点类型
        todayOrderList1 = orderService.selectTodayOrderList(orderParam);// 获取今天的所有早餐订单

        List<SysOrder> todayOrderList2 = new ArrayList<>();
        orderParam.setOrderType("1");// 餐点类型
        todayOrderList2 = orderService.selectTodayOrderList(orderParam);// 获取今天的所有午餐订单

        List<SysOrder> todayOrderList3 = new ArrayList<>();
        orderParam.setOrderType("2");// 餐点类型
        todayOrderList3 = orderService.selectTodayOrderList(orderParam);// 获取今天的所有晚餐订单


        OrderGenerateHelper helper = new OrderGenerateHelper();
        StringBuffer result = new StringBuffer();

        OrderResultDTO resultDTO = new OrderResultDTO();

        List<SysOrder> breakfastList = new ArrayList<>();// 早餐集合
        List<SysOrder> lunchList = new ArrayList<>();// 午餐集合
        List<SysOrder> dinnerList = new ArrayList<>();// 晚餐集合

        // 生成早餐
        resultDTO = helper.generateTodayOrders(todayOrderList1, oldmanList, courierList, "0");
        breakfastList = resultDTO.getOrderList();
        if (breakfastList.size() > 0) {
            int SendOrdersCount = orderService.saveOrders(breakfastList);
            result.append("【早餐】：已成功自动生成 " + SendOrdersCount + " 份今日早餐订单，已去除了重复的 " + resultDTO.getRepeatOrdersCount() + " 份早餐订单；");
        } else {
            result.append("【早餐】：未能生成早餐订单，因为没有足够的满足早餐订单生成条件的老人；");
        }

        // 生成午餐
        resultDTO = null;
        resultDTO = helper.generateTodayOrders(todayOrderList2, oldmanList, courierList, "1");
        lunchList = resultDTO.getOrderList();
        if (lunchList.size() > 0) {
            int SendOrdersCount = orderService.saveOrders(lunchList);
            result.append("【午餐】：已成功自动生成 " + SendOrdersCount + " 份今日午餐订单，已去除了重复的 " + resultDTO.getRepeatOrdersCount() + " 份午餐订单；");
        } else {
            result.append("【午餐】：未能生成午餐订单，因为没有足够的满足午餐订单生成条件的老人；");
        }

        // 生成晚餐
        resultDTO = null;
        resultDTO = helper.generateTodayOrders(todayOrderList3, oldmanList, courierList, "2");
        dinnerList = resultDTO.getOrderList();
        if (dinnerList.size() > 0) {
            int SendOrdersCount = orderService.saveOrders(dinnerList);
            result.append("【晚餐】：已成功自动生成 " + SendOrdersCount + " 份今日晚餐订单，已去除了重复的 " + resultDTO.getRepeatOrdersCount() + " 份晚餐订单；");
        } else {
            result.append("【晚餐】：未能生成晚餐订单，因为没有足够的满足晚餐订单生成条件的老人；");
        }

        return result.toString();
    }

    @RequiresPermissions("system:sendFood:TomorrowList")
    @PostMapping("/TomorrowList")
    @ResponseBody
    public TableDataInfo TomorrowList(SysOldman oldman) throws Exception {
        startPage();
        List<SysOrder> list = new ArrayList<>();
        /******************************************/
        System.err.println("进入生成明日送餐订单预览");
        int SendOrdersCount = 0;

        List<SysOldman> oldmanList = new ArrayList<>();// 老人List

        SysOldman oStatus = new SysOldman();
        oStatus.setStatus("0");
        oldmanList = oldManService.selectOldManList(oStatus);// 获取所有状态正常的老人信息

        for (SysOldman o : oldmanList) {
            if (StringUtils.isNotBlank(o.getDateCheck()) && o.getCourierId() != null && o.getCourierId() != 0 && StringUtils.isNotBlank(o.getAddress())) {// 判断老人是否设置了送餐日期、骑手、送餐地址

                String tomorrow = getTomorrow();
                Date date = sdfYMD.parse(tomorrow);

                /******** 此处比对订单分配里面的送餐日期，若含有明天的日期，则生成送餐预览 *******/
                boolean flag = false;
                flag = isFlag(o, tomorrow, flag);

                if (flag) {// 含有今天日期并且有骑手、送餐地址信息的老人
                    System.err.println("明天需要送餐的老人：" + o.getUserName());
                    SendOrdersCount++;
                }

            } else {// 没有设置用餐日期、骑手、送餐地址信息的老人的老人
                // System.err.println("当前老人没设置送餐日期：" + o.getUserName());
            }
        }

        return getDataTable(list);
    }

    /**
     * 修改送餐分配信息 跳转修改页面(本月&下月)
     *
     * @param userCodeStr
     * @param mmap
     * @return
     */
    @GetMapping("/edit/{userCode}")
    public String edit(@PathVariable("userCode") String userCodeStr, ModelMap mmap) {
        SysCourier courier = new SysCourier();
        courier.setStatus("0");
        SysFoodPackage foodPackage = new SysFoodPackage();
        foodPackage.setStatus("0");
        String userCodeStrDecode = URLDecoder.decode(userCodeStr);
        String key = userCodeStrDecode.substring(userCodeStrDecode.indexOf("KKKEEEYYY") + 9, userCodeStrDecode.length());
        String userCode = userCodeStrDecode.substring(0, userCodeStrDecode.indexOf("KKKEEEYYY"));

        String url = "";

        /** 早餐 **/
        if (key.equals("0")) {// 配置当月信息
            // 查询本月老人是否已经生成过账单
            SysOrderBill orderBill = new SysOrderBill();
            orderBill.setUserCode(Long.valueOf(userCode));
            Map<String, Object> params = new HashMap<>();
            params.put("beginTime", getYear() + "-" + getMonth() + "-01" + " 00:00:00");// 月头
            params.put("endTime", getYear() + "-" + getMonth() + "-" + getDay() + " 23:59:59");// 月尾
            orderBill.setParams(params);
            List<SysOrderBill> orderBillList = orderBillService.selectOrderBillList(orderBill);
            if (orderBillList.size() > 0) {// 如果已经生成过账单
                mmap.put("orderBill", orderBillList.get(0));
            } else {
                mmap.put("orderBill", orderBill);
            }

            List<SysCourier> courierList = new ArrayList<>();
            courierList = courierService.selectCourierList(courier);
            SysCourier c = new SysCourier();
            c.setId(Long.valueOf(0));
            c.setCourierName("不分配");
            courierList.add(0, c);

            List<SysFoodPackage> foodList = new ArrayList<>();
            foodList = foodPackageService.selectFoodPackageList(foodPackage);
            SysFoodPackage f = new SysFoodPackage();
            f.setId(Long.valueOf(0));
            f.setFoodPackageName("不分配");
            foodList.add(0, f);

            mmap.put("oldMan", oldManService.selectOldManById(Long.valueOf(userCode)));
            mmap.put("courierList", courierList);
            mmap.put("foodList", foodList);
            mmap.put("status", "0");

            url = prefix + "/editDateAndCourier";
        } else if (key.equals("1")) {// 配置下月信息
            // 查询本月老人是否已经生成过账单
            SysOrderBill orderBill = new SysOrderBill();
            orderBill.setUserCode(Long.valueOf(userCode));
            Map<String, Object> params = new HashMap<>();
            params.put("beginTime", getYear() + "-" + getMonth() + "-01" + " 00:00:00");// 月头
            params.put("endTime", getYear() + "-" + getMonth() + "-" + getDay() + " 23:59:59");// 月尾
            orderBill.setParams(params);
            List<SysOrderBill> orderBillList = orderBillService.selectOrderBillList(orderBill);
            if (orderBillList.size() > 0) {// 如果已经生成过账单
                mmap.put("orderBill", orderBillList.get(0));
            } else {
                mmap.put("orderBill", orderBill);
            }

            List<SysCourier> courierList = new ArrayList<>();
            courierList = courierService.selectCourierList(courier);
            SysCourier c = new SysCourier();
            c.setId(Long.valueOf(0));
            c.setCourierName("不分配");
            courierList.add(0, c);

            List<SysFoodPackage> foodList = new ArrayList<>();
            foodList = foodPackageService.selectFoodPackageList(foodPackage);
            SysFoodPackage f = new SysFoodPackage();
            f.setId(Long.valueOf(0));
            f.setFoodPackageName("不分配");
            foodList.add(0, f);

            mmap.put("oldMan", oldManService.selectOldManById(Long.valueOf(userCode)));
            mmap.put("courierList", courierList);
            mmap.put("foodList", foodList);
            mmap.put("status", "1");

            url = prefix + "/editDateAndCourierNextMonth";
        }

        /** 午餐 **/
        if (key.equals("00")) {// 配置当月信息
            // 查询本月老人是否已经生成过账单
            SysOrderBill orderBill = new SysOrderBill();
            orderBill.setUserCode(Long.valueOf(userCode));
            Map<String, Object> params = new HashMap<>();
            params.put("beginTime", getYear() + "-" + getMonth() + "-01" + " 00:00:00");// 月头
            params.put("endTime", getYear() + "-" + getMonth() + "-" + getDay() + " 23:59:59");// 月尾
            orderBill.setParams(params);
            List<SysOrderBill> orderBillList = orderBillService.selectOrderBillList(orderBill);
            if (orderBillList.size() > 0) {// 如果已经生成过账单
                mmap.put("orderBill", orderBillList.get(0));
            } else {
                mmap.put("orderBill", orderBill);
            }

            List<SysCourier> courierList = new ArrayList<>();
            courierList = courierService.selectCourierList(courier);
            SysCourier c = new SysCourier();
            c.setId(Long.valueOf(0));
            c.setCourierName("不分配");
            courierList.add(0, c);

            List<SysFoodPackage> foodList = new ArrayList<>();
            foodList = foodPackageService.selectFoodPackageList(foodPackage);
            SysFoodPackage f = new SysFoodPackage();
            f.setId(Long.valueOf(0));
            f.setFoodPackageName("不分配");
            foodList.add(0, f);

            mmap.put("oldMan", oldManService.selectOldManById(Long.valueOf(userCode)));
            mmap.put("courierList", courierList);
            mmap.put("foodList", foodList);
            mmap.put("status", "00");

            url = prefix + "/editDateAndCourier";
        } else if (key.equals("11")) {// 配置下月信息
            // 查询本月老人是否已经生成过账单
            SysOrderBill orderBill = new SysOrderBill();
            orderBill.setUserCode(Long.valueOf(userCode));
            Map<String, Object> params = new HashMap<>();
            params.put("beginTime", getYear() + "-" + getMonth() + "-01" + " 00:00:00");// 月头
            params.put("endTime", getYear() + "-" + getMonth() + "-" + getDay() + " 23:59:59");// 月尾
            orderBill.setParams(params);
            List<SysOrderBill> orderBillList = orderBillService.selectOrderBillList(orderBill);
            if (orderBillList.size() > 0) {// 如果已经生成过账单
                mmap.put("orderBill", orderBillList.get(0));
            } else {
                mmap.put("orderBill", orderBill);
            }

            List<SysCourier> courierList = new ArrayList<>();
            courierList = courierService.selectCourierList(courier);
            SysCourier c = new SysCourier();
            c.setId(Long.valueOf(0));
            c.setCourierName("不分配");
            courierList.add(0, c);

            List<SysFoodPackage> foodList = new ArrayList<>();
            foodList = foodPackageService.selectFoodPackageList(foodPackage);
            SysFoodPackage f = new SysFoodPackage();
            f.setId(Long.valueOf(0));
            f.setFoodPackageName("不分配");
            foodList.add(0, f);

            mmap.put("oldMan", oldManService.selectOldManById(Long.valueOf(userCode)));
            mmap.put("courierList", courierList);
            mmap.put("foodList", foodList);
            mmap.put("status", "11");

            url = prefix + "/editDateAndCourierNextMonth";
        }

        /** 晚餐 **/
        if (key.equals("000")) {// 配置当月信息
            // 查询本月老人是否已经生成过账单
            SysOrderBill orderBill = new SysOrderBill();
            orderBill.setUserCode(Long.valueOf(userCode));
            Map<String, Object> params = new HashMap<>();
            params.put("beginTime", getYear() + "-" + getMonth() + "-01" + " 00:00:00");// 月头
            params.put("endTime", getYear() + "-" + getMonth() + "-" + getDay() + " 23:59:59");// 月尾
            orderBill.setParams(params);
            List<SysOrderBill> orderBillList = orderBillService.selectOrderBillList(orderBill);
            if (orderBillList.size() > 0) {// 如果已经生成过账单
                mmap.put("orderBill", orderBillList.get(0));
            } else {
                mmap.put("orderBill", orderBill);
            }

            List<SysCourier> courierList = new ArrayList<>();
            courierList = courierService.selectCourierList(courier);
            SysCourier c = new SysCourier();
            c.setId(Long.valueOf(0));
            c.setCourierName("不分配");
            courierList.add(0, c);

            List<SysFoodPackage> foodList = new ArrayList<>();
            foodList = foodPackageService.selectFoodPackageList(foodPackage);
            SysFoodPackage f = new SysFoodPackage();
            f.setId(Long.valueOf(0));
            f.setFoodPackageName("不分配");
            foodList.add(0, f);

            mmap.put("oldMan", oldManService.selectOldManById(Long.valueOf(userCode)));
            mmap.put("courierList", courierList);
            mmap.put("foodList", foodList);
            mmap.put("status", "000");

            url = prefix + "/editDateAndCourier";
        } else if (key.equals("111")) {// 配置下月信息
            // 查询本月老人是否已经生成过账单
            SysOrderBill orderBill = new SysOrderBill();
            orderBill.setUserCode(Long.valueOf(userCode));
            Map<String, Object> params = new HashMap<>();
            params.put("beginTime", getYear() + "-" + getMonth() + "-01" + " 00:00:00");// 月头
            params.put("endTime", getYear() + "-" + getMonth() + "-" + getDay() + " 23:59:59");// 月尾
            orderBill.setParams(params);
            List<SysOrderBill> orderBillList = orderBillService.selectOrderBillList(orderBill);
            if (orderBillList.size() > 0) {// 如果已经生成过账单
                mmap.put("orderBill", orderBillList.get(0));
            } else {
                mmap.put("orderBill", orderBill);
            }

            List<SysCourier> courierList = new ArrayList<>();
            courierList = courierService.selectCourierList(courier);
            SysCourier c = new SysCourier();
            c.setId(Long.valueOf(0));
            c.setCourierName("不分配");
            courierList.add(0, c);

            List<SysFoodPackage> foodList = new ArrayList<>();
            foodList = foodPackageService.selectFoodPackageList(foodPackage);
            SysFoodPackage f = new SysFoodPackage();
            f.setId(Long.valueOf(0));
            f.setFoodPackageName("不分配");
            foodList.add(0, f);

            mmap.put("oldMan", oldManService.selectOldManById(Long.valueOf(userCode)));
            mmap.put("courierList", courierList);
            mmap.put("foodList", foodList);
            mmap.put("status", "111");

            url = prefix + "/editDateAndCourierNextMonth";
        }

        return url;
    }

    /**
     * 修改送餐分配日期
     *
     * @param sysOldman
     * @return
     */
    @RequiresPermissions("system:sendFood:edit")
    @Log(title = "送餐分配管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public AjaxResult editSave(SysOldman sysOldman) {
        sysOldman.setUpdateBy(ShiroUtils.getLoginName());
        return toAjax(oldManService.updateOldMan(sysOldman));
    }

    /**
     * 根据userCode查询老人信息，并跳转到批量修改老人订单送餐员页面
     *
     * @param ids
     * @param mmap
     * @return
     */
    @GetMapping("/editAll/{ids}")
    public String editAll(@PathVariable("ids") String ids, ModelMap mmap) {
        System.err.println(ids);
//        List<String> idsStr = Arrays.asList(ids.split("KKKEEE"));

        SysCourier courier = new SysCourier();
        courier.setStatus("0");
        List<SysCourier> courierList = courierService.selectCourierList(courier);
        SysCourier c = new SysCourier();
        c.setId(Long.valueOf(0));
        c.setCourierName("不分配");
        courierList.add(0, c);
        mmap.put("courierList", courierList);

        SysOldman sysOldman = new SysOldman();
        sysOldman.setIds(ids);
        mmap.put("sysOldman", sysOldman);
        return prefix + "/editAllCourier";
    }

    /**
     * 根据userCode查询老人信息，并跳转到批量修改老人订单送餐员页面
     *
     * @param ids
     * @param mmap
     * @return
     */
    @GetMapping("/editAll2/{ids}")
    public String editAll2(@PathVariable("ids") String ids, ModelMap mmap) {
        System.err.println(ids);
//        List<String> idsStr = Arrays.asList(ids.split("KKKEEE"));

        SysCourier courier = new SysCourier();
        courier.setStatus("0");
        List<SysCourier> courierList = courierService.selectCourierList(courier);
        SysCourier c = new SysCourier();
        c.setId(Long.valueOf(0));
        c.setCourierName("不分配");
        courierList.add(0, c);
        mmap.put("courierList", courierList);

        SysOldman sysOldman = new SysOldman();
        sysOldman.setIds(ids);
        mmap.put("sysOldman", sysOldman);
        return prefix + "/editAllCourier2";
    }

    /**
     * 根据userCode查询老人信息，并跳转到批量修改老人订单送餐员页面
     *
     * @param ids
     * @param mmap
     * @return
     */
    @GetMapping("/editAll3/{ids}")
    public String editAll3(@PathVariable("ids") String ids, ModelMap mmap) {
        System.err.println(ids);
//        List<String> idsStr = Arrays.asList(ids.split("KKKEEE"));

        SysCourier courier = new SysCourier();
        courier.setStatus("0");
        List<SysCourier> courierList = courierService.selectCourierList(courier);
        SysCourier c = new SysCourier();
        c.setId(Long.valueOf(0));
        c.setCourierName("不分配");
        courierList.add(0, c);
        mmap.put("courierList", courierList);

        SysOldman sysOldman = new SysOldman();
        sysOldman.setIds(ids);
        mmap.put("sysOldman", sysOldman);
        return prefix + "/editAllCourier3";
    }

    /**
     * 根据老人IDs以及配送员ID进行批量分配
     *
     * @param sysOldman
     * @return
     */
    @RequiresPermissions("system:sendFood:edit")
    @Log(title = "送餐批量分配管理", businessType = BusinessType.UPDATE)
    @PostMapping("/editAllCourier")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public AjaxResult editAllCourier(SysOldman sysOldman) {
        String ids = sysOldman.getIds();
        System.out.println("ids = " + ids);
        sysOldman.setUpdateBy(ShiroUtils.getLoginName());
        return toAjax(oldManService.updateOldManList(sysOldman));
    }
    /******************************************************************************************************************/

    /**
     * 获取当前年份
     *
     * @param
     * @return
     */
    public int getYear() {
        Calendar c = Calendar.getInstance(Locale.CHINA);//
        return c.get(Calendar.YEAR); // 获取当前年份
    }

    /**
     * 获取当前月份
     *
     * @param
     * @return
     */
    public int getMonth() {
        Calendar c = Calendar.getInstance(Locale.CHINA);//
        return c.get(Calendar.MONTH) + 1; //
    }

    /**
     * 获取当前月份的天数
     *
     * @param
     * @return
     */
    public int getDay() {
        Calendar a = Calendar.getInstance();
        a.set(Calendar.DATE, 1);
        a.roll(Calendar.DATE, -1);
        return a.get(Calendar.DATE);
    }

    public int getDaysOfMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    /**
     * 获取当前日期所对应的天数
     *
     * @param
     * @return
     */
    public static int getDayOfMonth() {
        Calendar c = Calendar.getInstance(Locale.CHINA);//
        return c.get(Calendar.DAY_OF_MONTH); //
    }

    /**
     * 返回明天
     *
     * @return
     */
    public String getTomorrow() {
        Date date = new Date();//取时间
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(calendar.DATE, 1);//把日期往后增加一天.整数往后推,负数往前移动
        date = calendar.getTime(); //这个时间就是日期往后推一天的结果
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(date);
        return dateString;
    }

    /**
     * 返回今天
     *
     * @return
     */
    public String getToday() {
        Date date = new Date();//取时间
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(calendar.DATE, 0);
        date = calendar.getTime(); //这个时间就是日期往后推一天的结果
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(date);
        return dateString;
    }

    /**
     * 判断是否存在日期范围
     *
     * @param o
     * @param date
     * @param flag
     * @return
     * @throws ParseException
     */
    private boolean isFlag(SysOldman o, String date, boolean flag) throws ParseException {
        if (StringUtils.isNotBlank(o.getDateCheck())) {
            String[] dateSplit = o.getDateCheck().split(",");
            for (String s : dateSplit) {
                s.trim();
//                        String bool = sdfYMD.format(sdfMDY.parse(s));
                if (sdfYMD.format(sdfMDY.parse(s)).equals(date)) {
                    flag = true;
                }
            }
        }
        return flag;
    }

    /**
     * 生成每日订单的公共方法
     *
     * @param sendOrdersCount
     * @param oldmanList
     * @return
     * @throws ParseException
     */
    public int getSendOrdersCount(int sendOrdersCount, List<SysOldman> oldmanList) throws ParseException {
        for (SysOldman o : oldmanList) {
            if (StringUtils.isNotBlank(o.getDateCheck()) && o.getCourierId() != null && o.getCourierId() != 0 && StringUtils.isNotBlank(o.getAddress())) {// 判断老人是否设置了送餐日期、骑手、送餐地址

                String today = getToday();
                Date date = sdfYMD.parse(today);

                /******** 此处比对订单分配里面的送餐日期，若含有今天的日期，则生成送餐信息 *******/
                boolean flag = false;
                flag = isFlag(o, today, flag);

                if (flag) {// 含有今天日期并且有骑手、送餐地址信息的老人
                    System.err.println("今日需要送餐的老人：" + o.getUserName());
                    sendOrdersCount++;
                }

            } else {// 没有设置用餐日期、骑手、送餐地址信息的老人的老人
                // System.err.println("当前老人没设置送餐日期：" + o.getUserName());
            }
        }
        return sendOrdersCount;
    }

}
