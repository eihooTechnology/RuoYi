package com.ruoyi.web.controller.tool;

import com.alibaba.fastjson.JSON;

/**
 * @ClassName JsonToList
 * @Description TODO
 * @Author lzjian
 * @Date 2019-04-11 17:36
 **/
public class JsonToList {

    public static <T> T getJsonToList(String jsonString, Class cls) {
        T t = null;
        try {
            t = (T) JSON.parseObject(jsonString, cls);
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }
        return t;
    }

}
