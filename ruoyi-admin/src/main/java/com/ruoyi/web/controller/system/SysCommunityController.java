package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.base.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.framework.web.base.BaseController;
import com.ruoyi.system.domain.SysCommunity;
import com.ruoyi.system.domain.SysRole;
import com.ruoyi.system.service.ISysCommunityService;
import com.ruoyi.web.controller.tool.CommunityGetKeyIDs;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

/**
 * 社区信息
 *
 * @author ruoyi
 */
@Controller
@RequestMapping("/system/community")
public class SysCommunityController extends BaseController {
    private String prefix = "system/community";

    @Autowired
    private ISysCommunityService communityService;

    @RequiresPermissions("system:community:view")
    @GetMapping()
    public String dept() {
        return prefix + "/community";
    }

    @RequiresPermissions("system:community:list")
    @GetMapping("/list")
    @ResponseBody
    public List<SysCommunity> list(SysCommunity community) {
        // 数据查看范围权限(要放在分页startPage();前面，否则只能查询出10个)
        SysCommunity s = new SysCommunity();
        s.setStatus("0");
        List<SysCommunity> communityList = communityService.selectCommunityList(s);
        CommunityGetKeyIDs getKeyIDs = new CommunityGetKeyIDs();
        community.setKeyIDs(getKeyIDs.communityGetKeyIDs(communityList));

        List<SysCommunity> deptList = communityService.selectCommunityList(community);
        return deptList;
    }

    /**
     * 新增社区
     */
    @GetMapping("/add/{parentId}")
    public String add(@PathVariable("parentId") Long parentId, ModelMap mmap) {
        if (parentId == 0) {
            mmap.put("community", null);
            return prefix + "/addRoot";
        } else {
            mmap.put("community", communityService.selectCommunityById(parentId));
            return prefix + "/add";
        }
    }

    /**
     * 新增保存社区
     */
    @Log(title = "社区管理", businessType = BusinessType.INSERT)
    @RequiresPermissions("system:community:add")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SysCommunity community) {
        community.setCreateBy(ShiroUtils.getLoginName());
        return toAjax(communityService.insertCommunity(community));
    }

    /**
     * 新增保存社区根节点
     */
    @Log(title = "社区管理", businessType = BusinessType.INSERT)
    @RequiresPermissions("system:community:addRoot")
    @PostMapping("/addRoot")
    @ResponseBody
    public AjaxResult addRootSave(SysCommunity community) {
        community.setCreateBy(ShiroUtils.getLoginName());
        return toAjax(communityService.insertRootCommunityTree(community));
    }

    /**
     * 修改
     */
    @GetMapping("/edit/{communityId}")
    public String edit(@PathVariable("communityId") Long communityId, ModelMap mmap) {
        SysCommunity community = communityService.selectCommunityById(communityId);
        if (StringUtils.isNotNull(community) && 100L == communityId) {
            community.setParentName("无");
        }
        mmap.put("community", community);
        return prefix + "/edit";
    }

    /**
     * 保存
     */
    @Log(title = "社区管理", businessType = BusinessType.UPDATE)
    @RequiresPermissions("system:community:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysCommunity community) {
        community.setUpdateBy(ShiroUtils.getLoginName());
        return toAjax(communityService.updateCommunity(community));
    }

    /**
     * 删除
     */
    @Log(title = "社区管理", businessType = BusinessType.DELETE)
    @RequiresPermissions("system:community:remove")
    @PostMapping("/remove/{communityId}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("communityId") Long communityId) {
        if (communityService.selectCommunityCount(communityId) > 0) {
            return error(1, "存在下级社区,不允许删除");
        }
        if (communityService.checkCommunityExistUser(communityId)) {
            return error(1, "社区存在用户,不允许删除");
        }
        return toAjax(communityService.deleteCommunityById(communityId));
    }

    /**
     * 校验社区名称
     */
    @PostMapping("/checkCommunityNameUnique")
    @ResponseBody
    public String checkCommunityNameUnique(SysCommunity community) {
        return communityService.checkCommunityNameUnique(community);
    }

    /**
     * 选择社区树
     */
    @GetMapping("/selectCommunityTree/{communityId}")
    public String selectCommunityTree(@PathVariable("communityId") Long communityId, ModelMap mmap) {
        mmap.put("community", communityService.selectCommunityById(communityId));
        return prefix + "/tree";
    }

    /**
     * 加载社区列表树
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Map<String, Object>> treeData() {
        // 数据查看范围权限(要放在分页startPage();前面，否则只能查询出10个)
        SysCommunity s = new SysCommunity();
        s.setStatus("0");
        List<SysCommunity> communityList = communityService.selectCommunityList(s);
        CommunityGetKeyIDs getKeyIDs = new CommunityGetKeyIDs();
        s.setKeyIDs(getKeyIDs.communityGetKeyIDs(communityList));

        List<Map<String, Object>> tree = communityService.selectCommunityTree(s);
        return tree;
    }

    /**
     * 加载角色社区（数据权限）列表树
     */
    @GetMapping("/roleCommunityTreeData")
    @ResponseBody
    public List<Map<String, Object>> CommunityTreeData(SysRole role) {
        List<Map<String, Object>> tree = communityService.roleCommunityTreeData(role);
        return tree;
    }
}
