package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.base.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.framework.web.base.BaseController;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.quartz.util.Encryption;
import com.ruoyi.system.domain.SysCourier;
import com.ruoyi.system.domain.SysOldman;
import com.ruoyi.system.domain.SysOrder;
import com.ruoyi.system.domain.SysOrderBill;
import com.ruoyi.system.service.*;
import com.ruoyi.web.controller.tool.OrderGenerateHelper;
import com.ruoyi.web.controller.tool.OrderResultDTO;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 老人管理调用层
 *
 * @author ruoyi
 */
@Controller
@RequestMapping("/system/task")
public class SysTaskController extends BaseController {
    private String prefix = "system/task";

    @Autowired
    private ISysOldManService oldManService;

    @Autowired
    private ISysRoleService roleService;

    @Autowired
    private ISysPostService postService;

    @Autowired
    private ISysOrderBillService orderBillService;

    @Autowired
    private ISysCourierService courierService;

    @Autowired
    private ISysOrderPreviewService orderPreviewService;

    @Autowired
    private ISysOrderService orderService;

    private SimpleDateFormat sdfMDY = new SimpleDateFormat("MM/dd/yyyy");
    private SimpleDateFormat sdfYMD = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");

    /**
     * 定时刷新明日订单预览(接口)
     *
     * @return
     */
    @Log(title = "定时刷新明日订单预览", businessType = BusinessType.INSERT)
    @PostMapping("/generateTorromowOrdersPreviewApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public void generateTorromowOrdersPreviewApi(String timeStamp, String MD5) throws Exception {
        System.err.println("定时刷新明日订单预览，执行时间：" + df.format(new Date()));
        /**********************接口请求合法性验证********************/
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(MD5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(10000);// 超时时间为10s
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过10秒
                if (Encryption.encrypt(timeStamp).equals(MD5)) {
                    /**********开始执行逻辑**********/
                    orderPreviewService.deleteAllOrder();// 清除所有订单预览

                    List<SysOldman> oldmanList = new ArrayList<>();// 接收到的所有老人List
                    List<SysCourier> courierList = new ArrayList<>();// 骑手List
                    SysOldman oStatus = new SysOldman();
                    oStatus.setStatus("0");
                    oldmanList = oldManService.selectOldManList(oStatus);// 获取所有状态正常的老人信息
                    SysCourier cStatus = new SysCourier();
                    cStatus.setStatus("0");
                    courierList = courierService.selectCourierList(cStatus);// 获取所有状态正常的骑手信息

                    SysOrder orderParam = new SysOrder();
                    orderParam.setStatus(0);
                    Map<String, Object> params = new HashMap<>();
                    params.put("beginTime", getToday() + " 00:00:00");
                    params.put("endTime", getToday() + " 23:59:59");
                    orderParam.setParams(params);

                    List<SysOrder> todayOrderList1 = new ArrayList<>();
                    orderParam.setOrderType("0");// 餐点类型
                    todayOrderList1 = orderPreviewService.selectOrderList(orderParam);// 获取明天的所有早餐订单

                    List<SysOrder> todayOrderList2 = new ArrayList<>();
                    orderParam.setOrderType("1");// 餐点类型
                    todayOrderList2 = orderPreviewService.selectOrderList(orderParam);// 获取明天的所有午餐订单

                    List<SysOrder> todayOrderList3 = new ArrayList<>();
                    orderParam.setOrderType("2");// 餐点类型
                    todayOrderList3 = orderPreviewService.selectOrderList(orderParam);// 获取明天的所有晚餐订单


                    OrderGenerateHelper helper = new OrderGenerateHelper();
                    StringBuffer result = new StringBuffer();

                    OrderResultDTO resultDTO = new OrderResultDTO();

                    List<SysOrder> breakfastList = new ArrayList<>();// 早餐集合
                    List<SysOrder> lunchList = new ArrayList<>();// 午餐集合
                    List<SysOrder> dinnerList = new ArrayList<>();// 晚餐集合

                    // 生成早餐
                    resultDTO = helper.generateTorromowOrders(todayOrderList1, oldmanList, courierList, "0");
                    breakfastList = resultDTO.getOrderList();
                    if (breakfastList.size() > 0) {
                        int SendOrdersCount = orderPreviewService.saveOrders(breakfastList);
                        result.append("【早餐】：已成功自动生成 " + SendOrdersCount + " 份明日早餐订单，已去除了重复的 " + resultDTO.getRepeatOrdersCount() + " 份早餐订单；");
                    } else {
                        result.append("【早餐】：未能生成早餐订单，因为没有足够的满足早餐订单生成条件的老人；");
                    }

                    // 生成午餐
                    resultDTO = null;
                    resultDTO = helper.generateTorromowOrders(todayOrderList2, oldmanList, courierList, "1");
                    lunchList = resultDTO.getOrderList();
                    if (lunchList.size() > 0) {
                        int SendOrdersCount = orderPreviewService.saveOrders(lunchList);
                        result.append("【午餐】：已成功自动生成 " + SendOrdersCount + " 份明日午餐订单，已去除了重复的 " + resultDTO.getRepeatOrdersCount() + " 份午餐订单；");
                    } else {
                        result.append("【午餐】：未能生成午餐订单，因为没有足够的满足午餐订单生成条件的老人；");
                    }

                    // 生成晚餐
                    resultDTO = null;
                    resultDTO = helper.generateTorromowOrders(todayOrderList3, oldmanList, courierList, "2");
                    dinnerList = resultDTO.getOrderList();
                    if (dinnerList.size() > 0) {
                        int SendOrdersCount = orderPreviewService.saveOrders(dinnerList);
                        result.append("【晚餐】：已成功自动生成 " + SendOrdersCount + " 份明日晚餐订单，已去除了重复的 " + resultDTO.getRepeatOrdersCount() + " 份晚餐订单；");
                    } else {
                        result.append("【晚餐】：未能生成晚餐订单，因为没有足够的满足晚餐订单生成条件的老人；");
                    }

                    System.out.println(result.toString());

                    /**********执行逻辑结束**********/
                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                }
            } else {// 距离请求参数生成时间超过10秒时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
        }

    }

    /**
     * 生成今日送餐订单(接口)
     *
     * @return
     */
    @Log(title = "生成今日送餐订单", businessType = BusinessType.INSERT)
    @PostMapping("/addTodayOrdersApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public String addTodayOrdersApi(String timeStamp, String MD5) throws Exception {
        System.err.println("进入生成今日送餐订单Api，执行时间：" + df.format(new Date()));
        /**********************接口请求合法性验证********************/

        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(MD5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(10000);// 超时时间为10s
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过10秒
                if (Encryption.encrypt(timeStamp).equals(MD5)) {

                    List<SysOldman> oldmanList = new ArrayList<>();// 接收到的所有老人List
                    List<SysCourier> courierList = new ArrayList<>();// 骑手List
                    SysOldman oStatus = new SysOldman();
                    oStatus.setStatus("0");
                    oldmanList = oldManService.selectOldManList(oStatus);// 获取所有状态正常的老人信息
                    SysCourier cStatus = new SysCourier();
                    cStatus.setStatus("0");
                    courierList = courierService.selectCourierList(cStatus);// 获取所有状态正常的骑手信息

                    SysOrder orderParam = new SysOrder();
                    orderParam.setStatus(0);
                    Map<String, Object> params = new HashMap<>();
                    params.put("beginTime", getToday() + " 00:00:00");
                    params.put("endTime", getToday() + " 23:59:59");
                    orderParam.setParams(params);

                    List<SysOrder> todayOrderList1 = new ArrayList<>();
                    orderParam.setOrderType("0");// 餐点类型
                    todayOrderList1 = orderService.selectTodayOrderList(orderParam);// 获取今天的所有早餐订单

                    List<SysOrder> todayOrderList2 = new ArrayList<>();
                    orderParam.setOrderType("1");// 餐点类型
                    todayOrderList2 = orderService.selectTodayOrderList(orderParam);// 获取今天的所有午餐订单

                    List<SysOrder> todayOrderList3 = new ArrayList<>();
                    orderParam.setOrderType("2");// 餐点类型
                    todayOrderList3 = orderService.selectTodayOrderList(orderParam);// 获取今天的所有晚餐订单


                    OrderGenerateHelper helper = new OrderGenerateHelper();
                    StringBuffer result = new StringBuffer();

                    OrderResultDTO resultDTO = new OrderResultDTO();

                    List<SysOrder> breakfastList = new ArrayList<>();// 早餐集合
                    List<SysOrder> lunchList = new ArrayList<>();// 午餐集合
                    List<SysOrder> dinnerList = new ArrayList<>();// 晚餐集合

                    // 生成早餐
                    resultDTO = helper.generateTodayOrders(todayOrderList1, oldmanList, courierList, "0");
                    breakfastList = resultDTO.getOrderList();
                    if (breakfastList.size() > 0) {
                        int SendOrdersCount = orderService.saveOrders(breakfastList);
                        result.append("【早餐】：已成功自动生成 " + SendOrdersCount + " 份今日早餐订单，已去除了重复的 " + resultDTO.getRepeatOrdersCount() + " 份早餐订单；");
                    } else {
                        result.append("【早餐】：未能生成早餐订单，因为没有足够的满足早餐订单生成条件的老人；");
                    }

                    // 生成午餐
                    resultDTO = null;
                    resultDTO = helper.generateTodayOrders(todayOrderList2, oldmanList, courierList, "1");
                    lunchList = resultDTO.getOrderList();
                    if (lunchList.size() > 0) {
                        int SendOrdersCount = orderService.saveOrders(lunchList);
                        result.append("【午餐】：已成功自动生成 " + SendOrdersCount + " 份今日午餐订单，已去除了重复的 " + resultDTO.getRepeatOrdersCount() + " 份午餐订单；");
                    } else {
                        result.append("【午餐】：未能生成午餐订单，因为没有足够的满足午餐订单生成条件的老人；");
                    }

                    // 生成晚餐
                    resultDTO = null;
                    resultDTO = helper.generateTodayOrders(todayOrderList3, oldmanList, courierList, "2");
                    dinnerList = resultDTO.getOrderList();
                    if (dinnerList.size() > 0) {
                        int SendOrdersCount = orderService.saveOrders(dinnerList);
                        result.append("【晚餐】：已成功自动生成 " + SendOrdersCount + " 份今日晚餐订单，已去除了重复的 " + resultDTO.getRepeatOrdersCount() + " 份晚餐订单；");
                    } else {
                        result.append("【晚餐】：未能生成晚餐订单，因为没有足够的满足晚餐订单生成条件的老人；");
                    }

                    return result.toString();

                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    return "非法请求，已过滤";
                }
            } else {// 距离请求参数生成时间超过10秒时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                return "非法请求，已过滤";
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            return "非法请求，已过滤";
        }
    }

    /**
     * 生成今日待回收订单(接口)
     *
     * @return
     */
    @Log(title = "生成今日待回收订单", businessType = BusinessType.INSERT)
    @PostMapping("/updateRecoverOrdersApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public String updateRecoverOrdersApi(String timeStamp, String MD5) throws Exception {
        System.err.println("进入生成今日待回收订单Api，执行时间：" + df.format(new Date()));
        /**********************接口请求合法性验证********************/

        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(MD5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(10000);// 超时时间为10s
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过10秒
                if (Encryption.encrypt(timeStamp).equals(MD5)) {

                    List<SysOldman> oldmanList = new ArrayList<>();// 接收到的所有老人List
                    List<SysCourier> courierList = new ArrayList<>();// 骑手List
                    SysOldman oStatus = new SysOldman();
                    oStatus.setStatus("0");
                    oldmanList = oldManService.selectOldManList(oStatus);// 获取所有状态正常的老人信息
                    SysCourier cStatus = new SysCourier();
                    cStatus.setStatus("0");
                    courierList = courierService.selectCourierList(cStatus);// 获取所有状态正常的骑手信息

                    SysOrder orderParam = new SysOrder();
                    orderParam.setStatus(0);
                    Map<String, Object> params = new HashMap<>();
                    params.put("beginTime", getYesterday() + " 00:00:00");
                    params.put("endTime", getYesterday() + " 23:59:59");
                    orderParam.setParams(params);

                    List<SysOrder> yesterdayOrders = new ArrayList<>();
                    yesterdayOrders = orderService.selectTodayOrderList(orderParam);// 获取昨天的所有订单

                    StringBuffer idSb = new StringBuffer();

                    for (int i = 0; i < yesterdayOrders.size(); i++) {
                        if (yesterdayOrders.get(i).getSendType().equals("2")) {
                            idSb.append(yesterdayOrders.get(i).getId() + ",");
                        }
                    }

                    if (idSb.length() == 0) {
                        System.err.println(getYesterday() + " 00:00:00" + " 至 " + getYesterday() + " 23:59:59" + " 时间段内没有已送达的订单");
                        return getYesterday() + " 00:00:00" + " 至 " + getYesterday() + " 23:59:59" + " 时间段内没有已送达的订单";
                    } else {
                        String ids = idSb.substring(0, idSb.length() - 1);

                        SysOrder order = new SysOrder();
                        order.setIds(ids);
                        order.setRecoverType("0");

                        int count = orderService.updateOrderList(order);

                        System.err.println("生成了" + count + "个待回收订单，ID为" + ids);
                        return "生成了" + count + "个待回收订单";
                    }

                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    return "非法请求，已过滤";
                }
            } else {// 距离请求参数生成时间超过10秒时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                return "非法请求，已过滤";
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            return "非法请求，已过滤";
        }
    }

    /*****************************************************************************************************************/

    /**
     * 获取当前年份
     *
     * @param
     * @return
     */
    public int getYear() {
        Calendar c = Calendar.getInstance(Locale.CHINA);//
        return c.get(Calendar.YEAR); // 获取当前年份
    }

    /**
     * 获取当前月份
     *
     * @param
     * @return
     */
    public int getMonth() {
        Calendar c = Calendar.getInstance(Locale.CHINA);//
        return c.get(Calendar.MONTH) + 1; //
    }

    /**
     * 获取当前月份的天数
     *
     * @param
     * @return
     */
    public int getDay() {
        Calendar a = Calendar.getInstance();
        a.set(Calendar.DATE, 1);
        a.roll(Calendar.DATE, -1);
        return a.get(Calendar.DATE);
    }

    /**
     * 返回明天
     *
     * @return
     */
    public String getTomorrow() {
        Date date = new Date();//取时间
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(calendar.DATE, 1);//把日期往后增加一天.整数往后推,负数往前移动
        date = calendar.getTime(); //这个时间就是日期往后推一天的结果
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(date);
        return dateString;
    }

    /**
     * 返回今天
     *
     * @return
     */
    public String getToday() {
        Date date = new Date();//取时间
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(calendar.DATE, 0);
        date = calendar.getTime(); //这个时间就是日期往后推一天的结果
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(date);
        return dateString;
    }

    /**
     * 返回昨天
     *
     * @return
     */
    public String getYesterday() {
        Date date = new Date();//取时间
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(calendar.DATE, -1);
        date = calendar.getTime(); //这个时间就是日期往后推一天的结果
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(date);
        return dateString;
    }

    /**
     * 判断是否存在日期范围
     *
     * @param o
     * @param date
     * @param flag
     * @return
     * @throws ParseException
     */
    private boolean isFlag(SysOldman o, String date, boolean flag) throws ParseException {
        if (StringUtils.isNotBlank(o.getDateCheck())) {
            String[] dateSplit = o.getDateCheck().split(",");
            for (String s : dateSplit) {
                s.trim();
//                        String bool = sdfYMD.format(sdfMDY.parse(s));
                if (sdfYMD.format(sdfMDY.parse(s)).equals(date)) {
                    flag = true;
                }
            }
        }
        return flag;
    }

    /**
     * 生成每日订单的公共方法
     *
     * @param sendOrdersCount
     * @param oldmanList
     * @return
     * @throws ParseException
     */
    public int getSendOrdersCount(int sendOrdersCount, List<SysOldman> oldmanList) throws ParseException {
        for (SysOldman o : oldmanList) {
            if (StringUtils.isNotBlank(o.getDateCheck()) && o.getCourierId() != null && o.getCourierId() != 0 && StringUtils.isNotBlank(o.getAddress())) {// 判断老人是否设置了送餐日期、骑手、送餐地址

                String today = getToday();
                Date date = sdfYMD.parse(today);

                /******** 此处比对订单分配里面的送餐日期，若含有今天的日期，则生成送餐信息 *******/
                boolean flag = false;
                flag = isFlag(o, today, flag);

                if (flag) {// 含有今天日期并且有骑手、送餐地址信息的老人
                    System.err.println("今日需要送餐的老人：" + o.getUserName());
                    sendOrdersCount++;
                }

            } else {// 没有设置用餐日期、骑手、送餐地址信息的老人的老人
                // System.err.println("当前老人没设置送餐日期：" + o.getUserName());
            }
        }
        return sendOrdersCount;
    }

}