package com.ruoyi.web.controller.tool;

import java.util.Arrays;
import java.util.List;

public class HuaWeiTokenReq {

    /**
     * auth : {"identity":{"methods":["password"],"password":{"user":{"name":"eihoojk","password":"eihoojk123!@#","domain":{"name":"eihoojk"}}}},"scope":{"project":{"id":"b4a88b0c68e04273a66c5dfab10edf67"}}}
     */

    public AuthBean auth=new AuthBean();

    public static class AuthBean {
        /**
         * identity : {"methods":["password"],"password":{"user":{"name":"eihoojk","password":"eihoojk123!@#","domain":{"name":"eihoojk"}}}}
         * scope : {"project":{"id":"b4a88b0c68e04273a66c5dfab10edf67"}}
         */

        public IdentityBean identity=new IdentityBean();
        public ScopeBean scope=new ScopeBean();

        public static class IdentityBean {
            /**
             * methods : ["password"]
             * password : {"user":{"name":"eihoojk","password":"eihoojk123!@#","domain":{"name":"eihoojk"}}}
             */

            public PasswordBean password=new PasswordBean();
            public List<String> methods= Arrays.asList(new String("password"));

            public static class PasswordBean {
                /**
                 * user : {"name":"eihoojk","password":"eihoojk123!@#","domain":{"name":"eihoojk"}}
                 */

                public UserBean user=new UserBean();

                public static class UserBean {
                    /**
                     * name : eihoojk
                     * password : eihoojk123!@#
                     * domain : {"name":"eihoojk"}
                     */

                    public String name="eihoojk";
                    public String password="eihoojk123!@#";
                    public DomainBean domain=new DomainBean();

                    public static class DomainBean {
                        /**
                         * name : eihoojk
                         */

                        public String name="eihoojk";
                    }
                }
            }
        }

        public static class ScopeBean {
            /**
             * project : {"id":"b4a88b0c68e04273a66c5dfab10edf67"}
             */

            public ProjectBean project=new ProjectBean();

            public static class ProjectBean {
                /**
                 * id : b4a88b0c68e04273a66c5dfab10edf67
                 */

                public String id="b4a88b0c68e04273a66c5dfab10edf67";
            }
        }
    }
}
