package com.ruoyi.web.controller.tool;

import com.ruoyi.system.domain.SysOrder;

import java.util.List;

/**
 * @ClassName OrderResultDTO
 * @Description TODO
 * @Author lzjian
 * @Date 2019/1/8 0008 11:27
 **/
public class OrderResultDTO {

    private List<SysOrder> orderList;

    private int repeatOrdersCount;

    @Override
    public String toString() {
        return "OrderResultDTO{" +
                "orderList=" + orderList +
                ", repeatOrdersCount=" + repeatOrdersCount +
                '}';
    }

    public List<SysOrder> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<SysOrder> orderList) {
        this.orderList = orderList;
    }

    public int getRepeatOrdersCount() {
        return repeatOrdersCount;
    }

    public void setRepeatOrdersCount(int repeatOrdersCount) {
        this.repeatOrdersCount = repeatOrdersCount;
    }
}
