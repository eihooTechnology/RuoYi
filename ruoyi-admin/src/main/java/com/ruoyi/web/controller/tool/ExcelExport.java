package com.ruoyi.web.controller.tool;

import com.ruoyi.system.domain.SysOrder;
import com.ruoyi.web.entity.SysOrderExport;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @ClassName ExcelExport
 * @Description TODO
 * @Author lzjian
 * @Date 2019-04-25 14:36
 **/
public class ExcelExport {

    /**
     * 生成送餐订单导出格式列表
     *
     * @param orderList
     * @return
     */
    public List<SysOrderExport> toGetSendOrderList(List<SysOrder> orderList, String type) {

        List<SysOrderExport> orderExportList = new ArrayList<>();

        if (type.equals("0")) {
            /********************* 经过根据老人ID去重且累加的送餐订单集合 开始 *********************/

            Map<Long, SysOrder> resultMap = new HashMap<>();
            for (int i = 0; i < orderList.size(); i++) {
                SysOrder bean = orderList.get(i);
                Long key = bean.getUserCode();
                SysOrder bean2 = resultMap.get(key);
                //如果有存过
                if (bean2 != null) {
                    // 计算数量
                    bean2.setAvgCount(bean2.getAvgCount() + 1);
                    resultMap.put(key, bean2);
                } else {
                    bean.setAvgCount(1);
                    resultMap.put(key, bean);
                }
            }
            List<SysOrder> sysOrders = new ArrayList<>(resultMap.values());// 经过根据老人ID去重且累加的送餐订单集合
            /********************* 经过根据老人ID去重且累加的送餐订单集合 结束 *********************/

            /** 转化为送餐订单导出格式实体类 **/
            SysOrderExport orderExport;
            for (SysOrder bean : sysOrders) {
                orderExport = new SysOrderExport();
                orderExport.setUserCode(bean.getUserCode());
                orderExport.setUserName(bean.getUserName());
                orderExport.setMobile(bean.getMobile());
                orderExport.setLunchSum(bean.getAvgCount());
                orderExportList.add(orderExport);
            }
        } else if (type.equals("1")) {
            /** 转化为送餐订单导出格式实体类 **/
            SysOrderExport orderExport;
            for (SysOrder bean : orderList) {
                orderExport = new SysOrderExport();
                orderExport.setUserCode(bean.getUserCode());
                orderExport.setUserName(bean.getUserName());
                orderExport.setMobile(bean.getMobile());
                orderExport.setAddress(bean.getAddress());
                orderExport.setCommunityName(bean.getCommunityName());
                orderExportList.add(orderExport);
            }
        }


        return orderExportList;
    }

    /*******************************************************************************************/

    /**
     * 生成送餐订单详情Excel文件
     *
     * @param list
     * @return
     * @throws Exception
     */
    public static String toSendOrderExcelExport(List<SysOrderExport> list, String startTime, String endTime, String type) throws Exception {
        String result = "";
        /************************************************************/
        // 判断导出任务类型
        WritableWorkbook wwb = null;
        // 创建可写入的Excel工作簿
        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");// HH时mm分ss秒
        String nowDate = dateFormat.format(now);
        String title = "统计送餐订单 " + startTime + " 至 " + endTime + " (生成于" + nowDate + ").xls";
        String fileName = "/usr/local/excelExport/" + title;
        File file = new File(fileName);
        if (!file.exists()) {
            file.createNewFile();
        }
        // 以fileName为文件名来创建一个Workbook
        wwb = Workbook.createWorkbook(file);

        int width = 15;// 列宽，超出自动换行
        int cellLength = 5; // 列的数量（有多少列）
        WritableCellFormat cellFormat = new WritableCellFormat();
        //设置自动换行;
        cellFormat.setWrap(true);
        //设置文字居中对齐方式;
        cellFormat.setAlignment(Alignment.CENTRE);
        //设置垂直居中;
        cellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);

        // 创建工作表
        WritableSheet ws = wwb.createSheet("送餐订单统计", 0);

        setCellWidth(ws, cellLength, width);

        /***** 插入订单详情 *****/
        if (type.equals("0")) {
            Label lblUserCode = new Label(0, 0, "用户编号", cellFormat);
            Label lblUserName = new Label(1, 0, "用户姓名", cellFormat);
            Label lblMobile = new Label(2, 0, "用户联系电话", cellFormat);
            Label lblLunchSum = new Label(3, 0, "订单总数", cellFormat);
            // 插入列
            ws.addCell(lblUserCode);
            ws.addCell(lblUserName);
            ws.addCell(lblMobile);
            ws.addCell(lblLunchSum);
        } else if (type.equals("1")) {
            Label lblUserCode = new Label(0, 0, "用户编号", cellFormat);
            Label lblUserName = new Label(1, 0, "用户姓名", cellFormat);
            Label lblMobile = new Label(2, 0, "用户联系电话", cellFormat);
            Label lblCommunityName = new Label(3, 0, "用户社区", cellFormat);
            Label lblAddress = new Label(4, 0, "用户地址", cellFormat);
            // 插入列
            ws.addCell(lblUserCode);
            ws.addCell(lblUserName);
            ws.addCell(lblMobile);
            ws.addCell(lblCommunityName);
            ws.addCell(lblAddress);
        }

        if (type.equals("0")) {
            for (int i = 0; i < list.size(); i++) {

                SysOrderExport bean = list.get(i);

                // 要插入到的Excel表格的值
                Label lblUserCode_value = new Label(0, i + 1, String.valueOf(bean.getUserCode()));
                Label lblUserName_value = new Label(1, i + 1, bean.getUserName());
                Label lblMobile_value = new Label(2, i + 1, bean.getMobile());
                Label lblLunchSum_value = new Label(3, i + 1, String.valueOf(bean.getLunchSum()));
                // 插入列
                ws.addCell(lblUserCode_value);
                ws.addCell(lblUserName_value);
                ws.addCell(lblMobile_value);
                ws.addCell(lblLunchSum_value);

            }
        } else if (type.equals("1")) {
            for (int i = 0; i < list.size(); i++) {

                SysOrderExport bean = list.get(i);

                // 要插入到的Excel表格的值
                Label lblUserCode_value = new Label(0, i + 1, String.valueOf(bean.getUserCode()));
                Label lblUserName_value = new Label(1, i + 1, bean.getUserName());
                Label lblMobile_value = new Label(2, i + 1, bean.getMobile());
                Label lblCommunityName_value = new Label(3, i + 1, bean.getCommunityName());
                Label lblAddress_value = new Label(4, i + 1, bean.getAddress());
                // 插入列
                ws.addCell(lblUserCode_value);
                ws.addCell(lblUserName_value);
                ws.addCell(lblMobile_value);
                ws.addCell(lblCommunityName_value);
                ws.addCell(lblAddress_value);

            }
        }

        // 写进文档
        wwb.write();
        // 关闭Excel工作簿对象
        wwb.close();
        result = title;
        // JOptionPane.showMessageDialog(null, "导出成功!导出文件位置在:" + fileName, "提示",
        // JOptionPane.INFORMATION_MESSAGE);
        /************************************************************/
        return result;
    }

    /**
     * 设置列宽
     */
    public static void setCellWidth(WritableSheet ws, int cellLength, int cellWidth) {
        for (int i = 0; i < cellLength; i++) {
            ws.setColumnView(i, cellWidth);
        }
    }

}
