package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.base.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ExcelUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.shiro.service.SysPasswordService;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.framework.web.base.BaseController;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.quartz.util.Encryption;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.service.*;
import com.ruoyi.web.controller.tool.CommunityGetKeyIDs;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 老人管理调用层
 *
 * @author ruoyi
 */
@Controller
@RequestMapping("/system/oldMan")
public class SysOldManController extends BaseController {
    private String prefix = "system/oldMan";

    @Autowired
    private ISysOldManService oldManService;

    @Autowired
    private ISysRoleService roleService;

    @Autowired
    private ISysPostService postService;

    @Autowired
    private ISysOrderBillService orderBillService;

    @Autowired
    private ISysCommunityService communityService;

    private SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");

    /**
     * 点击老人管理页面
     *
     * @return
     */
    @RequiresPermissions("system:oldMan:view")
    @GetMapping()
    public String user() {
        return prefix + "/oldMan";
    }

    /**
     * 分页查询老人列表
     *
     * @param sysOldman
     * @return
     */
    @RequiresPermissions("system:oldMan:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysOldman sysOldman) {
        // 数据查看范围权限(要放在分页startPage();前面，否则只能查询出10个)
        SysCommunity s = new SysCommunity();
        s.setStatus("0");
        List<SysCommunity> communityList = communityService.selectCommunityList(s);
        CommunityGetKeyIDs getKeyIDs = new CommunityGetKeyIDs();
        sysOldman.setKeyIDs(getKeyIDs.communityGetKeyIDs(communityList));

        startPage();
        if (StringUtils.isBlank(sysOldman.getStatus())) {
            sysOldman.setStatus("0");
        }
        List<SysOldman> list = oldManService.selectOldManList(sysOldman);
        return getDataTable(list);
    }

    /**
     * 点击新增跳转至填写老人信息
     */
    @GetMapping("/add")
    public String add(ModelMap mmap) {
        mmap.put("roles", roleService.selectRoleAll());
        mmap.put("posts", postService.selectPostAll());
        return prefix + "/add";
    }

    /**
     * 校验老人是否重复
     *
     * @param sysOldman
     * @return
     */
    @PostMapping("/checkOldManUnique")
    @ResponseBody
    public String checkOldManUnique(SysOldman sysOldman) {
        return oldManService.checkOldManUnique(sysOldman.getIdentityId());
    }

    /**
     * 填写老人信息后，新增老人
     *
     * @param sysOldman
     * @return
     */
    @RequiresPermissions("system:oldMan:add")
    @Log(title = "老人管理", businessType = BusinessType.INSERT)
    @PostMapping("/saveOldMan")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public AjaxResult addSave(SysOldman sysOldman) {
        sysOldman.setCreateBy(ShiroUtils.getLoginName());
        return toAjax(oldManService.saveOldMan(sysOldman));
    }


    /**
     * 修改用户 跳转修改页面
     *
     * @param userCode
     * @param mmap
     * @return
     */
    @GetMapping("/edit/{userCode}")
    public String edit(@PathVariable("userCode") Long userCode, ModelMap mmap) {
        System.out.println("userCode = " + userCode);
        mmap.put("oldMan", oldManService.selectOldManById(userCode));
//        mmap.put("roles", roleService.selectRolesByUserId(userCode));
//        mmap.put("posts", postService.selectPostsByUserId(userCode));
        return prefix + "/edit";
    }

    /**
     * 修改老人信息
     *
     * @param sysOldman
     * @return
     */
    @RequiresPermissions("system:oldMan:edit")
    @Log(title = "老人管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public AjaxResult editSave(SysOldman sysOldman) throws Exception {

        Long userCode = sysOldman.getUserCode();
        // 查询本月老人是否已经生成过账单
        SysOrderBill orderBill = new SysOrderBill();
        orderBill.setUserCode(userCode);
        orderBill.setStatus("0");
        Map<String, Object> params = new HashMap<>();
        /************************************处理当月账单开始************************************/
        params.put("beginTime", getYear() + "-" + getMonth() + "-01" + " 00:00:00");// 月头
        params.put("endTime", getYear() + "-" + getMonth() + "-" + getDay() + " 23:59:59");// 月尾
        orderBill.setParams(params);
        List<SysOrderBill> orderBillList = orderBillService.selectOrderBillList(orderBill);
        if (orderBillList.size() > 0) {// 如果已经生成过账单
            orderBill = orderBillList.get(0);

            if (StringUtils.isNotBlank(orderBill.getRealDateCheck())) {
                orderBill.setHistoryDateCheck(sysOldman.getDateCheck());
            } else {
                orderBill.setRealDateCheck(sysOldman.getDateCheck());
                orderBill.setHistoryDateCheck(sysOldman.getDateCheck());
            }
            if (StringUtils.isNotBlank(orderBill.getRealDateCheck2())) {
                orderBill.setHistoryDateCheck2(sysOldman.getDateCheck2());
            } else {
                orderBill.setRealDateCheck2(sysOldman.getDateCheck2());
                orderBill.setHistoryDateCheck2(sysOldman.getDateCheck2());
            }
            if (StringUtils.isNotBlank(orderBill.getRealDateCheck3())) {
                orderBill.setHistoryDateCheck3(sysOldman.getDateCheck3());
            } else {
                orderBill.setRealDateCheck3(sysOldman.getDateCheck3());
                orderBill.setHistoryDateCheck3(sysOldman.getDateCheck3());
            }

            orderBill.setUpdateBy(ShiroUtils.getLoginName());
            orderBillService.updateOrderBill(orderBill);

            sysOldman.setUpdateBy(ShiroUtils.getLoginName());
        } else {
            orderBill.setUserCode(userCode);
            orderBill.setRealDateCheck(sysOldman.getDateCheck());
            orderBill.setHistoryDateCheck(sysOldman.getDateCheck());
            orderBill.setRealDateCheck2(sysOldman.getDateCheck2());
            orderBill.setHistoryDateCheck2(sysOldman.getDateCheck2());
            orderBill.setRealDateCheck3(sysOldman.getDateCheck3());
            orderBill.setHistoryDateCheck3(sysOldman.getDateCheck3());
            orderBill.setMoney(BigDecimal.valueOf(0));
            orderBill.setIsSettle("0");
            orderBill.setCreateBy(ShiroUtils.getLoginName());
            orderBillService.insertOrderBill(orderBill);

            sysOldman.setUpdateBy(ShiroUtils.getLoginName());
        }

        oldManService.updateOldMan(sysOldman);
        /************************************处理当月账单结束************************************/

        /************************************处理下月账单开始************************************/
//        System.err.println("下月头：" + getPerFirstDayOfMonth() + "\t 下月底：" + getLastMaxMonthDate());
        SysOrderBill orderBill2 = new SysOrderBill();
        orderBill2.setUserCode(userCode);
        orderBill2.setStatus("0");
        Map<String, Object> params2 = new HashMap<>();
        params2.put("beginTime", getPerFirstDayOfMonth() + " 00:00:00");// 下月头
        params2.put("endTime", getLastMaxMonthDate() + " 23:59:59");// 下月尾
        orderBill2.setParams(params2);
        List<SysOrderBill> orderBillListNextMonth = orderBillService.selectOrderBillList(orderBill2);
        SysOldman oldman = oldManService.selectOldManById(sysOldman.getUserCode());
        if (orderBillListNextMonth.size() > 0) {// 如果已经生成过账单
            orderBill2 = orderBillListNextMonth.get(0);

            if (StringUtils.isNotBlank(orderBill2.getRealDateCheck())) {
                orderBill2.setHistoryDateCheck(oldman.getDateCheckNextMonth());
            } else {
                orderBill2.setRealDateCheck(oldman.getDateCheckNextMonth());
                orderBill2.setHistoryDateCheck(oldman.getDateCheckNextMonth());
            }
            if (StringUtils.isNotBlank(orderBill2.getRealDateCheck2())) {
                orderBill2.setHistoryDateCheck2(oldman.getDateCheck2NextMonth());
            } else {
                orderBill2.setRealDateCheck2(oldman.getDateCheck2NextMonth());
                orderBill2.setHistoryDateCheck2(oldman.getDateCheck2NextMonth());
            }
            if (StringUtils.isNotBlank(orderBill2.getRealDateCheck3())) {
                orderBill2.setHistoryDateCheck3(oldman.getDateCheck3NextMonth());
            } else {
                orderBill2.setRealDateCheck3(oldman.getDateCheck3NextMonth());
                orderBill2.setHistoryDateCheck3(oldman.getDateCheck3NextMonth());
            }

            orderBill2.setUpdateBy(ShiroUtils.getLoginName());
            orderBillService.updateOrderBill(orderBill2);
        } else {
            if (StringUtils.isNotBlank(oldman.getDateCheckNextMonth()) || StringUtils.isNotBlank(oldman.getDateCheck2NextMonth()) || StringUtils.isNotBlank(oldman.getDateCheck3NextMonth())) {
                orderBill2.setUserCode(userCode);
                orderBill2.setRealDateCheck(oldman.getDateCheckNextMonth());
                orderBill2.setHistoryDateCheck(oldman.getDateCheckNextMonth());
                orderBill2.setRealDateCheck2(oldman.getDateCheck2NextMonth());
                orderBill2.setHistoryDateCheck2(oldman.getDateCheck2NextMonth());
                orderBill2.setRealDateCheck3(oldman.getDateCheck3NextMonth());
                orderBill2.setHistoryDateCheck3(oldman.getDateCheck3NextMonth());
                orderBill2.setMoney(BigDecimal.valueOf(0));
                orderBill2.setIsSettle("0");
                orderBill2.setCreateBy(ShiroUtils.getLoginName());
                String time = getPerFirstDayOfMonth() + " 02:00:00";
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                orderBill2.setCreateTime(sdf.parse(time));
                orderBillService.insertOrderBillByCreateTime(orderBill2);
            }
        }
        /************************************处理下月账单结束************************************/

        return toAjax(oldManService.updateOldMan(sysOldman));
    }

    /**
     * 清空老人表中当月的旧送餐日期(接口)
     *
     * @return
     */
    @Log(title = "清空老人表中当月的旧送餐日期", businessType = BusinessType.INSERT)
    @PostMapping("/updateOrderAndOldManInfoApi")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public String updateOrderAndOldManInfoApi(String timeStamp, String MD5) throws Exception {
        System.err.println("清空老人表中当月的旧送餐日期Api，执行时间：" + df.format(new Date()));
        int saveBillCount = 0;
        /**********************接口请求合法性验证********************/
        if (StringUtils.isNotBlank(timeStamp) && StringUtils.isNotBlank(MD5)) {// 当全部参数不为空时才允许进入
            Date date = new Date();
            Long nowTime = date.getTime();
            Long apiTime = Long.valueOf(timeStamp);
            Long val = nowTime - apiTime;
            Long timeEnd = new Long(10000);// 超时时间为10s
            System.err.println(nowTime + " - " + timeStamp + " = " + val + " (毫秒)");

            if (val.longValue() < timeEnd.longValue()) {// 判断，距离请求参数生成时间是否超过10秒
                if (Encryption.encrypt(timeStamp).equals(MD5)) {
                    /**********开始执行逻辑**********/
                    oldManService.clearAllOldManDateCheck();// 首先清除所有老人的旧订单分配日期

                    SysOldman oldman = new SysOldman();
                    oldman.setStatus("0");
                    List<SysOldman> oldmans = oldManService.selectOldManList(oldman);// 获取数据库中所有老人信息
                    if (oldmans.size() > 0) {// 获取到所有老人信息后

                        List<SysOldman> oldmanList = new ArrayList<>();// 所有含有下月订单分配日期的老人集合
                        // 将不含有下月订单分配日期的老人剔除
                        boolean flag = false;
                        for (SysOldman s : oldmans) {
                            flag = false;
                            if (StringUtils.isNotBlank(s.getDateCheckNextMonth())) {
                                flag = true;
                            }
                            if (StringUtils.isNotBlank(s.getDateCheck2NextMonth())) {
                                flag = true;
                            }
                            if (StringUtils.isNotBlank(s.getDateCheck3NextMonth())) {
                                flag = true;
                            }
                            if (flag) {
                                oldmanList.add(s);
                            }
                        }

                        List<SysOrderBill> orderBillList = new ArrayList<>();
                        SysOrderBill orderBill;
                        for (SysOldman s : oldmanList) {
                            orderBill = new SysOrderBill();
                            orderBill.setUserCode(s.getUserCode());
                            orderBill.setRealDateCheck(s.getDateCheckNextMonth());
                            orderBill.setHistoryDateCheck(s.getDateCheckNextMonth());
                            orderBill.setRealDateCheck2(s.getDateCheck2NextMonth());
                            orderBill.setHistoryDateCheck2(s.getDateCheck2NextMonth());
                            orderBill.setRealDateCheck3(s.getDateCheck3NextMonth());
                            orderBill.setHistoryDateCheck3(s.getDateCheck3NextMonth());
                            orderBill.setMoney(BigDecimal.valueOf(0));
                            orderBill.setIsSettle("0");
                            orderBill.setCreateBy("系统定时器");
                            orderBillList.add(orderBill);
                        }

                        saveBillCount = orderBillService.saveOrderBills(orderBillList);

                        for (SysOldman s : oldmanList) {// 将所有老人的下月订餐信息移至本月订单信息
                            s.setDateCheck(s.getDateCheckNextMonth());
                            s.setFoodPackage(s.getFoodPackageNextMonth());
                            s.setCourierId(s.getCourierIdNextMonth());
                            s.setDateCheck2(s.getDateCheck2NextMonth());
                            s.setFoodPackage2(s.getFoodPackage2NextMonth());
                            s.setCourierId2(s.getCourierId2NextMonth());
                            s.setDateCheck3(s.getDateCheck3NextMonth());
                            s.setFoodPackage3(s.getFoodPackage3NextMonth());
                            s.setCourierId3(s.getCourierId3NextMonth());
                            s.setDateCheckNextMonth("");
                            s.setFoodPackage(Long.valueOf(0));
                            s.setCourierIdNextMonth(Long.valueOf(0));
                            s.setDateCheck2NextMonth("");
                            s.setFoodPackage2(Long.valueOf(0));
                            s.setCourierId2NextMonth(Long.valueOf(0));
                            s.setDateCheck3NextMonth("");
                            s.setFoodPackage3(Long.valueOf(0));
                            s.setCourierId3NextMonth(Long.valueOf(0));
                            oldManService.updateOldMan(s);
                        }

                        return "本月旧送餐信息已清除，并生成了 " + saveBillCount + " 份新订单信息";

                    } else {
                        System.err.println("【数据库中老人表内老人数量不足】");
                        return "数据库中老人表内老人数量不足";
                    }

                } else {
                    System.err.println("非法请求，已过滤【加密错误】");
                    return "非法请求，已过滤";
                }
            } else {// 距离请求参数生成时间超过10秒时，则认为该参数已失去时效性(存在人为重复使用同一参数的可能)，判定当前请求为非法
                System.err.println("非法请求，已过滤【超时错误】");
                return "非法请求，已过滤";
            }
        } else {
            System.err.println("非法请求，已过滤【参数错误】");
            return "非法请求，已过滤";
        }

    }

    /**
     * 删除老人
     *
     * @param userCode
     * @return
     */
    @RequiresPermissions("system:oldMan:remove")
    @Log(title = "老人管理", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @Transactional(rollbackFor = Exception.class)
    @ResponseBody
    public String remove(Long userCode) {
        int i = oldManService.deleteOldManByUserCode(userCode);
        if (i > 0) {
            return "删除该老人以及老人订单成功！";
        } else {
            return "系统错误，删除该老人以及老人订单失败！";
        }
    }

    /*****************************************************************************************************************/

    /**
     * 获取当前年份
     *
     * @param
     * @return
     */
    public int getYear() {
        Calendar c = Calendar.getInstance(Locale.CHINA);//
        return c.get(Calendar.YEAR); // 获取当前年份
    }

    /**
     * 获取当前月份
     *
     * @param
     * @return
     */
    public int getMonth() {
        Calendar c = Calendar.getInstance(Locale.CHINA);//
        return c.get(Calendar.MONTH) + 1; //
    }

    /**
     * 获取当前月份的天数
     *
     * @param
     * @return
     */
    public int getDay() {
        Calendar a = Calendar.getInstance();
        a.set(Calendar.DATE, 1);
        a.roll(Calendar.DATE, -1);
        return a.get(Calendar.DATE);
    }

    /**
     * 描述:获取下一个月的第一天.
     *
     * @return
     */
    public String getPerFirstDayOfMonth() {
        SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        return dft.format(calendar.getTime());
    }

    /**
     * 描述:获取下个月的最后一天.
     *
     * @return
     */
    public String getLastMaxMonthDate() {
        SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return dft.format(calendar.getTime());
    }

}