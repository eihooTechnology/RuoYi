package com.ruoyi.web.entity.huaweiface;

/**
 * @ClassName FacesBean
 * @Description TODO
 * @Author lzjian
 * @Date 2019-04-22 15:26
 **/
public class FacesBean {
    /**
     * bounding_box : {"width":622,"top_left_y":594,"top_left_x":199,"height":622}
     * similarity : 0.95630765
     * external_image_id : 17
     * face_id : NC9rgtNt
     */

    private BoundingBoxBean bounding_box;
    private double similarity;
    private String external_image_id;
    private String face_id;

    public BoundingBoxBean getBounding_box() {
        return bounding_box;
    }

    public void setBounding_box(BoundingBoxBean bounding_box) {
        this.bounding_box = bounding_box;
    }

    public double getSimilarity() {
        return similarity;
    }

    public void setSimilarity(double similarity) {
        this.similarity = similarity;
    }

    public String getExternal_image_id() {
        return external_image_id;
    }

    public void setExternal_image_id(String external_image_id) {
        this.external_image_id = external_image_id;
    }

    public String getFace_id() {
        return face_id;
    }

    public void setFace_id(String face_id) {
        this.face_id = face_id;
    }

    @Override
    public String toString() {
        return "FacesBean{" +
                "bounding_box=" + bounding_box +
                ", similarity=" + similarity +
                ", external_image_id='" + external_image_id + '\'' +
                ", face_id='" + face_id + '\'' +
                '}';
    }
}
