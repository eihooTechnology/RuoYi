package com.ruoyi.web.entity.huaweiface;

/**
 * @ClassName ExternalFieldsBean
 * @Description TODO
 * @Author lzjian
 * @Date 2019-04-22 13:50
 **/
public class ExternalFieldsBean {
    /**
     * courierName : {"type":"string"}
     * score : {"type":"float"}
     * weight : {"type":"double"}
     * location : {"type":"long"}
     * userId : {"type":"string"}
     * male : {"type":"boolean"}
     * timestamp : {"type":"integer"}
     */

    private CourierNameBean courierName;
    private ScoreBean score;
    private WeightBean weight;
    private LocationBean location;
    private UserIdBean userId;
    private MaleBean male;
    private TimestampBean timestamp;

    public CourierNameBean getCourierName() {
        return courierName;
    }

    public void setCourierName(CourierNameBean courierName) {
        this.courierName = courierName;
    }

    public ScoreBean getScore() {
        return score;
    }

    public void setScore(ScoreBean score) {
        this.score = score;
    }

    public WeightBean getWeight() {
        return weight;
    }

    public void setWeight(WeightBean weight) {
        this.weight = weight;
    }

    public LocationBean getLocation() {
        return location;
    }

    public void setLocation(LocationBean location) {
        this.location = location;
    }

    public UserIdBean getUserId() {
        return userId;
    }

    public void setUserId(UserIdBean userId) {
        this.userId = userId;
    }

    public MaleBean getMale() {
        return male;
    }

    public void setMale(MaleBean male) {
        this.male = male;
    }

    public TimestampBean getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(TimestampBean timestamp) {
        this.timestamp = timestamp;
    }

    public static class CourierNameBean {
        /**
         * type : string
         */

        private String type;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public static class ScoreBean {
        /**
         * type : float
         */

        private String type;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public static class WeightBean {
        /**
         * type : double
         */

        private String type;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public static class LocationBean {
        /**
         * type : long
         */

        private String type;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public static class UserIdBean {
        /**
         * type : string
         */

        private String type;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public static class MaleBean {
        /**
         * type : boolean
         */

        private String type;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public static class TimestampBean {
        /**
         * type : integer
         */

        private String type;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    @Override
    public String toString() {
        return "ExternalFieldsBean{" +
                "courierName=" + courierName +
                ", score=" + score +
                ", weight=" + weight +
                ", location=" + location +
                ", userId=" + userId +
                ", male=" + male +
                ", timestamp=" + timestamp +
                '}';
    }
}
