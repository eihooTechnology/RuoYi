package com.ruoyi.web.entity.huaweiface;

import java.util.List;

/**
 * @ClassName FaceSearchSendBean
 * @Description TODO
 * @Author lzjian
 * @Date 2019-04-22 14:50
 **/
public class FaceSearchSendBean {


    /**
     * image_base64 : /9j/4AAQSkZJRgABAgEASABIAAD
     * sort : [{"timestamp":"asc"}]
     * return_fields : ["timestamp","id"]
     * filter : timestamp:12
     */

    private String external_image_id;

    private String image_base64;

    public String getImage_base64() {
        return image_base64;
    }

    public void setImage_base64(String image_base64) {
        this.image_base64 = image_base64;
    }

    public String getExternal_image_id() {
        return external_image_id;
    }

    public void setExternal_image_id(String external_image_id) {
        this.external_image_id = external_image_id;
    }
}
