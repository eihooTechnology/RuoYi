package com.ruoyi.web.entity.huaweiface;

/**
 * @ClassName DeleteFacesSuccessBean
 * @Description TODO
 * @Author lzjian
 * @Date 2019-04-22 16:15
 **/
public class DeleteFacesSuccessBean {

    /**
     * face_number : 1
     * face_set_id : 2rLYZQ4A
     * face_set_name : rider_debug
     */

    private int face_number;
    private String face_set_id;
    private String face_set_name;

    public int getFace_number() {
        return face_number;
    }

    public void setFace_number(int face_number) {
        this.face_number = face_number;
    }

    public String getFace_set_id() {
        return face_set_id;
    }

    public void setFace_set_id(String face_set_id) {
        this.face_set_id = face_set_id;
    }

    public String getFace_set_name() {
        return face_set_name;
    }

    public void setFace_set_name(String face_set_name) {
        this.face_set_name = face_set_name;
    }

    @Override
    public String toString() {
        return "DeleteFacesSuccessBean{" +
                "face_number=" + face_number +
                ", face_set_id='" + face_set_id + '\'' +
                ", face_set_name='" + face_set_name + '\'' +
                '}';
    }
}
