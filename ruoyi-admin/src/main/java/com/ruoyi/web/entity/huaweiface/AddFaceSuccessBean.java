package com.ruoyi.web.entity.huaweiface;

import java.util.List;

/**
 * @ClassName AddFaceSuccessBean
 * @Description TODO
 * @Author lzjian
 * @Date 2019-04-22 16:00
 **/
public class AddFaceSuccessBean {

    /**
     * face_set_id : 2rLYZQ4A
     * face_set_name : rider_debug
     * faces : [{"bounding_box":{"width":351,"top_left_y":264,"top_left_x":405,"height":351},"external_image_id":"666","external_fields":{},"face_id":"IsXQtLka"}]
     */

    private String face_set_id;
    private String face_set_name;
    private List<FacesBean> faces;

    @Override
    public String toString() {
        return "AddFaceSuccessBean{" +
                "face_set_id='" + face_set_id + '\'' +
                ", face_set_name='" + face_set_name + '\'' +
                ", faces=" + faces +
                '}';
    }

    public String getFace_set_id() {
        return face_set_id;
    }

    public void setFace_set_id(String face_set_id) {
        this.face_set_id = face_set_id;
    }

    public String getFace_set_name() {
        return face_set_name;
    }

    public void setFace_set_name(String face_set_name) {
        this.face_set_name = face_set_name;
    }

    public List<FacesBean> getFaces() {
        return faces;
    }

    public void setFaces(List<FacesBean> faces) {
        this.faces = faces;
    }
}
