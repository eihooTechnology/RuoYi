package com.ruoyi.web.entity.huaweiface;

/**
 * @ClassName FaceSetsInfoBean
 * @Description TODO
 * @Author lzjian
 * @Date 2019-04-22 13:50
 **/
public class FaceSetsInfoBean {
    /**
     * face_number : 7
     * external_fields : {"courierName":{"type":"string"},"score":{"type":"float"},"weight":{"type":"double"},"location":{"type":"long"},"userId":{"type":"string"},"male":{"type":"boolean"},"timestamp":{"type":"integer"}}
     * face_set_id : BMlyoHD5
     * face_set_name : rider_release
     * create_date : 2019-04-10 03:56:34
     * face_set_capacity : 10000
     */

    private int face_number;// 人脸库当中的人脸数量。
    private ExternalFieldsBean external_fields;// 用户的自定义字段。
    private String face_set_id;// 人脸库ID，随机生成的包含八个字符的字符串。
    private String face_set_name;// 人脸库名称。
    private String create_date;// 创建时间。
    private int face_set_capacity;// 人脸库最大的容量。

    public int getFace_number() {
        return face_number;
    }

    public void setFace_number(int face_number) {
        this.face_number = face_number;
    }

    public ExternalFieldsBean getExternal_fields() {
        return external_fields;
    }

    public void setExternal_fields(ExternalFieldsBean external_fields) {
        this.external_fields = external_fields;
    }

    public String getFace_set_id() {
        return face_set_id;
    }

    public void setFace_set_id(String face_set_id) {
        this.face_set_id = face_set_id;
    }

    public String getFace_set_name() {
        return face_set_name;
    }

    public void setFace_set_name(String face_set_name) {
        this.face_set_name = face_set_name;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public int getFace_set_capacity() {
        return face_set_capacity;
    }

    public void setFace_set_capacity(int face_set_capacity) {
        this.face_set_capacity = face_set_capacity;
    }

    @Override
    public String toString() {
        return "FaceSetsInfoBean{" +
                "face_number=" + face_number +
                ", external_fields=" + external_fields +
                ", face_set_id='" + face_set_id + '\'' +
                ", face_set_name='" + face_set_name + '\'' +
                ", create_date='" + create_date + '\'' +
                ", face_set_capacity=" + face_set_capacity +
                '}';
    }
}
