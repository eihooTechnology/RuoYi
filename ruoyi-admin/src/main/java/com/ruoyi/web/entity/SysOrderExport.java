package com.ruoyi.web.entity;

import com.ruoyi.system.domain.SysOrder;

import java.io.Serializable;

/**
 * @ClassName SysOrderExport
 * @Description TODO
 * @Author lzjian
 * @Date 2019-04-25 14:37
 **/
public class SysOrderExport implements Serializable, Comparable<SysOrderExport> {

    /**
     * 老人编号
     */
    private Long userCode;

    /**
     * 老人姓名
     */
    private String userName;

    /**
     * 老人电话号码
     */
    private String mobile;

    /**
     * 社区名称
     */
    private String communityName;

    /**
     * 送餐地址
     */
    private String address;

    /**
     * 老人用餐总数
     */
    private Integer lunchSum;

    @Override
    public String toString() {
        return "SysOrderExport{" +
                "userCode=" + userCode +
                ", userName='" + userName + '\'' +
                ", mobile='" + mobile + '\'' +
                ", communityName='" + communityName + '\'' +
                ", address='" + address + '\'' +
                ", lunchSum=" + lunchSum +
                '}';
    }

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getUserCode() {
        return userCode;
    }

    public void setUserCode(Long userCode) {
        this.userCode = userCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getLunchSum() {
        return lunchSum;
    }

    public void setLunchSum(Integer lunchSum) {
        this.lunchSum = lunchSum;
    }

    @Override
    public int compareTo(SysOrderExport o) {
        //自定义比较方法(根据ID大小进行比较)
        if (this.userCode <= o.getUserCode()) {
            return 1;
        }
        return -1;
    }
}
