package com.ruoyi.web.entity;

import com.ruoyi.common.base.BaseEntity;
import com.ruoyi.system.domain.SysOrder;

import java.util.List;

/**
 * @ClassName SysCourier
 * @Description TODO
 * @Author JiaoSimao
 * @Date 2018/12/18 0018 14:00
 **/
public class PadCourierBean {
    private static final long serialVersionUID = 1L;

    /**骑手id*/
    private Long id;

    /**骑手姓名*/
    private String courierName;

    /**骑手手机号码*/
    private String tel;

    /**骑手下的订单列表*/
    private List<SysOrder> orderList;

    @Override
    public String toString() {
        return "PadCourierBean{" +
                "id=" + id +
                ", courierName='" + courierName + '\'' +
                ", tel='" + tel + '\'' +
                ", orderList=" + orderList +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCourierName() {
        return courierName;
    }

    public void setCourierName(String courierName) {
        this.courierName = courierName;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public List<SysOrder> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<SysOrder> orderList) {
        this.orderList = orderList;
    }
}
