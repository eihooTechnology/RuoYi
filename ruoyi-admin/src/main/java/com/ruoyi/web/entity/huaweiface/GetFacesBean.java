package com.ruoyi.web.entity.huaweiface;

import java.util.List;

/**
 * @ClassName GetFacesBean
 * @Description TODO
 * @Author lzjian
 * @Date 2019-04-22 15:25
 **/
public class GetFacesBean {

    private List<FacesBean> faces;

    public List<FacesBean> getFaces() {
        return faces;
    }

    public void setFaces(List<FacesBean> faces) {
        this.faces = faces;
    }

}
