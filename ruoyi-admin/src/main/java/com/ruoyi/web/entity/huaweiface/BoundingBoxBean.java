package com.ruoyi.web.entity.huaweiface;

/**
 * @ClassName BoundingBoxBean
 * @Description TODO
 * @Author lzjian
 * @Date 2019-04-22 15:26
 **/
public class BoundingBoxBean {
    /**
     * width : 622
     * top_left_y : 594
     * top_left_x : 199
     * height : 622
     */

    private int width;
    private int top_left_y;
    private int top_left_x;
    private int height;

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getTop_left_y() {
        return top_left_y;
    }

    public void setTop_left_y(int top_left_y) {
        this.top_left_y = top_left_y;
    }

    public int getTop_left_x() {
        return top_left_x;
    }

    public void setTop_left_x(int top_left_x) {
        this.top_left_x = top_left_x;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "BoundingBoxBean{" +
                "width=" + width +
                ", top_left_y=" + top_left_y +
                ", top_left_x=" + top_left_x +
                ", height=" + height +
                '}';
    }
}
